<?php
	$servername = $_SERVER['HTTP_HOST'];
	//$servername = env('HTTP_HOST');
	$dirname = $_SERVER['PHP_SELF'];

	//$pathの指示URLに最後の / は含めずです。
	//footerNavは　ローカル変数があるので、switchのみでは対応不可です。変更したいときはローカル変数と条件分岐を変更のこと
	//検索用　footerNav()

	switch($servername)
	{
		case "localhost:8880":
		$path = "http://localhost:8880/m-marusan/";
		break;
		case "localhost:8888":
		$path = "http://localhost:8888/m-marusan/";
		break;
		case "localhost:8080":
		$path = "http://localhost:8080/m-marusan/";
		break;
		case "192.168.1.4:8000":
		$path = "http://192.168.1.4:8000/m-marusan/";
		break;
		case "preview-me.net":
		$path = "http://preview-me.net/m-marusan/";
		break;
	}


	function globalNav()
	{
		global $path;
		global $dirname;





$html = <<< EOF
		<ul id="globalNav">
			<li><a href="{$path}"><img src="../img/common/gNav-home.png" alt="" width="60" height="44" /></a></li>
			<li><a href="{$path}welcome/"><img src="../img/common/gNav-welcome.png" alt="" width="118" height="44" /></a></li>
			<li><a href="{$path}lessons/"><img src="../img/common/gNav-lesson.png" alt="" width="136" height="44" /></a></li>
			<li><a href="{$path}pantry/"><img src="../img/common/gNav-pantry.png" alt="" width="148" height="44" /></a></li>
			<li><a href="{$path}kitchenshop/"><img src="../img/common/gNav-kitchen.png" alt="" width="152" height="44" /></a></li>
			<li><a href="{$path}recipes/"><img src="../img/common/gNav-recipe.png" alt="" width="88" height="44" /></a></li>
			<li><a href="{$path}staff/"><img src="../img/common/gNav-staff.png" width="129" height="44" /></a></li>
			<li><a href="#"><img src="../img/common/gNav-blog.png" width="129" height="44" /></a></li>
		</ul>
EOF;
		echo $html, PHP_EOL;
	}


	function headerNav()
	{
		global $path;
		global $dirname;

$html = <<< EOF
			<ul id="headerNav">
				<li><a href="{$path}sitemap/"><img src="../img/common/hNav-sitemap.png" alt="" width="92" height="13" /></a></li>
				<li><a href="{$path}privacy/"><img src="../img/common/hNav-privacy.png" width="137" height="13" /></a></li>
			</ul>
EOF;
		echo $html, PHP_EOL;
	}