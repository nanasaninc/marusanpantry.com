<?php

require_once(dirname(__FILE__) . '/idiorm.php');

function site_urls(){
	if(strstr($_SERVER["HTTP_HOST"], 'preview-me')) {
		$homeUrl = 'http://marusanpantry.preview-me.net/';
	}elseif(strstr($_SERVER["HTTP_HOST"], 'marusan2.local')) {
		$homeUrl = 'http://marusan2.local/';
	}else{
		$homeUrl = 'http://www.marusanpantry.com/';
	}

	echo $homeUrl;
}
function site_urlss(){
	if(strstr($_SERVER["HTTP_HOST"], 'preview-me')) {
		$homeUrl = 'http://marusanpantry.preview-me.net/';
	}elseif(strstr($_SERVER["HTTP_HOST"], 'marusan2.local')) {
		$homeUrl = 'http://marusan2.local/';
	}else{
		$homeUrl = 'http://www.marusanpantry.com/';
	}
	return $homeUrl;
}
function _fh($str){
	$res = nl2br(htmlspecialchars($str, ENT_QUOTES));
	return $res;
}

class Kitchenshop{

	public $databases = array();

	public function __construct(){
		if(strstr($_SERVER["HTTP_HOST"], 'preview-me')) {
			$this->databases['host']     = 'mysql:host=mysql106.heteml.jp;dbname=_marusan';
			$this->databases['db_space'] = 'marusan';
			$this->databases['dbuser']   = '_marusan';
			$this->databases['dbpass']   = 'k6vx4cig';
		}elseif(strstr($_SERVER["HTTP_HOST"], 'marusan2.local')) {
			$this->databases['host']     = 'mysql:host=localhost;dbname=marusan';
			$this->databases['db_space'] = 'marusan';
			$this->databases['dbuser']   = 'root';
			$this->databases['dbpass']   = 'root';
		}else{
			$this->databases['host']     = 'mysql:host=localhost;dbname=_marusan';
			$this->databases['db_space'] = 'marusan';
			$this->databases['dbuser']   = 'root';
			$this->databases['dbpass']   = 'XG11RsKC';
		}
	}

	public function getItem(){

		ORM::configure($this->databases['host'], null, $this->databases['db_space']);
		ORM::configure('username', $this->databases['dbuser'], $this->databases['db_space']);
		ORM::configure('password', $this->databases['dbpass'], $this->databases['db_space']);

		$res = ORM::for_table('recommends', 'marusan')->where('recomflg', 1)->find_one();

		$data['item']['id']          = $res->id;
		$data['item']['title']       = $res->title;
		$data['item']['description'] = $res->description;
		$data['item']['point']       = $res->point;

		if(isset($res->id)){
			$rec_image_data = ORM::for_table('recommend_images', 'marusan')->where('recommend_id', $res->id)->find_many();
			$i = 0;
			foreach($rec_image_data as $k => $v){
				$data['photo'][$i] = $v->uploadpath;
				$i++;
			}
		}
		return $data;
	}

}

class Reconect{

	public $databases = array();

	public function __construct(){
		if(strstr($_SERVER["HTTP_HOST"], 'preview-me')) {
			$this->databases['host']     = 'mysql:host=mysql108.heteml.jp;dbname=_marusankitchen';
			$this->databases['db_space'] = 'kitchenshop';
			$this->databases['dbuser']   = '_marusankitchen';
			$this->databases['dbpass']   = 'marusankitchen';
		}elseif(strstr($_SERVER["HTTP_HOST"], 'marusan2.local')) {
			$this->databases['host']     = 'mysql:host=localhost;dbname=kitchenshop';
			$this->databases['db_space'] = 'kitchenshop';
			$this->databases['dbuser']   = 'root';
			$this->databases['dbpass']   = 'root';
		}else{
			$this->databases['host']     = 'mysql:host=localhost;dbname=kitchenshop';
			$this->databases['db_space'] = 'kitchenshop';
			$this->databases['dbuser']   = 'root';
			$this->databases['dbpass']   = 'XG11RsKC';
		}
	}


	public function getItem(){

		ORM::configure($this->databases['host'], null, $this->databases['db_space']);
		ORM::configure('username', $this->databases['dbuser'], $this->databases['db_space']);
		ORM::configure('password', $this->databases['dbpass'], $this->databases['db_space']);

		return ORM::for_table('wp_posts', 'kitchenshop')->where('id', 1)->find_one();
	}
}