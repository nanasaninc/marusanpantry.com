<?php
require_once('inc.php');
$recs = new Kitchenshop();
?>
<?php get_header(); ?>
<p class="topicPath"><a href="<?php site_urls(); ?>"><img src="<?php site_urls(); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><a href="<?php site_urls(); ?>kitchenshop/">キッチンショップ</a><span>&gt;</span><?php the_title(); ?></p>
<h1><img src="<?php site_urls(); ?>img/kitchenshop/h1.png" alt="キッチンショップ" width="183" height="19" /></h1>
<div id="primary" class="bread">
	<div class="mainImg"><?php
		$baseurl = site_urlss();
		$url = $baseurl.'inc/kitchenshop.html';
		$bnr = file_get_contents($url);
		echo $bnr;
		?>
	</div>
	<div class="box" id="infoBox">
		<h2><img src="<?php site_urls(); ?>img/kitchenshop/h2-infoBox.png" alt="キッチンショップからのお知らせ" width="680" height="60" /></h2>
		<?php
		if (have_posts()) :
			while (have_posts()) : the_post();
		?>
		<div class="article">
			<?php the_title('<h3>', '</h3>'); ?>
			<?php the_content(); ?>
			<p class="meta"><?php the_category(' '); ?>｜<?php the_time('Y-m-d'); ?></p>
		</div>
		<?php
			endwhile;
			endif;
		?>
		<?php
		$homes = site_urlss();
		$previmg = '<img src="'.$homes.'img/common/btn-prev.png" alt="前の記事へ" width="90" height="29" />';
		$nextimg = '<img src="'.$homes.'img/common/btn-fwd.png" alt="次の記事へ" width="90" height="29" />';
		?>
		<div class="localNav clearfix">
			<p class="btn-back"><?php next_posts_link($previmg, '0') ?></p>
			<p class="btn-next"><?php previous_posts_link($nextimg, '0'); ?></p>
		</div>
	</div>
	<?php $oss = $recs->getItem(); ?>
	<div class="box" id="ossmBox">
		<h2><img src="<?php site_urls(); ?>img/kitchenshop/h2-ossmBox.png" alt="キッチンショップ今月のおすすめ商品" width="680" height="60" /></h2>
		<div class="photoBox">
			<?php if(!empty($oss['photo'])): ?>
				<?php foreach($oss['photo'] as $k => $v): ?>
					<p><img src="<?php site_urls(); ?><?php echo $v; ?>" width="201" height="151" /></p>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<div class="contentBox">
			<h4><img src="<?php site_urls(); ?>img/common/icon-name.png" alt="商品名" width="62" height="19" /><?php echo _fh($oss['item']['title']); ?></h4>
			<?php if(!empty($oss['item']['description'])): ?>
				<p><?php echo _fh($oss['item']['description']); ?></p>
			<?php endif; ?>
			<h4><img src="<?php site_urls(); ?>img/common/icon-point.png" alt="ポイント" width="61" height="19" /></h4>
			<?php if(!empty($oss['item']['point'])): ?>
				<p><?php echo _fh($oss['item']['point']); ?></p>
			<?php endif; ?>
		</div>
		<p>&nbsp;</p>
	</div>
</div>
<?php
$recon = new Reconect();
$recon->getItem();
?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
