(function() {
	tinymce.PluginManager.requireLangPack('emoji');
	tinymce.create('tinymce.plugins.EmojiPlugin', {
		init : function(ed, url) {
			// Register commands
			ed.addCommand('mceEmoji', function() {
				ed.windowManager.open({
					file : url + '/emoji.htm',
					width : 600 + ed.getLang('emoji.delta_width', 0),
					height :600 + ed.getLang('emoji.delta_height', 0),
					inline : 1
				}, {
					plugin_url : url
				});
			});

			// Register buttons
			ed.addButton('emoji', {
					title : 'TypePadの絵文字パレット',
					cmd : 'mceEmoji',
					image : url + '/img/emoji_icon.gif'
			});
		},

		getInfo : function() {
			return {
				longname : 'Emoji plugin',
				author : 'Masaru hashizume',
				authorurl : 'http://www.veryposi.com/',
				infourl : 'http://www.veryposi.info/',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});

	// Register plugin
	tinymce.PluginManager.add('emoji', tinymce.plugins.EmojiPlugin);
})();