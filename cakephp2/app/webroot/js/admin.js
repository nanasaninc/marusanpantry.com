//ボタンオーバー
$(function() {
	$('#globalNav li a img,#headerNav li a img').addClass('btn');
})
//ボックスマージン
$(function() {
	//$('#recipe #recipeBox .box:nth-child(5n)').css('margin-right','0px')
	//$('#recipe #relatedBox .box:nth-child(4n+1)').css('margin-right','0px')
})
//zebraList
$(function() {
	$('.zebraList tr:even').addClass('even');
})

$(function(){
	$(".panel-edit ul").hide();
	var zIndexNumber = 10000;
	$('.panel-edit').each(function() {
		$(this).css('zIndex', zIndexNumber);
		zIndexNumber -= 1;
	});
	$(".panel-edit .link-edit").hover(function(){
		$("ul:not(:animated)",this).slideDown("fast").css('z-index', 11000);
		},
		function(){
			$("ul",this).fadeOut("fast");
	});
})

//recipe　ソート
$(function(){
	$("#recipeBox").sortable({
		cusor:'move',
		opacity:0.7,
		//connectWith:['.box'],
		//placeholder: 'ui-state-highlight',
		update: function(event, ui) {
			var i = 1;
			$("#recipeBox .box span input").each(function(){
				$(this).val(i);
				i++;
			});
		}
	});
	$("#relatedBox table tbody").sortable({
		cusor:'move',
		opacity:0.7,
		connectWith:['.box'],
		placeholder: 'ui-state-highlight',
		update: function(event, ui) {
			var i = 1;
			$("#relatedBox table tr span input").each(function(){
				$(this).val(i);
				i++;
			});
		}
	});
	$("#materialBox tbody").sortable({
		cusor:'move',
		opacity:0.7,
		update: function(event, ui) {
			var i = 1;
			$("#materialBox tr .sortorder").each(function(){
				$(this).val(i);
				i++;
			});
		}
	});
	$("#recipeBox .box, #relatedBox table tr, #materialBox tr").mouseenter(function(){
		$(this).css("background-color", "#FFFAF0")
		.mouseleave(function(){
			$(this).css("background-color", "#ffffff");
		});
	});
});





function setupDes() {
	var textarea = document.getElementsByTagName("textarea");
	for (i = 0; i < textarea.length; i++) {
		if (textarea[i].className.search("nodes") < 0) {
			if (textarea[i].value == textarea[i].defaultValue) {textarea[i].className += " ondes"; }
			textarea[i].onfocus = function() {offDes(this); }
			textarea[i].onblur = function() {onDes(this); }
		}
	}
	var input = document.getElementsByTagName("input");
	for (i = 0; i < input.length; i++) {
		if ((input[i].className.search("nodes") < 0) && ((input[i].getAttribute("type") == "text")||(input[i].getAttribute("type") == null))) {
			if (input[i].value == input[i].defaultValue) {input[i].className += " ondes"; }
			input[i].onfocus = function() {offDes(this); }
			input[i].onblur = function() {onDes(this); }
		}
	}
	return;
}

function offDes(from) {
	if (from.className.search("ondes") < 0) {return 0;}
	from.className = from.className.replace(/ondes/, "");
	from.value = "";
	return 1;
}
function onDes(from) {
	if (from.value != "") {return 0;}
	from.className += " ondes";
	from.value = from.defaultValue;
	return 1;
}
