﻿		<div id="secondary">
			<div class="box" id="shopBox">
				<h2><img src="<?php site_urls(); ?>img/pantry/scd-h2-pantry.png" alt="マルサンパントリーについて" width="245" height="45" /></h2>
				<div class="innerBox">
					<p><img src="<?php site_urls(); ?>img/pantry/p-pantry.jpg" width="210" height="126" /></p>
					<p>ご家庭でのお菓子やパン作りの食材が揃ったお店です。<br />
						プロのケーキ屋さんやパン屋さんが使用する業務用食材を使いやすいサイズで販売しています。<br />
					初心者の方もプロの方も、ぜひ一度お越しくださいませ。</p>
				</div>
			</div>
			<div class="box" id="shopInfoBox">
				<h2><img src="<?php site_urls(); ?>img/common/scd-h2-info.png" alt="営業情報" width="245" height="45" /></h2>
				<div class="innerBox">
					<dl>
						<dt>住所</dt>
						<dd>〒790-0005<br />
							愛媛県松山市花園町6-1[<a href="<?php site_urls(); ?>img/common/map.gif" class="fancymap">MAP</a>]</dd>
						<dt>TEL</dt>
						<dd>089-931-1147</dd>
						<dt>営業時間</dt>
						<dd>8:45-18:00</dd>
						<dt>定休日</dt>
						<dd>日曜日・祝日・年末年始</dd>
					</dl>
					<p><img src="<?php site_urls(); ?>inc/img/pantry/tcalendar.gif" alt="" width="210" height="151" alt="今月の営業日カレンダー" /></p>
					<p><img src="<?php site_urls(); ?>inc/img/pantry/ncalendar.gif" alt="" width="210" height="151" alt="来月の営業日カレンダー" /></p>
				</div>
			</div>
			<div class="box" id="monthlyBox">
				<h2><img src="<?php site_urls(); ?>img/common/scd-h2-monthly.png" alt="過去の記事" width="245" height="45" /></h2>
				<ul class="def">
					<?php wp_get_archives(); ?>
				</ul>
			</div>
			<div class="box" id="categoryBox">
				<h2><img src="<?php site_urls(); ?>img/common/scd-h2-category.png" alt="カテゴリ" width="245" height="45" /></h2>
				<ul class="def">
					<?php wp_list_cats('sort_column=name&optioncount=0&hierarchical=0'); ?>
				</ul>
			</div>
			<div class="bnrBox">
			<?php
				$baseurl = site_urlss();
				$url = $baseurl.'inc/sidebnr.html';
				$bnr = file_get_contents($url);
				echo $bnr;
			?>
			</div>
		</div>
	</div>
</div>