﻿<?php

	require_once('inc.php');

	$recs = new Recommend();

?>

<?php get_header(); ?>

	<p class="topicPath"><a href="<?php site_urls(); ?>"><img src="<?php site_urls(); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><a href="<?php site_urls(); ?>pantry/">マルサンパントリー</a><span>&gt;</span>社外事業部</p>

		<h1><img src="<?php site_urls(); ?>img/pantry/h1-outside.png" alt="社外事業部" width="100" height="19" /></h1>

		<div id="primary" class="bread">

			<div class="mainImg"><img src="<?php site_urls(); ?>img/pantry/main-outside.jpg" alt="社外事業部" width="680" height="553" alt="パン＆お菓子の美味しさ、手作りの楽しさを伝える活動を行っています！愛媛県松山市を中心に、学校などでお子様や親子を対象とした「出張お菓子教室」をボランティアにて行っています。また、社外事業部所属の社員がパンやお菓子を手作りし、フリーマーケットに出店する活動も行っています。「一人でも多くの方に手作りパン＆お菓子の美味しさ、楽しさに触れて貰えたら・・・」という想いで活動しています。（※フリーマーケットの売り上げはボランティア活動費に充当します）" /></div>

			<div class="box" id="volunteer">

				<h2><img src="<?php site_urls(); ?>img/pantry/h2-volunteer.png" alt="ボランティア教室" width="680" height="60" alt="ボランティア教室" /></h2>

				<div class="inner clearfix">

					<div class="photo"><img src="<?php site_urls(); ?>img/pantry/photo-volunteer.jpg" width="210" height="144" alt="写真：ボランティア教室" /></div>

					<div class="text">

						<p>愛媛県内の学校、地域振興活動等において、お子様や親子を対象とした出張パン・お菓子教室をボランティアにて行います。</p>

						<p style="text-align:right;">担当：大石、茅原、神野</p>

						<div class="button">

							<a href="http://shop.plaza.rakuten.co.jp/marusanpantry/diary/detail/201206200000/" target="_blank"><img src="<?php site_urls(); ?>img/pantry/btn-kyoshitu.png" width="143" height="30" alt="リンク：教室の様子はこちら" /></a>

						</div>

					</div>

				</div>

				<div id="irai">

					<p style="font-size:110%;font-weight:bold;">ボランティア（無償）での出張お菓子教室のお問い合わせ・ご依頼は、</p>

					<ol>

						<li>（１）依頼者様のおなまえ</li>

						<li>（２）メールアドレス</li>

						<li>（３）お電話番号※日中でもつながる番号</li>

						<li>（４）依頼者様の所属機関名※学校名など</li>

						<li>（５）ボランティア出張教室の開催場所の名称とその住所</li>

						<li>（６）ボランティア出張教室で作りたいメニュー※パン、お菓子など</li>

						<li>（７）ボランティア出張教室の対象者の年齢と人数</li>

						<li>（８）ボランティア出張教室の開催希望日時</li>

						<li>（９）その他ご要望など</li>

					</ol>

					<p>を明記の上、<a href="mailto:school@m-marusan.co.jp" style="font-weight:bold;">school@m-marusan.co.jp</a>までメールにてご連絡ください。</p>

					<ul>

						<li>※原則として、出張地域は松山市内または近郊とさせていただいております。</li>

						<li>※教育機関での開催、地域振興など営利目的でない場合を対象とします。</li>

						<li>※ご依頼いただいても、状況によりご要望にお応えできない場合がございます。</li>

					</ul>

				</div>

			</div>

			<div class="box" id="freemarket">

				<h2><img src="<?php site_urls(); ?>img/pantry/h2-freemarket.png" alt="フリーマーケットへの出店" width="680" height="59" /></h2>

				<div class="inner clearfix">

					<div class="photo"><img src="<?php site_urls(); ?>img/pantry/photo-freemarket.jpg" width="210" height="144" alt="写真：フリーマーケット" /></div>

					<div class="text">

						<p>社外事業部所属の社員がパンやお菓子を手作りし、フリーマーケットなどに出店します。<br />

						売上は、当事業部のボランティア活動費に充当します。</p>

						<p style="text-align:right;">担当：大石、茅原</p>

						<div class="button">

							<a href="http://shop.plaza.rakuten.co.jp/marusanpantry/diary/category/?cate_id=1000016" target="_blank"><img src="<?php site_urls(); ?>img/pantry/btn-syutten.png" width="143" height="30" alt="リンク：出店の様子はこちら" /></a>

						</div>

					</div>

				</div>

			</div>

		</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>