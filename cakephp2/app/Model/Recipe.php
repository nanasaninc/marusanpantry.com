<?php

App::uses('AppModel', 'Model');

class Recipe extends Model {

	public $useTable  = 'recipes';
	public $recursive = -1;
	public $actsAs = array(
		'Containable'
	);


	public $hasMany = array(
		'RecipeImage' => array(
			'className'  => 'RecipeImage',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'recipe_id',
		),
		'RecipeMaterial' => array(
			'className'  => 'RecipeMaterial',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'recipe_id',
		),
		'RecipeOrder' => array(
			'className'  => 'RecipeOrder',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'recipe_id',
		),
		'RecipeRelated' => array(
			'className'  => 'RecipeRelated',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'recipe_id',
		),
	);

	function notSpace($field = array()) {
		foreach($field as $name => $value){
			if (preg_match("/^( |　)+$/", $value)) {
				return false;
			} else {
				return true;
			}
		}
	}

	public function __construct() {
		parent::__construct();
	}

	public $validate;

	public $validate_default = array(
		'title' => array(
			array('rule'    => array('notBlank'), 'message' => 'タイトルを記入してください。'),
			array('rule'    => 'notSpace', 'message' => 'タイトルを記入してください。')
		)
	);

	public $validation_ajax = array(
		'ajax' => array()
	);

	public function set_validate_default () {
		$this->validate = $this->validate_default;
	}

	public function set_validation_ajax () {
		$this->validate = $this->validation_ajax;
	}







	public function get_toppage_paginator_setting(){
		return array(
			'conditions' => array('active' => 0, 'del_flg' => 0),
			'fields'     => array('*'),
			'paramType'  => 'querystring',
			'limit'      => 10,
			'order'      => array('id' => 'DESC'),
			'recursive'  => 2
		);
	}

	public function get_category_paginator_setting($param = '', $pagenum = 10){

		$this->contain('RecipeImage');

		$fields = array('id', 'title', 'category', 'active', 'description', 'created');

		if(empty($param)){

			$condition = array('active' => 0, 'del_flg' => 0);
			$order     = 'Recipe.modified DESC';

			return array(
				'conditions' => $condition,
				'fields'     => $fields,
				'paramType'  => 'querystring',
				'limit'      => $pagenum,
				'order'      => $order
			);
		}else{

			$condition = array('active' => 0, 'category' => (int)$param, 'del_flg' => 0);
			$order     = 'Recipe.modified DESC';

			return  array(
				'conditions' => $condition,
				'fields'     => $fields,
				'paramType'  => 'querystring',
				'limit'      => $pagenum,
				'order'      => $order
			);
		}
	}

	public function get_difficulty_paginator_setting($param = '', $pagenum = 10){

		$this->contain('RecipeImage');

		$fields = array('id', 'title', 'category', 'active', 'description', 'created');
		$order = 'Recipe.modified DESC';

		if(empty($param)){

			$condition = array('active' => 0, 'del_flg' => 0);

			return array(
				'conditions' => $condition,
				'fields'     => $fields,
				'paramType'  => 'querystring',
				'limit'      => $pagenum,
				'order'      => $order,
			);
		}else{

			$condition = array('active' => 0, 'difficulty' => $param, 'del_flg' => 0);

			return  array(
				'conditions' => $condition,
				'fields'     => $fields,
				'paramType'  => 'querystring',
				'limit'      => $pagenum,
				'order'      => $order,
			);
		}
	}

	public function get_recipe($param = ''){

		$this->contain(array(
			'RecipeImage',
			'RecipeMaterial' => array(
				'order' => 'RecipeMaterial.sort ASC'
			),
			'RecipeOrder' => array(
				'order' => 'RecipeOrder.sort ASC'
			),
			'RecipeRelated' => array(
				'order' => 'RecipeRelated.sort ASC'
			),
		));

		return $this->find('first', array(
				'conditions' => array(
					'Recipe.id'      => $param,
					'Recipe.active'  => 0,
					'Recipe.del_flg' => 0,
				)
			)
		);
	}
}
