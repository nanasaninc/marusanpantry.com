<?php

App::uses('AppModel', 'Model');

class Difficulty extends AppModel {

	public $useTable = false;

	public $difficulty = array(
		'1' => '★',
		'2' => '★★',
		'3' => '★★★',
		'4' => '★★★★',
		'5' => '★★★★★',
	);

}
