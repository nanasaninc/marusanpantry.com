<?php

App::uses('AppModel', 'Model');

class Active extends AppModel {

	public $useTable = false;

	public $active = array(
		'0' => '表示',
		'1' => '非表示',
	);
}
