<?php

App::uses('AppModel', 'Model');

class PantryImage extends AppModel {
	public $useTable  = 'pantry_images';

	public function formatName($name, $file) {
		return '';
	}

	public function check_exists($pantry_id, $sort){
		return $this->find('first', array(
			'conditions' => array(
				'pantry_id' => $pantry_id,
				'sort'      => $sort
			)
		));
	}
}
