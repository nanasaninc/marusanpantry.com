<?php

App::uses('AppModel', 'Model');

class Recommend extends AppModel {

	public $useTable  = 'recommends';
	public $recursive = -1;
	public $actsAs = array('Containable');

	public $hasMany = array(
		'RecommendImage' => array(
			'className'  => 'RecommendImage',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'recommend_id',
		),
	);

	function notSpace($field=array()) {
		foreach($field as $name => $value){
			if (preg_match("/^( |　)+$/", $value)) {
				return false;
			} else {
				return true;
			}
		}
	}

	public $validate;

	public $validate_default = array(
		'title' => array(
			array('rule'    => array('notBlank'), 'message' => 'タイトルを記入してください。'),
			array('rule'    => 'notSpace', 'message' => 'タイトルを記入してください。')
		)
	);

	public $validation_ajax = array(
		'ajax' => array()
	);

	public function set_validate_default () {
		$this->validate = $this->validate_default;
	}

	public function set_validation_ajax () {
		$this->validate = $this->validation_ajax;
	}
}
