<?php

App::uses('AppModel', 'Model');

class Reserve extends Model {
	public $useTable  = false;

	public $_schema = array(
		'name' => array(
			'type'    => 'varchar',
			'default' => null,
		),
		'phone' => array(
			'type'    => 'varchar',
			'default' => null,
		),
		'email' => array(
			'type'    => 'varchar',
			'default' => null,
		),
		'mailconfirm' => array(
			'type'    => 'varchar',
			'default' => null,
		),
	);

	function notSpace($field = array()) {
		foreach($field as $name => $value){
			if (preg_match("/^( |　)+$/", $value)) {
				return false;
			} else {
				return true;
			}
		}
	}

	function checkCompare(){
		if($this->data['Reserve']['email'] != $this->data['Reserve']['mailconfirm']){
			return false;
		}else{
			return true;
		}
	}


	public function __construct() {
		parent::__construct();
	}

	public $validate = array(
		'name' => array(
			'rule-1' => array(
				'required'   => true,
				'allowEmpty' => false,
				'rule' => array('notBlank'),
				'message' => '氏名を記入してください。'
			),
			'rule-2' => array(
				'rule' => array('notSpace'),
				'message' => '氏名を記入してください。'
			),
			'rule-3' => array(
				'rule' => array('maxLength', 100),
				'message' => '氏名は100文字以内記入してください。'
			),
		),
		'phone' => array(
			'rule-1' => array(
				'required'   => true,
				'allowEmpty' => false,
				'rule' => array('notBlank'),
				'message' => '電話番号を記入してください。'
			),
			'rule-2' => array(
				'rule' => array('notSpace'),
				'message' => '電話番号を記入してください。'
			),
		),
		'email'  => array(
			'rule-1' => array(
				'required'   => true,
				'allowEmpty' => false,
				'rule' => array('email'),
				'message' => 'メールアドレスを記入してください。'
			),
		),
		'active'  => array(
			'rule-1' => array(
				'required'   => true,
				'allowEmpty' => false,
				'rule' => array('notBlank'),
				'message' => '参加人数が満席になってしまいました。またのご利用をお待ちしています。'
			),
		),
		'mailconfirm' => array(
			'rule-1' => array(
				'required'   => true,
				'allowEmpty' => false,
				'rule' => array('email'),
				'message' => '確認用メールアドレスを入力してください'
			),
			'rule-2' => array(
				'rule' => array('checkCompare'),
				'message' => '上記メールアドレスと異なります。ご確認をお願いいたします。'
			)
		)
	);
}
