<?php

App::uses('AppModel', 'Model');

class Wordpress extends Model {

	public $useTable  = false;

	public $url = array(
		'pantry'      => '/pantry/wp-login.php',
		'kitchenshop' => '/kitchenshop/wp-login.php',
	);

}
