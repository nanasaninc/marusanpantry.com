<?php

App::uses('AppModel', 'Model');

class Lesson extends Model {
	public $useTable = 'lessons';
	public $recursive = -1;
	public $actsAs = array(
		'Containable'
	);

	public $hasMany = array(
		'LessonImage' => array(
			'className'  => 'LessonImage',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'lesson_id',
		),
		'LessonDate' => array(
			'className'  => 'LessonDate',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'lesson_id',
		),
	);

	function notSpace($field = array()) {
		foreach($field as $name => $value){
			if (preg_match("/^( |　)+$/", $value)) {
				return false;
			} else {
				return true;
			}
		}
	}

	public function __construct() {
		parent::__construct();
	}

	public $validate;

	public $validate_default = array(
		'title' => array(
			array('rule'    => array('notBlank'), 'message' => 'タイトルを記入してください。'),
			array('rule'    => 'notSpace', 'message' => 'タイトルを記入してください。')
		),
		'category' => array(
			array('rule'    => array('notBlank'), 'message' => 'カテゴリを選択してください。'),
			array('rule'    => 'notSpace', 'message' => 'カテゴリを選択してください。')
		),
		'limitday' => array(
			array('rule'    => array('notBlank'), 'message' => '掲載終了日を選択してください。'),
			array('rule'    => 'notSpace', 'message' => '掲載終了日を選択してください。')
		)
	);

	public $validation_ajax = array();

	public function set_validate_default () {
		$this->validate = $this->validate_default;
	}

	public function set_validation_ajax () {
		$this->validate = $this->validation_ajax;
	}








	/**
	 * [トップページおすすめ教室]
	 * @return [type] [description]
	 */
	public function get_osusumelesson(){

		$this->contain('LessonImage');

		return $this->find('first', array(
				"conditions" => array(
					'recomflg' => 1,
					"active"   => 0,
					'del_flg'  => 0
				),
			)
		);
	}


	/**
	 * [教室トップ]
	 * @return [type] [description]
	 */
	public function get_lesson($param = ''){

		$today = date('Y-m-d');

		$this->contain(array(
			'LessonDate' => array('order' => 'LessonDate.date ASC'),
			'LessonImage',
		));

		return $this->find('first', array(
				'conditions' => array(
					'Lesson.id' => (int)$param,
					'active'    => 0,
					'del_flg'   => 0,
					"Lesson.limitday > '{$today}'"
				)
			)
		);
	}


}
