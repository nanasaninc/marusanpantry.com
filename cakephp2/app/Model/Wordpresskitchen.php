<?php

App::uses('AppModel', 'Model');

class Wordpresskitchen extends Model {

	public $useDbConfig = 'kitchen';
	public $useTable    = 'wp_posts';
	public $primaryKey  = 'ID';

	public function getpost(){

		$conditions = array('post_status' => 'publish');
		$fields     = array('guid', 'post_title', 'post_date');
		$order      = array('post_date' => 'DESC');

		return $this->find('all', array(
			'conditions' => $conditions,
			'fields'     => $fields,
			'order'      => $order
			)
		);
	}
}
