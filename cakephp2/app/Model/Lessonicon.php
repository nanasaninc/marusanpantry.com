<?php

App::uses('AppModel', 'Model');

class Lessonicon extends AppModel {

	public $useTable = false;

	public $icons = array(
		'1' => '特別企画',
		'2' => 'パン教室',
		'3' => 'お菓子教室',
	);
}
