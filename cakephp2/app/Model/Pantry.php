<?php

App::uses('AppModel', 'Model');

class Pantry extends Model {
	public $useTable  = 'pantries';
	public $recursive = -1;
	public $actsAs    = array(
		'Containable'
	);

	public $hasMany = array(
		'PantryImage' => array(
			'className'  => 'PantryImage',
			'dependent'  => true,
			'order'      => '',
			'foreignKey' => 'pantry_id',
		),
	);

	function notSpace($field = array()) {
		foreach($field as $name => $value){
			if (preg_match("/^( |　)+$/", $value)) {
				return false;
			} else {
				return true;
			}
		}
	}

	public function __construct() {
		parent::__construct();
	}

	public $validate;

	public $validate_default = array(
		'title' => array(
			array('rule' => array('notBlank'), 'message' => 'タイトルを記入してください。'),
			array('rule' => 'notSpace', 'message' => 'タイトルを記入してください。')
		)
	);

	public $validation_ajax = array(
		'ajax' => array()
	);

	public function set_validate_default () {
		$this->validate = $this->validate_default;
	}

	public function set_validation_ajax () {
		$this->validate = $this->validation_ajax;
	}
}
