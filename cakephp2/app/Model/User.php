<?php

App::uses('AppModel', 'Model');

class User extends Model {

	public $useTable  = 'users';

	public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['raw_password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['raw_password']);
		}
		return true;
	}
}
