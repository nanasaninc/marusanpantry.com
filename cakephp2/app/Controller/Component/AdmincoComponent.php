<?php

App::uses('Component', 'Controller');

class AdmincoComponent extends Component{

	public function initialize(Controller $controller) {
		$this->Controller = $controller;
	}


	/**
	 * [アクションレッスンのコンストラクタ]
	 * @return [type] [description]
	 */
	public function lessons(){

		// $this->Controller->Auth->authError = 'ログインしてください。';

		$lesson_model = ClassRegistry::init('Lesson');


		$this->Controller->loadModel('LessonImage');
		$this->Controller->loadModel('LessonDate');
		$this->Controller->loadModel('Difficulty');
		$this->Controller->loadModel('Lessonicon');
		$this->Controller->loadModel('Lcategory');
		$this->Controller->loadModel('Active');
		$this->Controller->loadModel('Wordpress');

		$wordpress = $this->Controller->Wordpress->url;
		$this->Controller->set('wordpress', $wordpress);

		$difficulty = $this->Controller->Difficulty->difficulty;
		$this->Controller->set('difficulty', $difficulty);

		$lessonicon = $this->Controller->Lessonicon->icons;
		$this->Controller->set('lessonicon', $lessonicon);

		$lcategory = $this->Controller->Lcategory->lcategory;
		$this->Controller->set('lcategory', $lcategory);

		$active = $this->Controller->Active->active;
		$this->Controller->set('active', $active);

		$bodyId = 'lesson';
		$this->Controller->set('bodyId', $bodyId);

		//カテゴリをカウント
		$lessonnum['speciallesson'] = $lesson_model->find('count', array(
				'conditions' => array('Lesson.category' => 1),
			)
		);
		$lessonnum['breadlesson'] = $lesson_model->find('count', array(
				'conditions' => array('Lesson.category' => 2),
			)
		);
		$lessonnum['sweetlesson'] = $lesson_model->find('count', array(
				'conditions' => array('Lesson.category' => 3),
			)
		);
		$lessonnum['taikenlesson'] = $lesson_model->find('count', array(
				'conditions' => array('Lesson.category' => 4),
			)
		);
		$this->Controller->set('lessonnum', $lessonnum);

		//日付
		$dateArray = array(
			date('Y'),
			date('Y', strtotime("+1 year")),
		);

		$this->Controller->set('dateArray', $dateArray);
	}


	/**
	 * [アクションレシピのコンストラクタ]
	 * @return [type] [description]
	 */
	public function recipes(){

		// $this->Controller->Auth->authError = 'ログインしてください。';

		$recipe_model = ClassRegistry::init('Recipe');
		$recipecat_model = ClassRegistry::init('Recipecat');


		$this->Controller->loadModel('RecipeImage');
		$this->Controller->loadModel('RecipeMaterial');
		$this->Controller->loadModel('RecipeOrder');
		$this->Controller->loadModel('RecipeRelated');
		$this->Controller->loadModel('Difficulty');
		$this->Controller->loadModel('Active');
		$this->Controller->loadModel('Recipecat');
		$this->Controller->loadModel('Wordpress');

		$difficulty = $this->Controller->Difficulty->difficulty;
		$this->Controller->set('difficulty', $difficulty);

		$active = $this->Controller->Active->active;
		$this->Controller->set('active', $active);

		$bodyId = 'recipe';
		$this->Controller->set('bodyId', $bodyId);

		$wordpress = $this->Controller->Wordpress->url;
		$this->Controller->set('wordpress', $wordpress);

		//カテゴリー登録数検索
		//大カテ数検索
		$breadconditions = array('category LIKE' => "1%", 'del_flg' => 0);
		$cat['breadnum'] = $recipe_model->find('count',array(
			'conditions' => $breadconditions,
		));
		if(empty($cat['breadnum'])){
			$cat['breadnum'] = 0;
		}

		$sweetconditions = array('category LIKE' => "2%", 'del_flg' => 0);
		$cat['sweetnum'] = $recipe_model->find('count',array(
			'conditions' => $sweetconditions,
		));
		if(empty($cat['sweetnum'])){
			$cat['sweetnum'] = 0;
		}
		$this->Controller->set('cat', $cat);

		//大カテゴリ
		$bcate = $recipecat_model->bcat;
		$this->Controller->set('bcate', $bcate);
		//小カテゴリメニュー作成
		$subcategory = $recipecat_model->recategory;

		$this->Controller->set('subcategory', $subcategory);

		$lists = array();

		foreach($subcategory as $k => $v){
			$cond = array('category' => "{$k}", 'del_flg' => 0);
			$catnum = $recipe_model->find('count',array(
				'conditions' => $cond,
			));
			if(preg_match('/^1.+/', $k)){
				$lists['breadlist'][] = array('id' => $k, 'name' => $v, 'count' => $catnum);
			}elseif(preg_match('/^2.+/', $k)){
				$lists['sweetlist'][] = array('id' => $k, 'name' => $v, 'count' => $catnum);
			}
		}
		$this->Controller->set('lists', $lists);
	}
}
