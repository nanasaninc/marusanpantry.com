<?php

App::uses('Component', 'Controller');

class RecipeSupportComponent extends Component{

	public function initialize(Controller $controller) {

		$this->Controller = $controller;

		$recipe_model     = ClassRegistry::init('Recipe');
		$recipe_cat_model = ClassRegistry::init('Recipecat');
		$difficulty_model = ClassRegistry::init('Difficulty');


		$difficulty = $difficulty_model->difficulty;
		$this->Controller->set('difficulty', $difficulty);

		$bodyId = 'recipe';
		$this->Controller->set('bodyId', $bodyId);

		//大カテゴリ
		$bcate = $recipe_cat_model->bcat;
		$this->Controller->set('bcate', $bcate);
		//小カテゴリメニュー作成
		$subcategory = $recipe_cat_model->recategory;

		$this->Controller->set('subcategory', $subcategory);

		$lists = array();

		foreach($subcategory as $k => $v){
			$condition = array('category' => "{$k}", 'active' => 0, 'del_flg' => 0);
			$catnum = $recipe_model->find('count',array(
				'conditions' => $condition,
			));
			if(preg_match('/^1.+/', $k)){
				$lists['breadlist'][] = array('id' => $k, 'name' => $v, 'count' => $catnum);
			}elseif(preg_match('/^2.+/', $k)){
				$lists['sweetlist'][] = array('id' => $k, 'name' => $v, 'count' => $catnum);
			}
		}

		foreach($difficulty as $k  => $v){
			$condition = array('difficulty' => "{$k}", 'active' => 0, 'del_flg' => 0);
			$diffnum = $recipe_model->find('count',array(
				'conditions' => $condition,
			));
			$lists['difflist'][] = array('id' => $k, 'name' => $v, 'count' => $diffnum);
		}

		$this->Controller->set('lists', $lists);


		$today = date('Y-m-d H:i:s', strtotime(date('Y-m-d').' +30 day'));
		$this->Controller->set('today', $today);

	}

}
