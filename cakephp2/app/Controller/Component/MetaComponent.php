<?php

App::uses('Component', 'Controller');

class MetaComponent extends Component{

	public function initialize(Controller $controller) {

		$this->Controller = $controller;

	}

	public function meta($title = ''){
		$meta['title']       = $title . 'マルサンパントリー';
		$meta['keyword']     = '丸三,マルサン,ケーキ材料, パン材料, 製菓材料, 製パン材料, お菓子材料,製菓道具, ケーキレシピ, 製パン道具, marusan, marusanpantry';
		$meta['description'] = 'お菓子材料、パン材料、器具道具、焼き型などが揃っています。';
		$this->Controller->set('meta', $meta);
	}
}
