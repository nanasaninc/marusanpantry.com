<?php

App::uses('Component', 'Controller');

class CommonComponent extends Component{

	public function initialize(Controller $controller) {
		$this->Controller = $controller;
	}

	/*　乱数作成
	-------------------------*/
	public function _rand($nLengthRequired = 8){
		$sCharList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
		//mt_srand();
		$sRes = "";
		for($i=0; $i<$nLengthRequired; $i++){
			$rand = rand(0, (mb_strlen($sCharList)-1));
			$sRes .= $sCharList[$rand];
		}
		return $sRes;
	}

	public function _categorycount(){

		$recipe_model = ClassRegistry::init('Recipe');

		//大カテ数検索
		$breadconditions = array('category LIKE' => "1%");
		$cat['breadnum'] = $$recipe_model->find('count',array(
			'conditions' => $breadconditions,
		));
		if(empty($cat['breadnum'])){
			$cat['breadnum'] = 0;
		}

		$sweetconditions = array('category LIKE' => "2%");
		$cat['sweetnum'] = $recipe_model->find('count',array(
			'conditions' => $sweetconditions,
		));
		if(empty($cat['sweetnum'])){
			$cat['sweetnum'] = 0;
		}
		return $cat;
	}
}
