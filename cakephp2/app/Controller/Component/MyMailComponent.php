<?php
class MyMailComponent extends Component {

	private $cake_mail = 'default';

	public function initialize(Controller $controller) {
		$this->Controller = $controller;
		$this->cake_mail = 'default';
	}
	public function reserve_to_client ($to, $param) {
		$email = new CakeEmail($this->cake_mail);
		$email->to(array($to => $param['name']. ' 様'));
		$email->from(array('school@m-marusan.co.jp' => 'マルサンパントリー'));
		$email->subject($param['title'] . 'の教室へのお申し込みありがとうございます！ - マルサンパントリー');
		$email->emailFormat('text');
		$email->template('front/reserve_client');
		$email->viewVars($param);
		return $email->send();
	}
	public function reserve_to_marusan ($to, $param) {
		$email = new CakeEmail($this->cake_mail);
		$email->to(array($to => 'マルサンパントリー'));
		$email->from(array($param['email'] => "教室予約【{$param['name']}様】"));
		$email->subject($param['title'] . 'の教室へのお申し込みがありました。 - マルサンパントリー');
		$email->emailFormat('text');
		$email->template('front/reserve_marusan');
		$email->viewVars($param);
		return $email->send();
	}

}