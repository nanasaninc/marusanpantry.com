<?php

App::uses('Component', 'Controller');

class LessonSupportComponent extends Component{

	public function initialize(Controller $controller) {

		$this->Controller = $controller;

		//今月
		$thismonth = date('Y-m');
		//来月
		$nextmonth = date('Y-m', strtotime(date('Y-m-1').' +1 month'));
		//再来月
		$nextnextmonth = date('Y-m', strtotime(date('Y-m-1').' +2 month'));

		$tdata  = $this->_getmonth($thismonth);
		$ndata  = $this->_getmonth($nextmonth);
		$nndata = $this->_getmonth($nextnextmonth);

		$this->Controller->set(compact('tdata', 'ndata', 'nndata', 'thismonth', 'nextmonth', 'nextnextmonth'));


		//カレンダー
		$tdatas  = $this->_monthquery($thismonth);
		$ndatas  = $this->_monthquery($nextmonth);
		$nndatas = $this->_monthquery($nextnextmonth);

		$tkey  = $this->_toCalendar($thismonth);
		$nkey  = $this->_toCalendar($nextmonth);
		$nnkey = $this->_toCalendar($nextnextmonth);

		$tkey  = $this->_calandarData($tkey, $tdata, $tdatas);
		$nkey  = $this->_calandarData($nkey, $ndata, $ndatas);
		$nnkey = $this->_calandarData($nnkey, $nndata, $nndatas);

		$thismt = am($tkey, $nkey, $nnkey);

		$this->Controller->set('thismt', $thismt);
	}

	public function _toCalendar($months){

		$tmonth = $this->_monthquery($months);
		$tkey   = $this->_monthkey($tmonth);
		return $tkey;
	}

	public function _monthkey($arrays){
		if(!empty($arrays)){
			foreach($arrays as $k => $v){
				$arr[] = $v['LessonDate']['date'];
			}
			$arr = array_unique($arr);
			sort($arr);

			foreach($arr as $key => $val){
				$df[$val] = ' ';
			}
			return $df;
		}
	}
	public function _monthquery($mon){

		$lesson_date_model = ClassRegistry::init('LessonDate');

		$month = $lesson_date_model->find('all', array(
				"conditions" => array(
					"DATE_FORMAT(LessonDate.date, '%Y-%m')" => $mon
				),
				'fields' => array(),
			)
		);
		return $month;
	}

	public function _getmonth($month){

		$lesson_model      = ClassRegistry::init('Lesson');
		$lesson_date_model = ClassRegistry::init('LessonDate');

		$lesson_model->contain(array(
			'LessonImage',
			'LessonDate',
		));

		$thismonths = $lesson_date_model->find('all', array(
				"conditions" => array("DATE_FORMAT(LessonDate.date, '%Y-%m')" => $month),
				'fields'     => 'DISTINCT lesson_id',
			)
		);

		if(!empty($thismonths)){
			foreach($thismonths as $k => $v){
				$array[] = $v['LessonDate']['lesson_id'];
			}
		}else{
			$array = null;
		}
		$today = date('Y-m-d');

		$data = $lesson_model->find('all', array(
				'conditions' => array(
					'id'      => $array,
					'active'  => 0,
					'del_flg' => 0,
					"Lesson.limitday > '{$today}'"
				),
				'fields' => array(
					'id', 'category', 'title', 'description', 'active', 'del_flg'
				),
				'order' => array(
					'category' => 'ASC'
				),
			)
		);
		return $data;
	}

	public function _calandarData($keys, $data, $datas){
		if(!empty($keys)){
			$nums = count($datas);
			$counts = 0;
			foreach($datas as $k => $v){
				for($i = 0;$i < $nums; $i++){
					if(!empty($data[$i]['Lesson']['id'])){
						if($v['LessonDate']['lesson_id'] == $data[$i]['Lesson']['id']){
							$ac       = $data[$i]['Lesson']['active'];
							$del      = $data[$i]['Lesson']['del_flg'];
							$category = $data[$i]['Lesson']['category'];
							$titles   = $data[$i]['Lesson']['title'];
							break;
						}
					}
				}
				//if(!empty($category) && !empty($data[$i]['Lesson']['id']) && !empty($data[$i]['LessonImage'][0]['uploadpath'])){
				if(!empty($category) && !empty($data[$i]['Lesson']['id'])){
					//pr($data);
					if((int)$category === 1 && $ac == 0 && $del == 0){
						if(!empty($data[$i]['LessonImage'][0]['uploadpath'])){
							$images = str_replace('.jpg', '_th.jpg', $data[$i]['LessonImage'][0]['uploadpath']);
						}else{
							$images = 'img/common/nowprinting.jpg';
						}
						$keys[$v['LessonDate']['date']] .= '<div class="tipwrap"><p class="tips special"><img src="../img/common/icon-special.png" width="53" height="16" alt="特別教室" /></p><div class="tip"><div class="inner"><p class="clearfix"><span class="left"><a href="./lesson/'.$data[$i]['Lesson']['id'].'"><img src="../'.$images.'" width="50" /></a></span><span><img src="../img/common/icon-special.png" width="53" height="16" alt="特別教室" /><br /><a href="./lesson/'.$data[$i]['Lesson']['id'].'">'.$titles.'</a></span></p></div></div></div>';
					}elseif((int)$category === 2 && $ac == 0 && $del == 0){
						if(!empty($data[$i]['LessonImage'][0]['uploadpath'])){
							$images = str_replace('.jpg', '_th.jpg', $data[$i]['LessonImage'][0]['uploadpath']);
						}else{
							$images = 'img/common/nowprinting.jpg';
						}
						$keys[$v['LessonDate']['date']] .= '<div class="tipwrap"><p class="tips bread"><img src="../img/common/icon-bread.png" width="53" height="16" alt="パン教室" /></p><div class="tip"><div class="inner"><p class="clearfix"><span class="left"><a href="./lesson/'.$data[$i]['Lesson']['id'].'"><img src="../'.$images.'" width="50" /></a></span><img src="../img/common/icon-bread.png" width="53" height="16" alt="パン教室" /><br /><span><a href="./lesson/'.$data[$i]['Lesson']['id'].'">'.$titles.'</a></span></p></div></div></div>';
					}elseif((int)$category === 3 && $ac == 0 && $del == 0){
						if(!empty($data[$i]['LessonImage'][0]['uploadpath'])){
							$images = str_replace('.jpg', '_th.jpg', $data[$i]['LessonImage'][0]['uploadpath']);
						}else{
							$images = 'img/common/nowprinting.jpg';
						}
						$keys[$v['LessonDate']['date']] .= '<div class="tipwrap"><p class="tips sweets"><img src="../img/common/icon-sweets.png" width="53" height="16" alt="お菓子教室" /></p><div class="tip"><div class="inner"><p class="clearfix"><span class="left"><a href="./lesson/'.$data[$i]['Lesson']['id'].'"><img src="../'.$images.'" width="50" /></a></span><img src="../img/common/icon-sweets.png" width="53" height="16" alt="お菓子教室" /><br /><span><a href="./lesson/'.$data[$i]['Lesson']['id'].'">'.$titles.'</a></span></p></div></div></div>';
					}elseif((int)$category === 4 && $ac == 0 && $del == 0){
						if(!empty($data[$i]['LessonImage'][0]['uploadpath'])){
							$images = str_replace('.jpg', '_th.jpg', $data[$i]['LessonImage'][0]['uploadpath']);
						}else{
							$images = 'img/common/nowprinting.jpg';
						}
						$keys[$v['LessonDate']['date']] .= '<div class="tipwrap"><p class="tips taiken"><img src="../img/common/icon-taiken.png" width="53" height="16" alt="体験教室" /></p><div class="tip"><div class="inner"><p class="clearfix"><span class="left"><a href="./lesson/'.$data[$i]['Lesson']['id'].'"><img src="../'.$images.'" width="50" /></a></span><img src="../img/common/icon-taiken.png" width="53" height="16" alt="体験教室" /><br /><span><a href="./lesson/'.$data[$i]['Lesson']['id'].'">'.$titles.'</a></span></p></div></div></div>';
					}
				}
				$counts++;
			}

		}

		return $keys;
	}


	/**
	 * [教室予約]
	 * @return [type] [description]
	 */
	public function lesson_reserve(){

		$lesson_model     = ClassRegistry::init('Lesson');
		$lessondate_model = ClassRegistry::init('LessonDate');

		$lesson_model->contain(array(
			'LessonDate' => array('order' => 'LessonDate.date ASC'),
			'LessonImage',
		));

		switch($this->Controller->request->data['Reserve']['mode']){
			case '申し込み':
				$dateid = $lessondate_model->find('first', array(
						'conditions' => array(
							'id' => $this->Controller->request->data['Reserve']['date_id']
						),
						'fields' => array(),
						'order'  => array()
					)
				);

				$dataphoto = $lesson_model->find('first', array(
					'conditions' => array(
							'id' => $this->Controller->request->data['Reserve']['id']
						),
						'fields' => array(),
						'order'  => array()
					)
				);

				$this->Controller->set('dataphoto', $dataphoto);

				if(!empty($dateid)){
					for($i = 1;$i <= $dateid['LessonDate']['capacity'];$i++){
						$capanum[$i] = $i;
					}
				}
				if(!empty($capanum)){
					$this->Controller->set('capanum', $capanum);
				}

			break;

			case '申込み内容確認':

				$dataphoto = $lesson_model->find('first', array(
					'conditions' => array(
							'id' => $this->Controller->request->data['Reserve']['id']
						),
						'fields' => array(),
						'order'  => array()
					)
				);

				$this->Controller->set('dataphoto', $dataphoto);

			break;
		}

		$this->Controller->set('bodyId', 'reserve');
		$this->Controller->set('fancy', 'fancy');
	}

	public function lesson_capacity_confirm($post_data){
		// キャパシティが入力されていない場合リダイレクト
		if(isset($post_data['Reserve']['active']) && strlen($post_data['Reserve']['active'])){
			$lesson_model     = ClassRegistry::init('Lesson');
			$lessondate_model = ClassRegistry::init('LessonDate');

			// findを排他処理を入れたので、queryに変更
			// $lesson_date_data = $lessondate_model->find('first', array(
			// 	'conditions' => array(
			// 		'id' => $post_data['Reserve']['date_id']
			// 		),
			// 	'fields' => array('id', 'capacity'),
			// 	'order'  => array()
			// 	)
			// );
			$lesson_date_data = $lessondate_model->query('SELECT `LessonDate`.`id`, `LessonDate`.`capacity` FROM `_marusan`.`lesson_dates` AS `LessonDate`   WHERE `id` = ' . $post_data['Reserve']['date_id'] . ' LIMIT 1 FOR UPDATE', false);
			$lesson_date_data = $lesson_date_data[0];

		}else{
			$this->Controller->Session->setFlash('申し訳ございません。お申込みの教室は満席になりました。<br>別の教室にお申し込みいただくか、次回開催をお待ち下さい。');
			return $this->Controller->redirect('../lessons');
		}

		// キャパシティが0の場合リダイレクト
		if($lesson_date_data['LessonDate']['capacity'] == 0){
			$this->Controller->Session->setFlash('申し訳ございません。お申込みの教室は満席になりました。<br>別の教室にお申し込みいただくか、次回開催をお待ち下さい。');
			$this->Controller->redirect('../lessons');
		}

		// 申込人数とキャパシティが引き算で0を下回る場合、入力画面にリダイレクト
		$capacity = (int)$lesson_date_data['LessonDate']['capacity'] - (int)$post_data['Reserve']['active'];

		if($capacity < 0){
			$this->Controller->Session->setFlash('申し訳ございません。お申込みの人数が申し込める人数を超えています。<br>人数を変更して再度お申し込みください。');
			$this->Controller->redirect('../lessons');
		}

	}
}
