<?php

App::uses('Component', 'Controller');

class AdminPantryComponent extends Component{

	public $components = array('Upload');

	// うまく動かない。

	public function initialize(Controller $controller) {
		$this->Controller = $controller;
		$this->Upload->initialize($controller);
	}


	public function save_pantry_data($request_data){

		$pantry_model      = ClassRegistry::init('Pantry');
		$pantryimage_model = ClassRegistry::init('PantryImage');

		$pantry_model->contain(array(
			'PantryImage',
		));


		$pantry_model->set($request_data);

		$pantry_model->set_validate_default();

		if($pantry_model->validates()){

			if(!$pantry_model->save($request_data)){
				echo "イチオシ商品登録中にエラーが発生しました。";
			}else{
				$num = 0;
				foreach($request_data['PantryImage'] as $k => $v ){

					if(!$pantryimage_model->saveAll($v)){
						echo "イチオシ商品登録中にエラーが発生しました。";
					}

					$save_db_data = array();
					$save_db_data['id']         = $pantry_model->PantryImage->id;
					$save_db_data['pantry_id']  = $pantry_model->id;

					if($request_data['Pantry']['fileName' . $num]['error'] === 0){
						$upload_data = array();

						$upload_data['file']        = $request_data['Pantry']['fileName' . $num]['tmp_name'];
						$upload_data['dir']         = 'pantry';
						$upload_data['relation_id'] = $pantry_model->id;
						$upload_data['file_name']   = sprintf("%011d", $pantry_model->PantryImage->id);
						$upload_data['thumb_size']  = $this->thumbsize;
						$upload_data['model']       = $pantryimage_model;

						$this->Upload->upload_image($upload_data);

						$save_db_data['uploadpath'] = $this->Controller->Upload->get_upload_path('pantry', $pantry_model->id, sprintf("%011d", $pantry_model->PantryImage->id));

						if(!$pantry_model->PantryImage->saveAll($save_db_data)){
							echo "イチオシ商品登録中にエラーが発生しました。";
						}

						$savedata['image'] = $request_data['Pantry']['fileName' . $num];
						if ($pantryimage_model->save($savedata, true)) {
						}
					}else{
						if(!$pantryimage_model->saveAll($save_db_data)){
							echo "イチオシ商品登録中にエラーが発生しました。";
						}
					}
					$num++;
				}
				$this->Controller->Session->delete('token');
				$this->Controller->Session->setFlash('登録完了しました。');
				$this->Controller->redirect('../adminpantries');
			}
		}
	}
}
