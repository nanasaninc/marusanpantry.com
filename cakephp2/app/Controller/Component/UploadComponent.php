<?php

App::uses('Component', 'Controller');

class UploadComponent extends Component{

	public function initialize(Controller $controller) {

		$this->Controller = $controller;

	}

	public function upload_image($upload_data){

		$upload_model = $upload_data['model'];

		$size = getimagesize($upload_data['file']);

		if($size[0] === $size[1]){
			$upload_model->Behaviors->load('Uploader.Attachment', array('image' => array(
				'nameCallback' =>  'formatName',
				'append'       =>  $upload_data['file_name'],
				'uploadDir'    =>  WWW_ROOT . 'files/uploads/' . $upload_data['dir'] . '/' . $upload_data['relation_id'] . '/',
				'overwrite'    => true,
				'transforms' => array(
					'image_main' => array(
						'class'        => 'resize',
						'nameCallback' =>  'formatName',
						'quality'      => 100,
						'self'         => false,
						'prepend'      =>  $upload_data['file_name'],
						'append'       =>  '_th',
						'width'        => $upload_data['thumb_size'],
						'overwrite'    => true,
					)
				)
			)));
		}else{
			$upload_model->Behaviors->load('Uploader.Attachment', array('image' => array(
				'nameCallback' =>  'formatName',
				'append'       =>  $upload_data['file_name'],
				'uploadDir'    =>  WWW_ROOT . 'files/uploads/' . $upload_data['dir'] . '/' . $upload_data['relation_id'] . '/',
				'overwrite'    => true,
				'transforms' => array(
					'image_main' => array(
						'class'        => 'crop',
						'nameCallback' =>  'formatName',
						'quality'      => 100,
						'self'         => false,
						'prepend'      =>  $upload_data['file_name'],
						'append'       =>  '_th',
						'width'        => $upload_data['thumb_size'],
						'height'       => $upload_data['thumb_size'],
						'overwrite'    => true,
					)
				)
			)));
		}
	}


	public function get_upload_path($dir, $id, $filename, $names){
		$ext = pathinfo($names, PATHINFO_EXTENSION);
		return 'files/uploads/' . $dir . '/' . $id . '/' . $filename . '.' . $ext;
	}

}
