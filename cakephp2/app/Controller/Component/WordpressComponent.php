<?php

App::uses('Component', 'Controller');

class WordpressComponent extends Component{

	public function initialize(Controller $controller) {

		$this->Controller = $controller;

	}


	public function get_wordpress_post(){
		$wordpresspantry_model  = ClassRegistry::init('Wordpresspantry');
		$wordpresskitchen_model = ClassRegistry::init('Wordpresskitchen');

		//wordpress
		$pantry  = $wordpresspantry_model->getpost();
		$kitchen = $wordpresskitchen_model->getpost();

		$panarray = null;
		$i = 0;
		foreach($pantry as $k => $v){
			$panarray[$i]['title'] = $v['Wordpresspantry']['post_title'];
			$panarray[$i]['link'] = $v['Wordpresspantry']['guid'];
			$panarray[$i]['date'] = $v['Wordpresspantry']['post_date'];
			$i++;
		}
		$kitarray = null;
		$i = 0;
		foreach($kitchen as $k => $v){
			$kitarray[$i]['title'] = $v['Wordpresskitchen']['post_title'];
			$kitarray[$i]['link'] = $v['Wordpresskitchen']['guid'];
			$kitarray[$i]['date'] = $v['Wordpresskitchen']['post_date'];
			$i++;
		}

		$post = am($panarray, $kitarray);


		$date = null;
		$i = 0;
		foreach($post as $k => $v){
			$date[$k] = $v["date"];
		}
		array_multisort($date, SORT_DESC, $post);
		$this->Controller->set('post', $post);
	}

}
