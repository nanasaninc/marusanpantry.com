<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController{

	public $name = 'users';

	public $uses = array('User');

	public $autoRender = true;

	public $components = array('Auth');

	public $helpers = array(

	);


	public function beforeFilter(){

		// $this->Auth->allow(array('login', 'regist'));//@ado ユーザーの変更
		$this->Auth->allow(array('login'));
		$userdata = $this->Auth->user();
		$this->set('userinfodata', $userdata);
	}

	function login(){

		$this->layout = "Admin/adminlogin";

		if($this->request->is('post')) {
			if($this->Auth->login()){
				// ログインが成功した時の処理
				$this->redirect('/admins/');
			}else{
				$this->Session->setFlash('ユーザ名もしくはパスワードに誤りがあります');
			}
		}


		$userdata = $this->Auth->user();

		$this->render('/Front/Users/login');
	}

	function logout(){

		$this->Session->setFlash('ログアウトしました。');
		//$this->Session->delete('Auth.id');
		$this->Auth->logout();
		$this->redirect('/users/login/');
		//$this->redirect(array('action' => '.'));
	}

	function _regist(){

		//$this->autoLayout = true;
		//$this->autoRender = true;
		//$this->layout = "users/login";

		if(!empty($this->data)){

			$this->User->set($this->data);

			if($this->data['User']['mode'] == 'confirm' && $this->User->set($this->data)){

				return $this->render('/Front/Users/complete/confirm');

			}else{

				if($this->data['User']['mode'] == '登録する'){

					$savedata = $this->data;

					$this->User->set($savedata);
					$this->User->save($savedata);

					return $this->render('/Front/Users/complete/complete');
				}
			}
		}
		$this->render('/Front/Users/regist');
	}
}
