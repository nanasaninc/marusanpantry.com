<?php

App::uses('AppController', 'Controller');

class StaffController extends AppController{

	public $uses = null;

	public $autoRender = true;
	public $layout     = "Front/siteframe";

	public $components = array(
		'Meta'
	);

	public $helpers = array(

	);


	public function beforeFilter(){

		parent::beforeFilter();

		$bodyId = 'staff';
		$this->set('bodyId', $bodyId);

	}

	function index(){

		$this->Meta->meta('スタッフ紹介｜');

		$this->render('/Front/Staff/index');
	}

}
