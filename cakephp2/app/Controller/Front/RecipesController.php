<?php

App::uses('AppController', 'Controller');

class RecipesController extends AppController{

	public $uses = array('Recipe');

	public $autoRender = true;
	public $layout     = "Front/siteframe";

	public $components = array(
		'Common',
		'RecipeSupport',
		'Paginator',
		'Meta',
	);

	public $helpers = array(
		'Common',
		'Thcalendar',
		'Jaweek'
	);

	public $pagenum = 20;


	public function beforeFilter(){

		parent::beforeFilter();

		$this->loadModel('RecipeImage');
		$this->loadModel('RecipeMaterial');
		$this->loadModel('RecipeOrder');
		$this->loadModel('RecipeRelated');
		$this->loadModel('Difficulty');
		$this->loadModel('Active');
		$this->loadModel('Recipecat');

	}

	/**
	 * [トップページ]
	 * @return [type] [description]
	 */
	function index(){

		$this->Paginator->settings = $this->Recipe->get_toppage_paginator_setting();
		$recipe_data = $this->Paginator->paginate('Recipe');
		$this->set('data', $recipe_data);

		$this->Meta->meta('レシピ｜');

		$this->render('/Front/Recipes/index');
	}

	/**
	 * [カテゴリ検索]
	 * @param  [type] $param [description]
	 * @return [type]        [description]
	 */
	function category($param = ''){

		if(isset($this->Recipecat->recategory[$param])){
			$this->set('topicpath_name', '<span>&gt;</span>' . $this->Recipecat->recategory[$param]);
		}else{
			$this->set('topicpath_name', '');
		}

		$this->Paginator->settings = $this->Recipe->get_category_paginator_setting($param, $this->pagenum);
		$this->set('data', $this->Paginator->paginate('Recipe'));

		$this->Meta->meta('レシピカテゴリー｜レシピ｜');

		$this->render('/Front/Recipes/category');
	}

	/**
	 * [難易度検索]
	 * @param  [type] $param [description]
	 * @return [type]        [description]
	 */
	function difficulty($param = ''){
		if(isset($this->Difficulty->difficulty[$param])){
			$this->set('topicpath_name', '<span>&gt;</span>' . $this->Difficulty->difficulty[$param]);
		}else{
			$this->set('topicpath_name', '');
		}

		$this->Paginator->settings = $this->Recipe->get_difficulty_paginator_setting($param, $this->pagenum);
		$this->set('data', $this->Paginator->paginate('Recipe'));

		$this->Meta->meta('レシピカテゴリー｜レシピ｜');

		$this->render('/Front/Recipes/category');
	}



	/**
	 * [キーワード検索]
	 * @return [type] [description]
	 */
	function search(){

		$this->Recipe->contain('RecipeImage');

		$fields = array('id', 'title', 'category', 'description', 'active', 'created');

		$search_value['keyword'] = $this->request->query('keyword');
		$this->set('search_value', $search_value);

		if($this->request->query('keyword')){
			$this->set('topicpath_name', '<span>&gt;</span>' . htmlspecialchars($this->request->query('keyword'), ENT_QUOTES));
		}else{
			$this->set('topicpath_name', '');
		}


		$conditions['OR']['Recipe.title LIKE']       = '%'. $this->request->query('keyword'). '%';
		$conditions['OR']['Recipe.description LIKE'] = '%'. $this->request->query('keyword'). '%';
		$conditions['AND']['Recipe.active']          = 0;
		$conditions['AND']['Recipe.del_flg']         = 0;

		$setting = $this->Paginator->settings = array(
			'conditions' => $conditions,
			'fields'     => $fields,
			'paramType'  => 'querystring',
			'limit'      => $this->pagenum,
			'order'      => 'Recipe.modified DESC',
		);


		$this->set('data', $this->Paginator->paginate('Recipe'));

		$this->Meta->meta('レシピ検索｜レシピ｜');

		$this->render('/Front/Recipes/search');
	}

	/**
	 * [詳細ページ]
	 * @param  [type] $param [description]
	 * @return [type]        [description]
	 */
	function recipe($param = ''){

		$data = $this->Recipe->get_recipe($param);

		if(!empty($data)){
			$this->set('data', $data);
		}else{
			$this->Session->setFlash('該当のデータはありませんでした。');
			$this->redirect('../recipes');
		}

		$this->Meta->meta($data['Recipe']['title'] . '｜レシピ｜');

		$this->render('/Front/Recipes/recipe');
	}
}
