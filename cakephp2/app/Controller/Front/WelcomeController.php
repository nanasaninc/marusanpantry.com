<?php

App::uses('AppController', 'Controller');

class WelcomeController extends AppController{

	public $uses = null;

	public $autoRender = true;
	public $layout     = "Front/siteframe";

	public $components = array(
		'Meta'
	);

	public $helpers = array(

	);


	public function beforeFilter(){

		parent::beforeFilter();

		$bodyId = 'welcome';
		$this->set('bodyId', $bodyId);

	}

	function index(){

		$this->Meta->meta('ようこそ｜');

		$this->render('/Front/Welcome/index');
	}

}
