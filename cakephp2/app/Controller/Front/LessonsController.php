<?php

App::uses('AppController', 'Controller');

class LessonsController extends AppController{

	public $uses = array('Lesson', 'TransactionManager');

	public $autoRender = true;
	public $layout     = "Front/siteframe";

	public $components = array(
		'Common',
		'LessonSupport',
		'Session',
		'MyMail',
		'Meta',
	);

	public $helpers = array(
		'Common',
		'Thcalendar',
		'Jaweek'
	);


	public function beforeFilter(){

		parent::beforeFilter();

		$this->loadModel('LessonImage');
		$this->loadModel('LessonDate');
		$this->loadModel('Difficulty');
		$this->loadModel('Lessonicon');
		$this->loadModel('Lcategory');
		$this->loadModel('Reserve');

		$difficulty = $this->Difficulty->difficulty;
		$this->set('difficulty', $difficulty);

		$lessonicon = $this->Lessonicon->icons;
		$this->set('lessonicon', $lessonicon);

		$lcategory = $this->Lcategory->lcategory;
		$this->set('lcategory', $lcategory);

		$bodyId = 'lesson';
		$this->set('bodyId', $bodyId);

		$icons = array(
			'1' => 'special',
			'2' => 'bread',
			'3' => 'sweets',
			'4' => 'taiken',
		);
		$this->set('icons', $icons);
	}

	function index(){

		$this->Meta->meta('パン・お菓子教室｜');

		$this->render('/Front/Lessons/index');
	}

	function lesson($param = ''){

		$data = $this->Lesson->get_lesson($param);

		if(!empty($data)){
			$this->set('data', $data);
		}else{
			$this->Session->setFlash('該当のデータはありませんでした。');
			$this->redirect('../lessons');
		}

		$this->Meta->meta($data['Lesson']['title'] . '｜パン・お菓子教室｜');

		$this->render('/Front/Lessons/lesson');
	}


	function reserve(){

		if(!isset($this->request->data['Reserve']['mode'])){
			$this->redirect('../lessons');
		}

		$this->LessonSupport->lesson_reserve();


		if($this->request->is('post') || $this->request->is('put')){

			$this->set('data', $this->request->data);


			switch ($this->request->data['Reserve']['mode']) {

				case '申込み内容確認':

					$this->LessonSupport->lesson_capacity_confirm($this->request->data);

					$this->Meta->meta('参加申し込み｜パン・お菓子教室｜');

					$this->Reserve->set($this->request->data);

					if($this->Reserve->validates()){
						$this->Session->write('session_reserve', $this->request->data);
						return $this->render('/Front/Lessons/reserve_confirm');
					}
					break;

				case '申込む':

						try {

							$mail_data = $this->Session->read('session_reserve');

							$transaction = $this->TransactionManager->begin();

							$this->LessonSupport->lesson_capacity_confirm($mail_data);

							$this->Meta->meta('参加申し込み｜パン・お菓子教室｜');

							if(!isset($mail_data['Reserve'])){
								throw new Exception('送信内容が取得できませんでした。');
							}

							$lesson_date_data = $this->LessonDate->find('first', array(
									'conditions' => array(
										'id' => $mail_data['Reserve']['date_id']
									),
									'fields' => array('id', 'capacity'),
									'order'  => array()
								)
							);

							$savedata['LessonDate']['id'] = $lesson_date_data['LessonDate']['id'];
							$savedata['LessonDate']['capacity'] = (int)$lesson_date_data['LessonDate']['capacity'] - (int)$mail_data['Reserve']['active'];

							$reserve_data = $mail_data['Reserve'];

							$hikimail = array(
								'title'     => $reserve_data['title'],//教室名
								'date'      => $reserve_data['date'],//日付
								'datetxt'   => $reserve_data['datetxt'],//時間
								'name'      => $reserve_data['name'],//氏名
								'phone'     => $reserve_data['phone'],//電話番号
								'email'     => $reserve_data['email'],//メールアドレス
								'capacity'  => $reserve_data['active'],//申込み人数
								'syokai'    => $reserve_data['syokai'],//初回申込み
								'membernum' => $reserve_data['membernum'],//会員No
								'comment'   => $reserve_data['comment'],//備考
							);

							// @ado 例外が起きた時にここままでいい？ 画面表示して伝えなくてよいか？
							if (false == $this->MyMail->reserve_to_marusan(Configure::read('mail_config.mail_address'), $hikimail)) {
								throw new Exception('メールの送信ができませんでした。');
							}
							if (false == $this->MyMail->reserve_to_client($hikimail['email'], $hikimail)) {
								throw new Exception('メールの送信ができませんでした。');
							}
							if (false == $this->LessonDate->save($savedata)) {
								throw new Exception('メールが保存できませんでした。');
							}

							$this->TransactionManager->commit($transaction);

							$this->Session->delete('session_reserve');
							$this->redirect('../lessons/reserve_complete');
						} catch (Exception $e) {
							$this->TransactionManager->rollback($transaction);
							$this->Session->delete('session_reserve');
							$this->redirect('../lessons');
						}
					break;
				default:
						$this->Meta->meta('参加申し込み｜' . $this->data['Reserve']['title'] . '｜パン・お菓子教室｜');
						$this->Session->delete('session_reserve');
					break;
			}
		}else{
			$this->redirect('../lessons');
		}

		$this->render('/Front/Lessons/reserve');
	}

	function reserve_complete(){
		$this->set('bodyId', 'reserve');
		$this->set('fancy', 'fancy');

		$this->Meta->meta('参加申し込み完了｜パン・お菓子教室｜｜パン・お菓子教室｜');

		return $this->render('/Front/Lessons/reserve_complete');
	}


	function flow(){
		$this->Meta->meta('ご参加の流れ｜パン・お菓子教室｜');
		$this->render('/Front/Lessons/flow');
	}
}
