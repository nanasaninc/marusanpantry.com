<?php

App::uses('AppController', 'Controller');

class PrivacyController extends AppController{

	public $name = 'privacy';
	public $uses = null;

	public $autoRender = true;
	public $layout     = "Front/siteframe";

	public $components = array(
		'Meta'
	);

	public $helpers = array(

	);


	public function beforeFilter(){

		parent::beforeFilter();

		$bodyId = 'privacy';
		$this->set('bodyId', $bodyId);
	}

	function index(){

		$this->Meta->meta('プライバシーポリシー｜');

		$this->render('/Front/Privacy/index');
	}
}
