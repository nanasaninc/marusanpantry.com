<?php

App::uses('AppController', 'Controller');

class ScrapingsController extends AppController{

	public $name = 'Scrapings';
	public $uses = null;
	public $components = array();

	//public $helpers = array('Thcalendar');
	//基本URL
	private $url = 'http://www.m-pantry.com/';//shop/item/mpantry/picture/goods/7535_1.jpg

	public $layout     = false;
	public $autoRender = false;

	function beforeFilter(){
		$bodyId = 'home';
		$this->set('bodyId', $bodyId);
	}

	function index(){

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->request->is('ajax')){
			// POST情報は$this->params['data']で取得
			$urls = $this->params['data']['urls'];

			$html = @file_get_contents($urls);
			$item = array();
			$item['status'] = false;
			if($html != false){
				$html = mb_convert_encoding($html, "UTF-8", "Windows-31J");
				$item['status'] = true;
				$item['image']  = $this->_getimages($html);
				$item['title']  = $this->_gettitle($html);
				$item['price']  = $this->_getprice($html);
				echo json_encode($item);
			}else{
				echo json_encode($item);
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecipes/');
		}
	}


	function _getimages($html){
		preg_match('/<img src=\"\/shop\/item\/mpantry\/picture\/goods.+?class=\"thumbnail\"/', $html, $match);
		$match[0] = str_replace('<img src="', '', $match[0]);
		$match[0] = str_replace('" class="thumbnail"', '', $match[0]);
		return '<img src="' . $this->url . trim($match[0]) . '" />'."\n";
	}
	function _gettitle($html){
		preg_match('/<h1 class=\"itemTitle\">.+?<\/h1>/', $html, $match);
		$match[0] = str_replace('<h1 class="itemTitle">', '', $match[0]);
		$match[0] = str_replace('</h1>', '', $match[0]);
		$match[0] = preg_replace('/<span.+?>.+?<\/span>/', '', $match[0]);
		return trim($match[0]);
	}
	function _getprice($html){
		preg_match('/<span class=\"itemPrice\">.+?<\/span>/', $html, $match);
		$match[0] = str_replace('<span class="itemPrice">', '', $match[0]);
		$match[0] = str_replace('</span>', '', $match[0]);
		return trim($match[0]);
	}
}
