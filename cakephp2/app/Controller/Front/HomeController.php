<?php

App::uses('AppController', 'Controller');

class HomeController extends AppController{

	public $name = 'home';
	public $uses = array('Lesson');

	public $autoRender = true;
	public $layout     = "Front/siteframe";

	public $components = array(
		'Wordpress',
		'Meta'
	);

	public $helpers = array(
		'Common'
	);


	public function beforeFilter(){

		parent::beforeFilter();

		$bodyId = 'home';
		$this->set('bodyId', $bodyId);

		$this->loadModel('LessonImage');
		$this->loadModel('LessonDate');
		$this->loadModel('Lessonicon');
		$this->loadModel('Lcategory');

		//wordpress
		$this->Wordpress->get_wordpress_post();
		//wordpress


		$lcategory = $this->Lcategory->lcategory;
		$this->set('lcategory', $lcategory);

		$icons = array(
			'1' => 'special',
			'2' => 'bread',
			'3' => 'sweets',
			'4' => 'taiken',
		);
		$this->set('icons', $icons);

		//今月
		$thismonth = date('Y-m');
		//来月
		$nextmonth = date('Y-m', strtotime(date('Y-m-1').' +1 month'));
		//再来月
		$nextnextmonth = date('Y-m', strtotime(date('Y-m-1').' +2 month'));

		$tdata  = $this->_getmonth($thismonth);
		$ndata  = $this->_getmonth($nextmonth);
		$nndata = $this->_getmonth($nextnextmonth);

		$this->set(compact('tdata', 'ndata', 'nndata', 'thismonth', 'nextmonth', 'nextnextmonth'));
	}

	function index(){

		$this->Meta->meta('お菓子で笑顔を作る｜');

		$this->Lesson->contain('LessonImage');

		$data = $this->Lesson->get_osusumelesson();
		$this->set('osusumelesson', $data);

		$this->render('/Front/Home/index');
	}

	/**
	 * [月次のレッスンデータ取得]
	 * @param  [type] $month [description]
	 * @return [type]        [description]
	 */
	function _getmonth($month){
		$thismonths = $this->LessonDate->find('all', array(
				"conditions" => array(
					"DATE_FORMAT(LessonDate.date, '%Y-%m')" => $month
				),
				'fields' => 'DISTINCT lesson_id',
			)
		);
		$array = array();
		if(!empty($thismonths)){
			foreach($thismonths as $k => $v){
				$array[] = $v['LessonDate']['lesson_id'];
			}
		}else{
			$array = null;
		}

		$this->Lesson->contain(array(
			'LessonImage',
			'LessonDate',
		));
		$data = $this->Lesson->find('all', array(
				'conditions' => array(
					'id'      => $array,
					'active'  => 0,
					'del_flg' => 0
				),
				'fields' => array(
					'id',
					'category',
					'title',
					'description'
				),
				'order' => array(
					'category' => 'ASC'
				),
			)
		);

		return $data;
	}
}
