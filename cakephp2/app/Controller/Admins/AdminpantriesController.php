<?php

App::uses('AppController', 'Controller');

class AdminpantriesController extends AppController{

	public $name       = 'Adminpantries';
	public $uses       = array('Pantry', 'PantryImage');
	public $components = array(
		'Common',
		'Paginator',
		'Upload',
		'AdminPantry',
		'Auth'
	);
	public $helpers    = array('Thcalendar', 'Jaweek');

	public $autoRender = true;
	public $layout     = "Admin/siteframe";

	public $thumbsize = '200';//サムネイルサイズ

	public $pagenum = 20;

	function beforeFilter(){

		parent::beforeFilter();

        $this->Auth->authError = 'ログインしてください。';

		$this->loadModel('Wordpress');
		$this->loadModel('Active');

		$active = $this->Active->active;
		$this->set('active', $active);

		$wordpress = $this->Wordpress->url;
		$this->set('wordpress', $wordpress);

		$bodyId = 'pantry';
		$this->set('bodyId', $bodyId);

	}

	function index(){
		$this->Pantry->contain(array(
			'PantryImage',
		));
		$data['list'] = $this->Pantry->find('all', array(
				'conditions' => array('del_flg' => 0),
				'fields'     => array(),
				'order'      => 'id DESC'
			)
		);
		$this->set('data', $data);

		$this->render('/Admins/adminpantries/index');
	}

	/**
	 * [検索]
	 * @return [type] [description]
	 */
	function search(){

		$fields = array('id', 'title', 'description', 'recomflg', 'active');

		if($this->request->is('post') || $this->request->is('put')){

			$keyword = '';

			if($this->Session->check('pantywords')) {
				$keyword = $this->Session->read('pantywords');
			}

			if(isset($this->request->data['keyword']) && strlen($this->request->data['keyword'])){
				$keyword = $this->request->data['keyword'];
			}else{
				$keyword = '';
			}

			$this->Session->write('pantywords', $keyword);

			$conditions = array('OR' => array(
				'title LIKE'       => '%' . $keyword . '%',
				'description LIKE' => '%' . $keyword . '%'),
				'del_flg'          => 0
			);

			$this->Paginator->settings = array(
				'conditions' => $conditions,
				'fields'     => $fields,
				'limit'      => $this->pagenum,
				'order'      => 'id DESC',
				'recursive'  => 2
			);

			$pagebool = $this->Paginator->paginate('Pantry');
			$this->set('data', $pagebool);
			$this->set('searchword', $keyword);

			if(empty($pagebool)){
				$this->Session->setFlash('データがありません。');
			}
		}

		$this->render('/Admins/adminpantries/search');
	}


	function add(){

		$this->Pantry->contain(array(
			'PantryImage',
		));

		set_time_limit(0);

		if($this->request->is('post')){

			if((string)$this->Session->read('token') === (string)$this->request->data['Pantry']['token']){

				if($this->request->data['Pantry']['mode'] === '登録する'){

					$this->Pantry->set($this->request->data);

					$this->Pantry->set_validate_default();

					if($this->Pantry->validates()){
						if(!$this->Pantry->save($this->request->data)){
							echo "イチオシ商品登録中にエラーが発生しました。";
						}else{

							$num = 0;
							foreach($this->request->data['PantryImage'] as $k => $v ){

								if(!$this->Pantry->PantryImage->saveAll($v)){
									echo "イチオシ商品登録中にエラーが発生しました。";
								}

								$save_db_data = array();
								$save_db_data['id']         = $this->Pantry->PantryImage->id;
								$save_db_data['pantry_id']  = $this->Pantry->id;

								if($this->request->data['Pantry']['fileName' . $num]['error'] === 0){

									$upload_data = array();

									$upload_data['file']        = $this->request->data['Pantry']['fileName' . $num]['tmp_name'];
									$upload_data['dir']         = 'pantry';
									$upload_data['relation_id'] = $this->Pantry->id;
									$upload_data['file_name']   = sprintf("%011d", $this->Pantry->PantryImage->id);
									$upload_data['thumb_size']  = $this->thumbsize;
									$upload_data['model']       = ClassRegistry::init('PantryImage');

									$this->Upload->upload_image($upload_data);

									$save_db_data['uploadpath'] = $this->Upload->get_upload_path('pantry', $this->Pantry->id, sprintf("%011d", $this->Pantry->PantryImage->id), $this->request->data['Pantry']['fileName' . $num]['name']);

									if(!$this->Pantry->PantryImage->saveAll($save_db_data)){
										echo "イチオシ商品登録中にエラーが発生しました。";
									}


									$savedata['image'] = $this->request->data['Pantry']['fileName' . $num];
									if ($this->PantryImage->save($savedata, true)) {
									}
								}else{
									if(!$this->Pantry->PantryImage->saveAll($save_db_data)){
										echo "イチオシ商品登録中にエラーが発生しました。";
									}
								}
								$num++;
							}
							$this->Session->delete('token');
							$this->Session->setFlash('登録完了しました。');
							$this->redirect('../adminpantries');
						}
					}
				}
			}else{
				return $this->render('complete/add_error');
			}
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->set('token', $token);

		$this->render('/Admins/adminpantries/add');
	}


	function edit($param = ''){

		if(!strlen($param)){
			$this->redirect('../adminpantries');
		}

		$this->Pantry->contain(array(
			'PantryImage',
		));

		set_time_limit(0);

		if($this->request->is('post') || $this->request->is('put')){

			if((string)$this->Session->read('token') === (string)$this->request->data['Pantry']['token']){

				if($this->request->data['Pantry']['mode'] === '修正する'){

					$this->set('data', $this->request->data);
					$this->Pantry->set($this->request->data);

					$this->Pantry->set_validate_default();

					// $this->AdminPantry->save_pantry_data($this->request->data);

					if($this->Pantry->validates($this->request->data)){
						if(!$this->Pantry->save($this->request->data)){
							echo "イチオシ商品登録中にエラーが発生しました。";
						}else{
							$num = 0;
							foreach($this->request->data['PantryImage'] as $k => $v ){

								if(!$this->Pantry->PantryImage->saveAll($v)){
									echo "イチオシ商品登録中にエラーが発生しました。";
								}

								if($this->request->data['Pantry']['fileName' . $num]['error'] === 0){
									$upload_data = array();

									$upload_data['file']        = $this->request->data['Pantry']['fileName' . $num]['tmp_name'];
									$upload_data['dir']         = 'pantry';
									$upload_data['relation_id'] = $this->Pantry->id;
									$upload_data['file_name']   = sprintf("%011d", $this->Pantry->PantryImage->id);
									$upload_data['thumb_size']  = $this->thumbsize;
									$upload_data['model']       = ClassRegistry::init('PantryImage');

									$this->Upload->upload_image($upload_data);

									$save_db_data = array();
									$save_db_data['id']         = $this->Pantry->PantryImage->id;
									$save_db_data['pantry_id']  = $this->Pantry->id;
									$save_db_data['uploadpath'] = $this->Upload->get_upload_path('pantry', $this->Pantry->id, sprintf("%011d", $this->Pantry->PantryImage->id), $this->request->data['Pantry']['fileName' . $num]['name']);

									if(!$this->Pantry->PantryImage->saveAll($save_db_data)){
										echo "イチオシ商品登録中にエラーが発生しました。";
									}


									$savedata['image'] = $this->request->data['Pantry']['fileName' . $num];
									if ($this->PantryImage->save($savedata, true)) {
									}
								}

								$num++;
							}
							$this->Session->delete('token');
							$this->Session->setFlash('登録完了しました。');
							$this->redirect('../adminpantries');
						}

					}
				}
			}else{
				return $this->render('complete/add_error');
			}
		}else{
			$this->Pantry->id = $param;
			$this->data = $this->Pantry->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->set('token', $token);

		$this->render('/Admins/adminpantries/edit');
	}

	function duplicates($param = ''){

		if(!strlen($param)){
			$this->redirect('../adminpantries');
		}

		$this->Pantry->contain(array(
			'PantryImage',
		));

		set_time_limit(0);

		if($this->request->is('post') || $this->request->is('put')){

			if((string)$this->Session->read('token') === (string)$this->data['Pantry']['token']){

				if($this->request->data['Pantry']['mode'] === '登録する'){

					$this->Pantry->set_validate_default();

					if($this->Pantry->validates()){
						if(!$this->Pantry->save($this->request->data)){
							echo "イチオシ商品登録中にエラーが発生しました。";
						}else{

							$num = 0;
							foreach($this->request->data['PantryImage'] as $k => $v ){

								if(!$this->Pantry->PantryImage->saveAll($v)){
									echo "イチオシ商品登録中にエラーが発生しました。";
								}

								$save_db_data = array();
								$save_db_data['id']         = $this->Pantry->PantryImage->id;
								$save_db_data['pantry_id']  = $this->Pantry->id;

								if($this->request->data['Pantry']['fileName' . $num]['error'] === 0){

									$upload_data = array();

									$upload_data['file']        = $this->request->data['Pantry']['fileName' . $num]['tmp_name'];
									$upload_data['dir']         = 'pantry';
									$upload_data['relation_id'] = $this->Pantry->id;
									$upload_data['file_name']   = sprintf("%011d", $this->Pantry->PantryImage->id);
									$upload_data['thumb_size']  = $this->thumbsize;
									$upload_data['model']       = ClassRegistry::init('PantryImage');

									$this->Upload->upload_image($upload_data);

									$save_db_data['uploadpath'] = $this->Upload->get_upload_path('pantry', $this->Pantry->id, sprintf("%011d", $this->Pantry->PantryImage->id), $this->request->data['Pantry']['fileName' . $num]['name']);

									if(!$this->Pantry->PantryImage->saveAll($save_db_data)){
										echo "イチオシ商品登録中にエラーが発生しました。";
									}

									$savedata['image'] = $this->request->data['Pantry']['fileName' . $num];
									if ($this->PantryImage->save($savedata, true)) {
									}
								}else{
									if(!$this->Pantry->PantryImage->saveAll($save_db_data)){
										echo "イチオシ商品登録中にエラーが発生しました。";
									}
								}
								$num++;
							}

						}
					}
				}
				$this->Session->delete('token');
				$this->Session->setFlash('登録完了しました。');
				$this->redirect('../adminpantries');
			}else{
				return $this->render('complete/add_error');
			}
		}else{
			$this->Pantry->id = $param;
			$this->data = $this->Pantry->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->set('token', $token);

		$this->render('/Admins/adminpantries/duplicates');
	}


	function delete($param = ''){

		if(!strlen($param)){
			$this->redirect('../adminpantries');
		}

		if(!empty($param)){
			$savedata['Pantry']['id']      = $param;
			$savedata['Pantry']['del_flg'] =  1;

			if(!$this->Pantry->save($savedata)){
				$this->Session->setFlash('削除に失敗しました。');
				$this->redirect('../adminpantries/');
			}else{
				$this->Session->setFlash('削除しました。');
				$this->redirect('../adminpantries/');
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminpantries/');
		}
	}

	/**
	 * [未使用だけど置いておく　使うかもしれない]
	 * @return [type] [description]
	 */
	function active() {

		$this->layout = null;

		//バリデーションを無効に
		$this->Pantry->set_validation_ajax();

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->request->is('ajax')){
			// POST情報は$this->params['data']で取得
			$id          = $this->params['data']['id'];
			$activeparam = $this->params['data']['actives'];
			// DBに突っ込みます
			$this->Pantry->id = $id;
			$savedata['Pantry']['active'] = $activeparam;

			if(!$this->Pantry->save($savedata)){
				$message = '更新失敗！';
				$this->set('t', $message);
			}else{
				$message = '更新完了！';
				$this->set('t', $message);
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminpantries/');
		}
	}


	/**
	 * [オススメフラグAjax]
	 * @return [type] [description]
	 */
	function recom() {

		$this->layout     = false;
		$this->autoRender = false;

		//バリデーションを無効に
		$this->Pantry->set_validation_ajax();

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->request->is('ajax')){

			$json['status'] = true;

			if(!$this->Pantry->query('UPDATE pantries SET recomflg = 0')){
				$json['status']  = false;
				$json['message'] = 'おすすめ商品の設定に失敗しました';
			}

			$this->Pantry->id = $this->request->data('id');

			if(!$this->Pantry->saveField('recomflg', 1)){
				$json['status']  = false;
				$json['message'] = 'おすすめ商品の設定に失敗しました';
			}else{
				$json['status'] = true;
				$json['message'] = 'おすすめ商品に設定しました';
			}
			echo json_encode($json);
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminpantries/');
		}
	}
}
