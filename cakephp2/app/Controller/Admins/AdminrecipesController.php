<?php

App::uses('AppController', 'Controller');

class AdminrecipesController extends AppController{

	public $name = 'Adminrecipes';
	public $uses = array('Recipe', 'Recipecat');
	public $components = array(
		'Common',
		'Adminco',
		'Paginator',
		'Session',
		'Upload',
		'Auth'
	);
	public $helpers = array(
		// 'Tree'
	);

	public $autoRender = true;
	public $layout     = "Admin/siteframe";

	public $recipethumbsize = '313';//サムネイルサイズ
	public $pagenum = 20;


	function beforeFilter(){

		parent::beforeFilter();

		$this->Auth->authError = 'ログインしてください。';

		$this->Adminco->recipes();
	}

	function index($param = ''){

		$fields = array('id', 'title', 'category', 'active');
		$order  = 'modified DESC';

		$conditions = array();

		if(empty($param)){
			$conditions = array('del_flg' => 0);
		}else{
			if($param == 10000){
				$conditions   = array('category LIKE' => "1%", 'del_flg' => 0);
			}elseif($param == 20000){
				$conditions   = array('category LIKE' => "2%", 'del_flg' => 0);
			}else{
				$conditions   = array('category' => $param, 'del_flg' => 0);
			}
		}

		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'fields'     => $fields,
			'limit'      => $this->pagenum,
			'order'      => $order,
			'recursive'  => 2
		);

		$pagebool = $this->Paginator->paginate('Recipe');

		if(empty($pagebool)){
			$this->Session->setFlash('データがありません。');
		}

		$this->set('data', $this->Paginator->paginate('Recipe'));

		$this->render('/Admins/adminrecipes/index');
	}



	function search(){


		$this->Recipe->unbindModel(array(
			'hasMany' => array(
				'RecipeMaterial',
				'RecipeOrder',
				'RecipeRelated',
			),
		), false);


		$fields = array('id', 'title', 'category', 'description', 'active');

		$keyword = '';

		if($this->Session->check('recipewords')) {
			$keyword = $this->Session->read('recipewords');
		}

		if($this->request->is('post')){
			if(isset($this->request->data['keyword']) && strlen($this->request->data['keyword'])){
				$keyword = $this->request->data['keyword'];
			}else{
				$keyword = '';
			}
		}else{
			if($this->Session->check('recipewords')) {
				$keyword = $this->Session->read('recipewords');
			}
		}

		$this->Session->write('recipewords', $keyword);

		$conditions = array('OR' => array(
			'title LIKE'       => '%' . $keyword . '%',
			'description LIKE' => '%' . $keyword . '%'),
			'del_flg'          => 0
		);

		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'fields'     => $fields,
			'limit'      => $this->pagenum,
			'order'      => 'Recipe.modified DESC',
		);

		$pagebool = $this->Paginator->paginate('Recipe');
		$this->set('data', $pagebool);
		$this->set('searchword', $keyword);

		if(empty($pagebool)){
			$this->Session->setFlash('データがありません。');
		}

		$this->render('/Admins/adminrecipes/search');
	}



	function add(){

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Recipe']['token']){

				if($this->data['Recipe']['mode'] === '登録する'){
					$this->Recipe->set($this->data);

					$this->Recipe->set_validate_default();

					if($this->Recipe->validates()){
						if(!$this->Recipe->save($this->data)){
							echo "レシピ登録中にエラーが発生しました。";
						}else{
							//レシピ材料保存
							foreach($this->data['RecipeMaterial'] as $k => $v){
								if(!empty($v['name'])){
									$v['recipe_id'] = $this->Recipe->id;
									if(!$this->Recipe->RecipeMaterial->saveAll($v)){
										echo "レシピ登録中にエラーが発生しました。";
									}
								}
							}
							//レシピ手順保存
							$num = 0;
							foreach($this->data['RecipeOrder'] as $k => $v){
								if(!empty($v['comment'])){
									$v['recipe_id'] = $this->Recipe->id;
									if(!$this->Recipe->RecipeOrder->saveAll($v)){
										echo "レシピ登録中にエラーが発生しました。";
									}

									//画像保存
									$save_db_data = array();
									$save_db_data['id']         = $this->Recipe->RecipeOrder->id;
									$save_db_data['recipe_id']  = $this->Recipe->id;

									if(
										isset($this->request->data['Recipe']['orderPhoto' . $num]['name']) &&
										!empty($this->request->data['Recipe']['orderPhoto' . $num]['name'])
									){
										$upload_data = array();

										$upload_data['file']        = $this->request->data['Recipe']['orderPhoto' . $num]['tmp_name'];
										$upload_data['dir']         = 'order';
										$upload_data['relation_id'] = $this->Recipe->id;
										$upload_data['file_name']   = sprintf("%011d", $this->Recipe->RecipeOrder->id);
										$upload_data['thumb_size']  = $this->recipethumbsize;
										$upload_data['model']       = ClassRegistry::init('RecipeOrder');

										$this->Upload->upload_image($upload_data);

										$save_db_data['uploadpath'] = $this->Upload->get_upload_path('order', $this->Pantry->id, sprintf("%011d", $this->Pantry->PantryImage->id), $this->request->data['Recipe']['orderPhoto' . $num]['name']);

										$save_db_data['comment']    = $v['comment'];

										if(!$this->Recipe->RecipeOrder->saveAll($save_db_data)){
											echo "レシピ登録中にエラーが発生しました。";
										}

										$savedata['image'] = $this->request->data['Recipe']['orderPhoto' . $num];
										if ($this->RecipeOrder->save($savedata, true)) {
										}
									}else{
										if(!$this->Recipe->RecipeOrder->saveAll($save_db_data)){
											echo "レシピ登録中にエラーが発生しました。";
										}
									}

									$num++;
								}
							}
							//関連商品保存
							$num = 0;
							foreach($this->data['RecipeRelated'] as $k => $v){
								if(!empty($v['name'])){
									$v['recipe_id'] = $this->Recipe->id;
									if(!$this->Recipe->RecipeRelated->saveAll($v)){//エラー処理しよう
										echo "レシピ登録中にエラーが発生しました。";
									}
								}
								$num++;
							}

							$recipe_post       = $this->request->data['Recipe'];
							$recipe_image_post = $this->request->data['RecipeImage'];

							$num = 0;

							if(isset($recipe_image_post)){
								foreach($recipe_image_post as $k => $v ){

									$save_db_data = array();
									$save_db_data['id']        = $this->Recipe->RecipeImage->id;
									$save_db_data['recipe_id'] = $this->Recipe->id;
									$save_db_data['sort']      = $v['sort'];

									if(
										isset($recipe_post['fileName' . $num]['name']) &&
										!empty($recipe_post['fileName' . $num]['name'])
									){
										$upload_data = array();

										$upload_data['file']       = $recipe_post['fileName' . $num]['tmp_name'];
										$upload_data['dir']        = 'recipe';
										$upload_data['relation_id']  = $this->Recipe->id;
										$upload_data['file_name']  = ($num + 1);
										$upload_data['thumb_size'] = $this->recipethumbsize;
										$upload_data['model']      = ClassRegistry::init('RecipeImage');

										$this->Upload->upload_image($upload_data);

										$ext = pathinfo($recipe_post['fileName' . $num]['name'], PATHINFO_EXTENSION);

										$save_db_data['uploadpath'] = 'files/uploads/recipe/' . $this->Recipe->id . '/' . ($num + 1) . '.' . $ext;

										if(!$this->Recipe->RecipeImage->saveAll($save_db_data)){
											echo "レシピ登録中にエラーが発生しました。";
										}

										$savedata['image'] = $recipe_post['fileName' . $num];
										if ($this->RecipeImage->save($savedata, true)) {
										}
									}else{
										if(!$this->Recipe->RecipeImage->saveAll($save_db_data)){
											echo "レシピ登録中にエラーが発生しました。";
										}
									}
									$num++;
								}
							}
							$this->Session->delete('token');
							$this->Session->setFlash('登録完了しました。');
							$this->redirect('../adminrecipes/');
						}
					}
				}
			}else{
				$this->render('complete/add_error');
			}
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->set('token', $token);

		$this->render('/Admins/adminrecipes/add');
	}

	function edit($param = null){

		$this->Recipe->contain(array(
			'RecipeImage',
			'RecipeMaterial' => array(
				'order' => 'sort ASC'
			),
			'RecipeOrder' => array(
				'order' => 'sort ASC'
			),
			'RecipeRelated' => array(
				'order' => 'sort ASC'
			),
		));

		set_time_limit(0);

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Recipe']['token']){

				if($this->data['Recipe']['mode'] === '修正する'){

					$this->set('data', $this->data);
					$this->Recipe->set($this->data);

					$this->Recipe->set_validate_default();

					if($this->Recipe->validates()){
						if(!$this->Recipe->save($this->data)){
							echo "物件登録中にエラーが発生しました。";
						}else{

							//レシピ材料保存
							foreach($this->data['RecipeMaterial'] as $k => $v){
								if(!empty($v['name'])){
									$v['recipe_id'] = $this->Recipe->id;
									if(!$this->Recipe->RecipeMaterial->saveAll($v)){
										echo "レシピ登録中にエラーが発生しました。";
									}
								}else{
									if($v['id']){
										if(!$this->Recipe->RecipeMaterial->delete($v['id'])){
											echo "レシピ登録中にエラーが発生しました。";
										}
									}
								}
							}
							//レシピ手順保存
							$num = 0;
							foreach($this->data['RecipeOrder'] as $k => $v){
								if(!empty($v['comment'])){
									if($v['delitem'] == 0){
										$v['recipe_id'] = $this->Recipe->id;
										if($v['delphoto'] == 0){
											if(!$this->Recipe->RecipeOrder->saveAll($v)){
												echo "レシピ登録中にエラーが発生しました。";
											}
										}else{
											$photodel = array('id' => $v['id'], 'uploadpath' => null);
											if(!$this->Recipe->RecipeOrder->save($photodel)){
												echo "レシピ登録中にエラーが発生しました。";
											}
										}
									}else{
										if($v['id']){
											$delphotopath = $this->RecipeOrder->find('first', array(// !bookmark
												'conditions' => array('id' => $v['id']),
												'fields'     => array('uploadpath'),
											));
											$thumbs = str_replace('.jpg', '_th.jpg', $delphotopath);
											if(!$this->Recipe->RecipeOrder->delete($v['id'])){
												echo "レシピ登録中にエラーが発生しました。";
											}
										}
									}
									//画像保存
									if(
										isset($this->request->data['Recipe']['orderPhoto' . $num]['name']) &&
										!empty($this->request->data['Recipe']['orderPhoto' . $num]['name'])
									){
										$upload_data = array();

										$upload_data['file']        = $this->request->data['Recipe']['orderPhoto' . $num]['tmp_name'];
										$upload_data['dir']         = 'order';
										$upload_data['relation_id'] = $this->Recipe->id;
										$upload_data['file_name']   = sprintf("%011d", $this->Recipe->RecipeOrder->id);
										$upload_data['thumb_size']  = $this->recipethumbsize;
										$upload_data['model']       = ClassRegistry::init('RecipeOrder');

										$this->Upload->upload_image($upload_data);

										$ext = pathinfo($this->request->data['Recipe']['orderPhoto' . $num]['name'], PATHINFO_EXTENSION);

										$save_db_data = array();
										$save_db_data['id']         = $v['id'];
										$save_db_data['recipe_id']  = $this->Recipe->id;
										$save_db_data['uploadpath'] = 'files/uploads/order/' . $this->Recipe->id . '/' . sprintf("%011d", $this->Recipe->RecipeOrder->id) . '.' . $ext;
										$save_db_data['comment']    = $v['comment'];

										if(!$this->Recipe->RecipeOrder->saveAll($save_db_data)){
											echo "レシピ登録中にエラーが発生しました。";
										}


										$savedata['image'] = $this->request->data['Recipe']['orderPhoto' . $num];
										if ($this->RecipeOrder->save($savedata, true)) {
										}
									}
									$num++;
								}
							}
							//関連商品保存
							$num = 0;
							foreach($this->data['RecipeRelated'] as $k => $v){
								if(!empty($v['name'])){
									$v['recipe_id'] = $this->Recipe->id;
									if($v['delitem'] == 0){
										if(!$this->Recipe->RecipeRelated->saveAll($v)){
											echo "レシピ登録中にエラーが発生しました。";
										}
									}else{
										if(!$this->Recipe->RecipeRelated->delete($v['id'])){
											echo "レシピ登録中にエラーが発生しました。";
										}
									}
								}
							}

							$recipe_post       = $this->request->data['Recipe'];
							$recipe_image_post = $this->request->data['RecipeImage'];

							$num = 0;
							if(isset($recipe_image_post)){
								foreach($recipe_image_post as $k => $v ){
									if(
										isset($recipe_post['fileName' . $num]['name']) &&
										!empty($recipe_post['fileName' . $num]['name'])
									){
										$upload_data = array();

										$upload_data['file']        = $recipe_post['fileName' . $num]['tmp_name'];
										$upload_data['dir']         = 'recipe';
										$upload_data['relation_id'] = $recipe_post['id'];
										$upload_data['file_name']   = ($num + 1);
										$upload_data['thumb_size']  = $this->recipethumbsize;
										$upload_data['model']       = ClassRegistry::init('RecipeImage');

										$this->Upload->upload_image($upload_data);

										$ext = pathinfo($recipe_post['fileName' . $num]['name'], PATHINFO_EXTENSION);

										$save_db_data = array();
										$save_db_data['id']         = $v['id'];
										$save_db_data['sort']       = $v['sort'];
										$save_db_data['recipe_id']  = $this->Recipe->id;
										$save_db_data['uploadpath'] = 'files/uploads/recipe/' . $this->Recipe->id . '/' . ($num + 1) . '.' . $ext;

										if(!$this->Recipe->RecipeImage->saveAll($save_db_data)){
											echo "レシピ登録中にエラーが発生しました。";
										}

										$savedata['image'] = $recipe_post['fileName' . $num];
										if ($this->RecipeImage->save($savedata, true)) {
										}
									}
									$num++;
								}
							}
							$this->Session->delete('token');
							$this->Session->setFlash('修正完了しました。');
							$this->redirect('../adminrecipes/');
						}
						return $this->render('/Admins/adminrecipes/complete/update_complete');
					}
				}else if($this->data['Recipe']['mode'] === '削除する'){

					$conditions = array(
						'Recipe.id' => $this->data['Recipe']['id']
					);

					//pr($this->data);

					if(!$this->Recipe->delete($this->data['Recipe']['id'])){
					}

					return $this->render('/Admins/adminrecipes/complete/delete_complete');
				}
			}else{
				return $this->render('/Admins/adminrecipes/complete/add_error');
			}
		}else{
			$this->Recipe->id = $param;
			$this->data = $this->Recipe->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->set('token', $token);

		$this->render('/Admins/adminrecipes/edit');
	}

	function delete($param = null){
		if(!empty($param)){
			$savedata['Recipe']['id']      = $param;
			$savedata['Recipe']['del_flg'] =  1;

			if(!$this->Recipe->save($savedata)){
				$this->Session->setFlash('削除に失敗しました。');
				$this->redirect('../adminrecipes/');
			}else{
				$this->Session->setFlash('削除しました。');
				$this->redirect('../adminrecipes/');
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecipes/');
		}
	}
	//Ajax　表示状態更新
	function active(){

		$this->layout     = false;
		$this->autoRender = false;

		//バリデーションを無効に
		$this->Recipe->set_validation_ajax();

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->request->is('ajax')){

			$json['status'] = true;

			$this->Recipe->id = $this->request->data('id');

			if(!$this->Recipe->saveField('active', $this->request->data('value'))){
				$json['status']  = false;
				$json['message'] = '<p>表示設定に失敗しました</p>';
			}else{
				$json['status'] = true;
				$json['message'] = '<p>更新成功!</p>';
			}
			echo json_encode($json);
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecipes/');
		}
	}
	function bidcategory() {

		$this->layout = null;

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		$cate = $this->Recipecat->recategory;

		if($this->request->is('ajax')){
			 //POST情報は$this->params['form']で取得
			$id = $this->params['data']['id'];

			$ayy = '<option value="">選択してください</option>';
			if($id == 1000){
				foreach($cate as $k => $v){
					if($k < 20000){
						$ayy .= '<option value="'.$k.'">'.$v.'</option>';
					}
				}
			}elseif($id == 2000){
				foreach($cate as $k => $v){
					if($k > 20000){
						$ayy .= '<option value="'.$k.'">'.$v.'</option>';
					}
				}
			}else{
				foreach($cate as $k => $v){
					$ayy .= '<option value="'.$k.'">'.$v.'</option>';
				}
			}


			$this->set('t', $ayy);

		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecipes/');
		}
		$this->render('/Admins/adminrecipes/bidcategory');
	}


}
