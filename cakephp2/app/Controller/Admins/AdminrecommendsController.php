<?php

App::uses('AppController', 'Controller');

class AdminrecommendsController extends AppController{

	public $name       = 'Adminrecommends';
	public $uses       = array('Recommend', 'RecommendImage');
	public $components = array(
		'Common',
		'Paginator',
		'Upload',
		'Auth'
	);
	public $helpers    = array('Thcalendar', 'Jaweek');

	public $autoRender = true;
	public $layout     = "Admin/siteframe";

	public $thumbsize = '200';//サムネイルサイズ

	public $pagenum = 20;

	function beforeFilter(){

		parent::beforeFilter();

        $this->Auth->authError = 'ログインしてください。';

		$this->loadModel('Wordpress');
		$this->loadModel('Active');

		$active = $this->Active->active;
		$this->set('active', $active);

		$wordpress = $this->Wordpress->url;
		$this->set('wordpress', $wordpress);

		$bodyId = 'recommend';
		$this->set('bodyId', $bodyId);

	}

	function index(){
		$this->Recommend->contain(array(
			'RecommendImage',
		));
		$data['list'] = $this->Recommend->find('all', array(
				'conditions' => array('del_flg' => 0),
				'fields'     => array(),
				'order'      => 'id DESC'
			)
		);
		$this->set('data', $data);

		$this->render('/Admins/adminrecommends/index');
	}

	/**
	 * [検索]
	 * @return [type] [description]
	 */
	function search(){

		$fields = array('id', 'title', 'description', 'recomflg', 'active');

		if($this->request->is('post') || $this->request->is('put')){

			$keyword = '';

			if($this->Session->check('recommendwords')) {
				$keyword = $this->Session->read('recommendwords');
			}

			if(isset($this->request->data['keyword']) && strlen($this->request->data['keyword'])){
				$keyword = $this->request->data['keyword'];
			}else{
				$keyword = '';
			}

			$this->Session->write('recommendwords', $keyword);

			$conditions = array('OR' => array(
				'title LIKE'       => '%' . $keyword . '%',
				'description LIKE' => '%' . $keyword . '%'),
				'del_flg'          => 0
			);

			$this->Paginator->settings = array(
				'conditions' => $conditions,
				'fields'     => $fields,
				'limit'      => $this->pagenum,
				'order'      => 'id DESC',
				'recursive'  => 2
			);

			$pagebool = $this->Paginator->paginate('Recommend');
			$this->set('data', $pagebool);
			$this->set('searchword', $keyword);

			if(empty($pagebool)){
				$this->Session->setFlash('データがありません。');
			}
		}

		$this->render('/Admins/adminrecommends/search');
	}

	function add(){

		$this->Recommend->contain(array(
			'PantryImage',
		));

		set_time_limit(0);

		if($this->request->is('post')){

			if((string)$this->Session->read('token') === (string)$this->request->data['Recommend']['token']){

				if($this->data['Recommend']['mode'] === '登録する'){

					$this->Recommend->set($this->data);
					$this->Recommend->set_validate_default();

					if($this->Recommend->validates()){
						if(!$this->Recommend->save($this->request->data)){
							echo "イチオシ商品登録中にエラーが発生しました。";
						}else{

							$num = 0;
							foreach($this->request->data['RecommendImage'] as $k => $v){

								if(!$this->Recommend->RecommendImage->saveAll($v)){
									echo "イチオシ商品登録中にエラーが発生しました。";
								}

								$save_db_data = array();
								$save_db_data['id']           = $this->Recommend->RecommendImage->id;
								$save_db_data['recommend_id'] = $this->Recommend->id;

								if($this->request->data['Recommend']['fileName' . $num]['error'] === 0){

									$upload_data = array();

									$upload_data['file']        = $this->request->data['Recommend']['fileName' . $num]['tmp_name'];
									$upload_data['dir']         = 'recommend';
									$upload_data['relation_id'] = $this->Recommend->id;
									$upload_data['file_name']   = sprintf("%011d", $this->Recommend->RecommendImage->id);
									$upload_data['thumb_size']  = $this->thumbsize;
									$upload_data['model']       = ClassRegistry::init('RecommendImage');

									$this->Upload->upload_image($upload_data);

									$save_db_data['uploadpath'] = $this->Upload->get_upload_path('recommend', $this->Recommend->id, sprintf("%011d", $this->Recommend->RecommendImage->id), $this->request->data['Recommend']['fileName' . $num]['name']);

									if(!$this->Recommend->RecommendImage->saveAll($save_db_data)){
										echo "イチオシ商品登録中にエラーが発生しました。";
									}

									$savedata['image'] = $this->request->data['Recommend']['fileName' . $num];
									if ($this->RecommendImage->save($savedata, true)) {
									}
								}else{
									if(!$this->Recommend->RecommendImage->saveAll($save_db_data)){
										echo "イチオシ商品登録中にエラーが発生しました。";
									}
								}
								$num++;
							}
							$this->Session->delete('token');
							$this->Session->setFlash('登録完了しました。');
							$this->redirect('../adminrecommends');
						}
					}
				}
			}else{
				return $this->render('complete/add_error');
			}
		}

		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->set('token', $token);

		$this->render('/Admins/adminrecommends/add');
	}
	function edit($param = ''){

		if(!strlen($param)){
			$this->redirect('../adminrecommends');
		}

		$this->Recommend->contain(array(
			'RecommendImage',
		));

		set_time_limit(0);

		if($this->request->is('post') || $this->request->is('put')){

			if((string)$this->Session->read('token') === (string)$this->request->data['Recommend']['token']){

				if($this->data['Recommend']['mode'] === '修正する'){

					$this->set('data', $this->data);

					$this->Recommend->set($this->request->data);
					$this->Recommend->set_validate_default();

					if($this->Recommend->validates()){
						if(!$this->Recommend->save($this->request->data)){
							echo "イチオシ商品登録中にエラーが発生しました。";
						}else{
							$num = 0;
							foreach($this->request->data['RecommendImage'] as $k => $v ){
								if(!$this->Recommend->RecommendImage->saveAll($v)){
									echo "イチオシ商品登録中にエラーが発生しました。";
								}

								$save_db_data = array();
								$save_db_data['id']           = $this->Recommend->RecommendImage->id;
								$save_db_data['recommend_id'] = $this->Recommend->id;

								if($this->request->data['Recommend']['fileName' . $num]['error'] === 0){

									$upload_data = array();

									$upload_data['file']        = $this->request->data['Recommend']['fileName' . $num]['tmp_name'];
									$upload_data['dir']         = 'recommend';
									$upload_data['relation_id'] = $this->Recommend->id;
									$upload_data['file_name']   = sprintf("%011d", $this->Recommend->RecommendImage->id);
									$upload_data['thumb_size']  = $this->thumbsize;
									$upload_data['model']       = ClassRegistry::init('RecommendImage');

									$this->Upload->upload_image($upload_data);

									$save_db_data['uploadpath'] = $this->Upload->get_upload_path('recommend', $this->Recommend->id, sprintf("%011d", $this->Recommend->RecommendImage->id), $this->request->data['Recommend']['fileName' . $num]['name']);

									if(!$this->Recommend->RecommendImage->saveAll($save_db_data)){
										echo "イチオシ商品登録中にエラーが発生しました。";
									}

									$savedata['image'] = $this->request->data['Recommend']['fileName' . $num];
									if ($this->RecommendImage->save($savedata, true)) {
									}
								}else{
									if(!$this->Recommend->RecommendImage->saveAll($save_db_data)){
										echo "イチオシ商品登録中にエラーが発生しました。";
									}
								}

								$num++;
							}
							$this->Session->delete('token');
							$this->Session->setFlash('登録完了しました。');
							$this->redirect('../adminrecommends');
						}
					}
				}
			}else{
				return $this->render('complete/add_error');
			}
		}else{
			$this->Recommend->id = $param;
			$this->data = $this->Recommend->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->set('token', $token);

		$this->render('/Admins/adminrecommends/edit');
	}

	function duplicates($param = ''){

		if(!strlen($param)){
			$this->redirect('../adminrecommends');
		}

		$this->Recommend->contain(array(
			'RecommendImage',
		));

		set_time_limit(0);

		if($this->request->is('post') || $this->request->is('put')){

			if((string)$this->Session->read('token') === (string)$this->data['Recommend']['token']){

				if($this->request->data['Recommend']['mode'] === '登録する'){

					$this->Recommend->set_validate_default();

					if($this->Recommend->validates()){
						if(!$this->Recommend->save($this->request->data)){
							echo "イチオシ商品登録中にエラーが発生しました。";
						}else{
							$num = 0;
							foreach($this->request->data['RecommendImage'] as $k => $v ){

								if(!$this->Recommend->RecommendImage->saveAll($v)){
									echo "イチオシ商品登録中にエラーが発生しました。";
								}

								$save_db_data = array();
								$save_db_data['id']           = $this->Recommend->RecommendImage->id;
								$save_db_data['recommend_id'] = $this->Recommend->id;

								if($this->request->data['Recommend']['fileName' . $num]['error'] === 0){

									$upload_data = array();

									$upload_data['file']        = $this->request->data['Recommend']['fileName' . $num]['tmp_name'];
									$upload_data['dir']         = 'recommend';
									$upload_data['relation_id'] = $this->Recommend->id;
									$upload_data['file_name']   = sprintf("%011d", $this->Recommend->RecommendImage->id);
									$upload_data['thumb_size']  = $this->thumbsize;
									$upload_data['model']       = ClassRegistry::init('RecommendImage');

									$this->Upload->upload_image($upload_data);

									$save_db_data['uploadpath'] = $this->Upload->get_upload_path('recommend', $this->Recommend->id, sprintf("%011d", $this->Recommend->RecommendImage->id), $this->request->data['Recommend']['fileName' . $num]['name']);

									if(!$this->Recommend->RecommendImage->saveAll($save_db_data)){
										echo "イチオシ商品登録中にエラーが発生しました。";
									}

									$savedata['image'] = $this->request->data['Recommend']['fileName' . $num];
									if ($this->RecommendImage->save($savedata, true)) {
									}
								}else{
									if(!$this->Recommend->RecommendImage->saveAll($save_db_data)){
										echo "イチオシ商品登録中にエラーが発生しました。";
									}
								}
								$num++;
							}
							$this->Session->delete('token');
							$this->Session->setFlash('登録完了しました。');
							$this->redirect('../adminrecommends');
						}
					}
				}
			}else{
				return $this->render('complete/add_error');
			}
		}else{
			$this->Recommend->id = $param;
			$this->data = $this->Recommend->read();
			$this->set('data', $this->request->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->set('token', $token);

		$this->render('/Admins/adminrecommends/duplicates');
	}


	function delete($param = null){
		if(!empty($param)){
			$savedata['Recommend']['id']      = $param;
			$savedata['Recommend']['del_flg'] =  1;

			if(!$this->Recommend->save($savedata)){
				$this->Session->setFlash('削除に失敗しました。');
				$this->redirect('../adminrecommends/');
			}else{
				$this->Session->setFlash('削除しました。');
				$this->redirect('../adminrecommends/');
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecommends/');
		}
	}

	// Ajax　表示状態更新
	// ここも利用していない。
	function active() {

		$this->layout = null;

		//バリデーションを無効に
		$this->Recommend->setValidation('ajax');

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->request->is('ajax')){
			// POST情報は$this->params['data']で取得
			$id          = $this->params['data']['id'];
			$activeparam = $this->params['data']['actives'];
			// DBに突っ込みます
			$this->Recommend->id = $id;
			$this->data['Recommend']['active'] = $activeparam;

			if(!$this->Recommend->saveField('active', $activeparam)){
				$message = '更新失敗！';
				$this->set('t', $message);
			}else{
				$message = '更新完了！';
				$this->set('t', $message);
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecommends/');
		}
	}
	function recom() {

		$this->layout     = false;
		$this->autoRender = false;

		//バリデーションを変更
		$this->Recommend->set_validation_ajax();

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->request->is('ajax')){

			$json['status'] = true;

			if(!$this->Recommend->query('UPDATE recommends SET recomflg = 0')){
				$json['status']  = false;
				$json['message'] = 'おすすめ商品の設定に失敗しました';
			}

			$this->Recommend->id = $this->request->data('id');

			if(!$this->Recommend->saveField('recomflg', 1)){
				$json['status']  = false;
				$json['message'] = 'おすすめ商品の設定に失敗しました';
			}else{
				$json['status'] = true;
				$json['message'] = 'おすすめ商品に設定しました';
			}
			echo json_encode($json);
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecommends/');
		}
	}
}
