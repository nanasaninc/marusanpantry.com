<?php

App::uses('AppController', 'Controller');

class AdminlessonsController extends AppController{

	public $name = 'Adminlessons';
	public $uses = array('Lesson');
	public $components = array(
		'Common',
		'Upload',
		'Auth',
		'Paginator',
		'Adminco'
	);
	public $helpers = array('Thcalendar', 'Jaweek', 'Common');

	public $autoRender = true;
	public $layout     = "Admin/siteframe";


	public $pagenum     = 50;

	public $thumbsize = '200';//サムネイルサイズ

	function beforeFilter(){

		parent::beforeFilter();

		$this->Auth->authError = 'ログインしてください。';

		$this->Adminco->lessons();

	}

	function index($param = ''){
		$today = date('Y-m-d');

		$this->Lesson->contain(array(
			'LessonImage',
			'LessonDate',
		));

		if(empty($param)){
			$conditions = array('del_flg' => 0, "Lesson.limitday > '{$today}'");
		}else{
			$conditions = array('Lesson.category' => $param, 'del_flg' => 0, "Lesson.limitday > '{$today}'");
		}

		$this->Lesson->hasMany['LessonImage']['fields'] = array('uploadpath');
		$this->Lesson->hasMany['LessonDate']['fields'] = array('date');
		$this->Lesson->hasMany['LessonDate']['order'] = array('date ASC');

		$datas = $this->Lesson->find('all', array(
				'conditions' => $conditions,
				'fields'     => array('Lesson.id', 'Lesson.category', 'Lesson.title', 'Lesson.description', 'Lesson.recomflg', 'Lesson.active'),
				'order'      => 'id DESC'
			)
		);
		$this->set('data', $datas);

		$this->set('p', $param);

		$this->render('/Admins/adminlessons/index');

	}


	function past($param = ''){

		$today = date('Y-m-d');

		$this->Lesson->contain(array(
			'LessonImage',
			'LessonDate',
		));

		$this->Lesson->hasMany['LessonImage']['fields'] = array('uploadpath');
		$this->Lesson->hasMany['LessonDate']['fields'] = array('date');

		$conditions = array('del_flg' => 0, "Lesson.limitday <= '{$today}'");

		$this->Paginator->settings = array(
			'conditions' => $conditions,
			'fields'     => array('Lesson.id', 'Lesson.category', 'Lesson.title', 'Lesson.description', 'Lesson.recomflg', 'Lesson.active'),
			'limit'      => $this->pagenum,
			'order'      => 'Lesson.id DESC',
		);

		$this->set('data', $this->Paginator->paginate('Lesson'));
		$this->set('p', $param);

		$this->render('/Admins/adminlessons/past');
	}


	function add(){

		$this->Lesson->contain(array(
			'LessonImage',
			'LessonDate',
		));

		set_time_limit(0);

		if($this->request->is('post') || $this->request->is('put')){

			if((string)$this->Session->read('token') === (string)$this->request->data['Lesson']['token']){

				if($this->request->data['Lesson']['mode'] === '登録する'){

					$this->Lesson->set($this->request->data);
					$this->Lesson->set_validate_default();

					if($this->Lesson->validates()){
						if(!$this->Lesson->save($this->data)){
							echo "レッスン登録中にエラーが発生しました。";
						}else{
							//レッスン日程保存
							foreach($this->request->data['LessonDate'] as $k => $v){
								if(!empty($v['datetxt'])){
									$v['lesson_id'] = $this->Lesson->id;
									$v['capacity']  = $v['limit'];
									if(!$this->Lesson->LessonDate->saveAll($v)){//エラー処理しよう
										echo "レッスン登録中にエラーが発生しました。";
									}
								}
							}

							$lesson_post       = $this->request->data['Lesson'];
							$lesson_image_post = $this->request->data['LessonImage'];

							$num = 0;

							if(isset($lesson_image_post)){
								foreach($lesson_image_post as $k => $v ){

									if(!$this->Lesson->LessonImage->saveAll($v)){
										echo "教室登録中にエラーが発生しました。";
									}

									$save_db_data = array();
									$save_db_data['id']         = $this->Lesson->LessonImage->id;
									$save_db_data['lesson_id']  = $this->Lesson->id;
									$save_db_data['sort']       = $v['sort'];

									if(
										isset($lesson_post['fileName' . $num]['name']) &&
										!empty($lesson_post['fileName' . $num]['name'])
									){

										$upload_data = array();

										$upload_data['file']        = $lesson_post['fileName' . $num]['tmp_name'];
										$upload_data['dir']         = 'lesson';
										$upload_data['relation_id'] = $this->Lesson->id;
										$upload_data['file_name']   = ($num + 1);
										$upload_data['thumb_size']  = $this->thumbsize;
										$upload_data['model']       = ClassRegistry::init('LessonImage');

										$this->Upload->upload_image($upload_data);

										$ext = pathinfo($lesson_post['fileName' . $num]['name'], PATHINFO_EXTENSION);

										$save_db_data['uploadpath'] = 'files/uploads/lesson/' . $this->Lesson->id . '/' . ($num + 1) . '.' . $ext;

										if(!$this->Lesson->LessonImage->saveAll($save_db_data)){
											echo "レシピ登録中にエラーが発生しました。";
										}

										$savedata['image'] = $lesson_post['fileName' . $num];
										if ($this->LessonImage->save($savedata, true)) {
										}
									}else{
										if(!$this->Lesson->LessonImage->saveAll($save_db_data)){
											echo "レシピ登録中にエラーが発生しました。";
										}
									}
									$num++;
								}
							}

							$this->Session->delete('token');
							$this->Session->setFlash('登録完了しました。');
							$this->redirect('../adminlessons');
						}
					}
				}
			}else{
				$this->render('complete/add_error');
			}
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->set('token', $token);

		$this->render('/Admins/adminlessons/add');
	}


	function edit($param = ''){

		if(!strlen($param)){
			$this->redirect('../adminlessons');
		}

		$this->Lesson->contain(array(
			'LessonImage',
			'LessonDate',
		));

		$this->Lesson->hasMany['LessonDate']['order'] = array('date ASC');

		set_time_limit(0);

		if($this->request->is('post') || $this->request->is('put')){

			if((string)$this->Session->read('token') === (string)$this->data['Lesson']['token']){

				if($this->data['Lesson']['mode'] === '修正する'){

					$this->set('data', $this->data);
					$this->Lesson->set($this->data);
					$this->Lesson->set_validate_default();

					if($this->Lesson->validates()){
						if(!$this->Lesson->save($this->data)){
							echo "物件登録中にエラーが発生しました。";
						}else{
							//レッスン日程保存
							foreach($this->data['LessonDate'] as $k => $v){
								if(!empty($v['date'])){
									$v['lesson_id'] = $this->Lesson->id;
									if(!$this->Lesson->LessonDate->saveAll($v)){
										echo "レッスン登録中にエラーが発生しました。";
									}
								}else{
									if($v['id']){
										if(!$this->Lesson->LessonDate->delete($v['id'])){//削除
											echo "レッスン登録中にエラーが発生しました。";
										}
									}
								}
							}

							$lesson_post       = $this->request->data['Lesson'];
							$lesson_image_post = $this->request->data['LessonImage'];

							$num = 0;

							if(isset($lesson_image_post)){
								foreach($lesson_image_post as $k => $v ){
									// $v['id'] =>
									if(!$this->Lesson->LessonImage->saveAll($v)){
										echo "教室登録中にエラーが発生しました。";
									}

									if(!empty($v['delphoto'])){
										if($v['delphoto'] == 1){
											$photodel = array('id' => $v['id'], 'uploadpath' => null);
											if(!$this->Lesson->LessonImage->save($photodel)){
												echo "レッスン登録中にエラーが発生しました。";
											}
										}
									}

									if(
										isset($lesson_post['fileName' . $num]['name']) &&
										!empty($lesson_post['fileName' . $num]['name'])
									){
										$upload_data = array();

										$upload_data['file']        = $lesson_post['fileName' . $num]['tmp_name'];
										$upload_data['dir']         = 'lesson';
										$upload_data['relation_id'] = $this->Lesson->id;
										$upload_data['file_name']   = ($num + 1);
										$upload_data['thumb_size']  = $this->thumbsize;
										$upload_data['model']       = ClassRegistry::init('LessonImage');

										$this->Upload->upload_image($upload_data);

										$ext = pathinfo($lesson_post['fileName' . $num]['name'], PATHINFO_EXTENSION);

										$save_db_data = array();
										$save_db_data['id']         = $this->Lesson->LessonImage->id;
										$save_db_data['lesson_id']  = $this->Lesson->id;
										$save_db_data['uploadpath'] = 'files/uploads/lesson/' . $this->Lesson->id . '/' . ($num + 1) . '.' . $ext;
										$save_db_data['sort']       = $v['sort'];

										if(!$this->Lesson->LessonImage->saveAll($save_db_data)){
											echo "レシピ登録中にエラーが発生しました。";
										}

										$savedata['image'] = $lesson_post['fileName' . $num];
										if ($this->LessonImage->save($savedata, true)) {
										}
									}
									$num++;
								}
							}
							$this->Session->delete('token');
							$this->Session->setFlash('修正完了しました。');
							$this->redirect('../adminlessons');
						}
					}
				}
			}else{
				return $this->render('/Admins/adminlessons/complete/add_error');
			}
		}else{
			$this->Lesson->id = $param;
			$this->data = $this->Lesson->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->set('token', $token);

		$this->render('/Admins/adminlessons/edit');
	}

	function duplicates($param = ''){

		if(!strlen($param)){
			$this->redirect('../adminlessons');
		}

		$this->Lesson->contain(array(
			'LessonImage',
			'LessonDate',
		));

		$this->Lesson->hasMany['LessonDate']['order'] = array('date ASC');

		if($this->request->is('post') || $this->request->is('put')){

			if((string)$this->Session->read('token') === (string)$this->request->data['Lesson']['token']){

				if($this->request->data['Lesson']['mode'] === '登録する'){

					$this->Lesson->set_validate_default();

					if($this->Lesson->validates()){
						if(!$this->Lesson->save($this->data)){
							echo "レッスン登録中にエラーが発生しました。";
						}else{
							//レッスン日程保存
							foreach($this->request->data['LessonDate'] as $k => $v){
								if(!empty($v['datetxt'])){
									$v['lesson_id'] = $this->Lesson->id;
									$v['capacity'] = $v['limit'];
									if(!$this->Lesson->LessonDate->saveAll($v)){
										echo "レッスン登録中にエラーが発生しました。";
									}
								}
							}

							$lesson_post       = $this->request->data['Lesson'];
							$lesson_image_post = $this->request->data['LessonImage'];

							$num = 0;

							if(isset($lesson_image_post)){
								foreach($this->request->data['LessonImage'] as $k => $v ){

									if(!$this->Lesson->LessonImage->saveAll($v)){
										echo "教室登録中にエラーが発生しました。";
									}

									$save_db_data['id']         = $this->Lesson->LessonImage->id;
									$save_db_data['lesson_id']  = $this->Lesson->id;
									$save_db_data['sort']       = $v['sort'];

									if(
										isset($lesson_post['fileName' . $num]['name']) &&
										!empty($lesson_post['fileName' . $num]['name'])
									){
										$upload_data = array();

										$upload_data['file']        = $lesson_post['fileName' . $num]['tmp_name'];
										$upload_data['dir']         = 'lesson';
										$upload_data['relation_id'] = $this->Lesson->id;
										$upload_data['file_name']   = ($num + 1);
										$upload_data['thumb_size']  = $this->thumbsize;
										$upload_data['model']       = ClassRegistry::init('LessonImage');

										$this->Upload->upload_image($upload_data);

										$ext = pathinfo($lesson_post['fileName' . $num]['name'], PATHINFO_EXTENSION);

										$save_db_data['uploadpath'] = 'files/uploads/lesson/' . $this->Lesson->id . '/' . ($num + 1) . '.' . $ext;

										if(!$this->Lesson->LessonImage->saveAll($save_db_data)){
											echo "レシピ登録中にエラーが発生しました。";
										}

										$savedata['image'] = $lesson_post['fileName' . $num];
										if ($this->LessonImage->save($savedata, true)) {
										}
									}else{
										if(!$this->Lesson->LessonImage->saveAll($save_db_data)){
											echo "レシピ登録中にエラーが発生しました。";
										}
									}
									$num++;
								}
							}
						}
					}
				}
				$this->Session->delete('token');
				$this->Session->setFlash('登録完了しました。');
				$this->redirect('../adminlessons');
			}else{
				return $this->render('/Admins/adminlessons/complete/add_error');
			}
		}else{
			$this->Lesson->id = $param;
			$this->data = $this->Lesson->read();
			$this->set('data', $this->request->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->set('token', $token);

		$this->render('/Admins/adminlessons/duplicates');
	}
	function dateedit($param = ''){

		if(!strlen($param)){
			$this->redirect('../adminlessons');
		}

		$this->Lesson->contain(array(
			'LessonImage',
			'LessonDate',
		));

		$this->Lesson->hasMany['LessonDate']['order'] = array('date ASC');

		set_time_limit(0);

		if($this->request->is('post') || $this->request->is('put')){

			if((string)$this->Session->read('token') === (string)$this->request->data['Lesson']['token']){

				if($this->data['Lesson']['mode'] === '修正する'){

					$this->set('data', $this->request->data);

					if($this->Lesson->validates()){
						if(!$this->Lesson->save($this->request->data)){
							echo "物件登録中にエラーが発生しました。";
						}else{
							//レッスン日程保存
							foreach($this->request->data['LessonDate'] as $k => $v){
								if(!empty($v['limit'])){
									$v['lesson_id'] = $this->Lesson->id;
									if(!$this->Lesson->LessonDate->saveAll($v)){
										echo "レッスン登録中にエラーが発生しました。";
									}
								}
							}

							return $this->render('/Admins/adminlessons/complete/update_complete');
						}

					}
				}
				$this->Session->delete('token');
				$this->Session->setFlash('修正完了しました。');
				$this->redirect('../adminlessons');
			}else{
				return $this->render('/Admins/adminlessons/complete/add_error');
			}
		}else{
			$this->Lesson->id = $param;
			$this->data = $this->Lesson->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->set('token', $token);

		$this->render('/Admins/adminlessons/dateedit');
	}

	function delete($param = null){
		if(!empty($param)){

			if(!empty($this->request->data['mode'])){
				$this->request->data['mode'] = 'はい';
			}else{
				$this->request->data['mode'] = null;
			}

			$this->request->data['Lesson']['id'] = $param;
			$this->request->data['Lesson']['del_flg'] =  1;


			if($this->request->data['mode'] == 'はい'){
				if(!$this->Lesson->save($this->request->data)){
					$this->Session->setFlash('削除に失敗しました。');
					$this->redirect('../adminlessons/');
				}else{
					$this->Session->setFlash('削除しました。');
					$this->redirect('../adminlessons/');
				}
			}else{
				$data['list'] = $this->Lesson->find('first', array(
						'conditions' => array('id' => $param),
					)
				);
				$this->set('data', $data);
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminlessons/');
		}

		$this->render('/Admins/adminlessons/delete');
	}

	//Ajax　表示状態更新
	function active() {

		$this->layout     = false;
		$this->autoRender = false;

		//バリデーションを無効に
		$this->Lesson->set_validation_ajax();

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->request->is('ajax')){

			$json['status'] = true;

			$this->Lesson->id = $this->request->data('id');

			if(!$this->Lesson->saveField('active', $this->request->data('value'))){
				$json['status']  = false;
				$json['message'] = '表示設定に失敗しました';
			}else{
				$json['status'] = true;
				$json['message'] = '<p>更新成功!</p>';
			}
			echo json_encode($json);
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminlessons/');
		}
	}
	function recom() {

		$this->layout     = false;
		$this->autoRender = false;

		//バリデーションを無効に
		$this->Lesson->set_validation_ajax();

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->request->is('ajax')){

			$json['status'] = true;

			if(!$this->Lesson->query('UPDATE lessons SET recomflg = 0')){
				$json['status']  = false;
				$json['message'] = '<p class="osusumecomp">おすすめ教室の設定に失敗しました</p>';
			}

			$this->Lesson->id = $this->request->data('id');

			if(!$this->Lesson->saveField('recomflg', 1)){
				$json['status']  = false;
				$json['message'] = '<p class="osusumecomp">おすすめ教室の設定に失敗しました</p>';
			}else{
				$json['status'] = true;
				$json['message'] = '<p class="osusumecomp">おすすめ教室に設定しました</p>';
			}
			echo json_encode($json);
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminlessons/');
		}
	}

}
