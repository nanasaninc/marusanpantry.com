<?php

App::uses('AppController', 'Controller');

class AdminsController extends AppController{

	public $name = 'Admins';

	public $uses = array('User');

	public $autoRender = true;
	public $layout     = "Admin/siteframe";

	public $components = array(
		'Auth',
	);

	public $helpers = array(

	);


	public function beforeFilter(){

		$this->Auth->authError = 'ログインしてください。';

		$this->loadModel('Wordpress');

		$bodyId = 'home';
		$this->set('bodyId', $bodyId);

		$wordpress = $this->Wordpress->url;
		$this->set('wordpress', $wordpress);
	}

	function index(){
		$this->render('/Admins/index');
	}
}
