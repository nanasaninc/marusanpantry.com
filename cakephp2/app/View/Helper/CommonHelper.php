<?php

// App::uses('Helper', 'View');

class CommonHelper extends AppHelper {

	public $helpers = array('Html');

	public function limitdisplay($teiin, $weblimit){
		if($weblimit > 0 && $teiin > 0){
			$num = $teiin - ($teiin - $weblimit);
			$pers = $num / $teiin;
			//$sanbun = floor($teiin / 3) / $teiin;

			if($pers >= 0.5){
				$kigou = '◯';
			}elseif($pers < 0.5 && $pers >= 0.4){
				$kigou = '△';
			}elseif($pers < 0.4 && $pers != 0){
				$kigou = '残数' . $weblimit;
			}elseif($pers == 0){
				$kigou = '×';
			}else{
				$kigou = '×';
			}
		}else{
			$kigou = '×';
		}
		return $kigou;
	}
	public function _fh($str){
		$str = nl2br(h($str));
		return $str;
	}


	public function get_thumbs_path($uploadpath = ''){
		return $this->Html->url('/') . preg_replace("/(^.+?)(\..+?)$/", "$1_th$2", $uploadpath) . '?' . mt_rand();
	}


	/**
	 * [トップページレッスンタイトルの文字数削り]
	 * @param  string $title [description]
	 * @return [type]        [description]
	 */
	public function get_top_lesson_title($title = '', $size = 40){
		return mb_strimwidth(nl2br(h($title)), 0, $size, "...");
	}
}