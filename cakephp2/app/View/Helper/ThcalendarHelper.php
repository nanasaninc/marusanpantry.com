<?php
/**
 * Short description for file.
 *
 * Long description for file
 *
 * PHP versions 4 and 5
 *
 * Thcalendar Helper :  On CakePHP
 * Copyright 2009-2009 (http://blog.widget-info.net)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @filesource
 * @copyright     Copyright 2009-2009, blog.widget-info.net
 * @since         ThselectHelper on CakePHP v 1.2.0
 * @version       $Revision: 1.0.0 $
 * @modifiedby    $LastChangedBy: blog.widget-info.net $
 * @lastmodified  $Date: 2009-12-15 $
 * @license       MIT License
 */
class ThcalendarHelper extends AppHelper {
	/** 開始月設定
	 * 開始の月を指定します。
	 * 当月からの開始の場合は「0」を指定し、前月からの開始の場合は「-1」を指定します。
	 */
	public $prevMonth = 0;
	/** 終了月設定
	 * 当月からみて、表示する終了の月を指定します。
	 * 当月からの開始の場合は「0」を指定し、前月からの開始の場合は「-1」を指定します。
	 */
	public $nextMonth = 0;
	/** 月移動設定
	 * 「next()」「prev()」の移動の間隔を月単位で指定します。
	 */
	public $moveMonth = 1;
	/** カレンダー毎のヘッダー設定
	 * カレンダーごとに表示するヘッダー情報を設定します。
	 * ヘッダー情報を利用しない場合は「$this->headers」を「false」を指定します。
	 */
	public $headers = true;
	public $headerFormat = array(
		'ja'=>'%s年%s月'
	);

	public $weekAlpha = array(
		"01" => "JANUARY",
		"02" => "FEBRUARY",
		"03" => "MARCH",
		"04" => "APRIL",
		"05" => "MAY",
		"06" => "JUNE",
		"07" => "JULY",
		"08" => "AUGUST",
		"09" => "SEPTEMBER",
		"10" => "OCTOBER",
		"11" => "NOVEMBER",
		"12" => "DECEMBER",
	);

	/** 追加テキスト設定
	 * 表示日付ごとにテキストを追加する機能です。
	 * 配列のキーに対象の日付(YYY-mm-dd)、値に表示するテキストを格納することで
	 * 自動追加します。
	 * 例)
	 * array('2009-01-01'=>"休日");
	 */
	public $dayText = array();
	/** 日付リンク設定
	 * 表示日付ごとにリンク機能を追加する機能です。
	 * 配列のキーに対象の日付(YYY-mm-dd)、値にはさらに配列を格納します。
	 * 値の配列は次のように構成されています。
	 *
	 * array(
	 * 		'url'=>array(),//CakePHPのHTMLヘルパー「$html->link()」URL指定と同一です。
	 * 		'option'=>array()//CakePHPのHTMLヘルパー「$html->link()」オプション指定と同一です。
	 * )
	 */
	public $dayLink = array();
	/** 祝日判定機能設定
	 * 祝日判定機能を「true」「false」で指定します。
	 * 対象となる祝日の場合、対象日付にテキストが追加されます。
	 */
	public $holidayCheck = true;



	/*--------------------------------------------------------------------------------
	 * Main methods
	 --------------------------------------------------------------------------------*/
	/** calender()
	 * make calender
	 */
	public function calender(){
		$getCalenderList = $this->_makeList();
		$data = NULL;
		foreach($getCalenderList as $calenders){
			// !bookmark
			$data .= '<div class="calendarBox">';
			$data .= '<p class="month">' . sprintf('%d', $calenders['month']) . ' <span>' . $this->weekAlpha[$calenders['month']] . ' ' . $calenders['year'] . '</span></p>'.PHP_EOL;
			//$data .= '<p class="refine"><img class="specials" src="../img/common/icon-special.png" width="53" height="16" /><img class="breads" src="../img/common/icon-bread.png" width="53" height="16" /><img class="sweetss" src="../img/common/icon-sweets.png" alt="" width="53" height="16" /><img class="taikens" src="../img/common/icon-taiken.png" alt="" width="53" height="16" /></p>'.PHP_EOL;

			$data .= '<div class="thCalender">' . PHP_EOL;
			if($this->headers){
				$data .= "<div class='thCalenderHeader'>" . PHP_EOL;
				$data .= sprintf($this->headerFormat[$this->lang], $calenders['year'], $calenders['month'] + 0);
				$data .= "</div>" . PHP_EOL;
			}
			$data .= '<table class="thCalenderTabel">' . PHP_EOL;
			foreach($calenders['list'] as $week){
				$data .= '<tr>' . PHP_EOL;
				foreach($week as $days){
					if(!empty($days)){
						$data .= '<td class="thCalenderTd thCalender' . $days['weekname'][0] . '">' . PHP_EOL;
						$data .= '<span class="thCalenderDay">' . $days['day'] . "</span>" . PHP_EOL;
						if(!empty($days['text'])){
							$data .= nl2br($days['text']);
						}
						$data .= '</td>' . PHP_EOL;
					}else{
						$data .= '<td class="thCalenderTd">&nbsp;</td>' . PHP_EOL;
					}
				}
				$data .= '</tr>' . PHP_EOL;
			}
			$data .= '</table></div>' . PHP_EOL;
			$data .= '</div>';
		}
		return $this->output($data);
	}
	/** next()
	 * make next link
	 */
	public function next($neme = null, $htmlOption = array()){
		$date     = $this->_getTargetParamDate();
		$dateList = $this->_splitDate($date);
		$name     = (!empty($neme)) ? $neme: __('next calender', true);
		$named    = $this->params['named'];
		$named[$this->parmKeyName] = date("Y-m-d", mktime(0, 0, 0, $dateList[1]+$this->moveMonth, $dateList[2], $dateList[0]));
		$url = array(
			'controller' => $this->params['controller'],
			'action'     => $this->params['action'],
		);
		$url = am($url,$named);
		return $this->Html->link($name, $url, $htmlOption);
	}
	/** prev()
	 * make prev link
	 */
	public function prev($neme = null, $htmlOption = array()){
		$date     = $this->_getTargetParamDate();
		$dateList = $this->_splitDate($date);
		$name     = (!empty($neme)) ? $neme: __('prev calender',true);
		$named    = $this->params['named'];
		$named[$this->parmKeyName] = date("Y-m-d", mktime(0, 0, 0, $dateList[1]-$this->moveMonth, $dateList[2], $dateList[0]));
		$url = array(
			'controller' => $this->params['controller'],
			'action'     => $this->params['action'],
		);
		$url = am($url,$named);
		return $this->Html->link($name, $url, $htmlOption);
	}

	/*--------------------------------------------------------------------------------
	 * Commons values
	 --------------------------------------------------------------------------------*/
	//Helpers
	public $helpers = array('Html');
	//Language
	public $lang = 'ja';
	//CakePHP params name
	public $parmKeyName = 'cdate';
	//Use params value
	public $useParams = true;
	/*--------------------------------------------------------------------------------
	 * Commons function
	 --------------------------------------------------------------------------------*/
	/** _holiday()
	 * holiday check
	 * @week PHP Week number
	 */
	public function _holiday($y, $m, $d, $week){
		$holidays = false;
		$m = $m + 0;//return int
		$d = $d + 0;//return int
		$date = date("Y-m-d", mktime(0, 0, 0, $m,$d,$y));
		//is null before 1948
		if($y <= 1948 && $m <= 7 && $d <= 20){return $holidays;}
		//Check holiday
		if($m == 1){
			if($d == 1){return "元旦";}
			if($y >= 2000){
				$check = (floor(($d - 1) / 7) + 1) . ($week + 1);
				if($check == 22){return "成人の日";}
			}elseif($y >= 1949){
				if($d == 15){return "成人の日";}
			}
		}elseif($m == 2){
			if($y >= 1967 && $d == 11){
				return "建国記念の日";
			}elseif($y == 1989 && $d == 24){
				return "昭和天皇大喪の礼";
			}
		}elseif($m == 3){
			if($d == $this->_springHolidays($y)){return "春分の日";}
		}elseif($m == 4){
			if($d == 29){
				if($y >= 2007){
					return "昭和の日";
				}elseif($y >= 1989){
					return "みどりの日";
				}else{
					return "天皇誕生日";
				}
			}elseif($y == 1959 && $d == 10){
				return "明仁親王の結婚の儀";
			}
		}elseif($m == 5){
			if($d == 3){
				return "憲法記念日";
			}elseif($d == 4){
				if($y >= 2007){
					return "みどりの日";
				}elseif($y >= 1986 && $week > 0){
					return "国民の休日";
				}
			}elseif($d == 5){
				return "こどもの日";
			}elseif($d == 6){
				if($y >= 2007){
					if(($week == 2)|| ($week == 3)){return "振替休日";}
				}
			}
		}elseif($m == 6){
			if($y == 1993 && $d == 9){return "徳仁親王の結婚の儀";}
		}elseif($m == 7){
			if($y >= 2003){
				$check = (floor(($d - 1) / 7) + 1) . ($week + 1);
				if($check == 32){return "海の日";}
			}elseif($y >= 1996){
				if($d == 20){return "海の日";}
			}
		}elseif($m == 8){

		}elseif($m == 9){
			$autumns = $this->_autumnHolidays($y);
			if($d == $autumns){
				return "秋分の日";
			}else{
				if($y >= 2003){
					$check = (floor(($d - 1) / 7) + 1) . ($week + 1);
					if($check == "32"){
						return "敬老の日";
					}elseif($week == 2){
						if($d == ($autumns - 1)){return "国民の休日";}
					}
				}elseif($y >= 1966){
					if($d == 15){return "敬老の日";}
				}
			}
		}elseif($m == 10){
			if($y >= 2000){
				$check = (floor(($d - 1) / 7) + 1) . ($week + 1);
				if($check == "22"){return "体育の日";}
			}elseif($y >= 1966){
				if($d == 10){return "体育の日";}
			}
		}elseif($m == 11){
			if($d == 3){
				return "文化の日";
			}elseif($d == 23){
				return "勤労感謝の日";
			}elseif($y == 1990 && $d == 12){
				return "即位礼正殿の儀";
			}
		}elseif($m == 12){
			if($d == 23 && $y >= 1989){return "天皇誕生日";}
		}
		return $holidays;
	}
	/** _makeList()
	 * make calender data list
	 */
	public function _makeList(){
		$list=array();
		//Get date
		$date     = $this->_getTargetParamDate();
		$dateList = $this->_splitDate($date);
		//Make week setting
		$defaultWeekData = array(0 => array(), array(), array(), array(), array(), array(), array());
		//Make Calender
		for($m = $this->prevMonth;$m <= $this->nextMonth;++$m){
			//Set target date
			$targetDate = date("Y-m-d", mktime(0, 0, 0, $dateList[1] + $m, 1, $dateList[0]));
			$tdateList  = $this->_splitDate($targetDate);
			$result     = array();
			$result['year']  = $tdateList[0];
			$result['month'] = $tdateList[1];
			$eDay      = date("t", mktime(0, 0, 0, $tdateList[1], 1, $tdateList[0]));
			$weekCount = 0;
			$result['list'][$weekCount] = $defaultWeekData;
			//Make days
			for($d = 1;$d <= $eDay;++$d){
				$days          = array();
				$week          = date("w", mktime(0, 0, 0, $tdateList[1], $d, $tdateList[0]));
				$days['year']  = $tdateList[0];
				$days['month'] = $tdateList[1];
				$days['day']   = $d;
				$days['week']  = $week;
				$days['weekname'][] = date("l", mktime(0, 0, 0, $tdateList[1], $d,$tdateList[0]));
				$days['weekname'][] = date("D", mktime(0, 0, 0, $tdateList[1], $d,$tdateList[0]));
				$days['date']       = date("Y-m-d", mktime(0, 0, 0, $tdateList[1], $d, $tdateList[0]));
				//Add holidays
				$holidays = NULL;
				if($this->holidayCheck){
					$holidays = $this->_holiday($tdateList[0], $tdateList[1], $d, $week);
					if(!empty($holidays)){
						$this->_setDayText($days['date'], $holidays);
					}
					$days['holiday'] = $holidays;
					if(empty($holidays) && $week == 1){
						$bDate     = date("Y-m-d", mktime(0, 0, 0, $tdateList[1], $d - 1, $tdateList[0]));
						$bdateList = $this->_splitDate($bDate);
						$bweek     = date("w", mktime(0, 0, 0, $bdateList[1], $bdateList[2], $bdateList[0]));
						$bholidays = $this->_holiday($bdateList[0], $bdateList[1], $bdateList[2], $bweek);
						if(!empty($bholidays)){
							$this->_setDayText($days['date'], '振替休日');
						}
					}

				}
				//Add text
				$days['text'] = '';
				if(isset($this->dayText[$days['date']]) && !empty($this->dayText[$days['date']])){
					$days['text'] = $this->dayText[$days['date']];
				}
				//Add day link
				if(isset($this->dayLink[$days['date']]) && !empty($this->dayLink[$days['date']])){
					$link = $this->dayLink[$days['date']];
					if(is_array($link)){
						$options = array();
						if(isset($link['url'])){
							if(isset($link['option'])){
								$options = $link['option'];
							}
							$days['day'] = $this->Html->link($days['day'], $link['url'], $options);
						}
					}else{
						$days['day'] = $this->Html->link($days['day'], $link);
					}
				}
				//Add dyas
				$result['list'][$weekCount][$week] = $days;
				//Check week
				if($week == 6){
					++$weekCount;
					$result['list'][$weekCount] = $defaultWeekData;
				}
			}
			//Add list
			$list[] = $result;
		}
		return $list;
	}
	/** _setDayText()
	 * add date set detail text.
	 */
	public function _setDayText($date = null, $text = null){
		if(!empty($date) && !empty($text)){
			if(isset($this->dayText[$date])){
				$this->dayText[$date] = $text . $this->dayText[$date];
			}else{
				$this->dayText[$date] = $text;
			}
		}
	}
	/** _getTargetParamDate()
	 * Get now date.
	 */
	public function _getTargetParamDate(){
		return (isset($this->params['named'][$this->parmKeyName]) && !empty($this->params['named'][$this->parmKeyName]) && $this->useParams)?$this->params['named'][$this->parmKeyName]:date("Y-m-d");
	}
	/** _splitDate()
	 * split date.
	 */
	public function _splitDate($date = null){
		if(!empty($date) && strpos($date,"-")){
			$list = explode("-", $date);
			if(count($list) == 2){
				$list[] = "01";
			}
			return $list;
		}else{
			return date("Y-m-d");
		}
	}
	/** _springHolidays()
	 * Get Spring Holidays.
	 */
	public function _springHolidays($y){
		if($y <= 1947){
			return 99;
		}else{
			$left  = 0.242194 * ($y - 1980);
			$right = floor(($y - 1980) / 4);
			if($y <= 1979){
				return floor(20.8357 + $left - $right);
			}elseif($y <= 2099){
				return floor(20.8431 + $left - $right);
			}elseif($y <= 2150){
				return floor(21.851 + $left - $right);
			}else{
				return 99;
			}
		}
	}
	/** _autumnHolidays()
	 * Get Autumn Holidays.
	 */
	public function _autumnHolidays($y){
		if($y <= 1947){
			return 99;
		}else{
			$left  = 0.242194 * ($y - 1980);
			$right = floor(($y - 1980) / 4);
			if($y <= 1979){
				return floor(23.2588 + $left - $right);
			}elseif($y <= 2099){
				return floor(23.2488 + $left - $right);
			}elseif($y <= 2150){
				return floor(24.2488 + $left - $right);
			}else{
				return 99;
			}
		}
	}
}