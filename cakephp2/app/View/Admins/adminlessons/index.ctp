<?php echo $this->element('Admin/js/lessons_js'); ?>

<h1>パン・お菓子教室管理画面</h1>

<div id="primary">
	<div class="wbox">
		<h2>教室新規登録</h2>
		<p>教室を新規する場合は「教室新規作成」より教室の情報を入力してください。</p>
		<p class="btn"><a href="<?php echo $this->Html->url('/', true); ?>adminlessons/add/">教室新規登録</a></p>
		<h2>開催予定教室一覧</h2>
		<p>開催予定の教室一覧です。内容を編集する場合は「修正」ボタンを、削除する場合は「削除」ボタンをクリックして下さい。</p>
		<?php echo $this->Session->flash(); ?>

		<?php if(!empty($data)): ?>
			<table border="0" class="zebraList">
				<tr>
					<?php if(empty($p)): ?>
						<th>おすすめ</th>
					<?php endif; ?>
					<th>No</th>
					<th>教室タイプ</th>
					<th>画像</th>
					<th>教室名</th>
					<th>日程</th>
					<th>表示</th>
					<th>編集</th>
				</tr>
				<?php $i=0; ?>
				<?php foreach($data as $k => $v): ?>
					<tr>
						<?php if(empty($p)): ?>
							<td>
								<label>
									<?php echo $this->Form->hidden("Lesson.id", array('id' => false, 'class' => 'recommendid', 'value' => $v['Lesson']['id'])); ?>
									<?php if($v['Lesson']['recomflg'] == 1): ?>
										<input class="recom" name="recom[<?php echo $i; ?>]" type="radio" value="1" checked="checked" />
									<?php else: ?>
										<input class="recom" name="recom[<?php echo $i; ?>]" type="radio" value="1" />
									<?php endif; ?>
								</label>
							</td>
						<?php endif; ?>
						<td><?php echo $v['Lesson']['id']; ?></td>
						<td><?php echo $lcategory[$v['Lesson']['category']]; ?></td>
						<td><?php if(!empty($v['LessonImage'][$i]['uploadpath'])): ?>
							<?php $path = str_replace('.jpg', '_th.jpg', $v['LessonImage'][$i]['uploadpath']); ?>
							<a href="<?php echo $this->Html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>" target="_blank"><img src="<?php echo $this->Common->get_thumbs_path($v['LessonImage'][$i]['uploadpath']); ?>" alt="" width="50" /></a>
						<?php endif; ?></td>
						<td><a href="<?php echo $this->Html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>" target="_blank"><?php echo h($v['Lesson']['title']); ?></a></td>
						<td><?php
							$nums = count($v['LessonDate']);
							$numss = 1;
							?>
							<?php foreach($v['LessonDate'] as $key => $val): ?>
								<?php echo date('n/j', strtotime($val['date'])); ?><?php if($nums == $numss): ?><?php else: ?>,<?php endif; ?>
								<?php $numss++; ?>
							<?php endforeach; ?>

						</td>
						<td><label>
							<?php echo $this->Form->hidden("Lesson.id", array('id' => false, 'class' => 'activeid', 'value' => $v['Lesson']['id'])); ?>
							<?php echo $this->Form->input('Lesson.active', array(
								'type'     => 'select',
								'label'    => false,
								'id'       => false,
								'class'    => 'active',
								'selected' => $v['Lesson']['active'],
								'options'  => $active,
								'empty'    => null
							)); ?>
						</label></td>
						<td style="white-space:nowrap;">
							<ul class="panel-edit">
								<li class="link-edit"><a href="<?php echo $this->Html->url("/adminlessons/edit/{$v['Lesson']['id']}"); ?>">編集</a>
									<ul>
										<li><a href="<?php echo $this->Html->url("/adminlessons/dateedit/{$v['Lesson']['id']}"); ?>">人数変更</a></li>
										<li class="duplicates"><a href="<?php echo $this->Html->url("/adminlessons/duplicates/{$v['Lesson']['id']}"); ?>">コピー</a></li>
										<li class="deleteconfi"><a href="<?php echo $this->Html->url("/adminlessons/delete/{$v['Lesson']['id']}"); ?>">削除</a></li>
									</ul>
								</li>
							</ul>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		<?php else: ?>
			<p>該当のデータはありませんでした。</p>
		<?php endif; ?>
		<p class="link-past"><a href="<?php echo $this->Html->url("/adminlessons/past"); ?>">過去開催一覧はこちら</a></p>
	</div>
	<p class="btn-back">
		<a href="<?php echo $this->Html->url("/admins/"); ?>">戻る</a>
	</p>
</div>