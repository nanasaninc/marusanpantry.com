<?php echo $this->Form->create(null, array('type' => 'file')); ?>
<?php echo $this->Form->input('token', array('type' => 'hidden', 'value' => $token)); ?>

<?php echo $this->Form->hidden("Lesson.id"); ?>

<h1>教室管理</h1>
<div id="primary">
	<div class="wbox">
		<?php echo $this->Form->hidden("Lesson.id"); ?>
		<h2>応募可能数修正</h2>
		<h3>レッスンタイトル</h3>
		<p><?php echo $data['Lesson']['title'].PHP_EOL; ?></p>
		<?php if(!empty($data["LessonDate"][0]['id'])): ?>
			<h3>開催日</h3>
			<table>
				<tr>
					<th>開催日</th>
					<th>開催時間</th>
					<th>定員</th>
					<th>応募可能数</th>
				</tr>
				<?php for($i = 0;$i < 8; $i++): ?>
					<?php if(!empty($data["LessonDate"][$i]['id'])): ?>

						<?php echo $this->Form->hidden("LessonDate.{$i}.id"); ?>
						<tr>
							<td><?php echo date('Y月n日d日', strtotime($data["LessonDate"][$i]['date'])); ?>（<?php echo $this->Jaweek->weekname($data["LessonDate"][$i]['date']); ?>）</td>
							<td><?php echo $data["LessonDate"][$i]['datetxt']; ?></td>
							<td><?php echo $this->Form->text("LessonDate.{$i}.limit", array('size' => '1')); ?>人</td>
							<td><?php echo $this->Form->text("LessonDate.{$i}.capacity", array('size' => '1')); ?>人</td>
						</tr>
					<?php endif; ?>
				<?php endfor; ?>
			</table>
		</div>
	<?php else: ?>
		<p>日程の登録がありません<br /><a href="<?php echo $this->Html->url('/', true).'adminlessons/edit/'.$data['Lesson']['id']; ?>">日程の登録はこちらから</a></p>
	</div>
<?php endif; ?>
<p class="btn">
	<?php echo $this->Form->submit('修正する', array('name' => 'data[Lesson][mode]', 'div' => false)), PHP_EOL; ?>
</p>
<?php echo $this->Form->end(); ?>
</div>