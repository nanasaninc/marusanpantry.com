<script type="text/javascript">
	$(function(){
		$('.dateee .datepicker').change(function(){
			var s = $(this).parent().next().next().children();
			var v = s.val();
			if(v == ""){
				s.val('8');
			}
			var arrays = new Array();
			$('.dateee .datepicker').each(function(){
				days = $(this).val();
				if(days){
					arrays.push(parseInt(days.replace(/-/g,'')));
				}
			});
			var max = arrays[0];
			for(var i=1; i < arrays.length;i++){
				max = Math.max(max, arrays[i]);
			}
			//max.replace('2', '1');
			//console.log(max);
		});
	});
</script>
<h1>パン・お菓子教室管理画面</h1>
<div id="primary">
	<div class="wbox">
		<h2>基本情報登録</h2>
		<?php echo $this->Form->create(null, array('type' => 'file')); ?>
		<?php echo $this->Form->input('token', array('type' => 'hidden', 'value' => $token)); ?>
		<p>
			<strong>表示</strong>
			<?php echo $this->Form->input("Lesson.active", array(
				'type'    => 'select',
				'empty'   => false,
				'options' => $active,
				'label'   => false,
				'div'     => false,
			)) . PHP_EOL; ?>
		</p>
		<div id="photoBox">

			<?php for($i = 0;$i < 2;$i++): ?>
				<?php $num = $i + 1; ?>
				<?php echo $this->Form->hidden("LessonImage.{$i}.sort", array('value' => "{$num}")); ?>
				<div class="btn-upload">
					<p>
					<?php echo $this->Form->input("fileName{$i}", array('type' => 'file', 'label' => false, 'class' => 'file')).PHP_EOL; ?>
					</p>
				</div>
			<?php endfor; ?>
		</div>
		<div id="contentBox">
			<dl>
			<dt>カテゴリ</dt>
			<dd>
				<?php echo $this->Form->input("Lesson.category", array(
					'type'    => 'select',
					'empty'   => false,
					'options' => $lcategory,
					'label'   => false,
					'div'     => false,
				)) . PHP_EOL; ?>
				<?php echo $this->Form->error('Lesson.category'); ?>
			</dd>
			<dt>タイトル</dt><dd><?php echo $this->Form->text('Lesson.title', array('title' => 'タイトルを入力してください。必須です。', 'size' => '40')).PHP_EOL; ?><?php echo $this->Form->error('Lesson.title'); ?></dd>
			<dt>コメント</dt><dd><?php echo $this->Form->textarea('Lesson.description', array('title' => 'コメントを入力してください。', 'cols' => '40','rows' => '5')).PHP_EOL; ?></dd>
				<dt>講師</dt>
				<dd><?php echo $this->Form->text('Lesson.teacher', array('title' => '講師名を入力してください。', 'size' => '40')); ?></dd>
				<dt>開催時間</dt>
				<dd><?php echo $this->Form->textarea('Lesson.morning', array('title' => '開催時間を入力してください。', 'cols' => '40','rows' => '5')); ?></dd>
				<dt>参加費</dt>
				<dd><?php echo $this->Form->text('Lesson.price', array('title' => '参加費を入力してください。', 'size' => '20')); ?></dd>
				<dt>持参品</dt>
				<dd><?php echo $this->Form->textarea('Lesson.jisan', array('title' => '持参品を入力してください。', 'cols' => '40','rows' => '5')); ?></dd>
				<dt>定員</dt>
				<dd><?php echo $this->Form->text('Lesson.memberlimit', array('value' => '12','size' => '10')); ?>人</dd>
				<dt>内容</dt>
				<dd><?php echo $this->Form->textarea('Lesson.bikou', array('title' => '内容を入力してください。', 'cols' => '40','rows' => '5')); ?></dd>
			</dl>
		</div>
	</div>
	<div class="wbox">
		<h2>開催日程登録</h2>
		<dl>
			<dt>掲載終了日</dt>
			<dd><?php echo $this->Form->text('Lesson.limitday',array('size' => '20', 'readonly' => true, 'class' => 'datepicker')); ?><?php echo $this->Form->error('Lesson.limitday'); ?></dd>
		</dl>
		<table border="0">
			<tr>
				<th>開催日</th>
				<th>開催時間</th>
				<th>WEB受付数</th>
			</tr>
			<?php for($i = 0;$i < 8; $i++): ?>
				<tr>
					<td class="dateee">
					<?php echo $this->Form->text("LessonDate.{$i}.date", array('class' => 'datepicker')); ?>
					<?php echo $this->Form->error('LessonDate.{$i}.date'); ?>
					</td>
					<td><?php echo $this->Form->text("LessonDate.{$i}.datetxt"); ?></td>
					<td><?php echo $this->Form->text("LessonDate.{$i}.limit"); ?>人</td>
				</tr>
			<?php endfor; ?>
		</table>
	</div>
	<p class="btn-back">
		<input onclick="history.back();" name="" type="button" value="戻る" />
	</p>
	<p class="btn">
	<?php echo $this->Form->submit('登録する', array('name' => 'data[Lesson][mode]', 'div' => false)), PHP_EOL; ?>
	</p>
	<?php echo $this->Form->end(); ?>
</div>
