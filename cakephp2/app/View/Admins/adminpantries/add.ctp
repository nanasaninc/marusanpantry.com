<h1>マルサンパントリースタッフのイチオシ管理画面</h1>
<div id="primary">
	<div class="wbox">
		<h2>基本情報登録</h2>
		<?php echo $this->Form->create(null, array('type' => 'file')); ?>
		<?php echo $this->Form->input('token', array('type' => 'hidden', 'value' => $token)); ?>
		<div id="photoBox">
			<?php for($i = 0;$i < 2;$i++): ?>
				<div class="btn-upload">
					<?php $num = $i + 1; ?>
					<?php echo $this->Form->hidden("PantryImage.{$i}.sort", array('value' => "{$num}")); ?>
					<p><?php echo $this->Form->input("fileName{$i}", array('type' => 'file', 'label' => false,'class' => 'file')); ?><?php echo $this->Form->error("fileName" . $i); ?></p>
				</div>
			<?php endfor; ?>

		</div>
		<div id="contentBox">
			<dl>
				<dt>タイトル</dt>
				<dd><?php echo $this->Form->text('Pantry.title', array('title' => 'タイトルは必須です。', 'size' => '40')); ?><?php echo $this->Form->error('Pantry.title'); ?></dd>
			</dl>
			<dl>
				<dt>説明文</dt>
				<dd><?php echo $this->Form->textarea('Pantry.description',array('title' => '説明文を入力してください。', 'cols' => '40','rows' => '5')); ?></dd>
			</dl>
			<dl>
				<dt>ポイント</dt>
				<dd><?php echo $this->Form->textarea('Pantry.point',array('title' => 'ポイントを入力してください。', 'cols' => '40','rows' => '5')); ?></dd>
			</dl>
		</div>
	</div>

	<p class="btn-back">
		<input onclick="history.back();" name="" type="button" value="戻る" />
	</p>
	<p class="btn">
		<?php echo $this->Form->submit('登録する', array('name' => 'data[Pantry][mode]', 'div' => false)); ?>
	</p>
	<?php echo $this->Form->end(); ?>
</div>