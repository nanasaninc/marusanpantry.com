<?php echo $this->element('Front/Js/related'); ?>
<script type="text/javascript">
	$(function(){
		$(window).load(function(){
			$('table#materialBox tr.materials').each(function(){
				var u = $('input.matenames', this).val();
				if(u != ''){
					$(this).css('display', 'block');
				}
			});
			$('#recipeBox .box').each(function(){
				var u = $('textarea.ordercomment', this).text();
				if(u != ''){
					$(this).css('display', 'block');
				}
			});
		});

		$('p#mateAdd').click(function(){
			var i = 0;
			var count = 0;
			$('table#materialBox tr.materials').each(function(){
				var c = $(this).css('display');
				if(c == 'none' && count < 4){
					$(this).css('display', 'block');
					count++;
				}
				i++;
			});
		});

		$('p#orderAdd').click(function(){
			var i = 0;
			var count = 0;
			$('#recipeBox .box').each(function(){
				var c = $(this).css('display');
				if(c == 'none' && count < 5){
					$(this).css('display', 'block');
					count++;
				}
				i++;
			});
		});

		var s = $("select#RecipeCategory option:selected").val();
		if(s > 20000){
			$("select[name='bigcate']").val(2000);
		}else if(s < 20000){
			$("select[name='bigcate']").val(1000);
		}else{
			$("select[name='bigcate']").val('');
		}
		$("select[name='bigcate']").change(function(){
			var ids = $(this).val();
			var select = $('select#RecipeCategory');
			ajax_seadrch(ids, select);

		});
	});
	function ajax_seadrch(ids, values){
		var bool;
		$.post("<?php echo $this->Html->url('/adminrecipes/bidcategory/'); ?>", {id:ids}, function(data){
			if (data.length > 0){
				$('option', values).remove();
				values.append(data);
				//after(data);
			}
		}
		);
	}
</script>
<style type="text/css">
	table#materialBox tr.materials,
	#recipeBox .box{
		display: none;
	}
</style>
<?php echo $this->Form->create(null, array('enctype'=>'multipart/form-data')); ?>
<?php echo $this->Form->input('token', array('type' => 'hidden', 'value' => $token)); ?>
<?php echo $this->Form->hidden("Recipe.id"); ?>
<h1>レシピ管理</h1>
<div id="primary">
	<div class="wbox">
		<h2>基本情報登録</h2>
		<p>
			<strong>表示／非表示</strong>
			<?php echo $this->Form->input("Recipe.active", array(
				'type'    => 'select',
				'empty'   => '選択してください',
				'options' => $active,
				'label'   => false,
				'div'     => false,
			)).PHP_EOL; ?>
		</p>
		<div id="photoBox">
			<?php for($i = 0;$i < 1;$i++): ?>
				<p>
					<?php echo $this->Form->hidden("RecipeImage.{$i}.id").PHP_EOL; ?>
					<?php echo $this->Form->hidden("RecipeImage.{$i}.sort").PHP_EOL; ?>
					<?php if(!empty($data['RecipeImage'][$i]['uploadpath'])): ?>
						<img src="<?php echo $this->Common->get_thumbs_path($data['RecipeImage'][$i]['uploadpath']); ?>" width="200" alt="" />
					<?php endif; ?>
					<div class="btn-upload">
						<?php echo $this->Form->input("fileName{$i}", array('type' => 'file', 'label' => false, 'class' => 'file')).PHP_EOL; ?>
					</div>
				</p>
			<?php endfor; ?>
		</div>
		<div id="contentBox">
			<p>カテゴリ
				<select name="bigcate">
					<?php foreach($bcate as $k => $v): ?>
						<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
					<?php endforeach; ?>
				</select>
				<?php echo $this->Form->input("Recipe.category", array(
					'type'    => 'select',
					'empty'   => '選択してください',
					'options' => $subcategory,
					'label'   => false,
					'div'     => false,
				)); ?>
			</p>
			<p>レシピ名<?php echo $this->Form->text('Recipe.title', array('title' => 'レシピ名必須です。')).PHP_EOL; ?><?php echo $this->Form->error('Recipe.title'); ?></p>

			<p>
				難易度
				<?php echo $this->Form->input("Recipe.difficulty", array(
					'type'    => 'select',
					'empty'   => '選択してください',
					'options' => $difficulty,
					'label'   => false,
					'div'     => false,
				)).PHP_EOL; ?>
			</p>
			<p>コメント<br />
				<label>
					<?php echo $this->Form->textarea('Recipe.description', array('title' => 'コメントを入力してください。', 'cols' => '45', 'rows' => '10')).PHP_EOL; ?>
				</label>
			</p>
			<p>材料
				　出来上がり量（<?php echo $this->Form->text('Recipe.numberpeople', array('title' => '出来上がり量を入力してください。')).PHP_EOL; ?>）
			</p>
			<table id="materialBox" border="0" cellpadding="0" cellspacing="0">
				<tbody>
					<?php for($i = 0;$i < 30; $i++): ?>
						<tr class="materials">
							<?php echo $this->Form->hidden("RecipeMaterial.{$i}.id"); ?>
							<?php echo $this->Form->hidden("RecipeMaterial.{$i}.sort", array('class' => 'sortorder','value' => "{$i}")); ?>
							<td><?php echo $this->Form->text("RecipeMaterial.{$i}.name", array ('class' => 'matenames', 'size' => '30')); ?></td>
							<td class="right"><?php echo $this->Form->text("RecipeMaterial.{$i}.num", array ('title' => '数量を入力', 'size' => '10')); ?></td>
						</tr>
					<?php endfor; ?>
				</tbody>
			</table>
			<p id="mateAdd" class="link-add"><a href="javascript:void(0)">材料を追加する</a></p>
		</div>
	</div>
	<div class="wbox">
		<h2>手順　コツ・ポイント</h2>
		<h3>手順</h3>
		<div id="recipeBox" class="heightAlign">
			<?php for($i = 0;$i < 30; $i++): ?>
				<div class="box">
					<p class="num">手順<?php echo $i +1; ?></p>
					<?php echo $this->Form->hidden("RecipeOrder.{$i}.id"); ?>
					<span><?php echo $this->Form->hidden("RecipeOrder.{$i}.sort", array('value' => "{$i}")); ?></span>

					<p>	<label><?php echo $this->Form->textarea("RecipeOrder.{$i}.comment", array('title' => 'コメントを入力してください。', 'class' => 'ordercomment', 'label' => false)); ?></label></p>
					<p><?php if(!empty($data['RecipeOrder'][$i]['uploadpath'])): ?>
						<img src="<?php echo $this->Common->get_thumbs_path($data['RecipeOrder'][$i]['uploadpath']); ?>" alt="" width="130" />
					<?php endif; ?></p>
					<div class="btn-upload">
						<p><label><?php echo $this->Form->input("orderPhoto{$i}", array('type' => 'file', 'label' => false, 'class' => 'file')); ?></label></p>
					</div>
					<p>
						<label>
							<?php echo $this->Form->input("RecipeOrder.{$i}.delphoto", array(
								'type'  => 'checkbox',
								'div'   => false,
								'label' => false,
								'value' => 1,
							)); ?>
						</label>
						画像を削除</p>
						<p>
							<label>
								<?php echo $this->Form->input("RecipeOrder.{$i}.delitem", array(
									'type'  => 'checkbox',
									'div'   => false,
									'label' => false,
									'value' => 1,
								)); ?>
							</label>
							削除</p>
						</div>
					<?php endfor; ?>
				</div>
				<p id="orderAdd" class="link-add"><a href="javascript:void(0)">手順を追加する</a></p>
				<h3>コツ・ポイント</h3>
				<?php echo $this->Form->textarea('Recipe.point', array('title' => 'コツ・ポイントを入力してください。', 'label' => false ,'cols' => '80' , 'rows' => '10')).PHP_EOL; ?>
			</div>
			<div class="wbox" id="relatedBox">
				<h2>関連商品</h2>
				<table border="0">
					<tbody>
						<?php for($i = 0;$i < 8; $i++): ?>
							<tr>
								<?php echo $this->Form->hidden("RecipeRelated.{$i}.id"); ?>
								<th><?php echo $i + 1; ?></th>
								<td class="images">
									<?php if(!empty($data['RecipeRelated'][$i]['uploadpath'])): ?>
										<?php echo $data['RecipeRelated'][$i]['uploadpath']; ?>
									<?php endif; ?>
								</td>
								<td>
									<span><?php echo $this->Form->hidden("RecipeRelated.{$i}.sort", array('value' => "{$i}")); ?></span>
									<p>商品URL　<?php echo $this->Form->text("RecipeRelated.{$i}.link", array('title' => 'URLを入力してください。', 'size' => '45')); ?><a class="gets" href="javascript:void(0)">登録</a><br />
										商品名　<?php echo $this->Form->text("RecipeRelated.{$i}.name", array('class' => 'name', 'size' => '45')); ?><br />
										価格　<?php echo $this->Form->text("RecipeRelated.{$i}.price", array('class' => 'price', 'size' => '30')); ?>
										<?php echo $this->Form->hidden("RecipeRelated.{$i}.uploadpath", array()); ?>
									</p>
									<p><label><?php echo $this->Form->input("RecipeRelated.{$i}.delitem", array(
										'type'  => 'checkbox',
										'div'   => false,
										'label' => false,
										'value' => 1,
									)); ?>この商品を削除する</label></p>
								</td>
							</tr>
						<?php endfor; ?>
					</tbody>
				</table>
			</div>
			<p class="btn-back">
				<input onclick="history.back();" name="" type="button" value="戻る" />
			</p>
			<p class="btn">
				<?php echo $this->Form->submit('修正する', array('name' => 'data[Recipe][mode]', 'div' => false)), PHP_EOL; ?>
			</p>
		</div>
		<?php echo $this->Form->end(); ?>