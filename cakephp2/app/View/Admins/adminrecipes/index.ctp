
<script type="text/javascript">
	$(function(){
		$('select.active').change(function(e){
			var _self = $(this);
			var ids = _self.parents('label').find('.activeid').val();
			var values = _self.val();
			e.preventDefault();
			change_active(ids, values, _self);
		});
		$('li.deleteconfi').click(function(){
			return confirm("削除しますか？\nこの操作は取り消すことが出来ません。");
		});
	});

	function change_active(ids, values, obj){
		var bool;
		$.post(
			"<?php echo $this->Html->url('/adminrecipes/active/'); ?>",
			{
				id:ids,
				value:values
			},
			function(json){
				var res = $.parseJSON(json);
				if (false != res.status){
					obj.after(res.message);
				}else{
					obj.after(res.message);
				}
			}
		);
	}

</script>
<h1>レシピ管理</h1>
<div id="primary">
	<div class="wbox">
		<h2>レシピ新規登録</h2>
		<p>レシピを新規作成する場合は「レシピ新規作成」よりレシピの情報を入力してください。</p>
		<p class="btn">
			<a href="<?php echo $this->Html->url("/adminrecipes/add/"); ?>">レシピ新規登録</a>
		</p>
		<h2>レシピ一覧</h2>
		<p>開催予定の教室一覧です。内容を編集する場合は「修正」ボタンを、削除する場合は「削除」ボタンをクリックして下さい。</p>
		<?php echo $this->Session->flash(); ?>

		<?php
			$bool = $this->Paginator->numbers();
		?>

		<?php if(!empty($data)): ?>
			<?php if($bool != false): ?>
				<div class="pagination">
					<ul>
						<li class="prev"><?php echo $this->Paginator->prev('前へ').PHP_EOL; ?></li>
						<?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => false)).PHP_EOL; ?>
						<li class="next"><?php echo $this->Paginator->next('次へ').PHP_EOL; ?></li>
					</ul>
				</div>
			<?php endif; ?>
		<?php endif; ?>


		<table border="0" class="zebraList">
			<tr>
				<th>No</th>
				<th>写真</th>
				<th>カテゴリ</th>
				<th>レシピ名</th>
				<th>表示</th>
				<th>編集</th>
			</tr>
			<?php $i=0; ?>
			<?php foreach($data as $k => $v): ?>
				<tr>
					<td><?php echo $v['Recipe']['id']; ?></td>
					<td>
						<?php if(!empty($v['RecipeImage'][$i]['uploadpath'])): ?>
							<img src="<?php echo $this->Common->get_thumbs_path($v['RecipeImage'][$i]['uploadpath']); ?>" alt="" width="50" />
						<?php endif; ?>
					</td>
					<td>
						<?php if(preg_match('/^1.+/', $v['Recipe']['category'])): ?>
							<a href="<?php echo $this->Html->url("/adminrecipes/index/10000"); ?>">パン</a>
						<?php elseif(preg_match('/^2.+/', $v['Recipe']['category'])): ?>
							<a href="<?php echo $this->Html->url("/adminrecipes/index/20000"); ?>">お菓子</a>
						<?php endif; ?>
						<?php if(isset($v['Recipe']['category'])): ?>
						&gt;<a href="<?php echo $this->Html->url("/adminrecipes/index/{$v['Recipe']['category']}"); ?>"><?php echo $subcategory[$v['Recipe']['category']]; ?></a>
						<?php endif; ?>
					</td>
						<td><a target="_blank" href="<?php echo $this->Html->url("/recipes/recipe/{$v['Recipe']['id']}"); ?>"><?php echo h($v['Recipe']['title']); ?></a></td>
						<td><label>
							<?php echo $this->Form->hidden("Recipe.id", array('id' => false, 'class' => 'activeid', 'value' => $v['Recipe']['id'])); ?>
							<?php echo $this->Form->input("Recipe.active", array(
								'type'     => 'select',
								'empty'    => null,
								'selected' => $v['Recipe']['active'],
								'options'  => $active,
								'class'    => 'active',
								'id'       => false,
								'label'       => false
							)); ?>
						</label></td>
						<td><ul class="panel-edit">
							<li class="link-edit"><a href="<?php echo $this->Html->url("/adminrecipes/edit/{$v['Recipe']['id']}"); ?>">編集</a>
								<ul>
									<li class="deleteconfi"><a href="<?php echo $this->Html->url("/adminrecipes/delete/{$v['Recipe']['id']}"); ?>">削除</a></li>
								</ul>
							</li>
						</ul></td>
					</tr>
				<?php endforeach; ?>
			</table>
		</div>
		<p class="btn-back">
			<a href="<?php echo $this->Html->url("/admins/"); ?>">戻る</a>
		</p>
	</div>