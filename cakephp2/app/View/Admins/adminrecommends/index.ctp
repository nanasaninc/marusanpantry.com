<script type="text/javascript">
	$(function(){
		$('input.recom').change(function(e){
			var _self = $(this);
			$("p.osusumecomp").remove();
			var recomlength = $('input.recom').length -1;
			$(this).after('<p id="recoms"><img src="<?php echo $this->Html->url('/'); ?>img/admin/recom.gif" /></p>');

			var ids = _self.prev().val();
			e.preventDefault();
			ajax_recom(ids, _self);
		});
		$('li.duplicates').click(function(){
			return confirm("コピーしてよろしいですか？");
		});
		$('li.deleteconfi').click(function(){
			return confirm("削除しますか？\nこの操作は取り消すことが出来ません。");
		});
	});
	function ajax_recom(ids, obj){
		var bool;
		$.post(
			"<?php echo $this->Html->url('/adminrecommends/recom/'); ?>",
			{
				id:ids
			},
			function(json){
				var res = $.parseJSON(json);
				$('p#recoms').remove();
				if (false != res.status){
					obj.after('<p class="osusumecomp">' + res.message + '</p>');
				}else{
					obj.after('<p class="osusumecomp">' + res.message + '</p>');
				}
			}
		);
	}
</script>
<h1>キッチンショップスタッフのイチオシ商品</h1>
<div id="primary">
	<div class="wbox">
		<h2>キッチンショップスタッフのイチオシ商品新規登録</h2>
		<p>キッチンショップスタッフのイチオシ商品を新規する場合は「キッチンショップスタッフのイチオシ商品新規作成」よりキッチンショップスタッフのイチオシ商品の情報を入力してください。</p>
		<p class="btn"><a href="<?php echo $this->Html->url('/', true); ?>adminrecommends/add/">イチオシ商品登録</a></p>
		<h2>キッチンショップスタッフのイチオシ商品一覧</h2>
		<p>キッチンショップスタッフのイチオシ商品一覧です。内容を編集する場合は「修正」ボタンを、削除する場合は「削除」ボタンをクリックして下さい。</p>
		<?php echo $this->Session->flash(); ?>


		<?php if(!empty($data['list'])): ?>
			<table border="0" class="zebraList">
				<tr>
					<th>おすすめ</th>
					<th>No</th>
					<th>画像</th>
					<th>商品名</th>
					<th>編集</th>
				</tr>
				<?php $i=0; ?>
				<?php foreach($data['list'] as $k => $v): ?>
					<tr>
						<td>
							<label>
								<?php echo $this->Form->hidden("Recommend.id", array(
									'id' => false,
									'class' => 'recommendid',
									'value' => $v['Recommend']['id']
									)); ?>
									<?php if($v['Recommend']['recomflg'] == 1): ?>
										<input class="recom" name="recom[<?php echo $i; ?>]" type="radio" value="1" checked="checked" />
									<?php else: ?>
										<input class="recom" name="recom[<?php echo $i; ?>]" type="radio" value="1" />
									<?php endif; ?>
								</label>
							</td>
							<td><?php echo $v['Recommend']['id']; ?></td>
							<td><?php if(!empty($v['RecommendImage'][$i]['uploadpath'])): ?>
								<img src="<?php echo $this->Common->get_thumbs_path($v['RecommendImage'][$i]['uploadpath']); ?>" alt="" width="50" />
							<?php endif; ?></td>
							<td><?php echo nl2br(h($v['Recommend']['title'])); ?></td>
							<td style="white-space:nowrap;">
								<ul class="panel-edit">
									<li class="link-edit"><a href="<?php echo $this->Html->url("/adminrecommends/edit/{$v['Recommend']['id']}"); ?>">編集</a>
										<ul>
											<li class="duplicates"><a href="<?php echo $this->Html->url("/adminrecommends/duplicates/{$v['Recommend']['id']}"); ?>">コピー</a></li>
											<li class="deleteconfi"><a href="<?php echo $this->Html->url("/adminrecommends/delete/{$v['Recommend']['id']}"); ?>">削除</a></li>
										</ul>
									</li>
								</ul>
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
			<?php else: ?>
				<p>該当のデータはありませんでした。</p>
			<?php endif; ?>
			<!--p class="link-past"><a href="#">過去開催一覧はこちら</a></p-->
		</div>
		<p class="btn-back">
			<a href="<?php echo $this->Html->url("/admins/"); ?>">戻る</a>
		</p>
	</div>