<h1>キッチンショップスタッフのイチオシ管理画面</h1>
<div id="primary">
	<div class="wbox">
		<h2>基本情報登録</h2>
		<?php echo $this->Form->create(null, array('type' => 'file')); ?>
		<?php echo $this->Form->input('token', array('type' => 'hidden', 'value' => $token)); ?>
		<?php echo $this->Form->hidden("Recommend.id"); ?>
		<div id="photoBox">
			<?php for($i = 0;$i < 2;$i++): ?>
				<?php $num = $i + 1; ?>
				<?php echo $this->Form->hidden("RecommendImage.{$i}.id"); ?>
				<?php echo $this->Form->hidden("RecommendImage.{$i}.sort"); ?>
				<?php if(!empty($data['RecommendImage'][$i]['uploadpath'])): ?>
					<img src="<?php echo $this->Common->get_thumbs_path($data['RecommendImage'][$i]['uploadpath']); ?>" width="200" alt="" />
				<?php endif; ?>
				<div class="btn-upload">
					<p><?php echo $this->Form->input("fileName{$i}", array('type' => 'file', 'label' => false,'class' => 'file')); ?></p>
				</div>
			<?php endfor; ?>

		</div>
		<div id="contentBox">
			<dl>
				<dt>タイトル</dt>
				<dd><?php echo $this->Form->text('Recommend.title',array('title' => 'タイトルを入力してください。必須です。', 'size' => '40')); ?><?php echo $this->Form->error('Recommend.title'); ?></dd>
			</dl>
			<dl>
				<dt>説明文</dt>
				<dd><?php echo $this->Form->textarea('Recommend.description',array('title' => '説明文を入力してください。', 'cols' => '40','rows' => '5')); ?></dd>
			</dl>
			<dl>
				<dt>ポイント</dt>
				<dd><?php echo $this->Form->textarea('Recommend.point',array('title' => 'ポイントを入力してください。', 'cols' => '40','rows' => '5')); ?></dd>
			</dl>
		</div>
	</div>
	<p class="btn-back">
		<input onclick="history.back();" name="" type="button" value="戻る" />
	</p>
	<p class="btn">
		<?php echo $this->Form->submit('修正する', array('name' => 'data[Recommend][mode]', 'div' => false)); ?>
	</p>
	<?php echo $this->Form->end(); ?>
</div>