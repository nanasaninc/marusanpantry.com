<?php
	if($syokai == 1){
		$syokai = '初回申込み:初回申込みの方です。';
	}else{
		$syokai = '';
	}
?>
このメールは自動返信メールとなります。
こちらのメールアドレスにご返信されましても、確認できません。
あらかじめご了承ください。

予約状況を確認後、改めて「ご予約完了メール」をお送りさせていただきます。
「ご予約完了メール」が届いた時点で正式にご予約完了となりますので、
いましばらくお待ちくださいませ。

<?php echo h($name); ?>　様

この度はマルサンパントリー　【<?php echo h($title); ?>】　への参加お申し込み、誠にありがとうございます。
以下の内容で参加お申し込みを受付いたしましたので、ご確認をお願いいたします。

参加教室：<?php echo h($title) . PHP_EOL; ?>
参加日：<?php echo h($date) . PHP_EOL; ?>
時間：<?php echo h($datetxt) . PHP_EOL; ?>
お名前：<?php echo h($name) . PHP_EOL; ?>
メールアドレス：<?php echo h($email) . PHP_EOL; ?>

申込み人数：<?php echo h($capacity) . PHP_EOL; ?>

お電話番号：<?php echo h($phone) . PHP_EOL; ?>

<?php echo h($syokai) . PHP_EOL; ?>

会員No.:<?php echo h($membernum) . PHP_EOL; ?>

備考--
<?php echo h($comment); ?>



※入力内容に謝り・変更がある場合は、089-931-1147までお電話ください。


♪゜+.ｏ.+゜♪゜+.ｏ.+゜♪゜+.ｏ.+゜♪゜

　マルサンパントリー 　スイートキッチン

　school@m-marusan.co.jp
　www.marusanpantry.com
　〒790-0005 　愛媛県松山市花園町6-1
　TEL.089-931-1147  FAX.089-931-1148
　OPEN 8:45-18:00 ☆日曜・祝日定休

♪゜+.ｏ.+゜♪゜+.ｏ.+゜♪゜+.ｏ.+゜♪゜