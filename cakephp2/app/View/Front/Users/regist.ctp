
	<h1>管理者登録</h1>

<?php echo $this->Form->create(null, array('type'=>'post','url'=>'')); ?>
<table class="stripe">
	<tr>
		<th>ご希望のログインID</th>
		<td><?php echo $this->Form->text("User.username", array("size" => 15)); ?>
			<span>半角英数字　15文字以内</span>
			<?php echo $this->Form->error("User.username"); ?>
		</td>
	</tr>
	<tr>
		<th>ご希望のログインパスワード</th>
		<td><?php echo $this->Form->password("User.raw_password", array("size" => 15)); ?>
			<span>半角英数字　15文字以内　</span>
			<?php echo $this->Form->error("User.raw_password"); ?>
		</td>
	</tr>
	<tr>
		<th>メールアドレス</th>
		<td><?php echo $this->Form->text("User.email", array("size" => 30)); ?>

			<?php echo $this->Form->error("User.email"); ?>
		</td>
	</tr>
	<tr>
		<th>確認用メールアドレス</th>
		<td><?php echo $this->Form->text("User.mailaddress_confirm", array("size" => 30)); ?>
			<span>確認のためもう一度入力してください</span>
			<?php echo $this->Form->error("User.mailaddress_confirm"); ?>
		</td>
	</tr>
</table>

<?php  echo $this->Form->hidden('mode',array('value' => 'confirm')); ?>

<p class="center"><?php echo $this->Form->submit("確認画面へ", array('class' => 'btn-confirm', 'div' => false)); ?></p>
<?php echo $this->Form->end(),PHP_EOL; ?>
