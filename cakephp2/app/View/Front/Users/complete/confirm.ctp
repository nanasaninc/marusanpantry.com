	<h1>確認画面</h1>

<?php echo $this->Form->create(null, array('type'=>'post','url'=>'regist')); ?>
<table class="stripe">
	<tr>
		<th>ログインID</th>
		<td><?php echo $this->Form->value("User.username"); ?></td>
	</tr>
	<tr>
		<th>メールアドレス</th>
		<td><?php echo $this->Form->value("User.email"); ?></td>
	</tr>
	<tr>
		<th>ご希望のログインパスワード</th>
		<td><?php echo $this->Form->value("User.raw_password"); ?></td>
	</tr>
</table>

<?php echo $this->Form->hidden("User.username"); ?>
<?php echo $this->Form->hidden("User.email"); ?>
<?php echo $this->Form->hidden("User.raw_password"); ?>

	<p class="center"><?php echo $this->Form->submit('戻る', array('name' => 'data[User][mode]','class' => 'btn-back','div' => false)); ?><?php echo $this->Form->submit('登録する', array('name' => 'data[User][mode]','class' => 'btn-submit','div' => false)); ?></p>
<?php echo $this->Form->end(); ?>