<h1>マルサンパントリーウェブサイト管理</h1>
<div id="primary">
	<div class="wbox">
		<h2>ログイン</h2>
		<?php
			echo $this->Form->create('User', array('url' => 'login'));
		?>
		<div id="loginBox">
			<dl>
				<dt class="id">ログインID</dt>
				<dd><?php echo $this->Form->input('username', array('label'=>false,'class'=>'text')); ?></dd>
				<dt class="pw">パスワード</dt>
				<dd><?php echo $this->Form->input('password', array('label'=>false,'class'=>'text')); ?></dd>
			</dl>
			<?php echo $this->Session->flash(); ?>
			<div class="btn-submit">
				<div class="submit"><p class="btn"><?php echo $this->Form->submit('ログイン', array('div' => false)); ?></p></div>
			</div>
		</div>
		<?php echo $this->Form->end(); ?>
