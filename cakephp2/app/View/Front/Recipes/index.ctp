		<p class="topicPath"><a href="<?php echo $this->Html->url('/', true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span>レシピ
			<?php echo $this->Session->flash(); ?>
			<h1><img src="<?php echo $this->Html->url('/', true); ?>img/recipe/h1.png" alt="レシピ" width="89" height="23" /></h1>
			<div id="primary" class="bread">
				<div class="mainImg"><?php
					$baseurl = $this->Html->url('/', true);
					$url = $baseurl.'inc/recipe.html';
					$bnr = file_get_contents($url);
					echo $bnr;
					?></div>
					<?php echo $this->element('Front/Recipe/searchbox'); ?>
					<?php echo $this->element('Front/Recipe/front_category_box'); ?>

					<div class="box" id="newBox">
						<h2><img src="<?php echo $this->Html->url('/', true); ?>img/recipe/h2-newBox.png" alt="新着レシピ" width="680" height="72" /></h2>
						<?php $i=0; ?>
						<?php foreach($data as $k => $v): ?>
							<div class="box">
								<div class="innerBox clearfix">
									<p class="f_right">
										<?php if(!empty($v['RecipeImage'][$i]['uploadpath'])): ?>
											<a href="<?php echo $this->Html->url("./recipe/{$v['Recipe']['id']}", true); ?>"><img src="<?php echo $this->Common->get_thumbs_path($v['RecipeImage'][0]['uploadpath']); ?>" alt="" width="110" /></a>
										<?php else: ?>
											<a href="<?php echo $this->Html->url("./recipe/{$v['Recipe']['id']}", true); ?>"><img src="<?php echo $this->Html->url('/img/common/nowprinting.jpg'); ?>" alt="" width="110" /></a>
										<?php endif; ?>
									</p>
									<?php $limitday = date('Y-m-d H:i:s', strtotime($v['Recipe']['created'].' +30 day')); ?>
									<?php if($today < $limitday): ?>
										<h3 class="new"><a href="<?php echo $this->Html->url("./recipe/{$v['Recipe']['id']}", true); ?>"><?php echo h($v['Recipe']['title']); ?></a></h3>
									<?php else: ?>
										<h3><a href="<?php echo $this->Html->url("./recipe/{$v['Recipe']['id']}", true); ?>"><?php echo h($v['Recipe']['title']); ?></a></h3>
									<?php endif; ?>
									<p><?php echo $this->Common->_fh($v['Recipe']['description']); ?></p>
									<p class="btn-detail"><a href="<?php echo $this->Html->url("./recipe/{$v['Recipe']['id']}", true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/common/btn-detail.png" alt="<?php echo h($v['Recipe']['title']); ?>" width="106" height="25" /></a></p>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
