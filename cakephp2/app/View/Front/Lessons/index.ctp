<p class="topicPath"><a href="<?php echo $this->Html->url('/', true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span>パン・お菓子教室</p>
<h1><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/h1.png" alt="パン・お菓子教室" width="162" height="23" /></h1>
<div id="primary">
	<div class="mainImg">
		<?php
			$url = $this->Html->url('/', true) . 'inc/lesson.html';
			$bnr = file_get_contents($url);
			echo $bnr;
		?>
	</div>

	<?php echo $this->Session->flash(); ?>

	<?php echo $this->element('Front/Lesson/schedule_box'); ?>

	<div class="box" id="calendarBox">
		<h2><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/h2-calendar.png" width="680" height="60" alt="パン・お菓子教室カレンダー" /></h2>
		<div id="caleNav" class="localNav clearfix">
			<p><?php echo date('Y年n月'); ?></p>
			<p><?php echo date('Y年n月', strtotime(date('Y-m-1').' +1 month')); ?></p>
			<p><?php echo date('Y年n月', strtotime(date('Y-m-1').' +2 month')); ?></p>
		</div>
		<div id="calendarslideBox">
			<div id="calendarslideBody">
				<?php
					$this->Thcalendar->prevMonth = 0;
					$this->Thcalendar->nextMonth = 2;
					$this->Thcalendar->holidayCheck = false;
					$this->Thcalendar->dayText = $thismt;

					echo $this->Thcalendar->calender();
				?>
			</div>
		</div>

		<!-- nav -->
	</div>
</div>
