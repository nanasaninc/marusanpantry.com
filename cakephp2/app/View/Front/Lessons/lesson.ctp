	<p class="topicPath"><a href="<?php echo $this->Html->url('/', true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><a href="../">パン・お菓子教室</a><span>&gt;</span><?php echo $this->Common->_fh($data['Lesson']['title']); ?></p>

	<h1><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/h1.png" alt="パン・お菓子教室" width="162" height="23" /></h1>
	<div id="primary" class="<?php echo $icons[$data['Lesson']['category']]; ?>">
		<div class="box">
			<div id="photoBox">
				<?php if(!empty($data['LessonImage'][0]['uploadpath'])): ?>
					<p><img src="<?php echo $this->Common->get_thumbs_path($data['LessonImage'][0]['uploadpath']); ?>" alt="" width="200" /></p>
				<?php else: ?>
					<p><img src="<?php echo $this->Html->url('/', true); ?>img/common/nowprinting.jpg" alt="" width="200" /></p>
				<?php endif; ?>

				<?php if(!empty($data['LessonImage'][1]['uploadpath'])): ?>
					<p><img src="<?php echo $this->Common->get_thumbs_path($data['LessonImage'][1]['uploadpath']); ?>" alt="" width="200" /></p>
				<?php endif; ?>
			</div>
			<div id="contentBox">
				<h2><?php echo $this->Common->_fh($data['Lesson']['title']); ?></h2>
				<p class="comment"><?php echo $this->Common->_fh($data['Lesson']['description']); ?></p>
				<dl>
					<dt>講師</dt>
					<dd><?php echo $this->Common->_fh($data['Lesson']['teacher']); ?>　</dd>
					<dt>開催時間</dt>
					<dd><?php echo $this->Common->_fh($data['Lesson']['morning']); ?>　</dd>
					<dt>参加費</dt>
					<dd><?php echo str_replace("\\", '&yen;', $this->Common->_fh($data['Lesson']['price'])); ?>　</dd>
					<dt>持参品</dt>
					<dd><?php echo $this->Common->_fh($data['Lesson']['jisan']); ?>　</dd>
					<dt>定員</dt>
					<dd><?php echo $this->Common->_fh($data['Lesson']['memberlimit']); ?>　</dd>
					<dt>備考</dt>
					<dd><?php echo $this->Common->_fh($data['Lesson']['bikou']); ?>　</dd>
				</dl>
				<h3><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/h2-list.png" alt="開催日程・お申込" width="350" height="48" /></h3>
				<table>
					<tr>
						<th>開催日</th>
						<th>開催時間</th>
						<th>予約</th>
						<th>申し込み</th>
					</tr>
					<?php foreach($data['LessonDate'] as $k => $v): ?>
						<?php
						$kigous = $this->Common->limitdisplay($v['limit'], $v['capacity']);
						$weeks = $this->Jaweek->weekname($v['date']);
						$today = date('Y-m-d');

						$diffday = date('Y-m-d', strtotime($today.' +1 day'));
						?>
						<tr>
							<td><?php echo nl2br(h(date('Y年n月j日', strtotime($v['date'])))).'('.$weeks.')'; ?></td>
							<td><?php echo nl2br(h($v['datetxt'])); ?></td>
							<td><?php echo $this->Common->limitdisplay($v['limit'], $v['capacity']); ?></td>
							<td>
								<?php echo $this->Form->create('', array(
								'url' => 'reserve')), PHP_EOL; ?>

								<?php echo $this->Form->hidden("Reserve.id", array(
									'value' => $this->Common->_fh($data['Lesson']['id'])
									)), PHP_EOL; ?>
								<?php echo $this->Form->hidden("Reserve.date_id", array(
									'value' => $this->Common->_fh($v['id'])
									)), PHP_EOL; ?>
								<?php echo $this->Form->hidden("Reserve.title", array(
									'value' => $this->Common->_fh($data['Lesson']['title'])
									)), PHP_EOL; ?>
								<?php echo $this->Form->hidden("Reserve.date", array(
									'value' => nl2br(h(date('Y年n月j日', strtotime($v['date']))) . '(' . $weeks . ')')
									)); ?>
								<?php echo $this->Form->hidden("Reserve.datetxt", array(
									'value' => $this->Common->_fh($v['datetxt'])
									)), PHP_EOL; ?>
									<?php if($kigous == '×'): ?>
										満席
									<?php else: ?>
										<?php
										if(strtotime($v['date']) <= strtotime($today)): ?>
										申し込み終了
									<?php else: ?>
										<?php echo $this->Form->submit('申し込み', array('name' => 'data[Reserve][mode]', 'div' => false)), PHP_EOL; ?>
									<?php endif; ?>
								<?php endif; ?>
								<?php echo $this->Form->end(), PHP_EOL; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
		<?php echo $this->element('Front/Lesson/schedule_box'); ?>
	</div>
