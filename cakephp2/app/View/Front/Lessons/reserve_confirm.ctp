<p class="topicPath"><a href="<?php echo $this->Html->url('/', true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><a href="./">パン・お菓子教室</a><span>&gt;</span>参加申込み</p>
		<h1><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/h1.png" alt="パン・お菓子教室" width="162" height="23" /></h1>
		<div id="primary">
			<div id="reserveBox" class="box">
				<h2><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/reserve/h2-reserve.png" width="960" height="60" /></h2>
				<div id="photoBox">
					<?php if(!empty($dataphoto['LessonImage'][0]['uploadpath'])): ?>
					<?php $path = str_replace('.jpg', '_th.jpg', $dataphoto['LessonImage'][0]['uploadpath']); ?>
					<p><img src="<?php echo $this->Html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="" width="260" /></p>
					<?php else: ?>
					<p><img src="<?php echo $this->Html->url('/', true); ?>/img/common/nowprinting.jpg" alt="" width="200" /></p>
					<?php endif; ?>
					<?php if(!empty($dataphoto['LessonImage'][1]['uploadpath'])): ?>
					<?php $path = str_replace('.jpg', '_th.jpg', $dataphoto['LessonImage'][1]['uploadpath']); ?>
					<p><img src="<?php echo $this->Html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="" width="260" /></p>
					<?php endif; ?>
				</div>
				<div id="contentBox">
					<?php echo $this->Form->create('', array('url' => 'reserve')), PHP_EOL; ?>
					<dl>
						<dt>参加コース名</dt>
						<dd><?php echo h($data['Reserve']['title']); ?></dd>
						<dt>日付</dt>
						<dd><?php echo h($data['Reserve']['date']); ?></dd>
						<dt>時間</dt>
						<dd><?php echo h($data['Reserve']['datetxt']); ?></dd>
						<dt>氏名</dt>
						<dd><?php echo h($data['Reserve']['name']); ?></dd>
					<dt>電話番号</dt>
					<dd>
						<?php echo h($data['Reserve']['phone']); ?>
					</dd>
					<dt>申し込み人数</dt>
					<dd>
						<?php echo h($data['Reserve']['active']); ?>
					</dd>
					<dt>メールアドレス</dt>
					<dd>
						<?php echo h($data['Reserve']['email']); ?>
					</dd>
					<dt>メールアドレス確認</dt>
					<dd>
						<?php echo h($data['Reserve']['mailconfirm']); ?>
					</dd>
					<?php if($data['Reserve']['syokai'] == 1): ?>
					<dt>初回申し込み</dt>
					<dd>初回申し込みです。</dd>
					<?php else: ?>
					<?php endif; ?>
					<dt>会員No</dt>
					<dd>
						　<?php echo h($data['Reserve']['membernum']); ?>
					</dd>
					<dt>備考</dt>
					<dd>
						　<?php echo nl2br(h($data['Reserve']['comment'])); ?>
					</dd>
					</dl>
					<p>上記内容で参加申し込みします。よろしければ下のボタンをクリックして下さい。</p>
					<?php echo $this->Form->submit('申込む', array('name' => 'data[Reserve][mode]', 'div' => false, 'class' => "btn-submit")), PHP_EOL; ?>
					<?php echo $this->Form->end(), PHP_EOL; ?>
				</div>
			</div>
		</div>
		<div id="secondary"></div>
	</div>
</div>
