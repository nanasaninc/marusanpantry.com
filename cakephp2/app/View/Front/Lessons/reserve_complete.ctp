<p class="topicPath"><a href="<?php echo $this->Html->url('/', true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><a href="./">パン・お菓子教室</a><span>&gt;</span>予約申込み</p>
<h1><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/h1.png" alt="パン・お菓子教室" width="162" height="23" /></h1>
<div id="primary">
	<div id="reserveBox" class="box">
		<p>パン・お菓子教室のご予約申し込みありがとうございます。</p>
		<h2><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/reserve/h2-reserve.png" width="960" height="60" /></h2>
		<p>パン・お菓子教室の仮申し込みが完了しました。３日以内に予約状況を確認し「ご予約完了メール」をお送りいたします。<br />
			「ご予約完了メール」が届かない場合、何らかの原因で正しくメールが届いていない可能性があります。３日経過後もメールが届かない場合、大変お手数ですが【電話：089-931-1147】または【&#115;c&#104;ool&#64;m-&#109;a&#114;&#117;s&#97;n.&#99;o.&#106;&#112;】にご連絡いただきますようお願いいたします。</p>
			<div class="cautionBox">
				<h3><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/reserve/h3-caution.png" width="164" height="26" alt="ご注意下さい！" /></h3>
				<p>この画面はご予約の完了画面ではありません。<br />
					予約状況を確認後、改めて「ご予約完了メール」をお送りさせていただきます。<br />
					「ご予約完了メール」が届いた時点で正式にご予約完了となりますので、いましばらくお待ちくださいませ。</p>
				</div>
				<p class="link-back"><a href="../lessons/">パン・お菓子教室トップへ戻る</a></p>
			</div>
		</div>
		<div id="secondary"></div>
	</div>
</div>
