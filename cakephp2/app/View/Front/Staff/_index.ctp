<p class="topicPath"><a href="<?php echo $this->Html->url('/', true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><span>&gt;</span>スタッフ紹介</p>
		<h1><img src="<?php echo $this->Html->url('/', true); ?>img/staff/h1.png" alt="スタッフ紹介" width="122" height="22" /></h1>

		<div id="primary">
			<div class="box">
				<h2><img src="<?php echo $this->Html->url('/', true); ?>img/staff/h2-nishida.png" alt="西田伸子店長" width="680" height="60" /></h2>
				<p class="f_left"><img src="<?php echo $this->Html->url('/', true); ?>img/staff/photo-nishida.jpg" alt="写真：西田伸子店長" width="152" height="180" /></p>
				<div class="f_left">
					<dl>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-favor.png" alt="好きなお菓子orパン" width="110" height="23" /></dt>
						<dd>チョコ・クルミ･フルーツたっぷりのスイーツ</dd>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-hobby.png" alt="趣味" width="110" height="23" /> </dt>
						<dd>コンサート＆舞台鑑賞、ドライブ</dd>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-profile.png" alt="自己紹介" width="110" height="23" /> </dt>
						<dd>王子・車が大好き！はまりすぎて何かと多忙な店長・西田です。 <br />
							溺愛の愛犬に喜ばれるパンやおやつを作れるようになれたらいいな･･･。 <br />
							大切な家族に安心できる食材を提供できるよう頑張ります！！</dd>
					</dl>
				</div>
			</div>
			<div class="box">
				<h2><img src="<?php echo $this->Html->url('/', true); ?>img/staff/h2-wada.png" alt="和田真由美" width="680" height="60" /></h2>
				<p class="f_left"><img src="<?php echo $this->Html->url('/', true); ?>img/staff/photo-wada.jpg" alt="写真：和田真由美" width="152" height="180" /></p>
				<div class="f_left">
					<dl>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-favor.png" alt="好きなお菓子orパン" width="110" height="23" /></dt>
						<dd>マドレーヌ <br />
						ハード系のパン </dd>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-hobby.png" alt="趣味" width="110" height="23" /></dt>
						<dd>旅行、食べること、飲むこと。</dd>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-profile.png" alt="自己紹介" width="110" height="23" /></dt>
						<dd>パン、お菓子作り初心者なので勉強中です <br />
							でも作ることは大好きなので 作りだすと時間を忘れて作ってます <br />
							もっと勉強して皆様にいい提案ができるように頑張りマス！ <br />
						おいしいレシピなどあれば是非店頭で教えて下さい☆</dd>
					</dl>
				</div>
			</div>
			<div class="box">
				<h2><img src="<?php echo $this->Html->url('/', true); ?>img/staff/h2-tsuchimoto.png" alt="土元香苗 " width="680" height="60" /></h2>
				<p class="f_left"><img src="<?php echo $this->Html->url('/', true); ?>img/staff/photo-tsuchimoto.jpg" alt="写真：土元香苗 " width="152" height="180" /></p>
				<div class="f_left">
					<dl>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-favor.png" alt="好きなお菓子orパン" width="110" height="23" /></dt>
						<dd>シフォンケーキ・杏仁豆腐・クロワッサン・桃など･･･(^^) </dd>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-hobby.png" alt="趣味" width="110" height="23" /></dt>
						<dd>ショッピング・美容・料理 </dd>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-profile.png" alt="自己紹介" width="110" height="23" /></dt>
						<dd>ショッピングと美容に関することが大好きです♪  <br />
						料理も好きなので、たくさんの種類作れるように勉強中です！<br />
入社したてで、まだまだ分からないことがたくさんありますが、常に笑顔で接客したいです♪ </dd>
					</dl>
				</div>
			</div>
			<div class="box">
				<h2><img src="<?php echo $this->Html->url('/', true); ?>img/staff/h2-chihara.png" alt="茅原梨沙" width="680" height="60" /></h2>
				<p class="f_left"><img src="<?php echo $this->Html->url('/', true); ?>img/staff/photo-chihara.jpg" alt="写真：茅原梨沙" width="152" height="180" /></p>
				<div class="f_left">
					<dl>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-favor.png" alt="好きなお菓子orパン" width="110" height="23" /></dt>
						<dd>パウンドケーキ、チョコレート</dd>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-hobby.png" alt="趣味" width="110" height="23" /></dt>
						<dd>お菓子を作る、映画鑑賞</dd>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-profile.png" alt="自己紹介" width="110" height="23" /></dt>
						<dd>お菓子を作ることと食べることが大好きです。<br />
						パン作りはまだ挑戦したことがないので、これから学び、作っていきたいと思っています。もしよろしければ色々と教えてくださると助かります！<br />
						また、お話することも大好きなので、気軽にお声かけくださいね！</dd>
					</dl>
				</div>
			</div>
			<div class="box">
				<h2><img src="<?php echo $this->Html->url('/', true); ?>img/staff/h2-kondo.png" alt="近藤美由紀" width="680" height="60" /></h2>
				<p class="f_left"><img src="<?php echo $this->Html->url('/', true); ?>img/staff/photo-kondo.jpg" alt="写真：近藤美由紀" width="152" height="180" /></p>
				<div class="f_left">
					<dl>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-favor.png" alt="好きなお菓子orパン" width="110" height="23" /></dt>
						<dd>ベイクドチーズケーキ</dd>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-hobby.png" alt="趣味" width="110" height="23" /></dt>
						<dd>バドミントン、コーラス、韓流DVDを字幕で見ること</dd>
						<dt><img src="<?php echo $this->Html->url('/', true); ?>img/staff/icon-profile.png" alt="自己紹介" width="110" height="23" /></dt>
						<dd>主人、高校生の娘、中学生の息子の4人家族です。<br />
						お客様とお話させていただくのが楽しいですので、ご不明な点がありましたらお気軽に声をかけてくださいませ。</dd>
					</dl>
				</div>
			</div>
		</div>