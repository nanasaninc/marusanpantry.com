<script type="text/javascript">
	$(function(){
		$('img.specials').click(function(){
			var c = $(this).parent().siblings('.thCalender').children('p.special').toggle();
		});
	});
</script>
<style type="text/css">
	p.refine img{
		cursor:pointer;
	}
</style>
<div id="scheduleBox" class="box">
<h2><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/h2-schedule.png" width="680" height="70" /></h2>
	<div id="slideBox">
		<div id="slideBody">

			<?php if(!empty($tdata)): ?>
				<div class="monthbox clearfix heightAlign">
					<?php $i=0; ?>
					<?php foreach($tdata as $k => $v): ?>
						<dl class="<?php echo $icons[$v['Lesson']['category']]; ?>">
							<dt>
								<?php if(!empty($v['LessonImage'][$i]['uploadpath'])): ?>
									<a href="<?php echo $this->Html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><img src="<?php echo $this->Common->get_thumbs_path($v['LessonImage'][$i]['uploadpath']); ?>" alt="" width="60" /></a>
								<?php else: ?>
									<a href="<?php echo $this->Html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><img src="<?php echo $this->Html->url('/img/common/nowprinting.jpg'); ?>" alt="" width="60" /></a>
								<?php endif; ?></dt>
								<dd>
								<p><img src="<?php echo $this->Html->url('/', true); ?>img/common/icon-<?php echo $icons[$v['Lesson']['category']]; ?>.png" alt="" width="53" height="16" /></p>
									<ul>
										<?php foreach($v['LessonDate'] as $key => $val): ?>
											<li><?php echo date('n/j', strtotime($val['date'])); ?></li>
										<?php endforeach; ?>
									</ul>
									<p><strong><a href="<?php echo $this->Html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><?php echo $this->Common->_fh($v['Lesson']['title']); ?></a></strong><br /><?php echo $this->Common->get_top_lesson_title($v['Lesson']['description'] , 60); ?></p>
								</dd>
							</dl>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>


				<?php if(!empty($ndata)): ?>
					<div class="monthbox clearfix heightAlign">
						<?php $i=0; ?>
						<?php foreach($ndata as $k => $v): ?>
							<dl class="<?php echo $icons[$v['Lesson']['category']]; ?>">
								<dt>
									<?php if(!empty($v['LessonImage'][$i]['uploadpath'])): ?>
										<a href="<?php echo $this->Html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><img src="<?php echo $this->Common->get_thumbs_path($v['LessonImage'][$i]['uploadpath']); ?>" alt="" width="60" /></a>
									<?php else: ?>
										<a href="<?php echo $this->Html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><img src="<?php echo $this->Html->url('/img/common/nowprinting.jpg'); ?>" alt="" width="60" /></a>
									<?php endif; ?></dt>
									<dd>
									<p><img src="<?php echo $this->Html->url('/', true); ?>img/common/icon-<?php echo $icons[$v['Lesson']['category']]; ?>.png" alt="" width="53" height="16" /></p>
										<ul>
											<?php foreach($v['LessonDate'] as $key => $val): ?>
												<li><?php echo date('n/j', strtotime($val['date'])); ?></li>
											<?php endforeach; ?>
										</ul>
										<p><strong><a href="<?php echo $this->Html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><?php echo $this->Common->_fh($v['Lesson']['title']); ?></a></strong><br /><?php echo $this->Common->get_top_lesson_title($v['Lesson']['description'] , 60); ?></p>
									</dd>
								</dl>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>


					<?php if(!empty($nndata)): ?>
						<div class="monthbox clearfix heightAlign">
							<?php $i=0; ?>
							<?php foreach($nndata as $k => $v): ?>
								<dl class="<?php echo $icons[$v['Lesson']['category']]; ?>">
									<dt>
										<?php if(!empty($v['LessonImage'][$i]['uploadpath'])): ?>
											<a href="<?php echo $this->Html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><img src="<?php echo $this->Common->get_thumbs_path($v['LessonImage'][$i]['uploadpath']); ?>" alt="" width="60" /></a>
										<?php else: ?>
											<a href="<?php echo $this->Html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><img src="<?php echo $this->Html->url('/img/common/nowprinting.jpg'); ?>" alt="" width="60" /></a>
										<?php endif; ?></dt>
										<dd>
										<p><img src="<?php echo $this->Html->url('/', true); ?>img/common/icon-<?php echo $icons[$v['Lesson']['category']]; ?>.png" alt="" width="53" height="16" /></p>
											<ul>
												<?php foreach($v['LessonDate'] as $key => $val): ?>
													<li><?php echo date('n/j', strtotime($val['date'])); ?></li>
												<?php endforeach; ?>
											</ul>
											<p><strong><a href="<?php echo $this->Html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><?php echo $this->Common->_fh($v['Lesson']['title']); ?></a></strong><br /><?php echo $this->Common->get_top_lesson_title($v['Lesson']['description'] , 60); ?></p>
										</dd>
									</dl>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>

				<div id="scheNav" class="localNav clearfix">
					<?php if(!empty($tdata)): ?>
						<p><?php echo date('Y年n月'); ?></p>
					<?php endif; ?>
					<?php if(!empty($ndata)): ?>
						<p><?php echo date('Y年n月', strtotime(date('Y-m-1').' +1 month')); ?></p>
					<?php endif; ?>
					<?php if(!empty($nndata)): ?>
						<p><?php echo date('Y年n月', strtotime(date('Y-m-1').' +2 month')); ?></p>
					<?php endif; ?>
				</div>
			</div>