<?php
	$iconarray = array(
		'1' => 'special',
		'2' => 'bread',
		'3' => 'sweets',
		'4' => 'taiken',
	);
?>
	<div id="secondary">
		<div class="box">
			<h2><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/scd-h2-lesson.png" alt="今月の教室一覧" width="245" height="45" /></h2>
			<ul class="list">
				<?php if(!empty($tdata)): ?>
					<?php $i=0; ?>
					<?php foreach($tdata as $k => $v): ?>
						<li class="<?php echo $icons[$v['Lesson']['category']]; ?>"><a href="<?php echo $this->Html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><?php echo nl2br(h($v['Lesson']['title'])); ?></a></li>
					<?php endforeach; ?>
				<?php endif; ?>
			</ul>
		</div>
		<div class="box" id="flowBox">
			<h2><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/scd-h2-flow.png" alt="今月の教室一覧" width="245" height="45" /></h2>
			<ul>
				<li><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/scd-flow01.png" width="220" height="34" alt="参加したいコースを選びます" /></li>
				<li><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/scd-flow02.png" width="220" height="33" alt="参加申し込みをします"/></li>
				<li><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/scd-flow03.png" width="220" height="51" alt="当日は開始15分前にご来店下さい"/></li>
				<li><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/scd-flow04.png" width="220" height="33" alt="レジにて受付をお済ませ下さい"/></li>
				<li><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/scd-flow05.png" width="220" height="33" alt="パン・お菓子作り教室参加"/></li>
				<li><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/scd-flow06.png" width="220" height="16" alt="お疲れ様でした！"/></li>
			</ul>
			<p class="bnr"><a href="<?php echo $this->Html->url('/', true); ?>lessons/flow/"><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/bnr-flow.jpg" width="220" height="80" alt="リンク：ご参加の流れ" /></a></p>
		</div>
		<div class="box" id="teacherBox">
			<h2><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/scd-h2-teacher.png" alt="講師のご紹介" width="245" height="45" /></h2>
			<div class="innerBox">
				<p><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/scd-kikuchi.png" alt="菊池麻美（きくちまみ）" width="147" height="19" /></p>
				<p class="f_right"><img src="<?php echo $this->Html->url('/', true); ?>img/lesson/scd-h2-kikuchiPhoto.jpg" alt="写真：講師" width="97" height="98" /></p>
				<p>当店で13年にわたり、パン＆お菓子の講師を務めています。<br />
					「体に優しく、永く愛されるレシピ」を提案しており、女性に好評です。</p>
				</div>
			</div>
			<?php echo $this->element('Front/Bnrbox/bnr'); ?>
		</div>
	</div>
</div>