<div id="secondary">
	<div class="box">
	<h2><img src="<?php echo $this->Html->url('/', true); ?>img/recipe/scd-h2-category.png" alt="レシピカテゴリ一覧" width="245" height="45" /></h2>
	<h3><img src="<?php echo $this->Html->url('/', true); ?>img/recipe/scd-h3-bread.png" alt="パン" width="220" height="30" /> </h3>
		<ul class="list bread">
			<?php foreach($lists['breadlist'] as $k => $v): ?>
				<li><a href="<?php echo $this->Html->url("/recipes/category/{$v['id']}/"); ?>"><?php echo $v['name']; ?>(<?php echo $v['count']; ?>)</a></li>
			<?php endforeach; ?>
		</ul>
		<h3><img src="<?php echo $this->Html->url('/', true); ?>img/recipe/scd-h3-sweets.png" alt="お菓子" width="220" height="30" /> </h3>
		<ul class="list sweets">
			<?php foreach($lists['sweetlist'] as $k => $v): ?>
				<li><a href="<?php echo $this->Html->url("/recipes/category/{$v['id']}/"); ?>"><?php echo $v['name']; ?>(<?php echo $v['count']; ?>)</a></li>
			<?php endforeach; ?>
		</ul>
		<h3><img src="<?php echo $this->Html->url('/', true); ?>img/recipe/scd-h3-diff.png" alt="難易度" width="220" height="30" /> </h3>
		<ul class="list diff">
			<?php foreach($lists['difflist'] as $k => $v): ?>
				<li><a href="<?php echo $this->Html->url("/recipes/difficulty/{$v['id']}", true); ?>"><?php echo $v['name']; ?>(<?php echo $v['count']; ?>)</a></li>
			<?php endforeach; ?>
		</ul>
	</div>
	<?php echo $this->element('Front/Bnrbox/bnr'); ?>
</div>
</div>
</div>

