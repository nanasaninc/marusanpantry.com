<?php if(empty($bodyId)): ?>
	<?php $bodyId = 'sitemap'; ?>
<?php endif; ?>

<?php if($bodyId == 'recipe'): ?>
	<?php echo $this->element('Front/Common/scd-recipe'); ?>
<?php elseif($bodyId == 'lesson'): ?>
	<?php echo $this->element('Front/Common/scd-lesson'); ?>
<?php elseif($bodyId == 'privacy'): ?>
	<?php echo $this->element('Front/Common/scd-privacy'); ?>
<?php elseif($bodyId == 'sitemap'): ?>
	<?php echo $this->element('Front/Common/scd-sitemap'); ?>
<?php elseif($bodyId == 'welcome'): ?>
	<?php echo $this->element('Front/Common/scd-welcome'); ?>
<?php elseif($bodyId == 'staff'): ?>
	<?php echo $this->element('Front/Common/scd-staff'); ?>
<?php else: ?>
	<?php echo $this->element('Front/Common/scd-home'); ?>
<?php endif; ?>

<?php if($bodyId == 'reserve'): ?>
	<div id="footer">
		<div class="innerBox">
			<div class="innerBox">
			<p><img src="<?php echo $this->Html->url('/', true); ?>img/common/footer-logo.png" width="244" height="58" alt="株式会社松山丸三" /></p>
				<dl>
					<dt>マルサンパントリー/パン・お菓子教室</dt>
					<dd>愛媛県松山市花園町6-1<br />
						TEL:089-931-1147<br />
						営業時間：9:00〜18:00　定休日：日・祝日</dd>
					</dl>
					<dl>
						<dt>キッチンショップ</dt>
						<dd>愛媛県松山市三番町5-8-13<br />
							TEL:089-931-4161<br />
							営業時間：8:45〜17:30　定休日：土曜・日曜・年末年始</dd>
						</dl>
					</div>
				</div>
			</div>
		</body>
		</html>
	<?php else: ?>
		<div id="footer">
			<div class="innerBox">
			<p id="footerNav"><a href="<?php echo $this->Html->url('/', true); ?>">ホーム</a>｜<a href="<?php echo $this->Html->url('/welcome/', true); ?>">ようこそ</a>｜<a href="<?php echo $this->Html->url('/lessons/', true); ?>">パン・お菓子教室</a>｜<a href="<?php echo $this->Html->url('/pantry/', true); ?>">マルサンパントリー</a>｜<a href="<?php echo $this->Html->url('/kitchenshop/', true); ?>">キッチンショップ</a>｜<a href="<?php echo $this->Html->url('/recipes/', true); ?>">レシピ</a>｜<a href="http://www.m-pantry.com/blog/" target="_blank">スタッフブログ</a>｜<a href="<?php echo $this->Html->url('/privacy/', true); ?>">プライバシーポリシー</a>｜<a href="<?php echo $this->Html->url('/sitemap/', true); ?>">サイトマップ</a></p>
			<p><img src="<?php echo $this->Html->url('/', true); ?>img/common/footer-logo.png" width="244" height="58" alt="株式会社松山丸三" /></p>
				<dl>
					<dt>マルサンパントリー/パン・お菓子教室</dt>
					<dd>愛媛県松山市花園町6-1<br />
						TEL:089-931-1147<br />
						営業時間：8:45〜18:00　定休日：日・祝日</dd>
					</dl>
					<dl>
						<dt>キッチンショップ</dt>
						<dd>愛媛県松山市三番町5-8-13<br />
							TEL:089-931-4161<br />
							営業時間：8:45〜17:30　定休日：土曜・日曜・年末年始</dd>
						</dl>
					</div>
				</div>
				<script>
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

				ga('create', 'UA-90671592-1', 'auto');
				ga('send', 'pageview');

				</script>
			</body>
			</html>
		<?php endif; ?>