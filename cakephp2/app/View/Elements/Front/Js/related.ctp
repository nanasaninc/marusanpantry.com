<script type="text/javascript">
	$(function(){

		$('select[name="bigcate"]').change(function(){
			var v = $(this).val();
		});

		$('a.gets').click(function(e){
			var _self = $(this);
			var urls = $(this).prev().val();
			e.preventDefault();
			if(urls != ''){
				get_marusan_item(urls, _self);
			}
		});

	});

	function get_marusan_item(url, obj){
		$.post("<?php echo $this->Html->url('/scrapings/'); ?>",
			{
				urls: url
			},
			function(data){
				var values = $.parseJSON(data);
				if(false != values['status']){
					obj.parent().parent().prev().html(values['image']);
					obj.next().next('input.name').val(values['title']);
					obj.next().next().next().next('input.price').val(values['price'] + '(税抜)');
					obj.next().next().next().next().next('input').val(values['image']);
				}else{
					alert('取得に失敗しました。');
				}
			}
		);
	}
</script>
<style type="text/css">
	td.images img{
		width:90px;
	}
</style>