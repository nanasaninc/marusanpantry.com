		<p class="topicPath"><a href="<?php echo $this->Html->url('/', true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><a href="./">レシピ</a><?php echo $topicpath_name; ?></p>
		<?php echo $this->Session->flash(); ?>
		<h1><img src="<?php echo $this->Html->url('/', true); ?>img/recipe/h1.png" alt="レシピ" width="89" height="23" /></h1>
		<div id="primary" class="bread">
			<?php
			$bool = $this->Paginator->numbers();
			?>
			<div class="box" id="resultBox">
				<h2><img src="<?php echo $this->Html->url('/', true); ?>img/recipe/h2-listBox.png" alt="レシピ一覧" width="680" height="72" /></h2>
				<?php if(!empty($data)): ?>
					<?php if($bool != false): ?>
						<div class="pagination">
							<ul>
								<li class="prev"><?php echo $this->Paginator->prev('前へ') . PHP_EOL; ?></li>
								<?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => false)) . PHP_EOL; ?>
								<li class="next"><?php echo $this->Paginator->next('次へ') . PHP_EOL; ?></li>
							</ul>
						</div>
					<?php endif; ?>
				<?php endif; ?>

				<?php if(!empty($data)): ?>
					<?php $i=0; ?>
					<?php foreach($data as $k => $v): ?>
						<div class="box">
							<div class="innerBox clearfix">
								<p class="f_right">
									<?php if(!empty($v['RecipeImage'][$i]['uploadpath'])): ?></a>
										<a href="<?php echo $this->Html->url("./recipe/{$v['Recipe']['id']}", true); ?>"><img src="<?php echo $this->Common->get_thumbs_path($v['RecipeImage'][0]['uploadpath']); ?>" alt="" width="110" /></a>
									<?php else: ?>
										<a href="<?php echo $this->Html->url("./recipe/{$v['Recipe']['id']}", true); ?>"><img src="<?php echo $this->Html->url('/img/common/nowprinting.jpg'); ?>" alt="" width="110" /></a>
									<?php endif; ?></p>
									<?php $limitday = date('Y-m-d H:i:s', strtotime($v['Recipe']['created'].' +30 day')); ?>

									<?php if($today < $limitday): ?>
										<h3 class="new"><a href="<?php echo $this->Html->url("./recipe/{$v['Recipe']['id']}", true); ?>"><?php echo $this->Common->_fh($v['Recipe']['title']); ?></a></h3>
									<?php else: ?>
										<h3><a href="<?php echo $this->Html->url("./recipe/{$v['Recipe']['id']}", true); ?>"><?php echo $this->Common->_fh($v['Recipe']['title']); ?></a></h3>
									<?php endif; ?>
									<p><?php echo $this->Common->_fh($v['Recipe']['description']); ?></p>
									<p class="btn-detail"><a href="<?php echo $this->Html->url("./recipe/{$v['Recipe']['id']}", true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/common/btn-detail.png" alt="<?php echo h($v['Recipe']['title']); ?>" width="106" height="25" /></a></p>
								</div>
							</div>
						<?php endforeach; ?>
					<?php else: ?>
						<p>該当のデータが見つかりませんでした。</p>
					<?php endif; ?>

					<?php if(!empty($data)): ?>
						<?php if($bool != false): ?>
							<div class="pagination">
								<ul>
									<li class="prev"><?php echo $this->Paginator->prev('前へ').PHP_EOL; ?></li>
									<?php echo $this->Paginator->numbers(array('tag' => 'li', 'separator' => false)).PHP_EOL; ?>
									<li class="next"><?php echo $this->Paginator->next('次へ').PHP_EOL; ?></li>
								</ul>
							</div>
						<?php endif; ?>
					<?php endif; ?>
				</div>


				<?php echo $this->element('Front/Recipe/searchbox'); ?>
				<?php echo $this->element('Front/Recipe/front_category_box'); ?>


			</div>
