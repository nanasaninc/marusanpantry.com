			<div id="searchBox">
				<?php echo $this->Form->create(null, array('url' => 'search', 'type' => 'get')); ?>
				<h2><img src="<?php echo $this->Html->url('/', true); ?>img/recipe/h2-search.png" alt="レシピ検索" width="135" height="74" /></h2>
				<p>
					<label>
						<?php
						if(!isset($search_value['keyword'])){
							$search_value['keyword'] = '';
						}
						?>
						<?php echo $this->Form->text(null, array(
							'id'    => 'textfield',
							'class' => 'text',
							'name'  => 'keyword',
							'label' => false,
							'div'   => false,
							'value' => $search_value['keyword'],
							)); ?>
						</label>
					</p>
					<p class="btn-search"><?php echo $this->Form->submit('検索', array('name' => '', 'div' => false)), PHP_EOL; ?></p>
					<?php echo $this->Form->end(); ?>
				</div>
