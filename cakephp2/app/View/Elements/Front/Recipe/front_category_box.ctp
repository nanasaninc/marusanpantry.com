			<div class="box" id="categoryBox">
				<h2><img src="<?php echo $this->Html->url('/', true); ?>img/recipe/h2-category.png" alt="レシピカテゴリ一覧" width="680" height="71" /></h2>
				<div class="box">
					<h3><img src="<?php echo $this->Html->url('/', true); ?>img/recipe/h3-bread.png" width="200" height="30" /></h3>
					<ul class="list bread">
						<?php foreach($lists['breadlist'] as $k => $v): ?>
							<li><a href="<?php echo $this->Html->url("/recipes/category/{$v['id']}"); ?>"><?php echo $v['name']; ?>(<?php echo $v['count']; ?>)</a></li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="box">
					<h3><img src="<?php echo $this->Html->url('/', true); ?>img/recipe/h3-sweets.png" width="200" height="30" /></h3>
					<ul class="list sweets">
						<?php foreach($lists['sweetlist'] as $k => $v): ?>
							<li><a href="<?php echo $this->Html->url("/recipes/category/{$v['id']}"); ?>"><?php echo $v['name']; ?>(<?php echo $v['count']; ?>)</a></li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="box">
					<h3><img src="<?php echo $this->Html->url('/', true); ?>img/recipe/h3-diff.png" width="200" height="30" /></h3>
					<ul class="list diff">
						<?php foreach($lists['difflist'] as $k => $v): ?>
							<li><a href="<?php echo $this->Html->url("/recipes/difficulty/{$v['id']}", true); ?>"><?php echo $v['name']; ?>(<?php echo $v['count']; ?>)</a></li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>