
<script type="text/javascript">
	$(function(){
		// 表示状態更新
		$('select.active').change(function(e){
			var _self  = $(this);
			var ids    = _self.parents('label').find('.activeid').val();
			var values = _self.val();
			e.preventDefault();
			change_active(ids, values, _self);
		});

		// おすすめ設定
		$('input.recom').change(function(e){
			$("p.osusumecomp").remove();
			var recomlength = $('input.recom').length -1;
			var _self  = $(this);

			_self.after('<p id="recoms"><img src="<?php echo $this->Html->url('/'); ?>img/admin/recom.gif" /></p>');

			var ids    = _self.parents('label').find('.recommendid').val();
			e.preventDefault();
			change_recommend(ids, _self);
		});

		// 複製
		$('li.duplicates').click(function(){
			return confirm("コピーしてよろしいですか？");
		});

		// 削除
		$('li.deleteconfi').click(function(){
			return confirm("削除しますか？\nこの操作は取り消すことが出来ません。");
		});
	});

	function change_active(ids, values, obj){
		var bool;
		$.post(
			"<?php echo $this->Html->url('/adminlessons/active/'); ?>",
			{
				id:ids,
				value:values
			},
			function(json){
				var res = $.parseJSON(json);
				if (false != res.status){
					obj.after(res.message);
				}else{
					obj.after(res.message);
				}
			}
		);
	}
	function change_recommend(ids, obj){
		var bool;
		$.post(
			"<?php echo $this->Html->url('/adminlessons/recom/'); ?>",
			{
				id:ids
			},
			function(json){
				$('#recoms').remove();
				var res = $.parseJSON(json);
				if (false != res.status){
					obj.after(res.message);
				}else{
					obj.after(res.message);
				}
			}
		);
	}
</script>
