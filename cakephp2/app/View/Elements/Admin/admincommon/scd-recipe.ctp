		<div id="secondary">
			<p class="btn">
				<a href="<?php echo $this->Html->url("/adminrecipes/add/"); ?>">レシピ新規登録</a>
			</p>
			<div class="wbox">
				<h2>レシピカテゴリ</h2>
				<ul>
					<li><a href="<?php echo $this->Html->url("/adminrecipes/index/10000"); ?>">パン(<?php echo $cat['breadnum']; ?>)</a>
						<ul>
							<?php foreach($lists['breadlist'] as $k => $v): ?>
								<li><a href="<?php echo $this->Html->url("/adminrecipes/index/{$v['id']}/"); ?>"><?php echo h($v['name']); ?>(<?php echo $v['count']; ?>)</a></li>
							<?php endforeach; ?>
						</ul>
					</li>
					<li><a href="<?php echo $this->Html->url("/adminrecipes/index/20000"); ?>">お菓子(<?php echo $cat['sweetnum']; ?>)</a>
						<ul>
							<?php foreach($lists['sweetlist'] as $k => $v): ?>
								<li><a href="<?php echo $this->Html->url("/adminrecipes/index/{$v['id']}/"); ?>"><?php echo h($v['name']); ?>(<?php echo $v['count']; ?>)</a></li>
							<?php endforeach; ?>
						</ul>
					</li>
				</ul>
				<div class="searchBox">
					<?php
						if(!isset($searchword)){
							$searchword = '';
						}
					?>
					<?php echo $this->Form->create(null, array('url' => 'search')), PHP_EOL; ?>
					<?php echo $this->Form->input(null, array(
						'type' => 'text',
						'name' => 'keyword',
						'label' => false,
						'class' => 'searchTxt',
						'value' => $searchword,
					)), PHP_EOL; ?>
					<?php echo $this->Form->submit('検索', array('div' => false, 'name' => 'data[Recipe][mode]')), PHP_EOL; ?>
					<?php echo $this->Form->end(), PHP_EOL; ?>
				</div>
			</div>
		</div>
	</div>
