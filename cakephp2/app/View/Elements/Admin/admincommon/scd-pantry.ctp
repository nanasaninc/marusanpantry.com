		<div id="secondary">
			<p class="btn">
				<a href="<?php echo $this->Html->url("/adminpantries/add/"); ?>">パントリーイチオシ新規登録</a>
			</p>
			<div class="wbox">
				<h2>パントリー検索</h2>

				<div class="searchBox">
					<?php
						if(!isset($searchword)){
							$searchword = '';
						}
					?>
					<?php echo $this->Form->create(null, array('url' => 'search')), PHP_EOL; ?>
						<?php echo $this->Form->input('keyword', array(
							'type'  => 'text',
							'name'  => 'keyword',
							'label' => false,
							'class' => 'searchTxt',
							'value' => $searchword
						)), PHP_EOL; ?>
						<?php echo $this->Form->submit('検索', array('div' => false, 'name' => 'data[Pantry][mode]')), PHP_EOL; ?>
					<?php echo $this->Form->end(), PHP_EOL; ?>
				</div>
			</div>
		</div>
	</div>
