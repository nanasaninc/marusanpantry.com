<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Script-Type" content="javascript" />
<title>マルサンパントリー　ウェブサイト管理</title>
<link href="<?php echo $this->Html->url('/', true); ?>css/admin.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo $this->Html->url('/', true); ?>css/ui-lightness/jquery-ui-1.8.17.custom.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo $this->Html->url('/', true); ?>js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="<?php echo $this->Html->url('/', true); ?>js/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $this->Html->url('/', true); ?>js/jquery.ui.datepicker-ja.js"></script>
<script type="text/javascript" src="<?php echo $this->Html->url('/', true); ?>js/jquery.formtips.1.2.5.packed.js"></script>
<script type="text/javascript" src="<?php echo $this->Html->url('/', true); ?>js/admin.js"></script>
<script type="text/javascript" src="<?php echo $this->Html->url('/', true); ?>js/meca.js"></script>
<script>
	$(function() {
		$( ".datepicker" ).datepicker();
		$('form input, form textarea').formtips({
			tippedClass: 'tipped'
		});
	});
</script>
</head>
<body id="<?php echo $bodyId; ?>">
<div id="wrapper">
	<div id="header">
		<p id="logo"><img src="<?php echo $this->Html->url('/', true); ?>img/admin/logo.png" alt="マルサンパントリー　ウェブサイト管理" width="399" height="45" /></p>
		<p class="btn-logout"><a href="<?php echo $this->Html->url('/', true); ?>users/logout/"><img src="<?php echo $this->Html->url('/', true); ?>img/admin/btn-logout.png" alt="ログアウト" width="100" height="30" class="btn" /></a></p>
		<div id="globalNav" class="clearfix">
			<ul>
				<li><a href="<?php echo $this->Html->url('/admins/', true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/admin/gnav-home.png" alt="リンク：トップページ" width="107" height="32" /></a></li>
				<li><a href="<?php echo $this->Html->url('/adminrecipes/', true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/admin/gnav-recipe.png" alt="リンク：レシピ管理" width="107" height="32" /></a></li>
				<li><a href="<?php echo $this->Html->url('/adminlessons/', true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/admin/gnav-lesson.png" alt="リンク：教室管理" width="107" height="32" /></a></li>
				<li><a href="<?php echo $this->Html->url('/kitchenshop/', true); ?><?php //echo $wordpress['kitchenshop']; ?>" target="_blank"><img src="<?php echo $this->Html->url('/', true); ?>img/admin/gnav-kitcheninfo.png" alt="リンク：キッチンショップお知らせ" width="107" height="32" /></a></li>
				<li><a href="<?php echo $this->Html->url('/adminrecommends/', true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/admin/gnav-kitchen.png" alt="リンク：キッチンショップイチオシ管理" width="107" height="32" /></a></li>
				<li><a href="<?php echo $this->Html->url('/pantry/', true); ?><?php //echo $wordpress['pantry']; ?>" target="_blank"><img src="<?php echo $this->Html->url('/', true); ?>img/admin/gnav-pantryinfo.png" alt="リンク：パントリーイチオシ管理" width="107" height="32" /></a></li>
				<li><a href="<?php echo $this->Html->url('/adminpantries/', true); ?>"><img src="<?php echo $this->Html->url('/', true); ?>img/admin/gnav-pantry.png" alt="リンク：パントリーお知らせ" width="107" height="32" /></a></li>
			</ul>
		</div>
	</div>
	<div id="container">