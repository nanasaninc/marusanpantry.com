<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Script-Type" content="javascript" />
<title>マルサンパントリー｜マルサンパントリー</title>
<link href="<?php site_urls(); ?>css/common.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php site_urls(); ?>css/jquery.fancybox-1.3.1.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="<?php site_urls(); ?>js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="<?php site_urls(); ?>js/common.js"></script>
<script type="text/javascript" src="<?php site_urls(); ?>js/meca.js"></script>
<script type="text/javascript" src="<?php site_urls(); ?>js/scrolltopcontrol.js"></script>
<script type="text/javascript" src="<?php site_urls(); ?>js/jquery.fancybox-1.3.1.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$(".fancybox").fancybox();
	$(".fancymap").fancybox();
});
</script>
</head>
<body id="pantry">
<div id="header">
	<div class="innerBox">
		<p id="logo"><a href="<?php site_urls(); ?>"><img src="<?php site_urls(); ?>img/common/logo.png" width="328" height="67" alt="お菓子で笑顔をつくる　マルサンパントリー" /></a></p>
		<div class="box">
			<ul id="headerNav">
				<li><a href="<?php site_urls(); ?>img/common/map.gif" class="fancymap"><img src="<?php site_urls(); ?>img/common/hNav-map.png" alt="" width="101" height="13" alt="リンク：マップ・駐車場" /></a></li>
				<li><a href="<?php site_urls(); ?>privacy/"><img src="<?php site_urls(); ?>img/common/hNav-privacy.png" width="137" height="13" alt="リンク：プライバシーポリシー" /></a></li>
				<li><a href="<?php site_urls(); ?>sitemap/"><img src="<?php site_urls(); ?>img/common/hNav-sitemap.png" alt="" width="92" height="13" alt="リンク：サイトマップ" /></a></li>
			</ul>
			<p class="bnr-marusan"><a href="http://www.m-marusan.co.jp" target="_blank"><img src="<?php site_urls(); ?>img/common/bnr-marusan.png" width="198" height="65" /></a></p>
		</div>
		<ul id="globalNav">
			<li><a href="<?php site_urls(); ?>"><img src="<?php site_urls(); ?>img/common/gNav-home.png" alt="" width="60" height="44" alt="リンク：ホーム" /></a></li>
			<li><a href="<?php site_urls(); ?>welcome/"><img src="<?php site_urls(); ?>img/common/gNav-welcome.png" alt="" width="150" height="44" alt="リンク：ようこそ" /></a></li>
			<li><a href="<?php site_urls(); ?>lessons/"><img src="<?php site_urls(); ?>img/common/gNav-lesson.png" alt="" width="150" height="44" alt="リンク：パン・お菓子教室" /></a></li>
			<li><a href="<?php site_urls(); ?>pantry/"><img src="<?php site_urls(); ?>img/common/gNav-pantry.png" alt="" width="150" height="44" alt="リンク：マルサンパントリー" /></a></li>
			<li><a href="<?php site_urls(); ?>kitchenshop/"><img src="<?php site_urls(); ?>img/common/gNav-kitchen.png" alt="" width="150" height="44" alt="リンク：キッチンショップ" /></a></li>
			<li><a href="<?php site_urls(); ?>recipes/"><img src="<?php site_urls(); ?>img/common/gNav-recipe.png" alt="" width="150" height="44" alt="リンク：レシピ" /></a></li>
			<li><a href="http://shop.plaza.rakuten.co.jp/marusanpantry" target="_blank"><img src="<?php site_urls(); ?>img/common/gNav-blog.png" width="150" height="44" alt="リンク：スタッフブログ" /></a></li>
		</ul>
	</div>
</div>
<div id="wrapper">
	<div id="container">