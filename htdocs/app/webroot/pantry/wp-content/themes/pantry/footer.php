<div id="footer">
	<div class="innerBox">
		<div class="innerBox">
			<p id="footerNav"><a href="<?php site_urls(); ?>">ホーム</a>｜<a href="<?php site_urls(); ?>welcome/">ようこそ</a>｜<a href="<?php site_urls(); ?>lessons/">パン・お菓子教室</a>｜<a href="<?php site_urls(); ?>pantry/">マルサンパントリー</a>｜<a href="<?php site_urls(); ?>kitchenshop/">キッチンショップ</a>｜<a href="<?php site_urls(); ?>recipes/">レシピ</a>｜<a href="#">スタッフブログ</a>｜<a href="<?php site_urls(); ?>privacy/">プライバシーポリシー</a>｜<a href="<?php site_urls(); ?>sitemap/">サイトマップ</a></p>
			<p><img src="<?php site_urls(); ?>img/common/footer-logo.png" width="244" height="58" /></p>
			<dl>
				<dt>マルサンパントリー・スイートキッチン</dt>
				<dd>愛媛県松山市花園町6-1<br />
					TEL:089-931-1147<br />
					営業時間：9:00〜18:00　定休日：日・祝日</dd>
			</dl>
			<dl>
				<dt>キッチンショップ</dt>
				<dd>愛媛県松山市三番町5-8-13<br />
					TEL:089-931-4161<br />
					営業時間：9:00〜18:00　定休日：土・日・祝日</dd>
			</dl>
		</div>
	</div>
</div>
</body>
</html>