var EmojiDialog = {
	init : function(ed) {
		tinyMCEPopup.resizeToInnerSize();
	},

	insert : function(file) {
		var altname = file.slice(0,file.lastIndexOf('.'));
		var ed = tinyMCEPopup.editor, dom = ed.dom;
		ed.execCommand('mceInsertContent', false, dom.createHTML('img', {
			src : tinyMCEPopup.getWindowArg('plugin_url') + '/img/' + file,
			border : 0,
			alt : altname
		}));
	}
};

tinyMCEPopup.onInit.add(EmojiDialog.init, EmojiDialog);

var Emoji_pallet = {
	
	init : function() {
		var pallet_container = document.getElementById('emoji_pallet_container');
		var img_ary = [
            'sun.gif','cloud.gif','rain.gif','snow.gif','thunder.gif','typhoon.gif','mist.gif','sprinkle.gif','aries.gif','taurus.gif',
            'gemini.gif','cancer.gif','leo.gif','virgo.gif','libra.gif','scorpius.gif','sagittarius.gif','capricornus.gif','aquarius.gif','pisces.gif',
            'sports.gif','baseball.gif','golf.gif','tennis.gif','soccer.gif','ski.gif','basketball.gif','motorsports.gif','pocketbell.gif','train.gif',
            'subway.gif','bullettrain.gif','car.gif','rvcar.gif','bus.gif','ship.gif','airplane.gif','house.gif','building.gif','postoffice.gif',
            'hospital.gif','bank.gif','atm.gif','hotel.gif','24hours.gif','gasstation.gif','parking.gif','signaler.gif','toilet.gif','restaurant.gif',
            'cafe.gif','bar.gif','beer.gif','fastfood.gif','boutique.gif','hairsalon.gif','karaoke.gif','movie.gif','upwardright.gif','carouselpony.gif',
            'music.gif','art.gif','drama.gif','event.gif','ticket.gif','smoking.gif','nosmoking.gif','camera.gif','bag.gif','book.gif',
            'ribbon.gif','present.gif','birthday.gif','telephone.gif','mobilephone.gif','memo.gif','tv.gif','game.gif','cd.gif','heart.gif',
            'spade.gif','diamond.gif','club.gif','eye.gif','ear.gif','rock.gif','scissors.gif','paper.gif','downwardright.gif','upwardleft.gif',
            'foot.gif','shoe.gif','eyeglass.gif','wheelchair.gif','newmoon.gif','moon1.gif','moon2.gif','moon3.gif','fullmoon.gif','dog.gif',
            'cat.gif','yacht.gif','xmas.gif','downwardleft.gif','phoneto.gif','mailto.gif','faxto.gif','info01.gif','info02.gif','mail.gif',
            'by-d.gif','d-point.gif','yen.gif','free.gif','id.gif','key.gif','enter.gif','clear.gif','search.gif','new.gif',
            'flag.gif','freedial.gif','sharp.gif','mobaq.gif','one.gif','two.gif','three.gif','four.gif','five.gif','six.gif',
            'seven.gif','eight.gif','nine.gif','zero.gif','ok.gif','heart01.gif','heart02.gif','heart03.gif','heart04.gif','happy01.gif',
            'angry.gif','despair.gif','sad.gif','wobbly.gif','up.gif','note.gif','spa.gif','cute.gif','kissmark.gif','shine.gif',
            'flair.gif','annoy.gif','punch.gif','bomb.gif','notes.gif','down.gif','sleepy.gif','sign01.gif','sign02.gif','sign03.gif',
            'impact.gif','sweat01.gif','sweat02.gif','dash.gif','sign04.gif','sign05.gif','slate.gif','pouch.gif','pen.gif','shadow.gif',
            'chair.gif','night.gif','soon.gif','on.gif','end.gif','clock.gif','appli01.gif','appli02.gif','t-shirt.gif','moneybag.gif',
            'rouge.gif','denim.gif','snowboard.gif','bell.gif','door.gif','dollar.gif','pc.gif','loveletter.gif','wrench.gif','pencil.gif',
            'crown.gif','ring.gif','sandclock.gif','bicycle.gif','japanesetea.gif','watch.gif','think.gif','confident.gif','coldsweats01.gif','coldsweats02.gif',
            'pout.gif','gawk.gif','lovely.gif','good.gif','bleah.gif','wink.gif','happy02.gif','bearing.gif','catface.gif','crying.gif',
            'weep.gif','ng.gif','clip.gif','copyright.gif','tm.gif','run.gif','secret.gif','recycle.gif','r-mark.gif','danger.gif',
            'ban.gif','empty.gif','pass.gif','full.gif','leftright.gif','updown.gif','school.gif','wave.gif','fuji.gif','clover.gif',
            'cherry.gif','tulip.gif','banana.gif','apple.gif','bud.gif','maple.gif','cherryblossom.gif','riceball.gif','cake.gif','bottle.gif',
            'noodle.gif','bread.gif','snail.gif','chick.gif','penguin.gif','fish.gif','delicious.gif','smile.gif','horse.gif','pig.gif',
            'wine.gif','shock.gif'
			];
		var aImages		= img_ary ;
		var iCols		= 23;
		var iColWidth	= parseInt( 100 / iCols, 10 ) ;
		var iRows		= Math.ceil( aImages.length / iCols );
		var iColHeight	= parseInt( 500 / iRows, 10 ) ;
		var i = 0;
		var sTableHtml = '<table class="emoji" border="0" cellspacing="2" cellpadding="2" align="center">';
		while ( i < aImages.length) {
			sTableHtml += '<tr>';
			for(var j = 0 ; j < iCols ; j++) {
				if (aImages[i]) {
					var sFileName = aImages[i];
					sTableHtml += '<td width="' + iColWidth + '%" height="' + iColHeight + 'px" align="center" class="DarkBackground"  onmouseover="this.className=\'LightBackground\';" onmouseout="this.className=\'DarkBackground\';" onclick="EmojiDialog.insert(\'' + sFileName + '\');"><img src="img/' + sFileName + '" border="0" /></td>';
				} else {
					sTableHtml += '<td width="' + iColWidth + '%" height="' + iColHeight + 'px" align="center" class="DarkBackground"  onmouseover="this.className=\'LightBackground\';" onmouseout="this.className=\'DarkBackground\';">&nbsp;</td>';
				}
				i++;
			}
			sTableHtml += '</tr>';
		}
		sTableHtml += '</table>';
		pallet_container.innerHTML = sTableHtml;
	}
};