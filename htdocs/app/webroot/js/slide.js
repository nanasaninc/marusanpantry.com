	//スケジュールレッスンページ
	$(function(){
		var width = 640;
		var slideboxs = $('#scheduleBox #slideBox');
		var len = $('.monthbox', slideboxs).length;
		var box1 = $('.monthbox:eq(0)', slideboxs);
		var box2 = $('.monthbox:eq(1)', slideboxs);
		var box3 = $('.monthbox:eq(2)', slideboxs);
		var box1h = box1.height();
		var box2h = box2.height();
		var box3h = box3.height();
		var h = Math.max(box1h, box2h, box3h);
		var d = new Date();
		var days = d.getDate();
		var current = function(elm){
			elm.addClass('current');
		}
		days = parseInt(days);
		
		if(days > 15){
			$('#slideBody', slideboxs).css({marginLeft: "-640px", width: width*len});
			var elm = $('#scheNav p:eq(1)');
			current(elm);
		}else{
			$('#slideBody', slideboxs).css({marginLeft: "0px", width: width*len});
			var elm = $('#scheNav p:first-child');
			current(elm);
		}
		
		slideboxs.height(h);
		
		if(navigator.userAgent.indexOf('chrome' != -1) || navigator.userAgent.indexOf('safari' != -1)){
			slideboxs.height(h + 62);
		}
		
		box1.css({marginLeft: "0px"});
		box2.css({marginLeft: "640px"});
		box3.css({marginLeft: "1280px"});
		
		$('#scheNav p').click(function(){
			var index = $('#scheNav p').index(this);
			$('#slideBody', slideboxs).stop().animate({marginLeft: "-" + (index * width)}, 800, "easeOutQuart");
			$(this).siblings().removeClass();
			$(this).addClass('current');
		});
	});
	
	//カレンダー
	$(function(){
		var w = 640;
		var slidebox = $('#calendarBox #calendarslideBox');
		var leng = $('.calendarBox', slidebox).length;
		var calebox1 = $('.calendarBox:eq(0)', slidebox);
		var calebox2 = $('.calendarBox:eq(1)', slidebox);
		var calebox3 = $('.calendarBox:eq(2)', slidebox);
		var calebox1h = calebox1.height();
		var calebox2h = calebox2.height();
		var calebox3h = calebox3.height();
		var hi = Math.max(calebox1h, calebox2h, calebox3h);
		var d = new Date();
		var days = d.getDate();
		var current = function(elm){
			elm.addClass('current');
		}
		days = parseInt(days);
		
		
		if(days > 15){
			$('#calendarslideBody', slidebox).css({marginLeft: "-640px", width: w*leng});
			var elm = $('#caleNav p:eq(1)');
			current(elm);
		}else{
			$('#calendarslideBody', slidebox).css({marginLeft: "0px", width: w*leng});
			var elm = $('#caleNav p:first-child');
			current(elm);
		}
		
		slidebox.height(hi);
		
		calebox1.css({marginLeft: "0px"});
		calebox2.css({marginLeft: "640px"});
		calebox3.css({marginLeft: "1280px"});
		
		$('#caleNav p').click(function(){
			var index = $('#caleNav p').index(this);
			$('#calendarslideBody', slidebox).stop().animate({marginLeft: "-" + (index * w)}, 800, "easeOutQuart");
			$(this).siblings().removeClass();
			$(this).addClass('current');
		});
	});
	
	
	
$(function(){
	
	$('.tip').css('display', 'none');
	$('.tipwrap').hover(function(){
		var target = $(this).children('div.tip');
		var h = target.height();
		
		target.height(h).css('top', -h - 11);
		
		var cla = target.parent().parent().attr('class');
		
		if(cla == 'thCalenderTd thCalenderSunday sun'){
			target.stop(false, true).css('left', '-5px').fadeIn(500);
			target.parent().addClass('ltip');
		}else if(cla == 'thCalenderTd thCalenderSaturday sat'){
			target.stop(false, true).css('left', '-114px').fadeIn(500);
			target.parent().addClass('rtip');
		}else{
			target.stop(false, true).fadeIn(500);
		}
		
	}, function(){
		$(this).children('div.tip').stop(false, true).fadeOut(500);
		//$('#calebefore').remove();
	});
	
	$('.tip').hover(function(){
		$(this).stop(false, true).fadeIn(500);
	}, function(){
		$(this).stop(false, true).fadeOut(500);
	});
});