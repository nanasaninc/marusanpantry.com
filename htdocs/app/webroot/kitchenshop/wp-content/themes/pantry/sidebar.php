		<div id="secondary">
			<div class="box" id="shopBox">
				<h2><img src="<?php site_urls(); ?>img/kitchenshop/scd-h2-kitchenshop.png" alt="キッチンショップについて" width="245" height="45" /></h2>
				<div class="innerBox">
					<p><img src="<?php site_urls(); ?>img/kitchenshop/p-kitchenshop.jpg" width="210" height="126" /></p>
					<p>ご家庭でのお菓子やパン作りの型や道具が揃ったお店です。<br />
						また、プロ向けの業務用機械器具も豊富に揃っており、お取り寄せやオーダーなども可能です。<br />
					プロの方の出店図面計画や販促計画などのお手伝もさせていただきますので、お気軽にご相談下さい。</p>
				</div>
			</div>
			<div class="box" id="shopInfoBox">
				<h2><img src="<?php site_urls(); ?>img/common/scd-h2-info.png" alt="営業情報" width="245" height="45" /></h2>
				<div class="innerBox">
					<dl>
						<dt>住所</dt>
						<dd>〒790-0003<br />
							愛媛県松山市三番町5-8-13 [<a href="<?php site_urls(); ?>img/common/map.gif" class="fancymap">MAP</a>]</dd>
						<dt>TEL</dt>
						<dd>089-931-4161</dd>
						<dt>営業時間</dt>
						<dd>8:45-18:00</dd>
						<dt>定休日</dt>
						<dd>土曜・日曜・祝祭日・年末年始</dd>
					</dl>
					<p><img src="<?php site_urls(); ?>inc/img/kitchenshop/tcalendar.gif" alt="" width="210" height="151" alt="今月の営業日カレンダー" /></p>
					<p><img src="<?php site_urls(); ?>inc/img/kitchenshop/ncalendar.gif" alt="" width="210" height="151" alt="来月の営業日カレンダー" /></p>
				</div>
			</div>
			<div class="box" id="monthlyBox">
				<h2><img src="<?php site_urls(); ?>img/common/scd-h2-monthly.png" alt="過去の記事" width="245" height="45" /></h2>
				<ul class="def">
					<?php wp_get_archives(); ?>
				</ul>
			</div>
			<div class="box" id="categoryBox">
				<h2><img src="<?php site_urls(); ?>img/common/scd-h2-category.png" alt="カテゴリ" width="245" height="45" /></h2>
				<ul class="def">
					<?php wp_list_cats('sort_column=name&optioncount=0&hierarchical=1'); ?>
				</ul>
			</div>
			<div class="bnrBox">
			<?php
				$baseurl = site_urlss();
				$url = $baseurl.'inc/sidebnr.html';
				$bnr = file_get_contents($url);
				echo $bnr;
			?>
			</div>
		</div>
	</div>
</div>