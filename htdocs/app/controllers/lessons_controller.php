<?php
class LessonsController extends AppController{

	public $name = 'Lessons';
	public $uses = array('Lesson');
	public $components = array('Common', 'Qdmailer');
	public $helpers = array('Common', 'Thcalendar', 'Jaweek');
	
	public $autoRender = true;
	public $layout     = "common/siteframe";
	
	
	function beforeFilter(){
		$this->loadModel('LessonImage');
		$this->loadModel('LessonDate');
		$this->loadModel('Difficulty');
		$this->loadModel('Lessonicon');
		$this->loadModel('Lcategory');
		$this->loadModel('Reserve');
		
		$difficulty = $this->Difficulty->difficulty;
		$this->set('difficulty', $difficulty);
		
		$lessonicon = $this->Lessonicon->icons;
		$this->set('lessonicon', $lessonicon);
		
		$lcategory = $this->Lcategory->lcategory;
		$this->set('lcategory', $lcategory);
		
		$bodyId = 'lesson';
		$this->set('bodyId', $bodyId);
		
		$icons = array(
			'1' => 'special',
			'2' => 'bread',
			'3' => 'sweets',
			'4' => 'taiken',
		);
		$this->set('icons', $icons);
		
		
		//SELECT DISTINCT lesson_id FROM lesson_dates AS LessonDate WHERE DATE_FORMAT(`LessonDate`.`date`, '%Y-%m') = '2012-01' AND "conditions" => array("DATE_FORMAT(LessonDate.date, '%Y-%m')" => "2012-01"),, 'active' => 0, 'del_flg' => 0
		
		//今月
		$thismonth = date('Y-m');
		//来月
		$nextmonth = date('Y-m', strtotime(date('Y-m-1').' +1 month'));
		//再来月
		$nextnextmonth = date('Y-m', strtotime(date('Y-m-1').' +2 month'));
		
		$tdata     = $this->_getmonth($thismonth);
		$ndata     = $this->_getmonth($nextmonth);
		$nndata    = $this->_getmonth($nextnextmonth);
		
		$this->set(compact('tdata', 'ndata', 'nndata', 'thismonth', 'nextmonth', 'nextnextmonth'));
		
		
		//カレンダー
		$tdatas     = $this->_monthquery($thismonth);
		$ndatas     = $this->_monthquery($nextmonth);
		$nndatas    = $this->_monthquery($nextnextmonth);
		
		$tkey = $this->_toCalendar($thismonth);
		$nkey = $this->_toCalendar($nextmonth);
		$nnkey = $this->_toCalendar($nextnextmonth);
		
		$tkey = $this->_calandarData($tkey, $tdata, $tdatas);
		$nkey = $this->_calandarData($nkey, $ndata, $ndatas);
		$nnkey = $this->_calandarData($nnkey, $nndata, $nndatas);
		
		
		$thismt = am($tkey, $nkey, $nnkey);
		
		$this->set('thismt', $thismt);
		//カレンダー
	}
	
	
	//カレンダー生成用
	
	function _calandarData($keys, $data, $datas){
		if(!empty($keys)){
			$nums = count($datas);
			$counts = 0;
			foreach($datas as $k => $v){
				for($i=0;$i<$nums;$i++){
					if(!empty($data[$i]['Lesson']['id'])){
						if($v['LessonDate']['lesson_id'] == $data[$i]['Lesson']['id']){
							$ac = $data[$i]['Lesson']['active'];
							$del = $data[$i]['Lesson']['del_flg'];
							$category = $data[$i]['Lesson']['category'];
							$titles = $data[$i]['Lesson']['title'];
							break;
						}
					}
				}
				//if(!empty($category) && !empty($data[$i]['Lesson']['id']) && !empty($data[$i]['LessonImage'][0]['uploadpath'])){
				if(!empty($category) && !empty($data[$i]['Lesson']['id'])){
					//pr($data);
					if((int)$category === 1 && $ac == 0 && $del == 0){
						if(!empty($data[$i]['LessonImage'][0]['uploadpath'])){
							$images = str_replace('.jpg', '_th.jpg', $data[$i]['LessonImage'][0]['uploadpath']);
						}else{
							$images = 'img/common/nowprinting.jpg';
						}
						$keys[$v['LessonDate']['date']] .= '<div class="tipwrap"><p class="tips special"><img src="../img/common/icon-special.png" width="53" height="16" alt="特別教室" /></p><div class="tip"><div class="inner"><p class="clearfix"><span class="left"><a href="./lesson/'.$data[$i]['Lesson']['id'].'"><img src="../'.$images.'" width="50" /></a></span><span><img src="../img/common/icon-special.png" width="53" height="16" alt="特別教室" /><br /><a href="./lesson/'.$data[$i]['Lesson']['id'].'">'.$titles.'</a></span></p></div></div></div>';
					}elseif((int)$category === 2 && $ac == 0 && $del == 0){
						if(!empty($data[$i]['LessonImage'][0]['uploadpath'])){
							$images = str_replace('.jpg', '_th.jpg', $data[$i]['LessonImage'][0]['uploadpath']);
						}else{
							$images = 'img/common/nowprinting.jpg';
						}
						$keys[$v['LessonDate']['date']] .= '<div class="tipwrap"><p class="tips bread"><img src="../img/common/icon-bread.png" width="53" height="16" alt="パン教室" /></p><div class="tip"><div class="inner"><p class="clearfix"><span class="left"><a href="./lesson/'.$data[$i]['Lesson']['id'].'"><img src="../'.$images.'" width="50" /></a></span><img src="../img/common/icon-bread.png" width="53" height="16" alt="パン教室" /><br /><span><a href="./lesson/'.$data[$i]['Lesson']['id'].'">'.$titles.'</a></span></p></div></div></div>';
					}elseif((int)$category === 3 && $ac == 0 && $del == 0){
						if(!empty($data[$i]['LessonImage'][0]['uploadpath'])){
							$images = str_replace('.jpg', '_th.jpg', $data[$i]['LessonImage'][0]['uploadpath']);
						}else{
							$images = 'img/common/nowprinting.jpg';
						}
						$keys[$v['LessonDate']['date']] .= '<div class="tipwrap"><p class="tips sweets"><img src="../img/common/icon-sweets.png" width="53" height="16" alt="お菓子教室" /></p><div class="tip"><div class="inner"><p class="clearfix"><span class="left"><a href="./lesson/'.$data[$i]['Lesson']['id'].'"><img src="../'.$images.'" width="50" /></a></span><img src="../img/common/icon-sweets.png" width="53" height="16" alt="お菓子教室" /><br /><span><a href="./lesson/'.$data[$i]['Lesson']['id'].'">'.$titles.'</a></span></p></div></div></div>';
					}elseif((int)$category === 4 && $ac == 0 && $del == 0){
						if(!empty($data[$i]['LessonImage'][0]['uploadpath'])){
							$images = str_replace('.jpg', '_th.jpg', $data[$i]['LessonImage'][0]['uploadpath']);
						}else{
							$images = 'img/common/nowprinting.jpg';
						}
						$keys[$v['LessonDate']['date']] .= '<div class="tipwrap"><p class="tips taiken"><img src="../img/common/icon-taiken.png" width="53" height="16" alt="体験教室" /></p><div class="tip"><div class="inner"><p class="clearfix"><span class="left"><a href="./lesson/'.$data[$i]['Lesson']['id'].'"><img src="../'.$images.'" width="50" /></a></span><img src="../img/common/icon-taiken.png" width="53" height="16" alt="体験教室" /><br /><span><a href="./lesson/'.$data[$i]['Lesson']['id'].'">'.$titles.'</a></span></p></div></div></div>';
					}
				}
				$counts++;
			}
			
		}
		
		return $keys;
	}
	
	function _toCalendar($months){
		
		$tmonth = $this->_monthquery($months);
		$tkey = $this->_monthkey($tmonth);
		return $tkey;
	}
	function _monthkey($arrays){
		if(!empty($arrays)){
			foreach($arrays as $k => $v){
				$arr[] = $v['LessonDate']['date'];
			}
			$arr = array_unique($arr);
			sort($arr);
			
			foreach($arr as $key => $val){
				$df[$val] = ' ';
			}
			return $df;
		}
	}
	function _monthquery($mon){
		$month = $this->LessonDate->find('all', array(
				"conditions" => array("DATE_FORMAT(LessonDate.date, '%Y-%m')" => $mon),
				'fields'     => array(),
			)
		);
		return $month;
	}
	//カレンダー生成用
	
	
	
	function _getmonth($month){
	
		$this->Lesson->hasMany['LessonDate']['order'] = 'LessonDate.date ASC';
		
		$thismonths = $this->LessonDate->find('all', array(
				"conditions" => array("DATE_FORMAT(LessonDate.date, '%Y-%m')" => $month),
				'fields' => 'DISTINCT lesson_id',
			)
		);
		if(!empty($thismonths)){
			foreach($thismonths as $k => $v){
				$array[] = $v['LessonDate']['lesson_id'];
			}
		}else{
			$array = null;
		}
		$today = date('Y-m-d');
		$data = $this->Lesson->find('all', array(
				'conditions' => array('id' => $array, 'active' => 0, 'del_flg' => 0, "Lesson.limitday > '{$today}'"),
				'fields'     => array('id', 'category', 'title', 'description', 'active', 'del_flg'),
				'order'      => array('category' => 'ASC'),// !bookmark
			)
		);
		return $data;
	}
	
	
	
	
	function index(){
		
		$this->Lesson->hasMany['LessonImage']['fields'] = array('uploadpath');
		
		$meta['title'] = 'パン・お菓子教室｜マルサンパントリー';
		$meta['keyword'] = '丸三,マルサン,ケーキ材料, パン材料, 製菓材料, 製パン材料, お菓子材料,製菓道具, ケーキレシピ, 製パン道具, marusan, marusanpantry';
		$meta['description'] = 'お菓子材料、パン材料、器具道具、焼き型などが揃っています。';
		$this->set('meta', $meta);
		
	}
	function lesson($param = null){
		$today = date('Y-m-d');
		App::import('Sanitize');
		$param = Sanitize::clean($param);
		
		$this->Lesson->hasMany['LessonDate']['order'] = 'LessonDate.date ASC';
		
		$data = $this->Lesson->find('first', array(
				'conditions' => array(
					'Lesson.id' => $param,
					'active' => 0,
					'del_flg' => 0,
					"Lesson.limitday > '{$today}'"
				)
			)
		);
		if(!empty($data)){
			$this->set('data', $data);
		}else{
			$this->Session->setFlash('該当のデータはありませんでした。');
			$this->redirect('../lessons');
		}
		
		$meta['title'] = "{$data['Lesson']['title']}｜パン・お菓子教室｜マルサンパントリー";
		$meta['keyword'] = '丸三,マルサン,ケーキ材料, パン材料, 製菓材料, 製パン材料, お菓子材料,製菓道具, ケーキレシピ, 製パン道具, marusan, marusanpantry';
		$meta['description'] = 'お菓子材料、パン材料、器具道具、焼き型などが揃っています。';
		$this->set('meta', $meta);
		
	}
	
	function flow(){
	}
	
	function reserve(){
		$bodyId = 'reserve';
		$this->set('bodyId', $bodyId);
		
		//$today = date('Y-m-d');
		
		$fancybox = 'fancy';
		$this->set('fancy', $fancybox);
		
		$dataphoto = $this->Lesson->find('first', array(
				'conditions' => array('id' => $this->data['Reserve']['id']),
				'fields' => array(),
				'order'=>array()
			)
		);
		$this->set('dataphoto', $dataphoto);
		$dateid = $this->LessonDate->find('first', array(
				'conditions' => array('id' => $this->data['Reserve']['date_id']),
				'fields' => array(),
				'order'=>array()
			)
		);
		if(!empty($dateid)){
			for($i=1;$i<=$dateid['LessonDate']['capacity'];$i++){
				$capanum[$i] = $i;
			}
		}
		if(!empty($capanum)){
			$this->set('capanum', $capanum);
		}
		
		App::import('Sanitize');
		if(!empty($this->data)){
			
			$this->set('data', $this->data);
			
			if($this->data['Reserve']['mode'] === '申込み内容確認'){
				$token = $this->Common->_rand(12);
				$this->Session->write('token', $token);
				$this->data['Reserve']['token'] = $token;
				
				Configure::write('debug', 0);//ここだけDBがないのでバリデートが効かなくなる。
				$this->Reserve->set($this->data);
				if($this->Reserve->validates()){
					$this->render('reserve_confirm');
				}

			}else if($this->data['Reserve']['mode'] === '申込む'){
				if((string)$this->Session->read('token') === (string)$this->data['Reserve']['token']){
					unset($this->data['Reserve']['token']);
					$this->Session->delete('token');
					
					$datass = $this->LessonDate->find('first', array(
							'conditions' => array('id' => $this->data['Reserve']['date_id']),
							'fields' => array('id', 'capacity'),
							'order'=>array()
						)
					);
					
					$datass['LessonDate']['capacity'] = (int)$datass['LessonDate']['capacity'] - (int)$this->data['Reserve']['active'];
					if(!$this->LessonDate->save($datass)){
						
					}
					
					$hikimail = array(
						'title' => $this->data['Reserve']['title'],//教室名
						'date' => $this->data['Reserve']['date'],//日付
						'datetxt' => $this->data['Reserve']['datetxt'],//時間
						'name' => $this->data['Reserve']['name'],//氏名
						'phone' => $this->data['Reserve']['phone'],//電話番号
						'email' => $this->data['Reserve']['email'],//メールアドレス
						'capacity' => $this->data['Reserve']['active'],//申込み人数
						'syokai' => $this->data['Reserve']['syokai'],//初回申込み
						'membernum' => $this->data['Reserve']['membernum'],//会員No
						'comment' => $this->data['Reserve']['comment'],//備考
					);
					
					
					$this->_toClient($hikimail);
					$this->_toMarusan($hikimail);
					
					
					$this->render('reserve_complete');
					
					
					
				}else{
					$this->Session->setFlash('不正な操作が行われました');
					$this->redirect('../lessons');
				}
			}
		}else{
			$this->redirect('../lessons');
		}
		
		$meta['title'] = "参加申し込み｜{$this->data['Reserve']['title']}｜パン・お菓子教室｜マルサンパントリー";
		$meta['keyword'] = '丸三,マルサン,ケーキ材料, パン材料, 製菓材料, 製パン材料, お菓子材料,製菓道具, ケーキレシピ, 製パン道具, marusan, marusanpantry';
		$meta['description'] = 'お菓子材料、パン材料、器具道具、焼き型などが揃っています。';
		$this->set('meta', $meta);
	}
	
	function _toClient($array){
		if($array['syokai'] == 1){
			$array['syokai'] = '初回申込み:初回申込みの方です。';
		}else{
			$array['syokai'] = '';
		}
		
		$array['comment'] = str_replace('<br />', '', $array['comment']);
		
		$text = <<<EOL
このメールは自動返信メールとなります。 
こちらのメールアドレスにご返信されましても、確認できません。 
あらかじめご了承ください。

予約状況を確認後、改めて「ご予約完了メール」をお送りさせていただきます。
「ご予約完了メール」が届いた時点で正式にご予約完了となりますので、
いましばらくお待ちくださいませ。

{$array['name']}　様

この度はマルサンパントリー　【{$array['title']}】　への参加お申し込み、誠にありがとうございます。
以下の内容で参加お申し込みを受付いたしましたので、ご確認をお願いいたします。

参加教室：{$array['title']}
参加日：{$array['date']}
時間：{$array['datetxt']}
お名前：{$array['name']}
メールアドレス：{$array['email']}

申込み人数：{$array['capacity']}

お電話番号：{$array['phone']}

{$array['syokai']}

会員No.:{$array['membernum']}

備考--
{$array['comment']}



※入力内容に謝り・変更がある場合は、089-931-1147までお電話ください。


♪゜+.ｏ.+゜♪゜+.ｏ.+゜♪゜+.ｏ.+゜♪゜

　マルサンパントリー 　スイートキッチン

　school@m-marusan.co.jp
　www.marusanpantry.com
　〒790-0005 　愛媛県松山市花園町6-1
　TEL.089-931-1147  FAX.089-931-1148
　OPEN 8:45-18:00 ☆日曜・祝日定休

♪゜+.ｏ.+゜♪゜+.ｏ.+゜♪゜+.ｏ.+゜♪゜

EOL;

			$this->Qdmailer->to("{$array['email']}", "{$array['name']} 様");
			$this->Qdmailer->subject("【{$array['title']}】の教室へのお申し込みありがとうございます！ - マルサンパントリー");
			$this->Qdmailer->from("{$this->mailconfig['marusanmail']}", "{$this->mailconfig['companyname']}" );
			$this->Qdmailer->cakeText($text, 'lesson', 'email');
			$this->Qdmailer->send();
			
		}
		
		
		
		
		
		
		
		function _toMarusan($array){
		if($array['syokai'] == 1){
			$array['syokai'] = '初回申込み:初回申込みの方です。';
		}else{
			$array['syokai'] = '';
		}
		
		$array['comment'] = str_replace('<br />', '', $array['comment']);
		
			$text = <<<EOL
教室予約フォームより申込みがありました。
ご担当者様

【{$array['title']}】　教室予約フォームへのお申し込みがありました。
以下の内容でお申し込みを受付いたしましたので、ご確認をお願いいたします。

参加教室：{$array['title']}
参加日：{$array['date']}
時間：{$array['datetxt']}
お名前：{$array['name']}
メールアドレス：{$array['email']}

申込み人数：{$array['capacity']}

お電話番号：{$array['phone']}

{$array['syokai']}

会員No.:{$array['membernum']}

備考--
{$array['comment']}



以上ご確認よろしくお願い申し上げます。


♪゜+.ｏ.+゜♪゜+.ｏ.+゜♪゜+.ｏ.+゜♪゜

　マルサンパントリー 　スイートキッチン

　school@m-marusan.co.jp
　www.marusanpantry.com
　〒790-0005 　愛媛県松山市花園町6-1
　TEL.089-931-1147  FAX.089-931-1148
　OPEN 8:45-18:00 ☆日曜・祝日定休

♪゜+.ｏ.+゜♪゜+.ｏ.+゜♪゜+.ｏ.+゜♪゜

EOL;
		
		
			$this->Qdmailer->to("{$this->mailconfig['marusanmail']}", "{$this->mailconfig['companyname']}");
			$this->Qdmailer->subject("【{$array['title']}】の教室へのお申し込みがありました。 - マルサンパントリー");
			$this->Qdmailer->from("{$array['email']}", "教室予約【{$array['name']}様】");
			$this->Qdmailer->cakeText("{$text}", 'lesson','email');
			$this->Qdmailer->send();
		}
		
	/* 情報
	--------------------------*/
	public $mailconfig = array(
		'marusanmail' => 'school@m-marusan.co.jp',
		//'marusanmail' => 'd-nakayama@73web.net',
		'subject' => 'パン・お菓子教室へのお申し込みありがとうございます。 - マルサンパントリー',
		'companyname' => 'マルサンパントリー',
	);
	
	function _monthsearch($month){
		$thismonths = array("DATE_FORMAT(LessonDate.date, '%Y-%m')" => "{$month}");
		$thismonthss = array("DATE_FORMAT(Lesson.openmonth, '%Y-%m')" => "{$month}");
		$this->Lesson->hasMany['LessonDate']['conditions'] = $thismonths;
		
		$tdata = $this->Lesson->find('all', array(
				'conditions' => $thismonthss,
				'fields' => array(),
				'order'=>array()
			)
		);
		return $tdata;
	}
}
