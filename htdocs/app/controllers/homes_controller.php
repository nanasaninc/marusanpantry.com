<?php
class HomesController extends AppController{

	public $name = 'homes';
	public $uses = array('Lesson');
	//public $helpers = array('Thcalendar');
	
	public $autoRender = true;
	public $layout     = "common/siteframe";
	
	
	
	
	function beforeFilter(){
		$bodyId = 'home';
		$this->set('bodyId', $bodyId);
		
		$this->loadModel('LessonImage');
		$this->loadModel('LessonDate');
		$this->loadModel('Lessonicon');
		$this->loadModel('Lcategory');
		$this->loadModel('Wordpresspantry');
		$this->loadModel('Wordpresskitchen');
		
		
		//wordpress
		$pantry  = $this->Wordpresspantry->getpost();
		$kitchen = $this->Wordpresskitchen->getpost();
		
		$panarray = null;
		$i = 0;
		foreach($pantry as $k => $v){
			$panarray[$i]['title'] = $v['Wordpresspantry']['post_title'];
			$panarray[$i]['link'] = $v['Wordpresspantry']['guid'];
			$panarray[$i]['date'] = $v['Wordpresspantry']['post_date'];
			$i++;
		}
		$kitarray = null;
		$i = 0;
		foreach($kitchen as $k => $v){
			$kitarray[$i]['title'] = $v['Wordpresskitchen']['post_title'];
			$kitarray[$i]['link'] = $v['Wordpresskitchen']['guid'];
			$kitarray[$i]['date'] = $v['Wordpresskitchen']['post_date'];
			$i++;
		}
		
		$post = am($panarray, $kitarray);
		
		
		$date = null;
		$i = 0;
		foreach($post as $k => $v){
			$date[$k] = $v["date"];
		}
		array_multisort($date, SORT_DESC, $post);
		$this->set('post', $post);
		//wordpress
		
		
		$lcategory = $this->Lcategory->lcategory;
		$this->set('lcategory', $lcategory);
		
		
		$icons = array(
			'1' => 'special',
			'2' => 'bread',
			'3' => 'sweets',
			'4' => 'taiken',
		);
		$this->set('icons', $icons);
		
		
		//今月
		$thismonth = date('Y-m');
		//来月
		$nextmonth = date('Y-m', strtotime(date('Y-m-1').' +1 month'));
		//再来月
		$nextnextmonth = date('Y-m', strtotime(date('Y-m-1').' +2 month'));
		
		$tdata     = $this->_getmonth($thismonth);
		$ndata     = $this->_getmonth($nextmonth);
		$nndata    = $this->_getmonth($nextnextmonth);
		
		$this->set(compact('tdata', 'ndata', 'nndata', 'thismonth', 'nextmonth', 'nextnextmonth'));
	}
	
	function index(){
		
		$meta['title'] = 'お菓子で笑顔を作る｜マルサンパントリー';
		$meta['keyword'] = '丸三,マルサン,ケーキ材料, パン材料, 製菓材料, 製パン材料, お菓子材料,製菓道具, ケーキレシピ, 製パン道具, marusan, marusanpantry';
		$meta['description'] = 'お菓子材料、パン材料、器具道具、焼き型などが揃っています。';
		$this->set('meta', $meta);
		
		$osusumelesson = $this->Lesson->find('first', array(
				"conditions" => array('recomflg' => 1, "active" => 0, 'del_flg' => 0),
			)
		);
		$this->set('osusumelesson', $osusumelesson);
	}
	function _getmonth($month){
		$thismonths = $this->LessonDate->find('all', array(
				"conditions" => array("DATE_FORMAT(LessonDate.date, '%Y-%m')" => $month),
				'fields' => 'DISTINCT lesson_id',
			)
		);
		if(!empty($thismonths)){
			foreach($thismonths as $k => $v){
				$array[] = $v['LessonDate']['lesson_id'];
			}
		}else{
			$array = null;
		}
		$data = $this->Lesson->find('all', array(
				'conditions' => array('id' => $array, 'active' => 0, 'del_flg' => 0),
				'fields'     => array('id', 'category', 'title', 'description'),
				'order'      => array('category' => 'ASC'),// !bookmark
			)
		);
		return $data;
	}
}
