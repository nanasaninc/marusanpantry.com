<?php
class WelcomeController extends AppController{

	public $name = 'welcome';
	public $uses = null;
	//public $helpers = array('Thcalendar');
	
	public $autoRender = true;
	public $layout     = "common/siteframe";
	
	
	
	
	function beforeFilter(){
		$bodyId = 'welcome';
		$this->set('bodyId', $bodyId);
	}
	
	function index(){
		$meta['title'] = "ようこそ｜マルサンパントリー";
		$meta['keyword'] = '丸三,マルサン,ケーキ材料, パン材料, 製菓材料, 製パン材料, お菓子材料,製菓道具, ケーキレシピ, 製パン道具, marusan, marusanpantry';
		$meta['description'] = 'お菓子材料、パン材料、器具道具、焼き型などが揃っています。';
		$this->set('meta', $meta);
	}
}
