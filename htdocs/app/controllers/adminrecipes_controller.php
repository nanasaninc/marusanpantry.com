<?php
class AdminrecipesController extends AppController{

	public $name = 'Adminrecipes';
	public $uses = array('Recipe');
	public $components = array('Common', 'Uploader.Uploader', 'RequestHandler', 'Auth');
	public $helpers = array('Tree');

	public $autoRender = true;
	public $layout     = "admin/siteframe";

	public $recipethumbsize = '313';//サムネイルサイズ
	public $pagenum = 20;


	function beforeFilter(){

        $this->Auth->authError = 'ログインしてください。';

		$this->loadModel('RecipeImage');
		$this->loadModel('RecipeMaterial');
		$this->loadModel('RecipeOrder');
		$this->loadModel('RecipeRelated');
		$this->loadModel('Difficulty');
		$this->loadModel('Active');
		$this->loadModel('Recipecat');
		$this->loadModel('Wordpress');

		$difficulty = $this->Difficulty->difficulty;
		$this->set('difficulty', $difficulty);

		$active = $this->Active->active;
		$this->set('active', $active);

		$bodyId = 'recipe';
		$this->set('bodyId', $bodyId);

		$wordpress = $this->Wordpress->url;
		$this->set('wordpress', $wordpress);

		$this->Uploader->tempDir = TMP;


		//カテゴリー登録数検索
		//大カテ数検索
		$breadconditions = array('category LIKE' => "1%", 'del_flg' => 0);
		$cat['breadnum'] = $this->Recipe->find('count',array(
			'conditions' => $breadconditions,
		));
		if(empty($cat['breadnum'])){
			$cat['breadnum'] = 0;
		}

		$sweetconditions = array('category LIKE' => "2%", 'del_flg' => 0);
		$cat['sweetnum'] = $this->Recipe->find('count',array(
			'conditions' => $sweetconditions,
		));
		if(empty($cat['sweetnum'])){
			$cat['sweetnum'] = 0;
		}
		$this->set('cat', $cat);

		//大カテゴリ
		$bcate = $this->Recipecat->bcat;
		$this->set('bcate', $bcate);
		//小カテゴリメニュー作成
		$subcategory = $this->Recipecat->recategory;

		$this->set('subcategory', $subcategory);

		$lists = array();

		foreach($subcategory as $k => $v){
			$cond = array('category' => "{$k}", 'del_flg' => 0);
			$catnum = $this->Recipe->find('count',array(
				'conditions' => $cond,
			));
			if(preg_match('/^1.+/', $k)){
				$lists['breadlist'][] = array('id' => $k, 'name' => $v, 'count' => $catnum);
			}elseif(preg_match('/^2.+/', $k)){
				$lists['sweetlist'][] = array('id' => $k, 'name' => $v, 'count' => $catnum);
			}
		}
		$this->set('lists', $lists);
	}

	function index($param = null){

		$fields = array('id', 'title', 'category', 'active');
		$order  = array('modified' => 'DESC');

		if(empty($param)){
			$cond = array('del_flg' => 0);

			$this->paginate['Recipe'] = array(
				'conditions' => $cond,
				'limit' => $this->pagenum,
				'fields' => $fields,
				'order' => $order,
			);
		}else{
			if($param == 10000){
				$cond   = array('category LIKE' => "1%", 'del_flg' => 0);
			}elseif($param == 20000){
				$cond   = array('category LIKE' => "2%", 'del_flg' => 0);
			}else{
				$cond   = array('category' => $param, 'del_flg' => 0);
			}

			$this->paginate['Recipe'] = array(
				'conditions' => $cond,
				'limit' => $this->pagenum,
				'fields' => $fields,
				'order' => $order,
			);
		}
		$pagebool = $this->paginate('Recipe');

		if(empty($pagebool)){
			$this->Session->setFlash('データがありません。');
		}

		$this->set('data', $this->paginate('Recipe'));
	}
	function search(){
		$this->Recipe->unbindModel(array(
			'hasMany' => array(
				'RecipeMaterial',
				'RecipeOrder',
				'RecipeRelated',
			),
		), false);



		$fields = array('id', 'title', 'category', 'description', 'active');


		if($this->data['Recipe']['mode'] == '検索'){
			if(!empty($this->params['form']['keyword'])){

				$keyword = $this->params['form']['keyword'];

				App::import('Sanitize');
				$keyword = Sanitize::clean($keyword);

				$this->Session->write('words', $keyword);

				$cond   = array('OR' => array('title LIKE' => '%'.$keyword.'%', 'description LIKE' => '%'.$keyword.'%'), 'del_flg' => 0);
				$order  = array('modified' => 'DESC');


				$this->paginate['Recipe'] = array(
					'conditions' => $cond,
					'order'      => $order,
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);

				$this->set('data', $this->paginate('Recipe'));
				$this->set('searchword', $keyword);

				$pagebool = $this->paginate('Recipe');

				if(empty($pagebool)){
					$this->Session->setFlash('データがありません。');
				}

			}else{
				$this->paginate['Recipe'] = array(
					'conditions' => array('del_flg' => 0),
					'order'      => array('modified' => 'DESC'),
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);

				$this->set('data', $this->paginate('Recipe'));
				//$this->set('searchword', $keyword);
				$this->Session->destroy('words');
			}
		}else{

			if($this->Session->check('words')) {
				$condition = $this->Session->read('words');
			}
			if(!empty($condition)){

				$cond   = array('OR' => array('title LIKE' => '%'.$condition.'%', 'description LIKE' => '%'.$condition.'%'), 'del_flg' => 0);
				$order  = array('modified' => 'DESC');

				$this->paginate['Recipe'] = array(
					'conditions' => $cond,
					'order'      => $order,
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);

				$this->set('data', $this->paginate('Recipe'));
				$this->set('searchword', $condition);

			}else{

				$this->paginate['Recipe'] = array(
					'conditions' => array('del_flg' => 0),
					'order'      => array('modified' => 'DESC'),
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);

				$this->set('data', $this->paginate('Recipe'));
				//$this->set('searchword', $keyword);
				$this->Session->destroy('words');
			}
		}
	}
	function add(){

		App::import('Sanitize');

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Recipe']['token']){

				//$this->data = Sanitize::clean($this->data);

				if($this->data['Recipe']['mode'] === '登録する'){
					$this->Recipe->set($this->data);
					if($this->Recipe->validates()){
						if(!$this->Recipe->save($this->data)){
							echo "レシピ登録中にエラーが発生しました。";
						}else{
							//レシピ材料保存
							foreach($this->data['RecipeMaterial'] as $k => $v){
								if(!empty($v['name'])){
									$v['recipe_id'] = $this->Recipe->id;
									if(!$this->Recipe->RecipeMaterial->saveAll($v)){//エラー処理しよう
										echo "レシピ登録中にエラーが発生しました。";
									}
								}
							}
							//レシピ手順保存
							$num = 0;
							foreach($this->data['RecipeOrder'] as $k => $v){
								if(!empty($v['comment'])){
									$v['recipe_id'] = $this->Recipe->id;
									if(!$this->Recipe->RecipeOrder->saveAll($v)){//エラー処理しよう
										echo "レシピ登録中にエラーが発生しました。";
									}
									//画像保存
									if($data = $this->Uploader->upload("orderPhoto{$num}", array('overwrite' => true, 'name' => $this->Recipe->id))) {
										if($data['width'] === $data['height']){
											$resized_path = $this->Uploader->resize(array('width' => $this->recipethumbsize, 'append' => '_th', 'quality' => 70));
										}else{
											$resized_path = $this->Uploader->crop(array('width' => $this->recipethumbsize, 'height' => $this->recipethumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
										}

										$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'order'.DS;
										if(!is_dir($folder_url.$this->Recipe->id)) {
											mkdir($folder_url.$this->Recipe->id);
										}
										//アップロードファイル移動
										//$jpgName = $num + 1;
										$jpgName = sprintf("%010d", $this->Recipe->RecipeOrder->id);

										$resize_url = $folder_url.$this->Recipe->id.DS.$jpgName.'.jpg';
										$thumbs_url = $folder_url.$this->Recipe->id.DS.$jpgName.'_th.jpg';

										$this->Uploader->move($data['path'], $resize_url, true);
										$this->Uploader->move($resized_path, $thumbs_url, true);

										$uploadfile['id'] = $this->Recipe->RecipeOrder->id;
										$uploadfile['recipe_id'] = $this->Recipe->id;
										$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

										$this->Recipe->RecipeOrder->saveAll($uploadfile);

									}
									$num++;
								}
							}
							//関連商品保存
							$num = 0;
							foreach($this->data['RecipeRelated'] as $k => $v){
								if(!empty($v['name'])){
									$v['recipe_id'] = $this->Recipe->id;
									if(!$this->Recipe->RecipeRelated->saveAll($v)){//エラー処理しよう
										echo "レシピ登録中にエラーが発生しました。";
									}


									//画像保存
									if($data = $this->Uploader->upload("relatedPhoto{$num}", array('overwrite' => true, 'name' => $this->Recipe->id))) {
										if($data['width'] === $data['height']){
											$resized_path = $this->Uploader->resize(array('width' => $this->recipethumbsize, 'append' => '_th', 'quality' => 70));
										}else{
											$resized_path = $this->Uploader->crop(array('width' => $this->recipethumbsize, 'height' => $this->recipethumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
										}

										$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'related'.DS;
										if(!is_dir($folder_url.$this->Recipe->id)) {
											mkdir($folder_url.$this->Recipe->id);
										}
										//アップロードファイル移動
										$jpgName = $num + 1;
										$resize_url = $folder_url.$this->Recipe->id.DS.$jpgName.'.jpg';
										$thumbs_url = $folder_url.$this->Recipe->id.DS.$jpgName.'_th.jpg';

										$this->Uploader->move($data['path'], $resize_url, true);
										$this->Uploader->move($resized_path, $thumbs_url, true);

										$uploadfile['id'] = $this->Recipe->RecipeRelated->id;
										$uploadfile['recipe_id'] = $this->Recipe->id;
										$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

										$this->Recipe->RecipeRelated->saveAll($uploadfile);

									}
									$num++;
								}
							}


							$num = 0;
							foreach($this->data['RecipeImage'] as $k => $v ){
								$keys = "fileName".$num;

								if(!empty($this->data['Recipe'][$keys]['name'])){
									$v['recipe_id'] = $this->Recipe->id;

									if(!$this->Recipe->RecipeImage->saveAll($v)){//エラー処理しよう
										echo "レシピ登録中にエラーが発生しました。";
									}
									if($data = $this->Uploader->upload("fileName{$num}", array('overwrite' => true, 'name' => $this->Recipe->id))) {
										if($data['width'] === $data['height']){
											$resized_path = $this->Uploader->resize(array('width' => $this->recipethumbsize, 'append' => '_th', 'quality' => 70));
										}else{
											$resized_path = $this->Uploader->crop(array('width' => $this->recipethumbsize, 'height' => $this->recipethumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
										}

										$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'recipe'.DS;
										if(!is_dir($folder_url.$this->Recipe->id)) {
											mkdir($folder_url.$this->Recipe->id);
										}
										//アップロードファイル移動
										$jpgName = $num + 1;
										$resize_url = $folder_url.$this->Recipe->id.DS.$jpgName.'.jpg';
										$thumbs_url = $folder_url.$this->Recipe->id.DS.$jpgName.'_th.jpg';

										$this->Uploader->move($data['path'], $resize_url, true);
										$this->Uploader->move($resized_path, $thumbs_url, true);

										//$uploadfile['id'] = $this->data['RecipeImage'][$num]['id'];
										$uploadfile['id'] = $this->Recipe->RecipeImage->id;
										$uploadfile['recipe_id'] = $this->Recipe->id;
										$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

										$this->Recipe->RecipeImage->saveAll($uploadfile);

									}
									$num++;
								}
							}
							$this->Session->setFlash('登録完了しました。');
							$this->redirect('../adminrecipes/');
						}
					}
				}
			}else{
				$this->render('complete/add_error');
			}
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->data['Recipe']['token'] = $token;
	}

	function edit($param = null){

		App::import('Sanitize');

		set_time_limit(0);

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Recipe']['token']){
				//$this->data = Sanitize::clean($this->data);//ここチェックよ

				if($this->data['Recipe']['mode'] === '修正する'){

					$this->set('data', $this->data);
					$this->Recipe->set($this->data);

					if($this->Recipe->validates()){
						if(!$this->Recipe->save($this->data)){
							echo "物件登録中にエラーが発生しました。";
						}else{

							//レシピ材料保存
							foreach($this->data['RecipeMaterial'] as $k => $v){
								if(!empty($v['name'])){
									$v['recipe_id'] = $this->Recipe->id;
									if(!$this->Recipe->RecipeMaterial->saveAll($v)){//エラー処理しよう
										echo "レシピ登録中にエラーが発生しました。";
									}
								}else{
									if($v['id']){
										if(!$this->Recipe->RecipeMaterial->delete($v['id'])){
											echo "レシピ登録中にエラーが発生しました。";
										}
									}
								}
							}
							//レシピ手順保存
							$num = 0;
							foreach($this->data['RecipeOrder'] as $k => $v){
								if(!empty($v['comment'])){
									if($v['delitem'] == 0){
										$v['recipe_id'] = $this->Recipe->id;
										if($v['delphoto'] == 0){
											if(!$this->Recipe->RecipeOrder->saveAll($v)){
												echo "レシピ登録中にエラーが発生しました。";
											}
										}else{
											$photodel = array('id' => $v['id'], 'uploadpath' => null);
											if(!$this->Recipe->RecipeOrder->save($photodel)){
												echo "レシピ登録中にエラーが発生しました。";
											}
										}
									}else{
										if($v['id']){
											$delphotopath = $this->RecipeOrder->find('first',array(// !bookmark
												'conditions' => array('id' => $v['id']),
												'fields'     => array('uploadpath'),
											));
											$thumbs = str_replace('.jpg', '_th.jpg', $delphotopath);
											if(!$this->Recipe->RecipeOrder->delete($v['id'])){
												echo "レシピ登録中にエラーが発生しました。";
											}
										}
									}
									//画像保存
									if($data = $this->Uploader->upload("orderPhoto{$num}", array('overwrite' => true, 'name' => $this->Recipe->id))) {
										if($data['width'] === $data['height']){
											$resized_path = $this->Uploader->resize(array('width' => $this->recipethumbsize, 'append' => '_th', 'quality' => 70));
										}else{
											$resized_path = $this->Uploader->crop(array('width' => $this->recipethumbsize, 'height' => $this->recipethumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
										}

										$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'order'.DS;
										if(!is_dir($folder_url.$this->Recipe->id)) {
											mkdir($folder_url.$this->Recipe->id);
										}
										//アップロードファイル移動
										//$jpgName = $num + 1;
										$jpgName = sprintf("%011d", $this->Recipe->RecipeOrder->id);
										$resize_url = $folder_url.$this->Recipe->id.DS.$jpgName.'.jpg';
										$thumbs_url = $folder_url.$this->Recipe->id.DS.$jpgName.'_th.jpg';

										$this->Uploader->move($data['path'], $resize_url, true);
										$this->Uploader->move($resized_path, $thumbs_url, true);

										$uploadfile['id'] = $this->Recipe->RecipeOrder->id;
										$uploadfile['recipe_id'] = $this->Recipe->id;
										$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

										$this->Recipe->RecipeOrder->saveAll($uploadfile);

									}
									$num++;
								}
							}
							//関連商品保存
							$num = 0;
							foreach($this->data['RecipeRelated'] as $k => $v){
								if(!empty($v['name'])){
									$v['recipe_id'] = $this->Recipe->id;
									if($v['delitem'] == 0){
										if(!$this->Recipe->RecipeRelated->saveAll($v)){
											echo "レシピ登録中にエラーが発生しました。";
										}
									}else{
										if(!$this->Recipe->RecipeRelated->delete($v['id'])){
											echo "レシピ登録中にエラーが発生しました。";
										}
									}
								}
							}

							$num = 0;
							foreach($this->data['RecipeImage'] as $k => $v ){
								//$num = $v['id'] - 1;
								if($data = $this->Uploader->upload("fileName{$num}", array('overwrite' => true, 'name' => $this->data['Recipe']['id']))) {
									if($data['width'] === $data['height']){
										$resized_path = $this->Uploader->resize(array('width' => $this->recipethumbsize, 'append' => '_th', 'quality' => 70));
									}else{
										$resized_path = $this->Uploader->crop(array('width' => $this->recipethumbsize, 'height' => $this->recipethumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
									}

									$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'recipe'.DS;
									if(!is_dir($folder_url.$this->data['Recipe']['id'])) {
										mkdir($folder_url.$this->data['Recipe']['id']);
									}
									//アップロードファイル移動
									$resize_url = $folder_url.$this->data['Recipe']['id'].DS.$this->data['RecipeImage'][$num]['id'].'.jpg';
									$thumbs_url = $folder_url.$this->data['Recipe']['id'].DS.$this->data['RecipeImage'][$num]['id'].'_th.jpg';

									$this->Uploader->move($data['path'], $resize_url, true);
									$this->Uploader->move($resized_path, $thumbs_url, true);

									$uploadfile['id'] = $this->data['RecipeImage'][$num]['id'];
									$uploadfile['recipe_id'] = $this->data['Recipe']['id'];
									$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

									$this->Recipe->RecipeImage->save($uploadfile);
								}
								$num++;
							}
							$this->Session->setFlash('修正完了しました。');
							$this->redirect('../adminrecipes/');
						}
						//$this->render('complete/update_complete');
					}
				}else if($this->data['Recipe']['mode'] === '削除する'){

					$conditions = array(
						'Recipe.id' => $this->data['Recipe']['id']
					);

					//pr($this->data);

					if(!$this->Recipe->delete($this->data['Recipe']['id'])){
					}

					$this->render('complete/delete_complete');
				}
			}else{
				$this->render('complete/add_error');
			}
		}else{
			$this->Recipe->hasMany['RecipeMaterial']['order'] = array('sort' => 'ASC');
			$this->Recipe->hasMany['RecipeOrder']['order'] = array('sort' => 'ASC');
			$this->Recipe->hasMany['RecipeRelated']['order'] = array('sort' => 'ASC');
			$this->Recipe->id = $param;
			$this->data = $this->Recipe->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->data['Recipe']['token'] = $token;
	}
	function delete($param = null){
		if(!empty($param)){
			$this->data['Recipe']['id'] = $param;
			$this->data['Recipe']['del_flg'] =  1;

			if(!$this->Recipe->save($this->data)){
				$this->Session->setFlash('削除に失敗しました。');
				$this->redirect('../adminrecipes/');
			}else{
				$this->Session->setFlash('削除しました。');
				$this->redirect('../adminrecipes/');
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecipes/');
		}
	}
	//Ajax　表示状態更新
	function active(){

		//バリデーションを無効に
		$this->Recipe->setValidation('ajax');

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->RequestHandler->isAjax()){
			// POST情報は$this->params['form']で取得
			$id = $this->params['form']['id'];
			$activeparam = $this->params['form']['actives'];
			// DBに突っ込みます
			$this->Recipe->id = $id;
			$this->data['Recipe']['active'] = $activeparam;

			if(!$this->Recipe->save($this->data)){
				$message = '更新失敗！';
				$this->set('t', $message);
			}else{
				$message = '更新完了！';
				$this->set('t', $message);
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecipes/');
		}
	}
	function bidcategory() {

		//バリデーションを無効に
		$this->Recipe->setValidation('ajax');

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		$cate = $this->Recipecat->recategory;

		if($this->RequestHandler->isAjax()){
			 //POST情報は$this->params['form']で取得
			$id = $this->params['form']['id'];

			$ayy = '<option value="">選択してください</option>';
			if($id == 1000){
				foreach($cate as $k => $v){
					if($k < 20000){
						$ayy .= '<option value="'.$k.'">'.$v.'</option>';
					}
				}
			}elseif($id == 2000){
				foreach($cate as $k => $v){
					if($k > 20000){
						$ayy .= '<option value="'.$k.'">'.$v.'</option>';
					}
				}
			}else{
				foreach($cate as $k => $v){
					$ayy .= '<option value="'.$k.'">'.$v.'</option>';
				}
			}


			$this->set('t', $ayy);

		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecipes/');
		}
	}


}
