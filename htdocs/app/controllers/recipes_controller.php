<?php
class RecipesController extends AppController{

	public $name = 'Recipes';
	public $uses = array('Recipe');
	
	public $autoRender = true;
	public $layout     = "common/siteframe";
	
	public $pagenum = 20;//ページング数
	
	function beforeFilter(){
		$this->loadModel('RecipeImage');
		$this->loadModel('RecipeMaterial');
		$this->loadModel('RecipeCategory');
		$this->loadModel('RecipeParentCategory');
		$this->loadModel('RecipeOrder');
		$this->loadModel('RecipeRelated');
		$this->loadModel('Difficulty');
		$this->loadModel('Active');
		$this->loadModel('Recipecat');
		
		$difficulty = $this->Difficulty->difficulty;
		$this->set('difficulty', $difficulty);
		
		$bodyId = 'recipe';
		$this->set('bodyId', $bodyId);
		
		//大カテゴリ
		$bcate = $this->Recipecat->bcat;
		$this->set('bcate', $bcate);
		//小カテゴリメニュー作成
		$subcategory = $this->Recipecat->recategory;
		
		
		$this->set('subcategory', $subcategory);
		
		$lists = array();
		
		foreach($subcategory as $k => $v){
			$cond = array('category' => "{$k}", 'active' => 0, 'del_flg' => 0);
			$catnum = $this->Recipe->find('count',array(
				'conditions' => $cond,
			));
			if(preg_match('/^1.+/', $k)){
				$lists['breadlist'][] = array('id' => $k, 'name' => $v, 'count' => $catnum);
			}elseif(preg_match('/^2.+/', $k)){
				$lists['sweetlist'][] = array('id' => $k, 'name' => $v, 'count' => $catnum);
			}
		}
		
		foreach($difficulty as $k  => $v){
			$condd = array('difficulty' => "{$k}", 'active' => 0, 'del_flg' => 0);
			$diffnum = $this->Recipe->find('count',array(
				'conditions' => $condd,
			));
			$lists['difflist'][] = array('id' => $k, 'name' => $v, 'count' => $diffnum);
		}
		
		$this->set('lists', $lists);
		
		
		$today = date('Y-m-d H:i:s', strtotime(date('Y-m-d').' +30 day'));
		$this->set('today', $today);
	}
	
	function index(){
		$this->Recipe->unbindModel(array(
			'hasMany' => array(
				'RecipeMaterial',
				'RecipeOrder',
				'RecipeRelated',
			),
		), false);
		
		$this->paginate['Recipe'] = array(
			'conditions' => array('active' => 0, 'del_flg' => 0),
			'limit' => $this->pagenum,
			'fields' => array(),
			'order' => array('id' => 'DESC')
		);
		$this->set('data', $this->paginate('Recipe'));
		
		$meta['title'] = "レシピ｜マルサンパントリー";
		$meta['keyword'] = '丸三,マルサン,ケーキ材料, パン材料, 製菓材料, 製パン材料, お菓子材料,製菓道具, ケーキレシピ, 製パン道具, marusan, marusanpantry';
		$meta['description'] = 'お菓子材料、パン材料、器具道具、焼き型などが揃っています。';
		$this->set('meta', $meta);
	}
	function category($param = null){
		
		$this->Recipe->unbindModel(array(
			'hasMany' => array(
				'RecipeMaterial',
				'RecipeOrder',
				'RecipeRelated',
			),
		), false);
		
		
		$fields = array('id', 'title', 'category', 'active', 'description');
		if(empty($param)){
			$cond   = array('active' => 0, 'del_flg' => 0);
			$order  = array('modified' => 'DESC');
			
			$this->paginate['Recipe'] = array(
				'conditions' => $cond,
				'order'      => $order,
				'limit'      => $this->pagenum,
				'fields'     => $fields,
			);
		}else{
			$cond   = array('active' => 0, 'category' => $param, 'del_flg' => 0);
			$order  = array('modified' => 'DESC');
			
			$this->paginate['Recipe'] = array(
				'conditions' => $cond,
				'order'      => $order,
				'limit'      => $this->pagenum,
				'fields'     => $fields,
			);
		}
		$this->set('data', $this->paginate('Recipe'));
		
		$meta['title'] = "レシピカテゴリー｜レシピ｜マルサンパントリー";
		$meta['keyword'] = '丸三,マルサン,ケーキ材料, パン材料, 製菓材料, 製パン材料, お菓子材料,製菓道具, ケーキレシピ, 製パン道具, marusan, marusanpantry';
		$meta['description'] = 'お菓子材料、パン材料、器具道具、焼き型などが揃っています。';
		$this->set('meta', $meta);
	}
	function difficulty($param = null){
		$this->Recipe->unbindModel(array(
			'hasMany' => array(
				'RecipeMaterial',
				'RecipeOrder',
				'RecipeRelated',
			),
		), false);
		
		$paging = $this->pagenum;//1ページ表示数
		
		$fields = array('id', 'title', 'category', 'active', 'description');
		if(empty($param)){
			$cond   = array('active' => 0, 'del_flg' => 0);
			$order  = array('modified' => 'DESC');
			
			$this->paginate['Recipe'] = array(
				'conditions' => $cond,
				'order'      => $order,
				'limit'      => $this->pagenum,
				'fields'     => $fields,
			);
		}else{
			$cond   = array('active' => 0, 'difficulty' => $param, 'del_flg' => 0);
			$order  = array('modified' => 'DESC');
			
			$this->paginate['Recipe'] = array(
				'conditions' => $cond,
				'order'      => $order,
				'limit'      => $this->pagenum,
				'fields'     => $fields,
			);
		}
		
		$this->set('data', $this->paginate('Recipe'));
		
		$meta['title'] = "レシピカテゴリー｜レシピ｜マルサンパントリー";
		$meta['keyword'] = '丸三,マルサン,ケーキ材料, パン材料, 製菓材料, 製パン材料, お菓子材料,製菓道具, ケーキレシピ, 製パン道具, marusan, marusanpantry';
		$meta['description'] = 'お菓子材料、パン材料、器具道具、焼き型などが揃っています。';
		$this->set('meta', $meta);
	}
	function search(){
	
	
		$this->Recipe->unbindModel(array(
			'hasMany' => array(
				'RecipeMaterial',
				'RecipeOrder',
				'RecipeRelated',
			),
		), false);
		
		
		
		$fields = array('id', 'title', 'category', 'description', 'active');
		
		
		if($this->data['Recipe']['mode'] == '検索'){
			if(!empty($this->params['form']['keyword'])){
			
				$keyword = $this->params['form']['keyword'];
				
				App::import('Sanitize');
				$keyword = Sanitize::clean($keyword);
				
				$this->Session->write('searchword', $keyword);
				
				$cond   = array('OR' => array('title LIKE' => '%'.$keyword.'%', 'description LIKE' => '%'.$keyword.'%'), 'active' => 0, 'del_flg' => 0);
				$order  = array('modified' => 'DESC');
				
				
				$this->paginate['Recipe'] = array(
					'conditions' => $cond,
					'order'      => $order,
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);
				
				$this->set('data', $this->paginate('Recipe'));
				$this->set('searchword', $keyword);
				
			}else{
				$this->paginate['Recipe'] = array(
					'conditions' => array('active' => 0, 'del_flg' => 0),
					'order'      => array('modified' => 'DESC'),
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);
				
				$this->set('data', $this->paginate('Recipe'));
				//$this->set('searchword', $keyword);
				$this->Session->destroy('searchword');
			}
		}else{
			
			if($this->Session->check('searchword')) {
				$condition = $this->Session->read('searchword');
			}
			if(!empty($condition)){
				
				$cond   = array('OR' => array('title LIKE' => '%'.$condition.'%', 'description LIKE' => '%'.$condition.'%'), 'active' => 0, 'del_flg' => 0);
				$order  = array('modified' => 'DESC');
				
				$this->paginate['Recipe'] = array(
					'conditions' => $cond,
					'order'      => $order,
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);
				
				$this->set('data', $this->paginate('Recipe'));
				$this->set('searchword', $condition);
				
				
			}else{
				
				$this->paginate['Recipe'] = array(
					'conditions' => array('active' => 0, 'del_flg' => 0),
					'order'      => array('modified' => 'DESC'),
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);
				
				$this->set('data', $this->paginate('Recipe'));
				//$this->set('searchword', $keyword);
				$this->Session->destroy('searchword');
			}
		}
		
		$meta['title'] = "レシピ検索｜レシピ｜マルサンパントリー";
		$meta['keyword'] = '丸三,マルサン,ケーキ材料, パン材料, 製菓材料, 製パン材料, お菓子材料,製菓道具, ケーキレシピ, 製パン道具, marusan, marusanpantry';
		$meta['description'] = 'お菓子材料、パン材料、器具道具、焼き型などが揃っています。';
		$this->set('meta', $meta);
	}
	
	function recipe($param = null){
		$this->Recipe->hasMany['RecipeMaterial']['order'] = array('sort' => 'ASC');
		$this->Recipe->hasMany['RecipeOrder']['order'] = array('sort' => 'ASC');
		$this->Recipe->hasMany['RecipeRelated']['order'] = array('sort' => 'ASC');
		
		App::import('Sanitize');
		$param = Sanitize::clean($param);
		$data = $this->Recipe->find('first', array(
				'conditions' => array(
					'Recipe.id' => $param,
					'Recipe.active' => 0,
					'Recipe.del_flg' => 0,
				)
			)
		);
		
		if(!empty($data)){
			$this->set('data', $data);
		}else{
			$this->Session->setFlash('該当のデータはありませんでした。');
			$this->redirect('../recipes');
		}
		
		$meta['title'] = "{$data['Recipe']['title']}｜レシピ｜マルサンパントリー";
		$meta['keyword'] = '丸三,マルサン,ケーキ材料, パン材料, 製菓材料, 製パン材料, お菓子材料,製菓道具, ケーキレシピ, 製パン道具, marusan, marusanpantry';
		$meta['description'] = 'お菓子材料、パン材料、器具道具、焼き型などが揃っています。';
		$this->set('meta', $meta);
	}
}
