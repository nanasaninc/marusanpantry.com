<?php
class AdminpantriesController extends AppController{

	public $name       = 'Adminpantries';
	public $uses       = array('Pantry', 'PantryImage');
	public $components = array('Common', 'Uploader.Uploader', 'RequestHandler', 'Auth');
	public $helpers    = array('Thcalendar', 'Jaweek');

	public $autoRender = true;
	public $layout     = "admin/siteframe";

	public $thumbsize = '200';//サムネイルサイズ

	public $pagenum = 20;

	function beforeFilter(){

        $this->Auth->authError = 'ログインしてください。';

		$this->loadModel('Active');
		$this->loadModel('Wordpress');

		$active = $this->Active->active;
		$this->set('active', $active);

		$bodyId = 'pantry';
		$this->set('bodyId', $bodyId);

		$wordpress = $this->Wordpress->url;
		$this->set('wordpress', $wordpress);

	}

	function index(){
		$data['list'] = $this->Pantry->find('all', array(
				'conditions' => array('del_flg' => 0),
				'fields' => array(),
				'order'=>array('id' => 'DESC')
			)
		);
		$this->set('data', $data);
	}
	function add(){

		App::import('Sanitize');

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Pantry']['token']){

				//$this->data = Sanitize::clean($this->data);

				if($this->data['Pantry']['mode'] === '登録する'){
					//debug($this->data);
					$this->Pantry->set($this->data);
					if($this->Pantry->validates()){
						if(!$this->Pantry->save($this->data)){
							echo "レッスン登録中にエラーが発生しました。";
						}else{

							$num = 0;
							foreach($this->data['PantryImage'] as $k => $v ){

								$keys = "fileName".$num;
								if(!empty($this->data['Pantry'][$keys]['name'])){
									$v['Pantry_id'] = $this->Pantry->id;
									if(!$this->Pantry->PantryImage->saveAll($v)){//エラー処理しよう
										echo "レシピ登録中にエラーが発生しました。";
									}
									if($data = $this->Uploader->upload("fileName{$num}", array('overwrite' => true, 'name' => $this->Pantry->id))) {
										if($data['width'] === $data['height']){
											$resized_path = $this->Uploader->resize(array('width' => $this->thumbsize, 'append' => '_th', 'quality' => 70));
										}else{
											$resized_path = $this->Uploader->crop(array('width' => $this->thumbsize, 'height' => $this->thumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
										}

										$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'pantry'.DS;
										if(!is_dir($folder_url.$this->Pantry->id)) {
											mkdir($folder_url.$this->Pantry->id);
										}
										//アップロードファイル移動
										$jpgName = sprintf("%010d", $this->Pantry->PantryImage->id);

										$resize_url = $folder_url.$this->Pantry->id.DS.$jpgName.'.jpg';
										$thumbs_url = $folder_url.$this->Pantry->id.DS.$jpgName.'_th.jpg';

										$this->Uploader->move($data['path'], $resize_url, true);
										$this->Uploader->move($resized_path, $thumbs_url, true);

										$uploadfile['id'] = $this->Pantry->PantryImage->id;
										$uploadfile['pantry_id'] = $this->Pantry->id;
										$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

										$this->Pantry->PantryImage->saveAll($uploadfile);

									}
									$num++;
								}
							}
							$this->Session->setFlash('登録完了しました。');
							$this->redirect('../adminpantries');
						}
					}
				}
			}else{
				$this->render('complete/add_error');
			}
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->data['Pantry']['token'] = $token;
	}
	function edit($param = null){

		App::import('Sanitize');

		set_time_limit(0);

		//debug($this->data);

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Pantry']['token']){
				//$this->data = Sanitize::clean($this->data);//ここチェックよ

				if($this->data['Pantry']['mode'] === '修正する'){

					$this->set('data', $this->data);
					$this->Pantry->set($this->data);
					if($this->Pantry->validates()){
						if(!$this->Pantry->save($this->data)){
							echo "物件登録中にエラーが発生しました。";
						}else{


							$num = 0;
							foreach($this->data['PantryImage'] as $k => $v ){
								//$num = $v['id'] - 1;
								if(!$this->Pantry->PantryImage->saveAll($v)){//エラー処理しよう
									echo "レッスン登録中にエラーが発生しました。";
								}
								if($data = $this->Uploader->upload("fileName{$num}", array('overwrite' => true, 'name' => $this->data['Pantry']['id']))) {
									if($data['width'] === $data['height']){
										$resized_path = $this->Uploader->resize(array('width' => $this->thumbsize, 'append' => '_th', 'quality' => 70));
									}else{
										$resized_path = $this->Uploader->crop(array('width' => $this->thumbsize, 'height' => $this->thumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
									}

									$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'pantry'.DS;
									if(!is_dir($folder_url.$this->data['Pantry']['id'])) {
										mkdir($folder_url.$this->data['Pantry']['id']);
									}
									//アップロードファイル移動
									$jpgName = sprintf("%010d", $this->Pantry->PantryImage->id);

									$resize_url = $folder_url.$this->data['Pantry']['id'].DS.$jpgName.'.jpg';
									$thumbs_url = $folder_url.$this->data['Pantry']['id'].DS.$jpgName.'_th.jpg';

									$this->Uploader->move($data['path'], $resize_url, true);
									$this->Uploader->move($resized_path, $thumbs_url, true);


									if(!empty($this->data['PantryImage'][$num]['id'])){
										$uploadfile['id'] = $this->data['PantryImage'][$num]['id'];
									}else{
										$uploadfile['id'] = $this->Pantry->PantryImage->id;
									}
									$uploadfile['pantry_id'] = $this->data['Pantry']['id'];
									$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

									$this->Pantry->PantryImage->save($uploadfile);
								}
								$num++;
							}
							$this->render('complete/update_complete');
						}

					}
				}else if($this->data['Pantry']['mode'] === '削除する'){

					$conditions = array(
						'Pantry.id' => $this->data['Pantry']['id']
					);

					//pr($this->data);

					if(!$this->Pantry->delete($this->data['Pantry']['id'])){
					}

					$this->render('complete/delete_complete');
				}
			}else{
				$this->render('complete/add_error');
			}
		}else{
			$this->Pantry->id = $param;
			$this->data = $this->Pantry->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->data['Pantry']['token'] = $token;
	}

	function duplicates($param = null){

		App::import('Sanitize');

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Pantry']['token']){

				//$this->data = Sanitize::clean($this->data);


				if($this->data['Pantry']['mode'] === '登録する'){
					//debug($this->data);
					if($this->Pantry->validates()){
						if(!$this->Pantry->save($this->data)){
							echo "レッスン登録中にエラーが発生しました。";
						}else{
							$num = 0;
							foreach($this->data['PantryImage'] as $k => $v ){

								$keys = "fileName".$num;
								if(!empty($this->data['Pantry'][$keys]['name'])){
									$v['pantry_id'] = $this->Pantry->id;
									if(!$this->Pantry->PantryImage->saveAll($v)){//エラー処理しよう
										echo "おすすめ商品登録中にエラーが発生しました。";
									}
									if($data = $this->Uploader->upload("fileName{$num}", array('overwrite' => true, 'name' => $this->Pantry->id))) {
										if($data['width'] === $data['height']){
											$resized_path = $this->Uploader->resize(array('width' => $this->thumbsize, 'append' => '_th', 'quality' => 70));
										}else{
											$resized_path = $this->Uploader->crop(array('width' => $this->thumbsize, 'height' => $this->thumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
										}

										$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'pantry'.DS;
										if(!is_dir($folder_url.$this->Pantry->id)) {
											mkdir($folder_url.$this->Pantry->id);
										}
										//アップロードファイル移動
										$jpgName = sprintf("%010d", $this->Pantry->PantryImage->id);

										$resize_url = $folder_url.$this->Pantry->id.DS.$jpgName.'.jpg';
										$thumbs_url = $folder_url.$this->Pantry->id.DS.$jpgName.'_th.jpg';

										$this->Uploader->move($data['path'], $resize_url, true);
										$this->Uploader->move($resized_path, $thumbs_url, true);

										//$uploadfile['id'] = $this->data['RecipeImage'][$num]['id'];
										$uploadfile['id'] = $this->Pantry->PantryImage->id;
										$uploadfile['recommend_id'] = $this->Pantry->id;
										$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

										$this->Pantry->PantryImage->saveAll($uploadfile);

									}
									$num++;
								}
							}

						}
					}
				}
				$this->Session->setFlash('登録完了しました。');
				$this->redirect('../adminpantries');
			}else{
				$this->render('complete/add_error');
			}
		}else{
			$this->Pantry->id = $param;
			$this->data = $this->Pantry->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->data['Pantry']['token'] = $token;
	}


	function delete($param = null){
		if(!empty($param)){
			$this->data['Pantry']['id'] = $param;
			$this->data['Pantry']['del_flg'] =  1;

			if(!$this->Pantry->save($this->data)){
				$this->Session->setFlash('削除に失敗しました。');
				$this->redirect('../adminpantries/');
			}else{
				$this->Session->setFlash('削除しました。');
				$this->redirect('../adminpantries/');
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminpantries/');
		}
	}

	//Ajax　表示状態更新
	function active() {

		//バリデーションを無効に
		$this->Recommend->setValidation('ajax');

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->RequestHandler->isAjax()){
			// POST情報は$this->params['form']で取得
			$id = $this->params['form']['id'];
			$activeparam = $this->params['form']['actives'];
			// DBに突っ込みます
			$this->Pantry->id = $id;
			$this->data['adminpantries']['active'] = $activeparam;

			if(!$this->Pantry->save($this->data)){
				$message = '更新失敗！';
				$this->set('t', $message);
			}else{
				$message = '更新完了！';
				$this->set('t', $message);
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminpantries/');
		}
	}
	function recom() {

		//バリデーションを無効に
		$this->Pantry->setValidation('ajax');

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->RequestHandler->isAjax()){
			// POST情報は$this->params['form']で取得
			$id = $this->params['form']['id'];
			$activeparam = $this->params['form']['actives'];
			// DBに突っ込みます
			$this->Pantry->id = $id;
			if(!empty($activeparam)){
				$this->data['Pantry']['recomflg'] = $activeparam;
			}else{
				$this->data['Pantry']['recomflg'] = 0;
			}

			if(!$this->Pantry->save($this->data)){
				$message = '更新失敗！';
				$this->set('t', $message);
			}else{
				$message = '更新完了！';
				$this->set('t', $message);
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminpantries/');
		}
	}

	function search(){

		$fields = array('id', 'title', 'description', 'recomflg', 'active');

		if($this->data['Pantry']['mode'] == '検索'){
			if(!empty($this->params['form']['keyword'])){

				$keyword = $this->params['form']['keyword'];

				App::import('Sanitize');
				$keyword = Sanitize::clean($keyword);

				$this->Session->write('pantywords', $keyword);

				$cond   = array('OR' => array('title LIKE' => '%'.$keyword.'%', 'description LIKE' => '%'.$keyword.'%'), 'del_flg' => 0);
				$order  = array('modified' => 'DESC');


				$this->paginate['Pantry'] = array(
					'conditions' => $cond,
					'order'      => $order,
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);


				$this->set('data', $this->paginate('Pantry'));
				$this->set('searchword', $keyword);

				$pagebool = $this->paginate('Pantry');

				if(empty($pagebool)){
					$this->Session->setFlash('データがありません。');
				}


			}else{
				$this->paginate['Pantry'] = array(
					'conditions' => array('del_flg' => 0),
					'order'      => array('modified' => 'DESC'),
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);

				$this->set('data', $this->paginate('Pantry'));
				//$this->set('searchword', $keyword);
				$this->Session->destroy('pantywords');
			}
		}else{
			if($this->Session->check('pantywords')) {
				$keyword = $this->Session->read('pantywords');
			}
			if(!empty($condition)){

				$cond   = array('OR' => array('title LIKE' => '%'.$keyword.'%', 'description LIKE' => '%'.$keyword.'%'), 'del_flg' => 0);
				$order  = array('modified' => 'DESC');

				$this->paginate['Pantry'] = array(
					'conditions' => $cond,
					'order'      => $order,
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);

				$this->set('data', $this->paginate('Pantry'));
				$this->set('searchword', $keyword);

			}else{

				$this->paginate['Pantry'] = array(
					'conditions' => array('del_flg' => 0),
					'order'      => array('modified' => 'DESC'),
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);

				$this->set('data', $this->paginate('Pantry'));
				//$this->set('searchword', $keyword);
				$this->Session->destroy('pantywords');

			}
		}
	}
}
