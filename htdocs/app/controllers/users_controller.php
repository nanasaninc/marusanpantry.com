<?php
class UsersController extends AppController{

	public $name = 'Users';
	public $uses = array('User');

	public $components = array('Auth');

	public $autoRender = true;
	//public $layout     = "admin/siteframe";


	function beforeFilter(){
		$this->Auth->allow(array('login'));//@ado ユーザーの変更
		$userdata = $this->Auth->user();
		$this->Auth->autoRedirect = false;
		$this->Auth->loginError = 'ユーザ名もしくはパスワードに誤りがあります';
		$this->Auth->authError = 'ログインしてください。';
		$this->set('userinfodata', $userdata);
	}
	function login(){
		//$this->pageTitle = 'ログイン｜松山丸三管理ページ';
		//$this->set('pageTitle', $this->pageTitle);
		
		
		$this->layout = "admin/adminlogin";
		
		$userdata = $this->Auth->user();
		if(!empty($userdata)){
			//ログインが成功した時の処理
			$this->redirect('/admins/'); //好きな場所へリダイレクト
		}else{
			//ログインが失敗した時の処理
			//$this->Session->setFlash('ユーザ名もしくはパスワードに誤りがあります');
			$this->Auth->authError = 'ログインしてください。';
		}
	}
	function logout(){
		//$this->pageTitle = 'ログアウト｜松山丸三管理ページ';
		//$this->set('pageTitle', $this->pageTitle);

		$this->Session->setFlash('ログアウトしました。');
		//$this->Session->delete('Auth.id');
		$this->Auth->logout();
		$this->redirect('/users/login/');
		//$this->redirect(array('action' => '.'));
	}

	function regist(){

		//$this->autoLayout = true;
		//$this->autoRender = true;
		//$this->layout = "users/login";

		App::import('Sanitize');
		if(!empty($this->data)){

			Sanitize::clean($this->data);

			$this->User->set($this->data);

			if($this->data['User']['mode'] == 'confirm' && $this->User->set($this->data)){

				$this->render('complete/confirm');


			}else{

				if($this->data['User']['mode'] == '登録する'){

					$this->data['User']['password'] = $this->Auth->password($this->data['User']['raw_password']);//パスワードハッシュ化



					$blackList = array('protected', 'fields', 'mailaddress_confirm');

					$this->User->set($this->data);//postデータ初期化
					$this->User->save($this->data, true, array_diff(array_keys($this->User->schema()), $blackList));

					$this->render('complete/complete');
				}
			}
		}
	}


	function _generatePwd(){
		$len = 8;
		srand ( (double) microtime () * 1000000);
		$seed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		$pass = "";
		while ($len--) {
			$pos = rand(0,61);
			$pass .= $seed[$pos];
		}
		return $pass;
	}
}
