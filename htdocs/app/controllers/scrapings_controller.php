<?php
class ScrapingsController extends AppController{

	public $name = 'scrapings';
	public $uses = null;
	public $components = array('RequestHandler');
	
	//public $helpers = array('Thcalendar');
	//基本URL
	private $url = 'http://www.m-pantry.com/';//shop/item/mpantry/picture/goods/7535_1.jpg
	
	public $autoRender = true;
	public $layout     = "common/siteframe";
	
	function beforeFilter(){
		$bodyId = 'home';
		$this->set('bodyId', $bodyId);
	}
	
	function index(){
		// デバッグ情報出力を抑制
		Configure::write('debug', 0);
		
		if($this->RequestHandler->isAjax()){
			// POST情報は$this->params['form']で取得
			$urls = $this->params['form']['urls'];

			$html = file_get_contents($urls);
			if($html != false){
				$html = mb_convert_encoding($html, "UTF-8", "Windows-31J");
				$this->_getimages($html);
				$this->_gettitle($html);
				$this->_getprice($html);
			}else{
				echo '取得に失敗しました。';
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecipes/');
		}
	}
	
	
	function _getimages($html){
		preg_match('/<img src=\"\/shop\/item\/mpantry\/picture\/goods.+?class=\"thumbnail\"/', $html, $match);
		$match[0] = str_replace('<img src="', '', $match[0]);
		$match[0] = str_replace('" class="thumbnail"', '', $match[0]);
		echo '<img src="'.$this->url.$match[0].'" />'."\n";
	}
	function _gettitle($html){
		preg_match('/<h1 class=\"itemTitle\">.+?<\/h1>/', $html, $match);
		$match[0] = str_replace('<h1 class="itemTitle">', '', $match[0]);
		$match[0] = str_replace('</h1>', '', $match[0]);
		$match[0] = preg_replace('/<span.+?>.+?<\/span>/', '', $match[0]);
		echo $match[0]."\n";
	}
	function _getprice($html){
		preg_match('/<span class=\"itemPrice\">.+?<\/span>/', $html, $match);
		$match[0] = str_replace('<span class="itemPrice">', '', $match[0]);
		$match[0] = str_replace('</span>', '', $match[0]);
		echo $match[0];
	}
}
