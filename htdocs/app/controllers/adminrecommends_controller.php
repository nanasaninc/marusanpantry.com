<?php
class AdminrecommendsController extends AppController{

	public $name       = 'Adminrecommends';
	public $uses       = array('Recommend', 'RecommendImage');
	public $components = array('Common', 'Uploader.Uploader', 'RequestHandler', 'Auth');
	public $helpers    = array('Thcalendar', 'Jaweek');

	public $autoRender = true;
	public $layout     = "admin/siteframe";

	public $thumbsize = '200';//サムネイルサイズ

	public $pagenum = 20;

	function beforeFilter(){

        $this->Auth->authError = 'ログインしてください。';

		$this->loadModel('Wordpress');
		$this->loadModel('Active');

		$active = $this->Active->active;
		$this->set('active', $active);

		$wordpress = $this->Wordpress->url;
		$this->set('wordpress', $wordpress);

		$bodyId = 'recommend';
		$this->set('bodyId', $bodyId);

	}

	function index(){
		$data['list'] = $this->Recommend->find('all', array(
				'conditions' => array('del_flg' => 0),
				'fields' => array(),
				'order'=>array('id' => 'DESC')
			)
		);
		$this->set('data', $data);
	}
	function add(){

		App::import('Sanitize');

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Recommend']['token']){

				//$this->data = Sanitize::clean($this->data);

				if($this->data['Recommend']['mode'] === '登録する'){
					//debug($this->data);
					$this->Recommend->set($this->data);
					if($this->Recommend->validates()){
						if(!$this->Recommend->save($this->data)){
							echo "レッスン登録中にエラーが発生しました。";
						}else{

							$num = 0;
							foreach($this->data['RecommendImage'] as $k => $v ){

								$keys = "fileName".$num;
								if(!empty($this->data['Recommend'][$keys]['name'])){
									$v['Recommend_id'] = $this->Recommend->id;
									if(!$this->Recommend->RecommendImage->saveAll($v)){//エラー処理しよう
										echo "レシピ登録中にエラーが発生しました。";
									}
									if($data = $this->Uploader->upload("fileName{$num}", array('overwrite' => true, 'name' => $this->Recommend->id))) {
										if($data['width'] === $data['height']){
											$resized_path = $this->Uploader->resize(array('width' => $this->thumbsize, 'append' => '_th', 'quality' => 70));
										}else{
											$resized_path = $this->Uploader->crop(array('width' => $this->thumbsize, 'height' => $this->thumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
										}

										$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'recommend'.DS;
										if(!is_dir($folder_url.$this->Recommend->id)) {
											mkdir($folder_url.$this->Recommend->id);
										}
										//アップロードファイル移動
										$jpgName = sprintf("%010d", $this->Recommend->RecommendImage->id);

										$resize_url = $folder_url.$this->Recommend->id.DS.$jpgName.'.jpg';
										$thumbs_url = $folder_url.$this->Recommend->id.DS.$jpgName.'_th.jpg';

										$this->Uploader->move($data['path'], $resize_url, true);
										$this->Uploader->move($resized_path, $thumbs_url, true);

										//$uploadfile['id'] = $this->data['RecipeImage'][$num]['id'];
										$uploadfile['id'] = $this->Recommend->RecommendImage->id;
										$uploadfile['recommend_id'] = $this->Recommend->id;
										$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

										$this->Recommend->RecommendImage->saveAll($uploadfile);

									}
									$num++;
								}
							}
							$this->Session->setFlash('登録完了しました。');
							$this->redirect('../adminrecommends');
						}
					}
				}
			}else{
				$this->render('complete/add_error');
			}
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->data['Recommend']['token'] = $token;
	}
	function edit($param = null){

		App::import('Sanitize');

		set_time_limit(0);

		//debug($this->data);

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Recommend']['token']){
				//$this->data = Sanitize::clean($this->data);//ここチェックよ

				if($this->data['Recommend']['mode'] === '修正する'){

					$this->set('data', $this->data);
					$this->Recommend->set($this->data);
					if($this->Recommend->validates()){
						if(!$this->Recommend->save($this->data)){
							echo "物件登録中にエラーが発生しました。";
						}else{


							$num = 0;
							foreach($this->data['RecommendImage'] as $k => $v ){
								//$num = $v['id'] - 1;
								if(!$this->Recommend->RecommendImage->saveAll($v)){//エラー処理しよう
									echo "レッスン登録中にエラーが発生しました。";
								}
								if($data = $this->Uploader->upload("fileName{$num}", array('overwrite' => true, 'name' => $this->data['Recommend']['id']))) {
									if($data['width'] === $data['height']){
										$resized_path = $this->Uploader->resize(array('width' => $this->thumbsize, 'append' => '_th', 'quality' => 70));
									}else{
										$resized_path = $this->Uploader->crop(array('width' => $this->thumbsize, 'height' => $this->thumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
									}

									$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'recommend'.DS;
									if(!is_dir($folder_url.$this->data['Recommend']['id'])) {
										mkdir($folder_url.$this->data['Recommend']['id']);
									}
									//アップロードファイル移動
									$jpgName = sprintf("%010d", $this->Recommend->RecommendImage->id);

									$resize_url = $folder_url.$this->data['Recommend']['id'].DS.$jpgName.'.jpg';
									$thumbs_url = $folder_url.$this->data['Recommend']['id'].DS.$jpgName.'_th.jpg';

									$this->Uploader->move($data['path'], $resize_url, true);
									$this->Uploader->move($resized_path, $thumbs_url, true);


									if(!empty($this->data['RecommendImage'][$num]['id'])){
										$uploadfile['id'] = $this->data['RecommendImage'][$num]['id'];
									}else{
										$uploadfile['id'] = $this->Recommend->RecommendImage->id;
									}
									$uploadfile['recommend_id'] = $this->data['Recommend']['id'];
									$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

									$this->Recommend->RecommendImage->save($uploadfile);
								}
								$num++;
							}
							$this->render('complete/update_complete');
						}

					}
				}else if($this->data['Recommend']['mode'] === '削除する'){

					$conditions = array(
						'Recommend.id' => $this->data['Recommend']['id']
					);

					//pr($this->data);

					if(!$this->Recommend->delete($this->data['Recommend']['id'])){
					}

					$this->render('complete/delete_complete');
				}
			}else{
				$this->render('complete/add_error');
			}
		}else{
			$this->Recommend->id = $param;
			$this->data = $this->Recommend->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->data['Recommend']['token'] = $token;
	}

	function duplicates($param = null){

		App::import('Sanitize');

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Recommend']['token']){

				//$this->data = Sanitize::clean($this->data);


				if($this->data['Recommend']['mode'] === '登録する'){
					//debug($this->data);
					if($this->Recommend->validates()){
						if(!$this->Recommend->save($this->data)){
							echo "レッスン登録中にエラーが発生しました。";
						}else{
							$num = 0;
							foreach($this->data['RecommendImage'] as $k => $v ){

								$keys = "fileName".$num;
								if(!empty($this->data['Recommend'][$keys]['name'])){
									$v['recommend_id'] = $this->Recommend->id;
									if(!$this->Recommend->RecommendImage->saveAll($v)){//エラー処理しよう
										echo "おすすめ商品登録中にエラーが発生しました。";
									}
									if($data = $this->Uploader->upload("fileName{$num}", array('overwrite' => true, 'name' => $this->Recommend->id))) {
										if($data['width'] === $data['height']){
											$resized_path = $this->Uploader->resize(array('width' => $this->thumbsize, 'append' => '_th', 'quality' => 70));
										}else{
											$resized_path = $this->Uploader->crop(array('width' => $this->thumbsize, 'height' => $this->thumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
										}

										$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'recommend'.DS;
										if(!is_dir($folder_url.$this->Recommend->id)) {
											mkdir($folder_url.$this->Recommend->id);
										}
										//アップロードファイル移動
										$jpgName = sprintf("%010d", $this->Recommend->RecommendImage->id);

										$resize_url = $folder_url.$this->Recommend->id.DS.$jpgName.'.jpg';
										$thumbs_url = $folder_url.$this->Recommend->id.DS.$jpgName.'_th.jpg';

										$this->Uploader->move($data['path'], $resize_url, true);
										$this->Uploader->move($resized_path, $thumbs_url, true);

										//$uploadfile['id'] = $this->data['RecipeImage'][$num]['id'];
										$uploadfile['id'] = $this->Recommend->RecommendImage->id;
										$uploadfile['recommend_id'] = $this->Recommend->id;
										$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

										$this->Recommend->RecommendImage->saveAll($uploadfile);

									}
									$num++;
								}
							}

						}
					}
				}
				$this->Session->setFlash('登録完了しました。');
				$this->redirect('../adminlessons');
			}else{
				$this->render('complete/add_error');
			}
		}else{
			$this->Recommend->id = $param;
			$this->data = $this->Recommend->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->data['Recommend']['token'] = $token;
	}


	function delete($param = null){
		if(!empty($param)){
			$this->data['Recommend']['id'] = $param;
			$this->data['Recommend']['del_flg'] =  1;

			if(!$this->Recommend->save($this->data)){
				$this->Session->setFlash('削除に失敗しました。');
				$this->redirect('../adminrecommends/');
			}else{
				$this->Session->setFlash('削除しました。');
				$this->redirect('../adminrecommends/');
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecommends/');
		}
	}

	//Ajax　表示状態更新
	function active() {

		//バリデーションを無効に
		$this->Recommend->setValidation('ajax');

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->RequestHandler->isAjax()){
			// POST情報は$this->params['form']で取得
			$id = $this->params['form']['id'];
			$activeparam = $this->params['form']['actives'];
			// DBに突っ込みます
			$this->Recommend->id = $id;
			$this->data['Recommend']['active'] = $activeparam;

			if(!$this->Recommend->save($this->data)){
				$message = '更新失敗！';
				$this->set('t', $message);
			}else{
				$message = '更新完了！';
				$this->set('t', $message);
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecommends/');
		}
	}
	function recom() {

		//バリデーションを無効に
		$this->Recommend->setValidation('ajax');

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->RequestHandler->isAjax()){
			// POST情報は$this->params['form']で取得
			$id = $this->params['form']['id'];
			$activeparam = $this->params['form']['actives'];
			// DBに突っ込みます
			$this->Recommend->id = $id;
			if(!empty($activeparam)){
				$this->data['Recommend']['recomflg'] = $activeparam;
			}else{
				$this->data['Recommend']['recomflg'] = 0;
			}

			if(!$this->Recommend->save($this->data)){
				$message = '更新失敗！';
				$this->set('t', $message);
			}else{
				$message = '更新完了！';
				$this->set('t', $message);
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminrecommends/');
		}
	}
	function search(){

		$fields = array('id', 'title', 'description', 'recomflg', 'active');

		if($this->data['Recommend']['mode'] == '検索'){
			if(!empty($this->params['form']['keyword'])){

				$keyword = $this->params['form']['keyword'];

				App::import('Sanitize');
				$keyword = Sanitize::clean($keyword);

				$this->Session->write('recommendwords', $keyword);

				$cond   = array('OR' => array('title LIKE' => '%'.$keyword.'%', 'description LIKE' => '%'.$keyword.'%'), 'del_flg' => 0);
				$order  = array('modified' => 'DESC');


				$this->paginate['Recommend'] = array(
					'conditions' => $cond,
					'order'      => $order,
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);


				$this->set('data', $this->paginate('Recommend'));
				$this->set('searchword', $keyword);

				$pagebool = $this->paginate('Recommend');

				if(empty($pagebool)){
					$this->Session->setFlash('データがありません。');
				}


			}else{
				$this->paginate['Recommend'] = array(
					'conditions' => array('del_flg' => 0),
					'order'      => array('modified' => 'DESC'),
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);

				$this->set('data', $this->paginate('Recommend'));
				//$this->set('searchword', $keyword);
				$this->Session->destroy('recommendwords');
			}
		}else{
			if($this->Session->check('recommendwords')) {
				$keyword = $this->Session->read('recommendwords');
			}
			if(!empty($condition)){

				$cond   = array('OR' => array('title LIKE' => '%'.$keyword.'%', 'description LIKE' => '%'.$keyword.'%'), 'del_flg' => 0);
				$order  = array('modified' => 'DESC');

				$this->paginate['Recommend'] = array(
					'conditions' => $cond,
					'order'      => $order,
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);

				$this->set('data', $this->paginate('Recommend'));
				$this->set('searchword', $keyword);

			}else{

				$this->paginate['Recommend'] = array(
					'conditions' => array('del_flg' => 0),
					'order'      => array('modified' => 'DESC'),
					'limit'      => $this->pagenum,
					'fields'     => $fields,
				);

				$this->set('data', $this->paginate('Recommend'));
				//$this->set('searchword', $keyword);
				$this->Session->destroy('recommendwords');

			}
		}
	}
}
