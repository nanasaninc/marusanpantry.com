<?php
class AdminlessonsController extends AppController{

	public $name = 'Adminlessons';
	public $uses = array('Lesson');
	public $components = array('Common', 'Uploader.Uploader', 'RequestHandler', 'Auth');
	public $helpers = array('Thcalendar', 'Jaweek', 'Common');

	public $autoRender = true;
	public $layout     = "admin/siteframe";

	public $thumbsize = '200';//サムネイルサイズ

	function beforeFilter(){

        $this->Auth->authError = 'ログインしてください。';

		$this->loadModel('LessonImage');
		$this->loadModel('LessonDate');
		$this->loadModel('Difficulty');
		$this->loadModel('Lessonicon');
		$this->loadModel('Lcategory');
		$this->loadModel('Active');
		$this->loadModel('Wordpress');

		$wordpress = $this->Wordpress->url;
		$this->set('wordpress', $wordpress);

		$difficulty = $this->Difficulty->difficulty;
		$this->set('difficulty', $difficulty);

		$lessonicon = $this->Lessonicon->icons;
		$this->set('lessonicon', $lessonicon);

		$lcategory = $this->Lcategory->lcategory;
		$this->set('lcategory', $lcategory);

		$active = $this->Active->active;
		$this->set('active', $active);

		$bodyId = 'lesson';
		$this->set('bodyId', $bodyId);

		//カテゴリをカウント
		$lessonnum['speciallesson'] = $this->Lesson->find('count', array(
				'conditions' => array('Lesson.category' => 1),
			)
		);
		$lessonnum['breadlesson'] = $this->Lesson->find('count', array(
				'conditions' => array('Lesson.category' => 2),
			)
		);
		$lessonnum['sweetlesson'] = $this->Lesson->find('count', array(
				'conditions' => array('Lesson.category' => 3),
			)
		);
		$lessonnum['taikenlesson'] = $this->Lesson->find('count', array(
				'conditions' => array('Lesson.category' => 4),
			)
		);
		$this->set('lessonnum', $lessonnum);

		//日付
		$dateArray = array(
			date('Y'),
			date('Y', strtotime("+1 year")),
		);

		$this->set('dateArray', $dateArray);

	}

	function index($param = null){
		$today = date('Y-m-d');
		App::import('Sanitize');
		$param = Sanitize::clean($param);
		if(empty($param)){
			$this->Lesson->hasMany['LessonImage']['fields'] = array('uploadpath');
			$this->Lesson->hasMany['LessonDate']['fields'] = array('date');
			$this->Lesson->hasMany['LessonDate']['order'] = array('date ASC');

			$datas = $this->Lesson->find('all', array(
					'conditions' => array('del_flg' => 0, "Lesson.limitday > '{$today}'"),
					'fields' => array('Lesson.id', 'Lesson.category', 'Lesson.title', 'Lesson.description', 'Lesson.recomflg', 'Lesson.active'),
					'order'=>array('id' => 'DESC')
				)
			);
			$this->set('data', $datas);
		}else{
			$this->Lesson->hasMany['LessonImage']['fields'] = array('uploadpath');
			$this->Lesson->hasMany['LessonDate']['fields'] = array('date');
			$this->Lesson->hasMany['LessonDate']['order'] = array('date ASC');

			$datas = $this->Lesson->find('all', array(
					'conditions' => array('Lesson.category' => $param, 'del_flg' => 0, "Lesson.limitday > '{$today}'"),
					'fields' => array('Lesson.id', 'Lesson.category', 'Lesson.title', 'Lesson.description', 'Lesson.recomflg', 'Lesson.active'),
					'order'=>array('id' => 'DESC')
				)
			);
			$this->set('data', $datas);
		}
		//pr($datas);
		$this->set('p', $param);

		//$calendar = $this->LessonDate->find('all');

		$thismonth = date('Y-m');//今月
		$nmonth = date('Y-m', strtotime(date('Y-m-1').' +1 month'));//来月
		$nnmonth = date('Y-m', strtotime(date('Y-m-1').' +2 month'));//再来月

		//date('Y年m月', strtotime(date('Y-m-1').' -1 month'));//先月

		$tmonth = array("DATE_FORMAT(LessonDate.date, '%Y-%m')" => "{$thismonth}");
		$data = $this->LessonDate->find('all', array(
				'conditions' => $tmonth,
				'fields' => array('id', 'lesson_id', 'date'),
				'order'=>array()
			)
		);

		//debug($data);

	}
	function past($param = null){
		$today = date('Y-m-d');
		App::import('Sanitize');
		$param = Sanitize::clean($param);
		if(empty($param)){
			$this->Lesson->hasMany['LessonImage']['fields'] = array('uploadpath');
			$this->Lesson->hasMany['LessonDate']['fields'] = array('date');

			$datas = $this->Lesson->find('all', array(
					'conditions' => array('del_flg' => 0, "Lesson.limitday <= '{$today}'"),
					'fields' => array('Lesson.id', 'Lesson.category', 'Lesson.title', 'Lesson.description', 'Lesson.recomflg', 'Lesson.active'),
					'order'=>array('id' => 'DESC')
				)
			);
			$this->set('data', $datas);
		}else{
			$this->Lesson->hasMany['LessonImage']['fields'] = array('uploadpath');
			$this->Lesson->hasMany['LessonDate']['fields'] = array('date');
			$datas = $this->Lesson->find('all', array(
					'conditions' => array('Lesson.category' => $param, 'del_flg' => 0, "Lesson.limitday <= '{$today}'"),
					'fields' => array('Lesson.id', 'Lesson.category', 'Lesson.title', 'Lesson.description', 'Lesson.recomflg', 'Lesson.active'),
					'order'=>array('id' => 'DESC')
				)
			);
			$this->set('data', $datas);
		}
		$this->set('p', $param);
	}
	function add(){

		App::import('Sanitize');

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Lesson']['token']){

				//$this->data = Sanitize::clean($this->data);


				if($this->data['Lesson']['mode'] === '登録する'){
					//debug($this->data);
					$this->Lesson->set($this->data);
					if($this->Lesson->validates()){
						if(!$this->Lesson->save($this->data)){
							echo "レッスン登録中にエラーが発生しました。";
						}else{
							//レッスン日程保存
							foreach($this->data['LessonDate'] as $k => $v){
								if(!empty($v['datetxt'])){
									$v['lesson_id'] = $this->Lesson->id;
									$v['capacity'] = $v['limit'];
									if(!$this->Lesson->LessonDate->saveAll($v)){//エラー処理しよう
										echo "レッスン登録中にエラーが発生しました。";
									}
								}
							}
							$num = 0;
							foreach($this->data['LessonImage'] as $k => $v ){

								$keys = "fileName".$num;
								if(!empty($this->data['Lesson'][$keys]['name'])){
									$v['lesson_id'] = $this->Lesson->id;
									if(!$this->Lesson->LessonImage->saveAll($v)){//エラー処理しよう
										echo "レシピ登録中にエラーが発生しました。";
									}
									if($data = $this->Uploader->upload("fileName{$num}", array('overwrite' => true, 'name' => $this->Lesson->id))) {
										if($data['width'] === $data['height']){
											$resized_path = $this->Uploader->resize(array('width' => $this->thumbsize, 'append' => '_th', 'quality' => 70));
										}else{
											$resized_path = $this->Uploader->crop(array('width' => $this->thumbsize, 'height' => $this->thumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
										}

										$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'lesson'.DS;
										if(!is_dir($folder_url.$this->Lesson->id)) {
											mkdir($folder_url.$this->Lesson->id);
										}
										//アップロードファイル移動
										$jpgName = $num + 1;
										$resize_url = $folder_url.$this->Lesson->id.DS.$jpgName.'.jpg';
										$thumbs_url = $folder_url.$this->Lesson->id.DS.$jpgName.'_th.jpg';

										$this->Uploader->move($data['path'], $resize_url, true);
										$this->Uploader->move($resized_path, $thumbs_url, true);

										//$uploadfile['id'] = $this->data['RecipeImage'][$num]['id'];
										$uploadfile['id'] = $this->Lesson->LessonImage->id;
										$uploadfile['lesson_id'] = $this->Lesson->id;
										$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

										$this->Lesson->LessonImage->saveAll($uploadfile);

									}
									$num++;
								}
							}
							$this->Session->setFlash('登録完了しました。');
							$this->redirect('../adminlessons');
						}
					}
				}
			}else{
				$this->render('complete/add_error');
			}
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->data['Lesson']['token'] = $token;
	}
	function edit($param = null){

		$this->Lesson->hasMany['LessonDate']['order'] = array('date ASC');

		App::import('Sanitize');

		set_time_limit(0);

		//debug($this->data);

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Lesson']['token']){
				//$this->data = Sanitize::clean($this->data);//ここチェックよ

				if($this->data['Lesson']['mode'] === '修正する'){

					$this->set('data', $this->data);
					$this->Lesson->set($this->data);
					if($this->Lesson->validates()){
						if(!$this->Lesson->save($this->data)){
							echo "物件登録中にエラーが発生しました。";
						}else{
							//レッスン日程保存
							foreach($this->data['LessonDate'] as $k => $v){
								if(!empty($v['date'])){
									$v['lesson_id'] = $this->Lesson->id;
									if(!$this->Lesson->LessonDate->saveAll($v)){
										echo "レッスン登録中にエラーが発生しました。";
									}
								}else{
									if($v['id']){
										if(!$this->Lesson->LessonDate->delete($v['id'])){//削除
											echo "レッスン登録中にエラーが発生しました。";
										}
									}
								}
							}


							$num = 0;
							foreach($this->data['LessonImage'] as $k => $v ){
								//$num = $v['id'] - 1;
								if(!$this->Lesson->LessonImage->saveAll($v)){//エラー処理しよう
									echo "レッスン登録中にエラーが発生しました。";
								}

								if(!empty($v['delphoto'])){
									if($v['delphoto'] == 1){
										$photodel = array('id' => $v['id'], 'uploadpath' => null);
										if(!$this->Lesson->LessonImage->save($photodel)){
											echo "レッスン登録中にエラーが発生しました。";
										}
									}
								}
								//pr($this->data);

								if($data = $this->Uploader->upload("fileName{$num}", array('overwrite' => true, 'name' => $this->data['Lesson']['id']))) {
									if($data['width'] === $data['height']){
										$resized_path = $this->Uploader->resize(array('width' => $this->thumbsize, 'append' => '_th', 'quality' => 70));
									}else{
										$resized_path = $this->Uploader->crop(array('width' => $this->thumbsize, 'height' => $this->thumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
									}

									$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'lesson'.DS;
									if(!is_dir($folder_url.$this->data['Lesson']['id'])) {
										mkdir($folder_url.$this->data['Lesson']['id']);
									}
									//アップロードファイル移動
									$jpgName = $num + 1;
									$resize_url = $folder_url.$this->data['Lesson']['id'].DS.$jpgName.'.jpg';
									$thumbs_url = $folder_url.$this->data['Lesson']['id'].DS.$jpgName.'_th.jpg';

									$this->Uploader->move($data['path'], $resize_url, true);
									$this->Uploader->move($resized_path, $thumbs_url, true);


									if(!empty($this->data['LessonImage'][$num]['id'])){
										$uploadfile['id'] = $this->data['LessonImage'][$num]['id'];
									}else{
										$uploadfile['id'] = $this->Lesson->LessonImage->id;
									}
									$uploadfile['lesson_id'] = $this->data['Lesson']['id'];
									$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

									$this->Lesson->LessonImage->save($uploadfile);
								}
								$num++;
							}
							$this->Session->setFlash('修正完了しました。');
							$this->redirect('../adminlessons');
						}

					}
				}else if($this->data['Lesson']['mode'] === '削除する'){

					$conditions = array(
						'Lesson.id' => $this->data['Lesson']['id']
					);

					//pr($this->data);

					if(!$this->Lesson->delete($this->data['Lesson']['id'])){
					}

					$this->render('complete/delete_complete');
				}
			}else{
				$this->render('complete/add_error');
			}
		}else{
			$this->Lesson->id = $param;
			$this->data = $this->Lesson->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->data['Lesson']['token'] = $token;
	}

	function duplicates($param = null){

		App::import('Sanitize');

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Lesson']['token']){

				//$this->data = Sanitize::clean($this->data);


				if($this->data['Lesson']['mode'] === '登録する'){
					//debug($this->data);
					if($this->Lesson->validates()){
						if(!$this->Lesson->save($this->data)){
							echo "レッスン登録中にエラーが発生しました。";
						}else{
							//レッスン日程保存
							foreach($this->data['LessonDate'] as $k => $v){
								if(!empty($v['datetxt'])){
									$v['lesson_id'] = $this->Lesson->id;
									$v['capacity'] = $v['limit'];
									if(!$this->Lesson->LessonDate->saveAll($v)){//エラー処理しよう
										echo "レッスン登録中にエラーが発生しました。";
									}
								}
							}
							$num = 0;
							foreach($this->data['LessonImage'] as $k => $v ){

								$keys = "fileName".$num;
								if(!empty($this->data['Lesson'][$keys]['name'])){
									$v['lesson_id'] = $this->Lesson->id;
									if(!$this->Lesson->LessonImage->saveAll($v)){//エラー処理しよう
										echo "レシピ登録中にエラーが発生しました。";
									}
									if($data = $this->Uploader->upload("fileName{$num}", array('overwrite' => true, 'name' => $this->Lesson->id))) {
										if($data['width'] === $data['height']){
											$resized_path = $this->Uploader->resize(array('width' => $this->thumbsize, 'append' => '_th', 'quality' => 70));
										}else{
											$resized_path = $this->Uploader->crop(array('width' => $this->thumbsize, 'height' => $this->thumbsize, 'append' => '_th', 'location' => UploaderComponent::LOC_CENTER));
										}

										$folder_url = WWW_ROOT.'files'.DS.'uploads'.DS.'lesson'.DS;
										if(!is_dir($folder_url.$this->Lesson->id)) {
											mkdir($folder_url.$this->Lesson->id);
										}
										//アップロードファイル移動
										$jpgName = $num + 1;
										$resize_url = $folder_url.$this->Lesson->id.DS.$jpgName.'.jpg';
										$thumbs_url = $folder_url.$this->Lesson->id.DS.$jpgName.'_th.jpg';

										$this->Uploader->move($data['path'], $resize_url, true);
										$this->Uploader->move($resized_path, $thumbs_url, true);

										//$uploadfile['id'] = $this->data['RecipeImage'][$num]['id'];
										$uploadfile['id'] = $this->Lesson->LessonImage->id;
										$uploadfile['lesson_id'] = $this->Lesson->id;
										$uploadfile['uploadpath'] = str_replace(WWW_ROOT, '', $resize_url);

										$this->Lesson->LessonImage->saveAll($uploadfile);

									}
									$num++;
								}
							}

						}
					}
				}
				$this->Session->setFlash('登録完了しました。');
				$this->redirect('../adminlessons');
			}else{
				$this->render('complete/add_error');
			}
		}else{
			$this->Lesson->id = $param;
			$this->data = $this->Lesson->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->data['Lesson']['token'] = $token;
	}
	function dateedit($param = null){


		$this->Lesson->hasMany['LessonDate']['order'] = array('date ASC');

		App::import('Sanitize');

		set_time_limit(0);

		if(!empty($this->data)){
			if((string)$this->Session->read('token') === (string)$this->data['Lesson']['token']){
				//$this->data = Sanitize::clean($this->data);//ここチェックよ

				if($this->data['Lesson']['mode'] === '修正する'){

					$this->set('data', $this->data);

					if($this->Lesson->validates()){
						if(!$this->Lesson->save($this->data)){
							echo "物件登録中にエラーが発生しました。";
						}else{
							//レッスン日程保存
							foreach($this->data['LessonDate'] as $k => $v){
								if(!empty($v['limit'])){
									$v['lesson_id'] = $this->Lesson->id;
									if(!$this->Lesson->LessonDate->saveAll($v)){//@ado エラー処理しよう
										echo "レッスン登録中にエラーが発生しました。";
									}
								}
							}


							$this->render('complete/update_complete');
						}

					}
				}else if($this->data['Lesson']['mode'] === '削除する'){

					$conditions = array(
						'Lesson.id' => $this->data['Lesson']['id']
					);

					//pr($this->data);

					if(!$this->Lesson->delete($this->data['Lesson']['id'])){
					}

					$this->render('complete/delete_complete');
				}
				$this->Session->setFlash('修正完了しました。');
				$this->redirect('../adminlessons');
			}else{
				$this->render('complete/add_error');
			}
		}else{
			$this->Lesson->id = $param;
			$this->data = $this->Lesson->read();
			$this->set('data', $this->data);
		}
		$token = $this->Common->_rand(12);
		$this->Session->write('token', $token);
		$this->data['Lesson']['token'] = $token;
	}

	function delete($param = null){
		if(!empty($param)){

			if(!empty($this->data['mode'])){
				$this->data['mode'] = 'はい';
			}else{
				$this->data['mode'] = null;
			}

			$this->data['Lesson']['id'] = $param;
			$this->data['Lesson']['del_flg'] =  1;


			if($this->data['mode'] == 'はい'){
				if(!$this->Lesson->save($this->data)){
					$this->Session->setFlash('削除に失敗しました。');
					$this->redirect('../adminlessons/');
				}else{
					$this->Session->setFlash('削除しました。');
					$this->redirect('../adminlessons/');
				}
			}else{
				$data['list'] = $this->Lesson->find('first', array(
						'conditions' => array('id' => $param),
					)
				);
				$this->set('data', $data);
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminlessons/');
		}
	}

	//Ajax　表示状態更新
	function active() {

		//バリデーションを無効に
		$this->Lesson->setValidation('ajax');

		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->RequestHandler->isAjax()){
			// POST情報は$this->params['form']で取得
			$id = $this->params['form']['id'];
			$activeparam = $this->params['form']['actives'];
			// DBに突っ込みます
			$this->Lesson->id = $id;
			$this->data['Lesson']['active'] = $activeparam;

			if(!$this->Lesson->save($this->data)){
				$message = '更新失敗！';
				$this->set('t', $message);
			}else{
				$message = '更新完了！';
				$this->set('t', $message);
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminlessons/');
		}
	}
	function recom() {

		//バリデーションを無効に
		$this->Lesson->setValidation('ajax');
		// デバッグ情報出力を抑制
		Configure::write('debug', 0);

		if($this->RequestHandler->isAjax()){
			// POST情報は$this->params['form']で取得

			$this->Lesson->query('UPDATE lessons SET recomflg = 0');

			$id = $this->params['form']['id'];
			$activeparam = $this->params['form']['actives'];
			// DBに突っ込みます
			$this->Lesson->id = $id;
			$this->data['Lesson']['id'] = $id;
			if(!empty($activeparam)){
				$this->data['Lesson']['recomflg'] = $activeparam;
			}else{
				$this->data['Lesson']['recomflg'] = 0;
			}

			if(!$this->Lesson->save($this->data)){
				$message = '更新失敗！';
				$this->set('t', $message);
			}else{
				$message = $this->data;
				$this->set('t', $message);
			}
		}else{
			$this->Session->setFlash('不正なアクセスです。');
			$this->redirect('../adminlessons/');
		}
	}

}
