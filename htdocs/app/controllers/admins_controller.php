<?php
class AdminsController extends AppController{

	public $name = 'Admins';
	public $uses = array('User', 'Recipe', 'RecipeCategory');

	public $components = array('Auth');

	public $autoRender = true;
	public $layout     = "admin/siteframe";


	function beforeFilter(){

        $this->Auth->authError = 'ログインしてください。';

		$this->loadModel('Wordpress');

		$category = $this->RecipeCategory->find('all');
		$this->set('category', $category);

		$bodyId = 'home';
		$this->set('bodyId', $bodyId);

		$wordpress = $this->Wordpress->url;
		$this->set('wordpress', $wordpress);

	}
	function index(){
		$data = $this->Recipe->find('all', array(
				'conditions' => array(),
				'fields' => array(),
				'order' => array(),
			)
		);
		$this->set('data', $data);
	}
}
