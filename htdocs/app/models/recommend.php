<?php
class Recommend extends AppModel {
	public $name = 'Recommend';
	public $actsAs = array('Multivalidatable'); 
	
	public $hasMany = array(
		'RecommendImage' => array(
			'className'  => 'RecommendImage',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'recommend_id',
		),
	);
	
	function notSpace($field=array()) {
		foreach($field as $name => $value){
			if (preg_match("/^( |　)+$/", $value)) {
				return false;
			} else {
				return true;
			}
		}
	}
	
	public $validate = array(
		'title' => array(
			array('rule'    => array('notEmpty'), 'message' => 'タイトルを記入してください。'),
			array('rule'    => 'notSpace', 'message' => 'タイトルを記入してください。')
		)
	);
	
	public $validationSets = array(
		'ajax' => array()
	);
}
