<?php
class Recipe extends AppModel {
	public $name = 'Recipe';
	public $actsAs = array('Multivalidatable'); 
	
	public $hasMany = array(
		'RecipeImage' => array(
			'className'  => 'RecipeImage',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'recipe_id',
		),
		'RecipeMaterial' => array(
			'className'  => 'RecipeMaterial',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'recipe_id',
		),
		'RecipeOrder' => array(
			'className'  => 'RecipeOrder',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'recipe_id',
		),
		'RecipeRelated' => array(
			'className'  => 'RecipeRelated',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'recipe_id',
		),
	);
	
	function notSpace($field=array()) {
		foreach($field as $name => $value){
			if (preg_match("/^( |　)+$/", $value)) {
				return false;
			} else {
				return true;
			}
		}
	}
	
	public $validate = array(
		'title' => array(
			array('rule'    => array('notEmpty'), 'message' => 'タイトルを記入してください。'),
			array('rule'    => 'notSpace', 'message' => 'タイトルを記入してください。')
		)
	);
	
	public $validationSets = array(
		'ajax' => array()
	);
}
