<?php
class Lcategory extends AppModel {
	public $name = 'Lcategory';
	public $useTable = false;
	
	public $lcategory = array(
		'1' => '特別企画',
		'2' => 'パン教室',
		'3' => 'お菓子教室',
		'4' => '体験教室',
	);
	
}
