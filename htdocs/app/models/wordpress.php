<?php
class Wordpress extends AppModel {
	public $name = 'Wordpress';
	public $useTable = false;
	
	public $url = array(
		'pantry'      => 'http://www.marusanpantry.com/pantry/wp-login.php',
		'kitchenshop' => 'http://www.marusanpantry.com/kitchenshop/wp-login.php',
	);
	
}
