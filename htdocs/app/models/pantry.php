<?php
class Pantry extends AppModel {
	public $name = 'Pantry';
	public $actsAs = array('Multivalidatable'); 
	
	public $hasMany = array(
		'PantryImage' => array(
			'className'  => 'PantryImage',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'pantry_id',
		),
	);
	function notSpace($field=array()) {
		foreach($field as $name => $value){
			if (preg_match("/^( |　)+$/", $value)) {
				return false;
			} else {
				return true;
			}
		}
	}
	
	public $validate = array(
		'title' => array(
			array('rule'    => array('notEmpty'), 'message' => 'タイトルを記入してください。'),
			array('rule'    => 'notSpace', 'message' => 'タイトルを記入してください。')
		)
	);
	
	public $validationSets = array(
		'ajax' => array()
	);
}