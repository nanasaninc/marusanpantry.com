<?php
class Reserve extends AppModel {
	public $name = 'Reserve';
	public $useTable = false;
	
	public $_schema = array(
		'name' => array( 
			'type' => 'varchar',
			'default'=>null,
		),
		'phone' => array( 
			'type' => 'varchar',
			'default'=>null,
		),
		'email' => array(
			'type' => 'varchar',
			'default'=>null,
		),
		'mailconfirm' => array(
			'type' => 'varchar',
			'default'=>null,
		), 
	);

	function checkCompare(){
		if($this->data['Reserve']['email'] != $this->data['Reserve']['mailconfirm']){
			return false;
		}else{
			return true;
		}
	}
	function notSpace($field=array()) {
		foreach($field as $name => $value){
			if (preg_match("/^( |　)+$/", $value)) {
				return false;
			} else {
				return true;
			}
		}
	}
	
	public $validate = array(
		'name' => array(
			array('rule'    => array('notEmpty'), 'message' => '氏名を記入してください。'),
			array('rule'    => 'notSpace', 'message' => '氏名を記入してください。')
		),
		'phone' => array(
			array('rule'    => 'notEmpty', 'required'=> true, 'message' => '電話番号を記入してください。'),
			array('rule'    => 'notSpace', 'message' => '電話番号を記入してください。')
		),
		'email'  => array(
			array('rule'     => array('email', false), 'required' => true, 'message'  => 'メールアドレスを記入してください。'),
		),
		'mailconfirm' => array(
			'notEmpty'    => array('rule' => array('email', false), 'required' => true, 'message' => '確認用メールアドレスを入力してください'),
			'equal'       => array('rule' => 'checkCompare', 'message' => '上記メールアドレスと異なります。ご確認をお願いいたします。')
		)
	);
}
