<?php
class Recipecat extends AppModel {
	public $name = 'Recipecat';
	public $useTable = false;
	
	public $category = array(
		'1' => 'パン教室',
		'2' => 'お菓子教室',
		'3' => '特別企画',
	);
	
}
