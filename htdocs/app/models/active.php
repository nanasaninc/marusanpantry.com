<?php
class Active extends AppModel {
	public $name = 'Active';
	public $useTable = false;
	
	public $active = array(
		'0' => '表示',
		'1' => '非表示',
	);
}
