<?php
class Lesson extends AppModel {
	public $name = 'Lesson';
	public $actsAs = array('Multivalidatable');
	
	public $hasMany = array(
		'LessonImage' => array(
			'className'  => 'LessonImage',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'lesson_id',
		),
		'LessonDate' => array(
			'className'  => 'LessonDate',
			'dependent'  => true,
			'order' => '',
			'foreignKey' => 'lesson_id',
		),
	);
	
	function notSpace($field = array()) {
		foreach($field as $name => $value){
			if (preg_match("/^( |　)+$/", $value)) {
				return false;
			} else {
				return true;
			}
		}
	}
	
	public $validate = array(
		'title' => array(
			array('rule'    => array('notEmpty'), 'message' => 'タイトルを記入してください。'),
			array('rule'    => 'notSpace', 'message' => 'タイトルを記入してください。')
		),
		'category' => array(
			array('rule'    => array('notEmpty'), 'message' => 'カテゴリを選択してください。'),
			array('rule'    => 'notSpace', 'message' => 'カテゴリを選択してください。')
		)
	);
	
	public $validationSets = array(
		'ajax' => array()
	);
}
