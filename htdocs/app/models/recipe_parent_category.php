<?php
class RecipeParentCategory extends AppModel {
	public $name = 'RecipeParentCategory';
	public $useTable = false;
	
	public $categoryname = array(
		'1' => 'パン',
		'2' => 'お菓子',
	);
}
