<?php
class Difficulty extends AppModel {
	public $name = 'Difficulty';
	public $useTable = false;
	
	public $difficulty = array(
		'1' => '★',
		'2' => '★★',
		'3' => '★★★',
		'4' => '★★★★',
		'5' => '★★★★★',
	);
	
}
