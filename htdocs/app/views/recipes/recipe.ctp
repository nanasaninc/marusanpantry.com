<style type="text/css">
	p.relatedimg img{
		width: 147px;
	}
</style>
		
		<p class="topicPath"><a href="<?php echo $html->url('/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><a href="../">レシピ</a><span>&gt;</span><?php echo nl2br(h($data['Recipe']['title'])); ?></p>
		<h1><img src="<?php echo $html->url('/', true); ?>img/recipe/h1.png" alt="レシピ" width="89" height="23" /></h1>
		<div id="primary">
			<div class="box">
				<div id="photoBox">
					<p>
					<?php if(!empty($data['RecipeImage'][0]['uploadpath'])): ?>
		<?php $path = str_replace('.jpg', '_th.jpg', $data['RecipeImage'][0]['uploadpath']); ?>
		<img src="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="" width="200" />
		<?php else: ?>
		<img src="<?php echo $html->url('/img/common/nowprinting.jpg'); ?>" alt="" width="200" />
		<?php endif; ?>
		
		</p>
				</div>
				<div id="contentBox">
<?php $limitday = date('Y-m-d H:i:s', strtotime($data['Recipe']['created'].' +30 day')); ?>
<?php if($today < $limitday): ?>
					<h2 class="new"><?php echo nl2br(h($data['Recipe']['title'])); ?></h2>
<?php else: ?>
					<h2><?php echo nl2br(h($data['Recipe']['title'])); ?></h2>
<?php endif; ?>
					<p class="btn-print"><a href="javascript:void(0)" onclick="print()"><img src="<?php echo $html->url('/', true); ?>img/common/btn-print.png" width="84" height="28" alt="印刷する" class="btn" /></a></p>
					<p class="diff"><img src="<?php echo $html->url('/', true); ?>img/recipe/diff0<?php echo $data['Recipe']['difficulty']; ?>.png" width="137" height="16" /></p>
					<p class="comment"><?php echo nl2br(h($data['Recipe']['description'])); ?></p>
					<div class="servingsBox">
						<h3>材料（<?php echo nl2br(h($data['Recipe']['numberpeople'])); ?>）</h3>
						<table border="0" cellpadding="0" cellspacing="0">
<?php foreach($data['RecipeMaterial'] as $key => $val): ?>
							<tr>
								<td><?php echo nl2br(h($val['name'])); ?></td>
								<td class="right"><?php echo nl2br(h($val['num'])); ?></td>
							</tr>
<?php endforeach; ?>
						</table>
					</div>
				</div>
				<div id="recipeBox" class="clearfix heightAlign">
<?php foreach($data['RecipeOrder'] as $key => $val): ?>
					<div class="box">
						<p class="num"><?php echo $val['sort'] + 1; ?></p>
						<p><?php if(!empty($val['uploadpath'])): ?>
						<?php $path = str_replace('.jpg', '_th.jpg', $val['uploadpath']); ?>
						<a class="fancybox" rel="order" href="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>"><img src="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="" width="100" /></a>
						<?php endif; ?></p>
						<p><?php echo nl2br(h($val['comment'])); ?></p>
					</div>
<?php endforeach; ?>
				</div>
<?php if(!empty($data['Recipe']['point'])): ?>
				<div id="pointBox">
					<h3><img src="<?php echo $html->url('/', true); ?>img/recipe/h3-point.png" alt="" width="158" height="39" /></h3>
					<p><?php echo nl2br(h($data['Recipe']['point'])); ?></p>
				</div>
<?php endif; ?>
			</div>
			<div class="box" id="relatedBox">
				<h2><img src="<?php echo $html->url('/', true); ?>img/recipe/h2-related.png" width="680" height="60" /></h2>
<?php foreach($data['RecipeRelated'] as $key => $val): ?>
				<div class="box">
					<p class="relatedimg">
						<a target="_blank" href="<?php echo $val['link']; ?>"><?php echo $val['uploadpath']; ?></a>
					</p>
					<p>
						<a target="_blank" href="<?php echo $val['link']; ?>"><?php echo nl2br(h($val['name'])); ?></a><br />
						<?php echo nl2br(h($val['price'])); ?>
					</p>
				</div>
<?php endforeach; ?>
			</div>
			<?php echo $this->element('frontrecipe/searchbox'); ?>
<?php echo $this->element('frontrecipe/frontcategoryBox'); ?>
		</div>
