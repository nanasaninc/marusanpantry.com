<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Script-Type" content="javascript" />
<title>マルサンパントリー　ウェブサイト管理</title>
<link href="<?php echo $html->url('/', true); ?>css/admin.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo $html->url('/', true); ?>js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="<?php echo $html->url('/', true); ?>js/admin.js"></script>
<script type="text/javascript" src="<?php echo $html->url('/', true); ?>js/meca.js"></script>
</head>
<body id="login">
<div id="wrapper">
	<div id="header">
		<p id="logo"><img src="<?php echo $html->url('/', true); ?>img/admin/logo.png" alt="マルサンパントリー　ウェブサイト管理" width="399" height="45" /></p>
	</div>
	<div id="container">
<?php echo $content_for_layout; ?>
</div>
		</div>
		<div id="secondary"> </div>
	</div>
	<div id="footer"><img src="<?php echo $html->url('/', true); ?>img/admin/copy.png" alt="Copyright ˝ matsuyama marusan pantry. All Rights Reserved." width="386" height="12" /></div>
</div>
</body>
</html>
