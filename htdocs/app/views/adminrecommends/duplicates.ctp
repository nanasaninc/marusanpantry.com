	<h1>キッチンショップスタッフのイチオシ管理画面</h1>
		<div id="primary">
			<div class="wbox">
				<h2>基本情報登録</h2>
<?php echo $form->create(null, array('enctype'=>'multipart/form-data')); ?>
<?php echo $form->input('token', array('type' => 'hidden')); ?>
				<div id="photoBox">
	<?php for($i = 0;$i < 2;$i++): ?>
	<div class="btn-upload">
		<?php $num = $i + 1; ?>
		<?php echo $form->hidden("RecommendImage.{$i}.sort", array('value' => "{$num}")); ?>
		<p><?php echo $form->input("fileName{$i}", array('type' => 'file', 'label' => false,'class' => 'file')).PHP_EOL; ?></p></div>
		<?php endfor; ?>

				</div>
	<div id="contentBox">
		<dl>
			<dt>タイトル</dt>
			<dd><?php echo $form->text('Recommend.title',array('size' => '40')); ?></dd>
		</dl>
		<dl>
			<dt>説明文</dt>
			<dd><?php echo $form->textarea('Recommend.description',array('cols' => '40','rows' => '5')); ?></dd>
		</dl>
		<dl>
			<dt>ポイント</dt>
			<dd><?php echo $form->textarea('Recommend.point',array('cols' => '40','rows' => '5')); ?></dd>
		</dl>
		</div>
	</div>
	
			<p class="btn-back">
				<input onclick="history.back();" name="" type="button" value="戻る" />
			</p>
			<p class="btn">
				<?php echo $form->submit('登録する', array('name' => 'data[Recommend][mode]', 'div' => false)), PHP_EOL; ?>
			</p>
<?php echo $form->end(); ?>
</div>