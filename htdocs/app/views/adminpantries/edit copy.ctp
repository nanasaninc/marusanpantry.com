	<h1>マルサンパントリースタッフのイチオシ管理画面</h1>
		<div id="primary">
			<div class="wbox">
				<h2>基本情報登録</h2>
<?php echo $form->create(null, array('enctype'=>'multipart/form-data')); ?>
<?php echo $form->input('token', array('type' => 'hidden')); ?>
<?php echo $form->hidden("Pantry.id"); ?>
				<div id="photoBox">
	<?php for($i = 0;$i < 2;$i++): ?>
	<div class="btn-upload">
		<?php $num = $i + 1; ?>
		<?php echo $form->hidden("PantryImage.{$i}.id").PHP_EOL; ?>
		<?php echo $form->hidden("PantryImage.{$i}.sort", array('value' => "{$num}")); ?>
		<?php if(!empty($data['PantryImage'][$i]['uploadpath'])): ?>
				<?php $path = str_replace('.jpg', '_th.jpg', $data['PantryImage'][$i]['uploadpath']); ?>
				<img src="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>" width="200" alt="" />
				<?php endif; ?>
		<p><?php echo $form->input("fileName{$i}", array('type' => 'file', 'label' => false,'class' => 'file')).PHP_EOL; ?></p></div>
		<?php endfor; ?>

				</div>
	<div id="contentBox">
		<dl>
			<dt>タイトル</dt>
			<dd><?php echo $form->text('Pantry.title',array('size' => '40')); ?></dd>
		</dl>
		<dl>
			<dt>説明文</dt>
			<dd><?php echo $form->textarea('Pantry.description',array('cols' => '40','rows' => '5')); ?></dd>
		</dl>
		<dl>
			<dt>ポイント</dt>
			<dd><?php echo $form->textarea('Pantry.point',array('cols' => '40','rows' => '5')); ?></dd>
		</dl>
		</div>
	</div>
	
			<p class="btn-back">
				<input name="" type="button" value="戻る" />
			</p>
			<p class="btn">
				<?php echo $form->submit('修正する', array('name' => 'data[Pantry][mode]', 'div' => false)), PHP_EOL; ?>
			</p>
<?php echo $form->end(); ?>
</div>