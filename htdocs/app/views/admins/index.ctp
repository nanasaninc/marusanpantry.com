<h1>マルサンパントリーウェブサイト管理</h1>
		<div id="primary">
			<div class="wbox">
				<h2>管理メニュー</h2>
				<p>管理するコンテンツを選択して下さい</p>
				<ul>
					<li><a href="<?php echo $html->url('/adminrecipes/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/admin/icon-recipe.png" alt="リンク：レシピ管理" width="110" height="110" /></a></li>
					<li><a href="<?php echo $html->url('/adminlessons/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/admin/icon-lesson.png" alt="リンク：教室管理" width="110" height="110" /></a></li>
					<li><a href="<?php echo $wordpress['kitchenshop']; ?>" target="_blank"><img src="<?php echo $html->url('/', true); ?>img/admin/icon-kitcheninfo.png" alt="リンク：キッチンショップお知らせ" width="110" height="110" /></a></li>
					<li><a href="<?php echo $html->url('/adminrecommends/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/admin/icon-kitchen.png" alt="リンク：キッチンショップスタッフイチオシ管理" width="110" height="110" /></a></li>
					<li><a href="<?php echo $wordpress['pantry']; ?>" target="_blank"><img src="<?php echo $html->url('/', true); ?>img/admin/icon-pantryinfo.png" alt="リンク：パントリーお知らせ" width="110" height="110" /></a></li>
					<li><a href="<?php echo $html->url('/adminpantries/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/admin/icon-pantry.png" alt="リンク：パントリースタッフイチオシ管理" width="110" height="110" /></a></li>
				</ul>
			</div>
		</div>
		