<?php
	$v = $data['list']['Lesson'];
?>

<h1>パン・お菓子教室管理画面</h1>

<div id="primary">
	<div class="wbox">
<?php if(!empty($data['list'])): ?>
		<h2>教室データ削除</h2>
		<p>No.<?php echo $v['id']; ?>　<?php echo $v['title']; ?>　<a href="<?php echo $html->url("/lessons/lesson/{$v['id']}"); ?>" target="_blank">【公開中のページを確認する】</a></p>
		<p>本当に上記データを削除しますか？</p>
<?php echo $form->create(null, array()); ?>
		<p class="btn-back">
			<input onclick="history.back();" name="" type="button" value="いいえ" />
		</p>
		<p class="btn">
			<?php echo $form->submit('はい', array('name' => 'data[mode]', 'div' => false)), PHP_EOL; ?>
		</p>
<?php echo $form->end(); ?>
<?php endif; ?>
		
		
	</div>
</div>