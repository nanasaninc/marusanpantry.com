
<script type="text/javascript">
	$(function(){
		$('.datepicker').change(function(){
			$(this).parent().next().next().children().val('8');
		});
	});
</script>
		<h1>パン・お菓子教室管理画面</h1>
		<div id="primary">
			<div class="wbox">
				<h2>基本情報登録</h2>
<?php echo $form->create(null, array('enctype'=>'multipart/form-data')); ?>
<?php echo $form->input('token', array('type' => 'hidden')); ?>
	<?php echo $form->hidden("Lesson.id"); ?>
				<p>
					<strong>表示</strong>
					<?php echo $form->select("Lesson.active", $active, null, array('empty' => false), true).PHP_EOL; ?>
				</p>
				<div id="photoBox">
					
<?php for($i = 0;$i < 2;$i++): ?>
		<div class="btn-upload">
		<p>
		<?php $num = $i + 1; ?>
		<?php if(!empty($data['LessonImage'][$i]['uploadpath'])): ?>
		<?php echo $form->hidden("LessonImage.{$i}.id").PHP_EOL; ?>
		<?php $path = str_replace('.jpg', '_th.jpg', $data['LessonImage'][$i]['uploadpath']); ?>
		<img src="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="" />
		<?php endif; ?>
		<?php echo $form->hidden("LessonImage.{$i}.sort", array('value' => "{$num}")); ?>
		<?php echo $form->input("fileName{$i}", array('type' => 'file', 'label' => false, 'class' => 'file')).PHP_EOL; ?>
		</p>
		</div>
<?php endfor; ?>
				</div>
	<div id="contentBox">
		<p><?php echo $form->select("Lesson.category", $lcategory, null, array('empty' => '選択してください'), true).PHP_EOL; ?></p>
		<p><?php echo $form->text('Lesson.title', array('size' => '40')).PHP_EOL; ?></p>
		<p><?php echo $form->textarea('Lesson.description', array('cols' => '40','rows' => '5')).PHP_EOL; ?></p>
		
		<dl>
			<dt>講師</dt>
			<dd><?php echo $form->text('Lesson.teacher', array('size' => '40')); ?></dd>
			<dt>開催時間</dt>
			<dd><?php echo $form->textarea('Lesson.morning', array('cols' => '40','rows' => '5')); ?></dd>
			<dt>参加費</dt>
			<dd><?php echo $form->text('Lesson.price', array('size' => '20')); ?></dd>
			<dt>持参品</dt>
			<dd><?php echo $form->textarea('Lesson.jisan', array('cols' => '40','rows' => '5')); ?></dd>
			<dt>定員</dt>
			<dd><?php echo $form->text('Lesson.memberlimit', array('value' => '12','size' => '10')); ?>人</dd>
			<dt>備考</dt>
			<dd><?php echo $form->textarea('Lesson.bikou', array('cols' => '40','rows' => '5')); ?></dd>
		</dl>
		</div>
	</div>
	<div class="wbox">
		<h2>開催日程登録</h2>
		<table border="0">
			<tr>
				<th>開催日</th>
				<th>開催時間</th>
				<th>WEB受付数</th>
			</tr>
		<?php for($i = 0;$i < 8; $i++): ?>
			<?php echo $form->hidden("LessonDate.{$i}.id"); ?>
			<tr>
				<td>
					<?php echo $form->text("LessonDate.{$i}.date", array('class' => 'datepicker')); ?>
					<?php echo $form->error('LessonDate.{$i}.date'); ?>
				</td>
				<td><?php echo $form->text("LessonDate.{$i}.datetxt"); ?></td>
				<td><?php echo $form->text("LessonDate.{$i}.limit"); ?>人</td>
			</tr>
	<?php endfor; ?>
		</table>
		</div>
		<p class="btn-back">
			<input name="" type="button" value="戻る" />
		</p>
		<p class="btn">
			<?php echo $form->submit('修正する', array('name' => 'data[Lesson][mode]', 'div' => false)), PHP_EOL; ?>
		</p>
<?php echo $form->end(); ?>
</div>
