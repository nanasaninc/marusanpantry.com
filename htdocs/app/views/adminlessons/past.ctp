<script type="text/javascript">
$(function(){
	$('select.active').change(function(e){
		var thiss = $(this);
		var ids = $(this).prev().val();
		var values = $(this).val();
		e.preventDefault();
		ajax_search(ids, values, thiss);
	});
	$('input.recom').change(function(e){
		$("p.osusumecomp").remove();
		var recomlength = $('input.recom').length -1;
		$(this).after('<p id="recoms"><img src="<?php echo $html->url('/'); ?>img/admin/recom.gif" /></p>');
		var rethis = $(this);
		$('input.recom').each(function(i){
			var thiss = $(this);
			var ids = $(this).prev().val();
			if(thiss.attr("checked")){
				var values = $(this).val();
			}else{
				var values = 0;
			}
			e.preventDefault();
			ajax_recom(ids, values, thiss, i, recomlength, rethis);
		});
		//$('#primary table').before('<p>おすすめ更新完了しました。</p>');
	});
	$('li.duplicates').click(function(){
		return confirm("コピーしてよろしいですか？");
	});
	$('li.deleteconfi').click(function(){
		return confirm("削除しますか？\nこの操作は取り消すことが出来ません。");
	});
});

function ajax_search(ids, values, thiss){
		var bool;
		$.post("<?php echo $html->url('/adminlessons/active/'); ?>", {id:ids, actives:values}, function(data){
			if (data.length > 0){
				thiss.after(data);
			}
		}
	)
}
function ajax_recom(ids, values, thiss, i, recomlength, rethis){
		var bool;
		$.post("<?php echo $html->url('/adminlessons/recom/'); ?>", {id:ids, actives:values}, function(data){
			if (data.length > 0){
				if(i == recomlength){
					//$('#primary table').before('<p>おすすめ更新完了しました。</p>');
					$('p#recoms').remove();
					rethis.after('<p class="osusumecomp">おすすめ教室に設定しました。</p>');
				}
			}
		}
	)
}
</script>
<?php echo $p; ?>
<h1>パン・お菓子教室管理画面</h1>
	<div id="primary">
			<div class="wbox">
				<h2>教室新規登録</h2>
				<p>教室を新規する場合は「教室新規作成」より教室の情報を入力してください。</p>
				<p class="btn"><a href="<?php echo $html->url('/', true); ?>adminlessons/add/">教室新規登録</a></p>
			<h2>開催予定教室一覧</h2>
			<p>開催予定の教室一覧です。内容を編集する場合は「修正」ボタンを、削除する場合は「削除」ボタンをクリックして下さい。</p>
<?php echo $this->Session->flash(); ?>


<?php if(!empty($data)): ?>
<table border="0" class="zebraList">
				<tr>
<?php if(empty($p)): ?>
					<th>おすすめ</th>
<?php endif; ?>
					<th>No</th>
					<th>教室タイプ</th>
					<th>画像</th>
					<th>教室名</th>
					<th>日程</th>
					<th>表示</th>
					<th>編集</th>
				</tr>
<?php $i=0; ?>
<?php foreach($data as $k => $v): ?>
	<tr>
<?php if(empty($p)): ?>
		<td>
			<label>
				<?php echo $form->hidden("Lesson.id", array('id' => false, 'class' => 'recommendid', 'value' => $v['Lesson']['id'])); ?>
				<?php if($v['Lesson']['recomflg'] == 1): ?>
					<input class="recom" name="recom[<?php echo $i; ?>]" type="radio" value="1" checked="checked" />
				<?php else: ?>
					<input class="recom" name="recom[<?php echo $i; ?>]" type="radio" value="1" />
				<?php endif; ?>
			</label>
		</td>
<?php endif; ?>
		<td><?php echo $v['Lesson']['id']; ?></td>
		<td><?php echo $lcategory[$v['Lesson']['category']]; ?></td>
		<td><?php if(!empty($v['LessonImage'][$i]['uploadpath'])): ?>
			<?php $path = str_replace('.jpg', '_th.jpg', $v['LessonImage'][$i]['uploadpath']); ?>
			<a href="<?php echo $html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>" target="_blank"><img src="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="" width="50" /></a>
			<?php endif; ?></td>
		<td><a href="<?php echo $html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>" target="_blank"><?php echo nl2br(h($v['Lesson']['title'])); ?></a></td>
		<td><?php
				$nums = count($v['LessonDate']);
				$numss = 1;
			?>
			<?php foreach($v['LessonDate'] as $key => $val): ?>
				<?php echo date('n/j', strtotime($val['date'])); ?><?php if($nums == $numss): ?><?php else: ?>,<?php endif; ?>
				<?php $numss++; ?>
			<?php endforeach; ?>
		
		</td>
		<td><label>
			<?php echo $form->hidden("Lesson.id", array('id' => false, 'class' => 'activeid', 'value' => $v['Lesson']['id'])); ?>
			<?php echo $form->select("Lesson.active", $active, $v['Lesson']['active'], array('empty' => null, 'class' => 'active', 'id' => false), true); ?>
					</label></td>
		<td style="white-space:nowrap;">
			<ul class="panel-edit">
				<li class="link-edit"><a href="<?php echo $html->url("/adminlessons/edit/{$v['Lesson']['id']}"); ?>">編集</a>
				<ul>
					<li><a href="<?php echo $html->url("/adminlessons/dateedit/{$v['Lesson']['id']}"); ?>">人数変更</a></li>
					<li class="duplicates"><a href="<?php echo $html->url("/adminlessons/duplicates/{$v['Lesson']['id']}"); ?>">コピー</a></li>
					<li class="deleteconfi"><a href="<?php echo $html->url("/adminlessons/delete/{$v['Lesson']['id']}"); ?>">削除</a></li>
				</ul>
				</li>
			</ul>
		</td>
	</tr>
<?php endforeach; ?>
</table>
<?php else: ?>
<p>該当のデータはありませんでした。</p>
<?php endif; ?>
			<p class="link-past"><a href="<?php echo $html->url("/adminlessons/"); ?>">開催一覧はこちら</a></p>
			</div>
			<p class="btn-back">
				<a href="<?php echo $html->url("/admins/"); ?>">戻る</a>
			</p>
	</div>