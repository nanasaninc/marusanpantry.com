
<script type="text/javascript">
	$(function(){
		$('.dateee .datepicker').change(function(){
			$(this).parent().next().next().children().val('8');
		});
	});
</script>
		<h1>パン・お菓子教室管理画面</h1>
		<div id="primary">
			<div class="wbox">
				<h2>基本情報登録</h2>
<?php echo $form->create(null, array('enctype'=>'multipart/form-data')); ?>
<?php echo $form->input('token', array('type' => 'hidden')); ?>
				<p>
					<strong>表示</strong>
					<?php echo $form->select("Lesson.active", $active, null, array('empty' => false), true).PHP_EOL; ?>
				</p>
				<div id="photoBox">
	<?php for($i = 0;$i < 2;$i++): ?>
	<div class="btn-upload">
		<?php $num = $i + 1; ?>
		<?php echo $form->hidden("LessonImage.{$i}.sort", array('value' => "{$num}")); ?>
		<p><?php echo $form->input("fileName{$i}", array('type' => 'file', 'label' => false,'class' => 'file')).PHP_EOL; ?></p></div>
		<?php endfor; ?>

				</div>
	<div id="contentBox">
		<dl>
			<dt>カテゴリ</dt><dd><?php echo $form->select("Lesson.category", $lcategory, null, array('empty' => '選択してください'), true).PHP_EOL; ?></dd>
			<dt>タイトル</dt><dd><?php echo $form->text('Lesson.title',array('title' => 'タイトルを入力してください。必須です。', 'size' => '40')).PHP_EOL; ?><?php echo $form->error('Lesson.title'); ?></dd>
			<dt>コメント</dt><dd><?php echo $form->textarea('Lesson.description',array('title' => 'コメントを入力してください。', 'cols' => '40','rows' => '5')).PHP_EOL; ?></dd>
			<dt>講師</dt>
			<dd><?php echo $form->text('Lesson.teacher',array('title' => '講師名を入力してください。', 'size' => '40')); ?></dd>
			<dt>開催時間</dt>
			<dd><?php echo $form->textarea('Lesson.morning',array('title' => '開催時間を入力してください。', 'cols' => '40','rows' => '5')); ?></dd>
			<dt>参加費</dt>
			<dd><?php echo $form->text('Lesson.price',array('title' => '参加費を入力してください。', 'size' => '20')); ?></dd>
			<dt>持参品</dt>
			<dd><?php echo $form->textarea('Lesson.jisan',array('title' => '持参品を入力してください。', 'cols' => '40','rows' => '5')); ?></dd>
			<dt>定員</dt>
			<dd><?php echo $form->text('Lesson.memberlimit', array('value' => '12','size' => '10')); ?>人</dd>
			<dt>内容</dt>
			<dd><?php echo $form->textarea('Lesson.bikou',array('title' => '内容を入力してください。', 'cols' => '40','rows' => '5')); ?></dd>
		</dl>
		</div>
	</div>
	<div class="wbox">
		<h2>開催日程登録</h2>
		<dl>
			<dt>掲載終了日</dt>
			<dd><?php echo $form->text('Lesson.limitday',array('size' => '20', 'readonly' => true, 'class' => 'datepicker')); ?></dd>
		</dl>
		<table border="0">
			<tr>
				<th>開催日</th>
				<th>開催時間</th>
				<th>WEB受付数</th>
			</tr>
	<?php for($i = 0;$i < 8; $i++): ?>
			<tr>
				<td class="dateee">
					<?php echo $form->text("LessonDate.{$i}.date", array('readonly' => true, 'class' => 'datepicker')); ?>
					<?php echo $form->error('LessonDate.{$i}.date'); ?>
				</td>
				<td><?php echo $form->text("LessonDate.{$i}.datetxt"); ?></td>
				<td><?php echo $form->text("LessonDate.{$i}.limit"); ?>人</td>
			</tr>
	<?php endfor; ?>
		</table>
		</div>
			<p class="btn-back">
				<input onclick="history.back();" name="" type="button" value="戻る" />
			</p>
			<p class="btn">
				<?php echo $form->submit('登録する', array('name' => 'data[Lesson][mode]', 'div' => false)), PHP_EOL; ?>
			</p>
<?php echo $form->end(); ?>
</div>