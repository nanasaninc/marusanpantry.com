		<p class="topicPath"><a href="<?php echo $html->url('/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><a href="./">レシピ</a><span>&gt;</span>教室名</p>
		<?php echo $this->Session->flash(); ?>
		<h1><img src="<?php echo $html->url('/', true); ?>img/recipe/h1.png" alt="レシピ" width="89" height="23" /></h1>
		<div id="primary" class="bread">
			<div id="searchBox">
			<h2><img src="<?php echo $html->url('/', true); ?>img/recipe/h2-search.png" alt="レシピ検索" width="135" height="74" /></h2>
					<p>
						<label>
							<input type="text" name="textfield" id="textfield" class="text" />
						</label>
						</p>
					<p><img src="<?php echo $html->url('/', true); ?>img/recipe/btn-search.png" alt="検索する" width="122" height="27" /></p>
			</div>
<?php echo $this->element('frontcategoryBox'); ?>


<?php
	$bool = $paginator->numbers();
?>

<div class="box" id="newBox">
	<h2><img src="<?php echo $html->url('/', true); ?>img/recipe/h2-listBox.png" alt="レシピ一覧" width="680" height="72" /></h2>
<?php if(!empty($data)): ?>
<?php if($bool != false): ?>
	<div class="pagination">
		<ul>
			<li class="prev"><?php echo $paginator->prev('前へ').PHP_EOL; ?></li>
			<?php echo $paginator->numbers(array('tag' => 'li', 'separator' => false)).PHP_EOL; ?>
			<li class="next"><?php echo $paginator->next('次へ').PHP_EOL; ?></li>
		</ul>
	</div>
<?php endif; ?>
<?php endif; ?>

<?php if(!empty($data)): ?>
<?php $i=0; ?>
<?php foreach($data as $k => $v): ?>
	<div class="box">
		<div class="innerBox clearfix">
			<p class="f_right">
			<a href="<?php echo $html->url("./recipe/{$v['Recipe']['id']}", true); ?>"><?php if(!empty($v['RecipeImage'][$i]['uploadpath'])): ?></a>
			<?php $path = str_replace('.jpg', '_th.jpg', $v['RecipeImage'][$i]['uploadpath']); ?>
			<img src="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="" width="110" />
			<?php endif; ?></p>
			<h3 class="new"><a href="<?php echo $html->url("./recipe/{$v['Recipe']['id']}", true); ?>"><?php echo nl2br(h($v['Recipe']['title'])); ?></a></h3>
			<p><?php echo nl2br(h($v['Recipe']['description'])); ?></p>
			<p class="btn-detail"><a href="<?php echo $html->url("./recipe/{$v['Recipe']['id']}", true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/btn-detail.png" alt="<?php echo nl2br(h($v['Recipe']['title'])); ?>" width="106" height="25" /></a></p>
		</div>
	</div>
<?php endforeach; ?>
<?php else: ?>
	<p>該当のデータが見つかりませんでした。</p>
<?php endif; ?>

<?php if(!empty($data)): ?>
<?php if($bool != false): ?>
	<div class="pagination">
		<ul>
			<li class="prev"><?php echo $paginator->prev('前へ').PHP_EOL; ?></li>
			<?php echo $paginator->numbers(array('tag' => 'li', 'separator' => false)).PHP_EOL; ?>
			<li class="next"><?php echo $paginator->next('次へ').PHP_EOL; ?></li>
		</ul>
	</div>
<?php endif; ?>
<?php endif; ?>
</div>
		</div>
		