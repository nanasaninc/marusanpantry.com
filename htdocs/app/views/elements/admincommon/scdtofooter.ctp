<?php if($bodyId == 'recipe'): ?>
<?php echo $this->element('admincommon/scd-recipe'); ?>
<?php elseif($bodyId == 'lesson'): ?>
<?php echo $this->element('admincommon/scd-lesson'); ?>
<?php elseif($bodyId == 'pantry'): ?>
<?php echo $this->element('admincommon/scd-pantry'); ?>
<?php elseif($bodyId == 'recommend'): ?>
<?php echo $this->element('admincommon/scd-recommend'); ?>
<?php else: ?>
<?php echo $this->element('admincommon/scd-home'); ?>
<?php endif; ?>

	<div id="footer"><img src="<?php echo $html->url('/', true); ?>img/admin/copy.png" alt="Copyright © matsuyama marusan pantry. All Rights Reserved." width="386" height="12" /></div>
</div>
</body>
</html>