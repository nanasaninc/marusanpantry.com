		<div id="secondary">
			<p class="btn">
				<a href="<?php echo $html->url("/adminrecipes/add/"); ?>">レシピ新規登録</a>
			</p>
			<div class="wbox">
				<h2>レシピカテゴリ</h2>
				<ul>
					<li><a href="<?php echo $html->url("/adminrecipes/index/10000"); ?>">パン(<?php echo $cat['breadnum']; ?>)</a>
						<ul>
<?php foreach($lists['breadlist'] as $k => $v): ?>
							<li><a href="<?php echo $html->url("/adminrecipes/index/{$v['id']}/"); ?>"><?php echo $v['name']; ?>(<?php echo $v['count']; ?>)</a></li>
<?php endforeach; ?>
						</ul>
					</li>
					<li><a href="<?php echo $html->url("/adminrecipes/index/20000"); ?>">お菓子(<?php echo $cat['sweetnum']; ?>)</a>
						<ul>
<?php foreach($lists['sweetlist'] as $k => $v): ?>
							<li><a href="<?php echo $html->url("/adminrecipes/index/{$v['id']}/"); ?>"><?php echo $v['name']; ?>(<?php echo $v['count']; ?>)</a></li>
<?php endforeach; ?>
						</ul>
					</li>
				</ul>
				<div class="searchBox">
					<?php echo $form->create(null, array('action' => 'search')), PHP_EOL; ?>
						<?php echo $form->text(null, array('name' => 'keyword', 'label' => false, 'class' => 'searchTxt')), PHP_EOL; ?>
						<?php echo $form->submit('検索', array('div' => false, 'name' => 'data[Recipe][mode]')), PHP_EOL; ?>
					<?php echo $form->end(), PHP_EOL; ?>
				</div>
			</div>
		</div>
	</div>
