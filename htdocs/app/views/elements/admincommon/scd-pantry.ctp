		<div id="secondary">
			<p class="btn">
				<a href="<?php echo $html->url("/adminpantries/add/"); ?>">パントリーイチオシ新規登録</a>
			</p>
			<div class="wbox">
				<h2>パントリー検索</h2>

				<div class="searchBox">
					<?php echo $form->create(null, array('action' => 'search')), PHP_EOL; ?>
						<?php echo $form->text(null, array('name' => 'keyword', 'label' => false, 'class' => 'searchTxt')), PHP_EOL; ?>
						<?php echo $form->submit('検索', array('div' => false, 'name' => 'data[Pantry][mode]')), PHP_EOL; ?>
					<?php echo $form->end(), PHP_EOL; ?>
				</div>
			</div>
		</div>
	</div>
