<?php if(empty($bodyId)): ?>
<?php $bodyId = 'sitemap'; ?>
<?php endif; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Script-Type" content="javascript" />
<title><?php echo $meta['title'];?></title>
<meta name="description" content="<?php echo $meta['description'];?>" />
<meta name="keywords" content="<?php echo $meta['keyword'];?>" />
<link href="<?php echo $html->url('/', true); ?>css/common.css" rel="stylesheet" type="text/css" media="all" />

<?php if($bodyId == 'lesson' || $bodyId == 'home' ): ?>
<link href="<?php echo $html->url('/', true); ?>css/slide.css" rel="stylesheet" type="text/css" media="all" />
<?php endif; ?>

<?php if($bodyId == 'recipe'): ?>
<link href="<?php echo $html->url('/', true); ?>css/print.css" rel="stylesheet" type="text/css" media="print" />
<?php endif; ?>

<script type="text/javascript" src="<?php echo $html->url('/', true); ?>js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="<?php echo $html->url('/', true); ?>js/common.js"></script>
<script type="text/javascript" src="<?php echo $html->url('/', true); ?>js/meca.js"></script>
<script type="text/javascript" src="<?php echo $html->url('/', true); ?>js/scrolltopcontrol.js"></script>
<link href="<?php echo $html->url('/', true); ?>css/jquery.fancybox-1.3.1.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="<?php echo $html->url('/', true); ?>js/jquery.fancybox-1.3.1.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$(".fancymap").fancybox();
})	
</script>
<?php if($bodyId == 'lesson' || $bodyId == 'home' ): ?>
<script type="text/javascript" src="<?php echo $html->url('/', true); ?>js/easing.js"></script>
<script type="text/javascript" src="<?php echo $html->url('/', true); ?>js/slide.js"></script>
<?php endif; ?>

<?php if($bodyId == 'home' ): ?>
<script type="text/javascript" src="<?php echo $html->url('/', true); ?>js/homeSlide.js"></script>
<?php endif; ?>

<?php if($bodyId == 'reserve' || $bodyId == 'recipe'): ?>
<script type="text/javascript">
$(document).ready(function() {
	$(".fancybox").fancybox({
		'width'				: 800,
		'height'			: 550,
		'autoScale'			: false,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none'
<?php if(!empty($fancy)): ?>
		,'type'				: 'iframe'
<?php endif; ?>
	});
	$(".fancy").fancybox();
});
</script>
<?php endif; ?>
</head>
<body id="<?php echo $bodyId; ?>">
<?php if($bodyId == 'reserve'): ?>
<div id="header">
	<div class="innerBox">
		<p id="logo"><img src="<?php echo $html->url('/', true); ?>img/common/logo.png" width="328" height="67" alt="お菓子で笑顔を作る　マルサンパントリー" /></p>
	</div>
</div>
<?php else: ?>
<div id="header">
	<div class="innerBox">
		<p id="logo"><a href="<?php echo $html->url('/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/logo.png" width="328" height="67" alt="お菓子で笑顔を作る　マルサンパントリー" /></a></p>
		<div class="box">
			<ul id="headerNav">
				<li><a href="<?php echo $html->url('/img/common/map.gif', true); ?>" class="fancymap"><img src="<?php echo $html->url('/', true); ?>img/common/hNav-map.png" alt="" width="101" height="13" alt="リンク：マップ・駐車場" /></a></li>
				<li><a href="<?php echo $html->url('/privacy/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/hNav-privacy.png" width="137" height="13" alt="リンク：プライバシーポリシー" /></a></li>
				<li><a href="<?php echo $html->url('/sitemap/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/hNav-sitemap.png" alt="" width="92" height="13" alt="リンク：サイトマップ" /></a></li>
			</ul>
			<p class="bnr-marusan"><a href="http://www.m-marusan.co.jp/" target="_blank"><img src="<?php echo $html->url('/', true); ?>img/common/bnr-marusan.png" width="198" height="65" alt="リンク：松山丸三コーポレートサイト" /></a></p>
		</div>
		<ul id="globalNav">
			<li><a href="<?php echo $html->url('/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/gNav-home.png" alt="リンク：マルサンパントリートップ" width="60" height="44" /></a></li>
			<li><a href="<?php echo $html->url('/welcome/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/gNav-welcome.png" alt="リンク：ようこそ" width="150" height="44" /></a></li>
			<li><a href="<?php echo $html->url('/lessons/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/gNav-lesson.png" alt="リンク：パン・お菓子教室" width="150" height="44" /></a></li>
			<li><a href="<?php echo $html->url('/pantry/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/gNav-pantry.png" alt="リンク：マルサンパントリー" width="150" height="44" /></a></li>
			<li><a href="<?php echo $html->url('/kitchenshop/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/gNav-kitchen.png" alt="リンク：キッチンショップ" width="150" height="44" /></a></li>
			<li><a href="<?php echo $html->url('/recipes/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/gNav-recipe.png" alt="リンク：レシピ" width="150" height="44" /></a></li>
			<li><a href="http://shop.plaza.rakuten.co.jp/marusanpantry" target="_blank"><img src="<?php echo $html->url('/', true); ?>img/common/gNav-blog.png" width="150" height="44" alt="リンク：スタッフグログ" /></a></li>
		</ul>
	</div>
</div>
<?php endif; ?>
<div id="wrapper">
	<div id="container">