		<p class="topicPath"><a href="<?php echo $html->url('/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><span>&gt;</span>ページが見つかりません</p>
		<h1><img src="<?php echo $html->url('/', true); ?>img/notfound/h1.png" alt="ページが見つかりません" width="227" height="20" /></h1>
		<div id="primary">
			<div class="box">
				<p><img src="<?php echo $html->url('/', true); ?>img/notfound/mainImg.png" alt="お探しのページは見つかりません" width="640" height="255" /></p>
				<p>申し訳ございません。お探しのページは削除されたかURLが変更になった可能性があります。<br />
				大変申し訳ありませんが、<a href="<?php echo $html->url('/', true); ?>">トップページ</a>にお戻りいただくか、<a href="<?php echo $html->url('/', true); ?>sitemap/">サイトマップ</a>からページをお探し下さい。</p>
			</div>
		</div>