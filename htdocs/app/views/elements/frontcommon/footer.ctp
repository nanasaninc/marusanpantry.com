<?php if(empty($bodyId)): ?>
<?php $bodyId = 'sitemap'; ?>
<?php endif; ?>

<?php if($bodyId == 'recipe'): ?>
<?php echo $this->element('frontcommon/scd-recipe'); ?>
<?php elseif($bodyId == 'lesson'): ?>
<?php echo $this->element('frontcommon/scd-lesson'); ?>
<?php elseif($bodyId == 'privacy'): ?>
<?php echo $this->element('frontcommon/scd-privacy'); ?>
<?php elseif($bodyId == 'sitemap'): ?>
<?php echo $this->element('frontcommon/scd-sitemap'); ?>
<?php elseif($bodyId == 'welcome'): ?>
<?php echo $this->element('frontcommon/scd-welcome'); ?>
<?php elseif($bodyId == 'staff'): ?>
<?php echo $this->element('frontcommon/scd-staff'); ?>
<?php else: ?>
<?php echo $this->element('frontcommon/scd-home'); ?>
<?php endif; ?>

<?php if($bodyId == 'reserve'): ?>
<div id="footer">
	<div class="innerBox">
		<div class="innerBox">
			<p><img src="<?php echo $html->url('/', true); ?>img/common/footer-logo.png" width="244" height="58" alt="株式会社松山丸三" /></p>
			<dl>
				<dt>マルサンパントリー/パン・お菓子教室</dt>
				<dd>愛媛県松山市花園町6-1<br />
					TEL:089-931-1147<br />
					営業時間：9:00〜18:00　定休日：日・祝日</dd>
			</dl>
			<dl>
				<dt>キッチンショップ</dt>
				<dd>愛媛県松山市三番町5-8-13<br />
					TEL:089-931-4161<br />
					営業時間：9:00〜18:00　定休日：土・日・祝日</dd>
			</dl>
		</div>
	</div>
</div>
</body>
</html>
<?php else: ?>
<div id="footer">
	<div class="innerBox">
		<p id="footerNav"><a href="<?php echo $html->url('/', true); ?>">ホーム</a>｜<a href="<?php echo $html->url('/welcome/', true); ?>">ようこそ</a>｜<a href="<?php echo $html->url('/lessons/', true); ?>">パン・お菓子教室</a>｜<a href="<?php echo $html->url('/pantry/', true); ?>">マルサンパントリー</a>｜<a href="<?php echo $html->url('/kitchenshop/', true); ?>">キッチンショップ</a>｜<a href="<?php echo $html->url('/recipes/', true); ?>">レシピ</a>｜<a href="http://shop.plaza.rakuten.co.jp/marusanpantry" target="_blank">スタッフブログ</a>｜<a href="<?php echo $html->url('/privacy/', true); ?>">プライバシーポリシー</a>｜<a href="<?php echo $html->url('/sitemap/', true); ?>">サイトマップ</a></p>
		<p><img src="<?php echo $html->url('/', true); ?>img/common/footer-logo.png" width="244" height="58" alt="株式会社松山丸三" /></p>
		<dl>
			<dt>マルサンパントリー/パン・お菓子教室</dt>
			<dd>愛媛県松山市花園町6-1<br />
				TEL:089-931-1147<br />
				営業時間：8:45〜18:00　定休日：日・祝日</dd>
		</dl>
		<dl>
			<dt>キッチンショップ</dt>
			<dd>愛媛県松山市三番町5-8-13<br />
				TEL:089-931-4161<br />
				営業時間：8:45〜18:00　定休日：土・日・祝日</dd>
		</dl>
	</div>
</div>
</body>
</html>
<?php endif; ?>