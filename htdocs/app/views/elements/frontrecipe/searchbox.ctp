			<div id="searchBox">
			<?php echo $form->create(null, array('action' => 'search')); ?>
			<h2><img src="<?php echo $html->url('/', true); ?>img/recipe/h2-search.png" alt="レシピ検索" width="135" height="74" /></h2>
					<p>
						<label>
						<?php if(!empty($searchword)): ?>
							<?php echo $form->text(null, array('id' => 'textfield', 'class' => 'text', 'name' => 'keyword', 'label' => false, 'div' => false)); ?>
						<?php else: ?>
							<?php echo $form->text(null, array('id' => 'textfield', 'class' => 'text', 'name' => 'keyword', 'label' => false, 'div' => false)); ?>
						<?php endif; ?>
						</label>
						</p>
					<p class="btn-search"><?php echo $form->submit('検索', array('name' => 'data[Recipe][mode]', 'div' => false)), PHP_EOL; ?></p>
					<?php echo $form->end(); ?>
			</div>
			