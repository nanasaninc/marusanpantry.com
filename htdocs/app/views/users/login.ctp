<h1>マルサンパントリーウェブサイト管理</h1>
		<div id="primary">
			<div class="wbox">
				<h2>ログイン</h2>
<?php
	echo $form->create('User', array('action' => 'login'));
?>
	<div id="loginBox">
		<dl>
			<dt class="id">ログインID</dt>
			<dd><?php echo $form->input('username', array('label'=>false,'class'=>'text')); ?></dd>
			<dt class="pw">パスワード</dt>
			<dd><?php echo $form->input('password', array('label'=>false,'class'=>'text')); ?></dd>
		</dl>
<?php
	if($session->check('Message.auth')){
		echo $session->flash('auth');
	}
	echo $session->flash();
?>
		<div class="btn-submit">
			<div class="submit"><p class="btn"><?php echo $form->submit('ログイン', array('div' => false)); ?></p></div>
		</div>
	</div>
<?php echo $form->end(); ?>
