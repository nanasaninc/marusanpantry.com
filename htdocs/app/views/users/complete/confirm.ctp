	<h1>確認画面</h1>
	
<?php echo $form->create(null, array('type'=>'post','action'=>'regist')); ?>
<table class="stripe">
	<tr>
		<th>ログインID</th>
		<td><?php echo $form->value("User.username"); ?></td>
	</tr>
	<tr>
		<th>メールアドレス</th>
		<td><?php echo $form->value("User.email"); ?></td>
	</tr>
	<tr>
		<th>ご希望のログインパスワード</th>
		<td><?php echo $form->value("User.raw_password"); ?></td>
	</tr>
</table>

<?php echo $form->hidden("User.name"); ?>
<?php echo $form->hidden("User.email"); ?>
<?php echo $form->hidden("User.raw_password"); ?>

	<p class="center"><?php echo $form->submit('戻る', array('name' => 'data[User][mode]','class' => 'btn-back','div' => false)); ?><?php echo $form->submit('登録する', array('name' => 'data[User][mode]','class' => 'btn-submit','div' => false)); ?></p>
<?php echo $form->end(); ?>