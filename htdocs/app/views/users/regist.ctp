
	<h1>管理者登録</h1>

<?php echo $form->create(null, array('type'=>'post','action'=>'')); ?>
<table class="stripe">
	<tr>
		<th>ご希望のログインID</th>
		<td><?php echo $form->text("User.username", array("size" => 15)); ?>
			<span>半角英数字　15文字以内</span>
			<?php echo $form->error("User.username"); ?>
		</td>
	</tr>
	<tr>
		<th>ご希望のログインパスワード</th>
		<td><?php echo $form->password("User.raw_password", array("size" => 15)); ?>
			<span>半角英数字　15文字以内　</span>
			<?php echo $form->error("User.raw_password"); ?>
		</td>
	</tr>
	<tr>
		<th>メールアドレス</th>
		<td><?php echo $form->text("User.email", array("size" => 30)); ?>
			
			<?php echo $form->error("User.email"); ?>
		</td>
	</tr>
	<tr>
		<th>確認用メールアドレス</th>
		<td><?php echo $form->text("User.mailaddress_confirm", array("size" => 30)); ?>
			<span>確認のためもう一度入力してください</span>
			<?php echo $form->error("User.mailaddress_confirm"); ?>
		</td>
	</tr>
</table>

<?php  echo $form->hidden('mode',array('value' => 'confirm')); ?>
	
<p class="center"><?php echo $form->submit("確認画面へ", array('class' => 'btn-confirm', 'div' => false)); ?></p>
<?php echo $form->end(),PHP_EOL; ?>
