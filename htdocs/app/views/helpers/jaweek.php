<?php
class JaweekHelper extends AppHelper {
	public $ja = array(
		'0' => '日',
		'1' => '月',
		'2' => '火',
		'3' => '水',
		'4' => '木',
		'5' => '金',
		'6' => '土',
	);
	
	public function weekname($date){
		$week = date('w', strtotime($date));
		$jaweek = $this->ja[$week];
		return $jaweek;
	}
}