		<p class="topicPath"><a href="<?php echo $html->url('/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><span>&gt;</span>サイトマップ</p>
		<h1><img src="<?php echo $html->url('/', true); ?>img/sitemap/h1.png" alt="サイトマップ" width="121" height="20" /></h1>
		<div id="primary">
			<div class="box">
				<ul>
					<li><a href="<?php echo $html->url('/', true); ?>">マルサンパントリーホーム</a></li>
					<li><a href="<?php echo $html->url('/', true); ?>welcome/">ようこそ</a></li>
					<li><a href="<?php echo $html->url('/', true); ?>lessons/">パン・お菓子教室</a></li>
					<ul>
						<li><a href="<?php echo $html->url('/', true); ?>lessons/flow/">ご参加の流れ</a></li>
					</ul>
					<li><a href="<?php echo $html->url('/', true); ?>pantry/">マルサンパントリー</a></li>
					<li><a href="<?php echo $html->url('/', true); ?>kitchen/">キッチンショップ</a></li>
					<li><a href="<?php echo $html->url('/', true); ?>reciipes/">レシピ</a></li>
					<li><a href="<?php echo $html->url('/', true); ?>staff/">スタッフ紹介</a></li>
					<li><a href="#">スタッフブログ</a></li>
					<li><a href="<?php echo $html->url('/', true); ?>privacy/">プライバシーポリシー</a></li>
					<li><a href="<?php echo $html->url('/', true); ?>sitemap/">サイトマップ </a></li>
				</ul>
			</div>
		</div>