<script type="text/javascript">
$(function(){
	/*
$('input#policycheck').click(function(){
		var checks = $(this).attr('checked');
		if(checks == 'checked'){
			$('input.btn-confirm').removeAttr("disabled");
			console.log(checks);
		}else{
			$('input.btn-confirm').attr("disabled", "disabled");
		}
	});
*/
	
	$('input.btn-confirm').click(function(){
		var checkss = $('input#policycheck').attr('checked');
		if(checkss != 'checked'){
			alert("キャンセルポリシーをお読みになり\n同意するにチェックしてください。");
			return false;
		}
		var syokaichecks = $('input#ReserveSyokai').attr('checked');
		var membernum = $('input#ReserveMembernum').val();
		
		if(syokaichecks != 'checked' && membernum == ''){
			alert("初回申し込みにチェック\nもしくは会員No.のご入力をしてください。");
			return false;
		}
	});
});
</script>

<p class="topicPath"><a href="<?php echo $html->url('/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><a href="./">パン・お菓子教室</a><span>&gt;</span>予約申込み</p>
		<h1><img src="<?php echo $html->url('/', true); ?>img/lesson/h1.png" alt="パン・お菓子教室" width="162" height="23" /></h1>
		<div id="primary">
			<div id="reserveBox" class="box">
				<h2><img src="<?php echo $html->url('/', true); ?>img/lesson/reserve/h2-reserve.png" width="960" height="60" /></h2>
				<div id="photoBox">
					<?php if(!empty($dataphoto['LessonImage'][0]['uploadpath'])): ?>
					<?php $path = $dataphoto['LessonImage'][0]['uploadpath']; ?>
					<p><img src="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="" width="260" /></p>
					<?php endif; ?>
					<?php if(!empty($dataphoto['LessonImage'][1]['uploadpath'])): ?>
					<?php $path = $dataphoto['LessonImage'][1]['uploadpath']; ?>
					<p><img src="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="" width="260" /></p>
					<?php endif; ?>
				</div>
				<div id="contentBox">
					<?php echo $form->create('', array('action' => 'reserve')), PHP_EOL; ?>
					<?php echo $form->hidden("Reserve.id", array('value' => nl2br(h($data['Reserve']['id'])))), PHP_EOL; ?>
					<?php echo $form->hidden("Reserve.title", array('value' => nl2br(h($data['Reserve']['title'])))), PHP_EOL; ?>
					<?php echo $form->hidden("Reserve.date", array('value' => nl2br(h($data['Reserve']['date'])))), PHP_EOL; ?>
					<?php echo $form->hidden("Reserve.datetxt", array('value' => nl2br(h($data['Reserve']['datetxt'])))), PHP_EOL; ?>
					<?php echo $form->hidden("Reserve.date_id", array('value' => nl2br(h($data['Reserve']['date_id'])))), PHP_EOL; ?>
					<dl>
						<dt>参加コース名</dt>
						<dd><?php echo nl2br(h($data['Reserve']['title'])); ?></dd>
						<dt>日付</dt>
						<dd><?php echo nl2br(h($data['Reserve']['date'])); ?></dd>
						<dt>時間</dt>
						<dd><?php echo nl2br(h($data['Reserve']['datetxt'])); ?></dd>
						<dt class="required">氏名</dt>
						<dd><?php echo $form->text('Reserve.name', array('size' => '40')); ?><?php echo $form->error('Reserve.name'); ?></dd>
					<dt>電話番号</dt>
					<dd>
						<?php echo $form->text('Reserve.phone', array('size' => '40')); ?><?php echo $form->error('Reserve.phone'), PHP_EOL; ?>
					</dd>
					<?php if(!empty($capanum)): ?>
					<dt class="required">参加人数</dt>
					<dd>
						<?php echo $form->select("Reserve.active", $capanum, null, array('empty' => false), true).PHP_EOL; ?> 人
					</dd>
					<?php endif; ?>
					<dt class="required">メールアドレス</dt>
					<dd>
						<?php echo $form->text('Reserve.email', array('size' => '40')); ?><?php echo $form->error('Reserve.email'), PHP_EOL; ?>
					</dd>
					<dt class="required">メールアドレス確認</dt>
					<dd>
						<?php echo $form->text('Reserve.mailconfirm', array('size' => '40')); ?><?php echo $form->error('Reserve.mailconfirm'), PHP_EOL; ?>
					</dd>
					<dt>初回申込み</dt>
					<dd>
						<?php echo $form->checkbox('Reserve.syokai', array()), PHP_EOL; ?>
					</dd>
					<dt class="required">会員No</dt>
					<dd>
						<?php echo $form->text('Reserve.membernum', array('size' => '20')), PHP_EOL; ?>
					</dd>
					<dt>備考</dt>
					<dd>
							<?php echo $form->textarea('Reserve.comment', array('cols' => '45', 'rows' => '10')), PHP_EOL; ?>
					</dd>
					</dl>
					<p><a href="<?php echo $html->url('/', true); ?>lesson/policy.html" class="fancybox">お申込の際にはキャンセルポリシーへの同意が必要となります。</a></p>
					<p><label for="policycheck"><?php echo $form->checkbox('Reserve.policy', array('id' => 'policycheck')); ?> キャンセルポリシーに同意する</label></p>
					<?php echo $form->submit('申込み内容確認', array('name' => 'data[Reserve][mode]', 'div' => false, 'class' => 'btn-confirm')), PHP_EOL; ?>
					<?php echo $form->end(), PHP_EOL; ?>
				</div>
			</div>
		</div>
		<div id="secondary"></div>
	</div>
</div>
