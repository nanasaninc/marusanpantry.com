<p class="topicPath"><a href="<?php echo $html->url('/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span>パン・お菓子教室</p>
<?php echo $this->Session->flash(); ?>
<h1><img src="<?php echo $html->url('/', true); ?>img/lesson/h1.png" alt="パン・お菓子教室" width="162" height="23" /></h1>
		<div id="primary">
			<div class="mainImg"><?php
	$baseurl = $html->url('/', true);
	$url = 'http://www.marusanpantry.com/inc/lesson.html';
	$bnr = file_get_contents($url);
	echo $bnr;
?></div>
			
<?php echo $this->element('frontlesson/scheduleBox'); ?>
			
			<div class="box" id="calendarBox">
				<h2><img src="<?php echo $html->url('/', true); ?>img/lesson/h2-calendar.png" width="680" height="60" alt="パン・お菓子教室カレンダー" /></h2>
				<div id="caleNav" class="localNav clearfix">
					<p><?php echo date('Y年n月'); ?></p>
					<p><?php echo date('Y年n月', strtotime(date('Y-m-1').' +1 month')); ?></p>
					<p><?php echo date('Y年n月', strtotime(date('Y-m-1').' +2 month')); ?></p>
				</div>
				<div id="calendarslideBox">
				<div id="calendarslideBody">
<?php
	$thcalendar->prevMonth = 0;
	$thcalendar->nextMonth = 2;
	$thcalendar->holidayCheck = false;
	$thcalendar->dayText = $thismt;
?>

<?php echo $thcalendar->calender(); ?>
				</div>
				</div>

				<!-- nav -->
			</div>
		</div>
		