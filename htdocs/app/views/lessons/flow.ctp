<p class="topicPath"><a href="<?php echo $html->url('/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><a href="./">パン・お菓子教室</a><span>&gt;</span>ご参加の流れ</p>
		<h1><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/h1.png" alt="パン・お菓子教室" width="120" height="20" /></h1>
		<div id="primary" class="flow">
			<div class="box" id="reserveBox">
				<h2><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/h2-reserveBox.png" alt="ご予約の流れ" width="680" height="62" /></h2>
				<div class="beforeBox">
					<h3>教室にご参加頂く前に</h3>
					<p>教室にご参加いただく全てのお客様に「マルサンパントリーポイントカード」へのご入会をお願いいたします。<br />
					※教室の入会金・年会費などは一切必要ありません。</p>
					<p>ポイントカード入会金：100円<br />
					<span>☆ご入会と同時に、当日使える100円割引券を発行いたします。</span></p>
				</div>
				<p class="f_right"><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/p01-1.jpg" alt="写真：教室選び" width="194" height="214" /></p>
				<h3 class="nb"><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/title01-1.png" alt="1.参加したい教室をお選びください。" width="393" height="28" /></h3>
				<p>参加募集中の教室より<br />
				お好きなコース・日時をお選びください。</p>
				<h3><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/title01-2.png" alt="2.当サイト（インターネット） 店頭・FAXにてお申し込みください。" width="415" height="62" /></h3>
				<p>店頭の場合、その場でご予約が確定します。インターネット・FAXの場合、当店から参加受付メールが届いた時点でご予約が確定します</p>
				<p class="caution">インターネット・FAXの場合、当店から参加受付メールが届いた時点でご予約が確定します。</p>
			</div>
			<div class="box" id="todayBox">
				<h2><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/h2-todayBox.png" alt="当日の流れ" width="680" height="62" /></h2>
				<h3 class="nb"><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/title02-1.png" alt="1.教室開始15分前までにご来店ください。 " width="439" height="37" /></h3>
				<p>教室参加当日は、エプロン、筆記用具、お持ち帰り用の容器、ハンドタオルをご持参のうえご来店ください。</p>
				<p class="f_right"><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/p02-1.jpg" alt="写真：受付の様子" width="194" height="212" /></p>
				<h3><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/title02-2.png" alt="2.1Fレジにて受付をお済ませください。" width="416" height="34" /></h3>
				<p>教室参加費をお支払いただきロッカーキーをお受け取りのうえ、2Fキッチンへご入室ください。</p>
				<h3><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/title02-3.png" alt="3.ロッカーに貴重品を入れ、 身だしなみを整えてください。 " width="344" height="63" /></h3>
				<p class="f_left"><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/p02-2.jpg" alt="写真：貴重品はロッカーへ" width="194" height="211" /></p>
				<dl class="cautionBox clearfix">
					<dt>★ロングヘアーの方は、髪を束ねてください。</dt>
					<dd><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/p02-3.jpg" alt="写真：髪を束ねる様子" width="166" height="124" /></dd>
					<dt>★ネイルアート(マニキュア含む)をされている方、爪が長い方はゴム手袋（１枚10円）をご着用ください。</dt>
					<dd><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/p02-4.jpg" alt="写真：ゴム手袋を着用" width="166" height="124" /></dd>
				</dl>
				<hr class="clear" />
				<p class="f_right" style="clear:right;"><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/p02-5.jpg" alt="写真：いよいよスタート！" width="194" height="211" /></p>
				<h3><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/title02-4.png" alt="4.いよいよパン＆お菓子教室 スタートです！" width="311" height="62" /></h3>
				<p>講師の実演を見ながら作業します。<br />
					分からないところや難しい工程も優しく<br />
				アドバイスしてくれるので安心です。</p>
				<hr class="clear" />
				<p class="f_right" style="clear:right;"><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/p02-6.jpg" alt="写真：完成です" width="194" height="211" /></p>
				<h3><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/title02-5.png" alt="5.完成です！みんなで 出来上がりを試食しましょう。 " width="344" height="62" /></h3>
				<p><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/p02-7.jpg" alt="イメージ：完成したお菓子やパン" width="344" height="132" /></p>
				<p class="f_right" style="clear:right;"><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/p02-8.jpg" alt="写真：お疲れ様でした" width="290" height="201" /></p>
				<h3><img src="<?php echo $html->url('/', true); ?>img/lesson/flow/title02-6.png" alt="6.お疲れ様でした。お忘れ物の ないようお帰りください。 " width="333" height="65" /></h3>
				<p>ロッカーキーはロッカーに<br />
				挿したままご退室ください</p>
			</div>
		</div>