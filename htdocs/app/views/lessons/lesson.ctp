	<p class="topicPath"><a href="<?php echo $html->url('/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><a href="../">パン・お菓子教室</a><span>&gt;</span><?php echo nl2br(h($data['Lesson']['title'])); ?></p>
	
	<h1><img src="<?php echo $html->url('/', true); ?>img/lesson/h1.png" alt="パン・お菓子教室" width="162" height="23" /></h1>
		<div id="primary" class="<?php echo $icons[$data['Lesson']['category']]; ?>">
			<div class="box">
				<div id="photoBox">
					<?php if(!empty($data['LessonImage'][0]['uploadpath'])): ?>
					<?php $path = str_replace('.jpg', '_th.jpg', $data['LessonImage'][0]['uploadpath']); ?>
					<p><img src="<?php echo $html->url('/', true).$path; ?>?<?php echo mt_rand(); ?>" alt="" width="200" /></p>
					<?php else: ?>
					<p><img src="<?php echo $html->url('/', true); ?>img/common/nowprinting.jpg" alt="" width="200" /></p>
					<?php endif; ?>
					
					<?php if(!empty($data['LessonImage'][1]['uploadpath'])): ?>
					<?php $path = str_replace('.jpg', '_th.jpg', $data['LessonImage'][1]['uploadpath']); ?>
					<p><img src="<?php echo $html->url('/', true).$path; ?>?<?php echo mt_rand(); ?>" alt="" width="200" /></p>
					<?php endif; ?>
				</div>
				<div id="contentBox">
					<h2><?php echo nl2br(h($data['Lesson']['title'])); ?></h2>
					<p class="comment"><?php echo nl2br(h($data['Lesson']['description'])); ?></p>
					<dl>
						<dt>講師</dt>
						<dd><?php echo nl2br(h($data['Lesson']['teacher'])); ?>　</dd>
						<dt>開催時間</dt>
						<dd><?php echo nl2br(h($data['Lesson']['morning'])); ?>　</dd>
						<dt>参加費</dt>
						<dd><?php echo str_replace("\\", '&yen;', nl2br(h($data['Lesson']['price']))); ?>　</dd>
						<dt>持参品</dt>
						<dd><?php echo nl2br(h($data['Lesson']['jisan'])); ?>　</dd>
						<dt>定員</dt>
						<dd><?php echo nl2br(h($data['Lesson']['memberlimit'])); ?>　</dd>
						<dt>備考</dt>
						<dd><?php echo nl2br(h($data['Lesson']['bikou'])); ?>　</dd>
					</dl>
					<h3><img src="<?php echo $html->url('/', true); ?>img/lesson/h2-list.png" alt="開催日程・お申込" width="350" height="48" /></h3>
					<table>
						<tr>
							<th>開催日</th>
							<th>開催時間</th>
							<th>予約</th>
							<th>申し込み</th>
						</tr>
<?php foreach($data['LessonDate'] as $k => $v): ?>
<?php
	$kigous = $common->limitdisplay($v['limit'], $v['capacity']);
	$weeks = $jaweek->weekname($v['date']);
	$today = date('Y-m-d');
	
	$diffday = date('Y-m-d', strtotime($today.' +1 day'));
?>
						<tr>
							<td><?php echo nl2br(h(date('Y年n月j日', strtotime($v['date'])))).'('.$weeks.')'; ?></td>
							<td><?php echo nl2br(h($v['datetxt'])); ?></td>
							<td><?php echo $common->limitdisplay($v['limit'], $v['capacity']); ?></td>
							<td>
								<?php echo $form->create('', array('action' => 'reserve')), PHP_EOL; ?>
								<?php echo $form->hidden("Reserve.id", array('value' => nl2br(h($data['Lesson']['id'])))), PHP_EOL; ?>
								<?php echo $form->hidden("Reserve.date_id", array('value' => nl2br(h($v['id'])))), PHP_EOL; ?>
								<?php echo $form->hidden("Reserve.title", array('value' => nl2br(h($data['Lesson']['title'])))), PHP_EOL; ?>
								<?php echo $form->hidden("Reserve.date", array('value' => nl2br(h(date('Y年n月j日', strtotime($v['date']))).'('.$weeks.')'))); ?>
								<?php echo $form->hidden("Reserve.datetxt", array('value' => nl2br(h($v['datetxt'])))), PHP_EOL; ?>
								<?php if($kigous == '×'): ?>
								満席
								<?php else: ?>
									<?php if(strtotime($v['date']) <= strtotime($today)): ?>
										申し込み終了
									<?php else: ?>
										<?php echo $form->submit('申し込み', array('name' => 'data[Reserve][mode]', 'div' => false)), PHP_EOL; ?>
									<?php endif; ?>
								<?php endif; ?>
								<?php echo $form->end(), PHP_EOL; ?>
							</td>
						</tr>
<?php endforeach; ?>
					</table>
				</div>
			</div>
			<?php echo $this->element('frontlesson/scheduleBox'); ?>
		</div>
		