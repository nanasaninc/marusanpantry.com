<p class="topicPath"><a href="<?php echo $html->url('/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/common/icon-home.png" alt="ホーム" width="15" height="13" /></a><span>&gt;</span><span>&gt;</span>ようこそ</p>
		<h1><img src="<?php echo $html->url('/', true); ?>img/welcome/h1.png" alt="ようこそ" width="81" height="19" /></h1>
		
		<div id="primary">
		<div id="welcomeBox">
			<h2><img src="<?php echo $html->url('/', true); ?>img/welcome/welcome01.png" alt="お菓子で笑顔をつくっています" width="461" height="128" /></h2>
			<p><img src="<?php echo $html->url('/', true); ?>img/welcome/welcome02.png" alt="コメント" width="534" height="407" /></p>
		</div>
			<div class="box" id="kitchenBox">
				<h2><img src="<?php echo $html->url('/', true); ?>img/welcome/h2-kitchen.png" alt="パン・お菓子教室" width="680" height="60" /></h2>
				<div class="innerBox">マルサンパントリー2Ｆにて、20年以上に渡りパン・お菓子教室を開催しております。<br />
					「たくさんの方にパンやお菓子作りの楽しさと、手作りの美味しさを知っていただけたら･･･」<br />
				そんな想いを胸に、毎回趣向を凝らしたメニューをご提案しております。</div>
				<p class="btn-detail"><a href="<?php echo $html->url('/', true); ?>lessons/"><img src="<?php echo $html->url('/', true); ?>img/common/btn-detail.png" alt="スイートキッチンについて詳細はこちら" width="106" height="25" /></a></p>
			</div>
			<div class="box" id="pantryBox">
				<h2><img src="<?php echo $html->url('/', true); ?>img/welcome/h2-pantry.png" alt="マルサンパントリー" width="680" height="60" /></h2>
				<div class="innerBox">ご家庭でのお菓子やパン作りの食材が揃ったお店です。<br />
					プロのケーキ屋さんやパン屋さんが使用する業務用食材を使いやすいサイズで販売しています。<br />
				初心者の方もプロの方も、ぜひ一度お越しくださいませ。</div>
				<p class="btn-detail"><a href="<?php echo $html->url('/', true); ?>pantry/"><img src="<?php echo $html->url('/', true); ?>img/common/btn-detail.png" alt="マルサンパントリーについて詳細はこちら" width="106" height="25" /></a></p>
			</div>
			<div class="box" id="kitchenshopBox">
				<h2><img src="<?php echo $html->url('/', true); ?>img/welcome/h2-kitchenshop.png" alt="キッチンショップ" width="680" height="60" /></h2>
				<div class="innerBox">ご家庭でのお菓子やパン作りの型や道具が揃ったお店です。<br />
					また、プロ向けの業務用機械器具も豊富に揃っており、お取り寄せやオーダーなども可能です。<br />
				プロの方の出店図面計画や販促計画などのお手伝もさせていただきますので、お気軽にご相談下さい。</div>
				<p class="btn-detail"><a href="<?php echo $html->url('/', true); ?>kitchenshop/"><img src="<?php echo $html->url('/', true); ?>img/common/btn-detail.png" alt="キッチンショップについて詳細はこちら" width="106" height="25" /></a></p>
			</div>
			<div class="box" id="webshopBox">
				<h2><img src="<?php echo $html->url('/', true); ?>img/welcome/h2-webshop.png" alt="マルサンパントリーウェブショップ" width="680" height="60" /></h2>
				<div class="innerBox">マルサンパントリー、キッチンショップ両店舗のアイテムを販売しております。<br />
					いつでもお好きなときにお買物をお楽しみください。<br />
				ネットショップ限定のキャンペーンも見逃せません！</div>
				<p class="btn-detail"><a href="http://www.m-pantry.com/"><img src="<?php echo $html->url('/', true); ?>img/common/btn-detail.png" alt="マルサンパントリーウェブショップはこちら" width="106" height="25" /></a></p>
			</div>
			<div class="box" id="marusanBox">
				<h2><img src="<?php echo $html->url('/', true); ?>img/welcome/h2-marusan.png" alt="株式会社松山丸三" width="680" height="60" /></h2>
				<div class="innerBox">プロのケーキ屋さんやパン屋さん、レストランなどに業務用食材や機械器具を販売しております。<br />
				また、店作り・店舗経営、販売促進、商品開発までトータルサポートを行い、お客様の発展に貢献しています。</div>
				<p class="btn-detail"><a href="http://m-marusan.co.jp/" target="_blank"><img src="<?php echo $html->url('/', true); ?>img/common/btn-detail.png" alt="株式会社松山丸三について詳細はこちら" width="106" height="25" /></a></p>
			</div>
		</div>