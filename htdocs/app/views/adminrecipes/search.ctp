
<script type="text/javascript">
$(function(){
	$('select.active').change(function(e){
		var thiss = $(this);
		var ids = $(this).prev().val();
		var values = $(this).val();
		e.preventDefault();
		ajax_search(ids, values, thiss);
	});
});

function ajax_search(ids, values, thiss){
		var bool;
		$.post("<?php echo $html->url('/adminrecipes/active/'); ?>", {id:ids, actives:values}, function(data){
			if (data.length > 0){
				thiss.after(data);
			}
		}
	)
}

</script>
		<h1>レシピ管理</h1>
		<div id="primary">
			<div class="wbox">
				<h2>レシピ新規登録</h2>
				<p>レシピを新規作成する場合は「レシピ新規作成」よりレシピの情報を入力してください。</p>
			<p class="btn">
				<a href="<?php echo $html->url("/adminrecipes/add/"); ?>">レシピ新規登録</a>
			</p>
			<h2>レシピ一覧</h2>
			<p>開催予定の教室一覧です。内容を編集する場合は「修正」ボタンを、削除する場合は「削除」ボタンをクリックして下さい。</p>
			<?php echo $this->Session->flash(); ?>
			
			<?php
				$bool = $paginator->numbers();
			?>
			
			<?php if(!empty($data)): ?>
			<?php if($bool != false): ?>
				<div class="pagination">
					<ul>
						<li class="prev"><?php echo $paginator->prev('前へ').PHP_EOL; ?></li>
						<?php echo $paginator->numbers(array('tag' => 'li', 'separator' => false)).PHP_EOL; ?>
						<li class="next"><?php echo $paginator->next('次へ').PHP_EOL; ?></li>
					</ul>
				</div>
			<?php endif; ?>
			<?php endif; ?>
			
			<table border="0" class="zebraList">
				<tr>
					<th>No</th>
					<th>写真</th>
					<th>カテゴリ</th>
					<th>レシピ名</th>
					<th>表示</th>
					<th>編集</th>
				</tr>
<?php $i=0; ?>
<?php foreach($data as $k => $v): ?>
				<tr>
					<td><?php echo $v['Recipe']['id']; ?></td>
					<td>
						<?php if(!empty($v['RecipeImage'][$i]['uploadpath'])): ?>
						<?php $path = str_replace('.jpg', '_th.jpg', $v['RecipeImage'][$i]['uploadpath']); ?>
						<img src="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="" width="50" />
						<?php endif; ?>
					</td>
					<td>
						<?php if(preg_match('/^1.+/', $v['Recipe']['category'])): ?>
							<a href="<?php echo $html->url("/adminrecipes/index/10000"); ?>">パン</a>
						<?php elseif(preg_match('/^2.+/', $v['Recipe']['category'])): ?>
							<a href="<?php echo $html->url("/adminrecipes/index/20000"); ?>">お菓子</a>
						<?php endif; ?>
						&gt;<a href="<?php echo $html->url("/adminrecipes/index/{$v['Recipe']['category']}"); ?>"><?php echo $subcategory[$v['Recipe']['category']]; ?></a></td>
					<td><a href="<?php echo $html->url("/adminrecipes/edit/{$v['Recipe']['id']}"); ?>"><?php echo h($v['Recipe']['title']); ?></a></td>
					<td><label>
							<?php echo $form->hidden("Recipe.id", array('id' => false, 'class' => 'activeid', 'value' => $v['Recipe']['id'])); ?>
							<?php echo $form->select("Recipe.active", $active, $v['Recipe']['active'], array('empty' => null, 'class' => 'active', 'id' => false), true); ?>
					</label></td>
					<td><ul class="panel-edit">
						<li class="link-edit"><a href="#">編集</a>
							<ul>
								<li><a href="<?php echo $html->url("/adminrecipes/edit/{$v['Recipe']['id']}"); ?>">編集</a></li>
								<li><a href="#">削除</a></li>
								</ul>
							</li>
					</ul></td>
				</tr>
<?php endforeach; ?>
			</table>
			</div>
			<p class="btn-back">
				<a href="<?php echo $html->url("/adminrecipes/"); ?>">戻る</a>
			</p>
		</div>