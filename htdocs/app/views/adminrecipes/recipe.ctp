<?php echo $this->element('admin/gnavi'); ?>
<ul>
<?php foreach($data as $k => $v): ?>
	<li><a href="<?php echo $html->url("recipedetail/{$v['Recipe']['id']}"); ?>"><?php echo h($v['Recipe']['title']); ?></a></li>
<?php endforeach; ?>
</ul>