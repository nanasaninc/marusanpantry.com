<?php echo $this->element('admin/gnavi'); ?>
<?php foreach($data as $k => $v): ?>
<div style="width:150px;float:left;">
	<p><a href="<?php echo $html->url("detail/{$v['Recipe']['id']}"); ?>"><?php echo h($v['Recipe']['title']); ?></a></p>
	<p><?php echo h($v['Recipe']['description']); ?></p>
	<p><?php echo nl2br($v['Recipe']['material']); ?></p>
</div>
<?php endforeach; ?>