<?php
	$slide = file_get_contents('http://www.marusanpantry.com/inc/slide.html');
?>
<div id="slideContents">
	<div id="slide" class="slidepart fl">
<?php echo $slide; ?>
	</div>
	<p class="arr-back prev"><a href="javascript:prevnext(0);"><img class="btn" src="<?php echo $html->url('/', true); ?>img/home/arr-back.png" alt="前へ" width="54" height="54" /></a></p>
	<p class="arr-next next"><a href="javascript:prevnext(1);"><img class="btn" src="<?php echo $html->url('/', true); ?>img/home/arr-next.png" alt="次へ" width="54" height="54" /></a></p>
</div>
		<div id="primary">
			<div id="lessonBox" class="box clearfix">
				<div id="scheduleBox">
				<h2><img src="<?php echo $html->url('/', true); ?>img/home/h2-lesson.png" alt="パン・お菓子教室スケジュール" width="455" height="73" /></h2>
				<div id="slideBox">
				<div id="slideBody">
<?php //echo date('Y年n月', strtotime($nextmonth)); ?>

<?php if(!empty($tdata)): ?>
				<div class="monthbox box clearfix heightAlign">
<?php $i=0; ?>
<?php foreach($tdata as $k => $v): ?>
					<dl class="<?php echo $icons[$v['Lesson']['category']]; ?>">
						<dt>
<?php if(!empty($v['LessonImage'][$i]['uploadpath'])): ?>
<?php $path = str_replace('.jpg', '_th.jpg', $v['LessonImage'][$i]['uploadpath']); ?>
							<a href="<?php echo $html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><img src="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="*" width="60" /></a>
<?php else: ?>
							<a href="<?php echo $html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><img src="<?php echo $html->url('/'); ?>img/common/nowprinting.jpg" alt="*" width="60" /></a>
<?php endif; ?>
						</dt>
						<dd>
							<p><img src="<?php echo $html->url('/', true); ?>img/common/icon-<?php echo $icons[$v['Lesson']['category']]; ?>.png" alt="" width="53" height="16" /></p>
							<p><a href="<?php echo $html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><?php echo mb_strimwidth(nl2br(h($v['Lesson']['title'])), 0, 40, "..."); ?></a></p>
						</dd>
					</dl>
<?php endforeach; ?>
				</div>
<?php endif; ?>

<?php if(!empty($ndata)): ?>
				<div class="monthbox box clearfix heightAlign">
<?php $i=0; ?>
<?php foreach($ndata as $k => $v): ?>
					<dl class="<?php echo $icons[$v['Lesson']['category']]; ?>">
						<dt>
<?php if(!empty($v['LessonImage'][$i]['uploadpath'])): ?>
<?php $path = str_replace('.jpg', '_th.jpg', $v['LessonImage'][$i]['uploadpath']); ?>
							<a href="<?php echo $html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><img src="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="*" width="60" /></a>
<?php else: ?>
							<a href="<?php echo $html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><img src="<?php echo $html->url('/'); ?>img/common/nowprinting.jpg" alt="*" width="60" /></a>
<?php endif; ?>
						</dt>
						<dd>
							<p><img src="<?php echo $html->url('/', true); ?>img/common/icon-<?php echo $icons[$v['Lesson']['category']]; ?>.png" alt="" width="53" height="16" /></p>
							<p><a href="<?php echo $html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><?php echo mb_strimwidth(nl2br(h($v['Lesson']['title'])), 0, 40, "..."); ?></a></p>
						</dd>
					</dl>
<?php endforeach; ?>
				</div>
<?php endif; ?>

<?php if(!empty($nndata)): ?>
				<div class="monthbox box clearfix heightAlign">
<?php $i=0; ?>
<?php foreach($nndata as $k => $v): ?>
					<dl class="<?php echo $icons[$v['Lesson']['category']]; ?>">
						<dt>
<?php if(!empty($v['LessonImage'][$i]['uploadpath'])): ?>
<?php $path = str_replace('.jpg', '_th.jpg', $v['LessonImage'][$i]['uploadpath']); ?>
							<a href="<?php echo $html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><img src="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="*" width="60" /></a>
<?php else: ?>
							<a href="<?php echo $html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><img src="<?php echo $html->url('/'); ?>img/common/nowprinting.jpg" alt="*" width="60" /></a>
<?php endif; ?>
						</dt>
						<dd>
							<p><img src="<?php echo $html->url('/', true); ?>img/common/icon-<?php echo $icons[$v['Lesson']['category']]; ?>.png" alt="" width="53" height="16" /></p>
							<p><a href="<?php echo $html->url("/lessons/lesson/{$v['Lesson']['id']}"); ?>"><?php echo mb_strimwidth(nl2br(h($v['Lesson']['title'])), 0, 40, "..."); ?></a></p>
						</dd>
					</dl>
<?php endforeach; ?>
				</div>
<?php endif; ?>
			</div>
			</div>

			<div id="scheNav" class="localNav clearfix">
			<?php if(!empty($tdata)): ?>
					<p><?php echo date('Y年n月'); ?></p>
			<?php endif; ?>
			<?php if(!empty($ndata)): ?>
					<p><?php echo date('Y年n月', strtotime(date('Y-m-1').' +1 month')); ?></p>
			<?php endif; ?>
			<?php if(!empty($nndata)): ?>
					<p><?php echo date('Y年n月', strtotime(date('Y-m-1').' +2 month')); ?></p>
			<?php endif; ?>
				</div>
			</div>
				<div id="ossmBox">
					<h2><img src="<?php echo $html->url('/', true); ?>img/home/h2-ossm.png" alt="今月のおすすめ教室" width="225" height="73" /></h2>
					<div class="box">
					<?php if(!empty($osusumelesson)): ?>
						<?php if(!empty($osusumelesson['LessonImage'][0]['uploadpath'])): ?>
						<?php $path = str_replace('.jpg', '_th.jpg', $osusumelesson['LessonImage'][0]['uploadpath']); ?>
						<p><a href="<?php echo $html->url("/lessons/lesson/{$osusumelesson['Lesson']['id']}"); ?>"><img src="<?php echo $html->url('/').$path; ?>?<?php echo mt_rand(); ?>" alt="<?php echo $osusumelesson['Lesson']['title']; ?>" width="197" /></a></p>
						<p class="cat"><img src="<?php echo $html->url('/', true); ?>img/common/icon-<?php echo $icons[$osusumelesson['Lesson']['category']]; ?>.png" alt="" width="53" height="16" /></p>
						<?php endif; ?>
						<h3><a href="<?php echo $html->url("/lessons/lesson/{$osusumelesson['Lesson']['id']}"); ?>"><?php echo $osusumelesson['Lesson']['title']; ?></a></h3>
						<p><?php echo $osusumelesson['Lesson']['description']; ?></p>
					<?php else: ?>
						<p>今月のおすすめレッスンはありません。</p>
					<?php endif; ?>
					</div>
				</div>
			</div>
			<div id="contentsBox" class="clearfix heightAlign">
				<div class="box" id="marusanBox">
					<p class="img"><img src="<?php echo $html->url('/', true); ?>img/home/head-marusanBox.jpg" alt="画像：マルサンパントリー" width="220" height="113" /></p>
					<div>
						<h2><a href="<?php echo $html->url('/pantry/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/home/h2-marusanBox.png" width="185" height="28" alt="リンク：マルサンパントリー" /></a></h2>
						<p>ご家庭でのお菓子やパン作りの食材が揃ったお店です。<br />
							プロのケーキ屋さんやパン屋さんが使用する業務用食材を<br />
							使いやすいサイズで販売しています。</p>
					</div>
				</div>
				<div class="box" id="kitchenBox">
					<p class="img"><img src="<?php echo $html->url('/', true); ?>img/home/head-kitchenBox.jpg" alt="画像：キッチンショップ" width="220" height="113" /></p>
					<div>
						<h2><a href="<?php echo $html->url('/kitchenshop/', true); ?>"><img src="<?php echo $html->url('/', true); ?>img/home/h2-kitchenBox.png" width="185" height="28" alt="リンク：キッチンショップ" /></a></h2>
						<p>ご家庭でのお菓子やパン作りの型や道具が揃ったお店です。<br />
						また、プロ向けの業務用機械器具も豊富に揃っており、お取り寄せやオーダーなども可能です。</p>
					</div>
				</div>
				<div class="box" id="ecBox">
					<p class="img"><img src="<?php echo $html->url('/', true); ?>img/home/head-ecBox.jpg" alt="画像:ネットショップ" width="220" height="113" /></p>
					<div>
						<h2><a href="http://www.m-pantry.com/" target="_blank"><img src="<?php echo $html->url('/', true); ?>img/home/h2-ecBox.png" alt="リンク：ネットショップ" width="185" height="28" /></a></h2>
						<p>マルサンパントリー、キッチンショップ両店舗のアイテムを販売しております。<br />
						いつでもお好きなときにお買物をお楽しみください。</p>
					</div>
				</div>
			</div>
		</div>
		<div id="secondary">
			<div id="newsBox" class="box">
				<h2><img src="<?php echo $html->url('/', true); ?>img/home/h2-news.png" alt="新着情報：マルサンパントリーからのお知らせ" width="269" height="74" /></h2>
				<ul>
<?php $i = 0; ?>
<?php foreach($post as $k => $v): ?>
<?php if($i < 5): ?>
					<li>
						<a href="<?php echo $v['link']; ?>"><?php echo $v['title']; ?></a>
						 [<?php echo date('Y年n月d日', strtotime($v['date'])); ?>]
					</li>
<?php endif; ?>
<?php $i++; ?>
<?php endforeach; ?>
				</ul>
			</div>
			<?php echo $this->element('frontbnrbox/bnr'); ?>